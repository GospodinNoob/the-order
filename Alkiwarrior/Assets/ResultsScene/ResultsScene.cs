﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ResultsScene : MonoBehaviour {

    MovebleObject[] units;
    int unCou;
    float time;

    public Texture2D levelUp;

    int language;
    public AudioSource au;

    // Use this for initialization
    void Start () {
        au.volume *= PlayerPrefs.GetFloat("Volume");
        PlayerPrefs.SetInt("ContinueBattle", 0);
        language = PlayerPrefs.GetInt("Language");
        loadWindow = false;
        lHelp = new LoadHelpObject(language);

        int fontSize = 18;

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;

        time = PlayerPrefs.GetFloat("Time");
        unCou = PlayerPrefs.GetInt("PlayerTeamCounter");
        units = new MovebleObject[6];
        for(int i = 0; i < unCou; i++)
        {
            units[i] = new MovebleObject("PlayerUnit" + i);
        }
        lose = PlayerPrefs.GetInt("Lose");
        gold = PlayerPrefs.GetInt("ExtraGold");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    int lose;
    int gold;
    public GUISkin skin;
    public Texture2D backGroundTexture;

    string[] fatigueText = { "Fatigue", "Усталость" };
    string[] skillPoints = { "Skill points", "Очки навыков" };
    string[] deadText = { "Dead", "Мёртв" };
    string[] goldText = { "Gold", "Золото" };
    string[] aliveText = { "Alive", "Здоров" };

    public Texture2D fatigue, dead, fon;
    public Texture2D[] face;

    void ResultWindow(int WindowID)
    {
        string st;
        for (int i = 0; i < unCou; i++)
        {
            GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 9 * i + Screen.height / 30, Screen.height / 9, Screen.height / 9), fon, ScaleMode.ScaleAndCrop);
            st = aliveText[language] + " ";
            if (units[i].skillPoints >= 0)
            {
                st += '\n' + skillPoints[language] + " " + units[i].skillPoints;
            }
            if (PlayerPrefs.GetInt("PlayerUnitOnMission" + i.ToString()) == 0)
            {
                st = deadText[language];
                //GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 9 * i + Screen.height / 30, Screen.height / 9, Screen.height / 9), dead, ScaleMode.ScaleAndCrop);
            }
            st = units[i].name + " " + st;
            GUI.Box(new Rect(0, Screen.height / 9 * i + Screen.height / 30, Screen.width, Screen.height / 9), st);
            GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 9 * i + Screen.height / 30, Screen.height / 9, Screen.height / 9), face[units[i].type], ScaleMode.ScaleAndCrop);
            if (units[i].fatigueFlag)
            {
                st = fatigueText[language] + " ";
                st += units[i].valueFatigue - units[i].startFatigue;
                GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 9 * i + Screen.height / 30, Screen.height / 9, Screen.height / 9), fatigue, ScaleMode.ScaleAndCrop);
            }
            if (units[i].skillPoints > 0)
            {
                GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 9 * i + Screen.height / 30, Screen.height / 45, Screen.height / 45), levelUp, ScaleMode.ScaleAndCrop);
            }
            if (PlayerPrefs.GetInt("PlayerUnitOnMission" + i.ToString()) == 0)
            {
                GUI.DrawTexture(new Rect(Screen.width / 6, Screen.height / 9 * i + Screen.height / 30, Screen.height / 9, Screen.height / 9), dead, ScaleMode.ScaleAndCrop);
            }
        }
        if (lose == 0)
        {
            GUI.Box(new Rect(0, Screen.height / 9 * unCou, Screen.width, Screen.height / 9), goldText[language] + " +" + gold.ToString());
        }
    }

    string[] resultsText = { "Results", "Результаты" };

    bool loadWindow;
    LoadHelpObject lHelp;
    string[] loadText = { "Loading", "Загрузка" };

    void LoadWindow(int WindowID)
    {
        int fontSize = 25;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    void OnGUI()
    {
        GUI.skin = skin;
        if (loadWindow)
        {
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
        else
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backGroundTexture, ScaleMode.ScaleAndCrop);
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height / 8 * 6), ResultWindow, resultsText[language]);
            if (GUI.Button(new Rect(0, Screen.height / 8 * 7 + Screen.height / 30, Screen.width, Screen.height / 8 - Screen.height / 30), "OK"))
            {
                if (lose == 0)
                {
                    PlayerPrefs.SetInt("Gold", gold + PlayerPrefs.GetInt("Gold"));
                }
                loadWindow = true;
                SceneManager.LoadSceneAsync("EventScene");
            }
        }
    }
}

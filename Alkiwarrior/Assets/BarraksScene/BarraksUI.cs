﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;

public class BarraksUI : MonoBehaviour {

    int curCheckUnit;
    int unitsInBarraks;

    List<Sprite> weaponList = new List<Sprite>();
    List<Sprite> armourList = new List<Sprite>();
    List<Sprite> itemList = new List<Sprite>();

    GameObject AbilitiesWindow;
    GameObject EquipmentWindow;
    Text StatsContent;
    Text BattleStatsContent;
    TextLocalaze ResoursesText;
    Image ClassIcon;
    Image FaceIcon;
    Text Name;

    Image RightHand;
    Image LeftHand;
    Image Armour;
    Image Item;

    string baseGround;

    ArrayList units;
    bool[] onMission;
    int eventId;

    GameEvent ev;

    TextLocalaze GainNewRecrut;
    TextLocalaze UpgradeRecrut;

    List<Sprite> classIcons = new List<Sprite>();
    Sprite defaultClassIcon;


    void Sort()
    {
        for (int i = 0; i < unitsInBarraks; i++)
        {
            for (int j = 0; j < unitsInBarraks - 1; j++)
            {
                if ((((MovebleObject)units[j]).id == 0) || (((MovebleObject)units[j]).level < ((MovebleObject)units[j + 1]).level) || ((((MovebleObject)units[j]).level == ((MovebleObject)units[j + 1]).level) && (((MovebleObject)units[j]).exp < ((MovebleObject)units[j + 1]).exp)))
                {
                    MovebleObject buf = ((MovebleObject)units[j]);
                    units[j] = units[j + 1];
                    units[j] = buf;
                }
            }
        }
    }

    public AudioSource au;

    MovebleObject obj;

    void AbilitiesClick()
    {
        AbilitiesWindow.gameObject.SetActive(true);
        EquipmentWindow.gameObject.SetActive(false);
    }

    void EquipmentClick()
    {
        EquipmentWindow.gameObject.SetActive(true);
        AbilitiesWindow.gameObject.SetActive(false);
    }

    bool showRightWeapon = true;

    void RightHandClick()
    {
        if (WeaponWindow.active && showRightWeapon)
        {
            OpenWeaponList();
        }
        showRightWeapon = true;
        ArmourWindow.SetActive(false);
        WeaponWindow.SetActive(true);
        ItemWindow.SetActive(false);
        //OpenWeaponList();
        Redraw();
    }

    void LeftHandClick()
    {
        if(WeaponWindow.active && !showRightWeapon)
        {
            OpenWeaponList();
        }
        showRightWeapon = false;
        ArmourWindow.SetActive(false);
        WeaponWindow.SetActive(true);
        ItemWindow.SetActive(false);
        Redraw();
    }

    void ArmourClick()
    {
        if (ArmourWindow.active)
        {
            OpenArmourList();
        }
        ArmourWindow.SetActive(true);
        WeaponWindow.SetActive(false);
        ItemWindow.SetActive(false);
        Redraw();
    }

    void ItemClick()
    {
        if (ItemWindow.active)
        {
            OpenItemList();
        }
        ArmourWindow.SetActive(false);
        WeaponWindow.SetActive(false);
        ItemWindow.SetActive(true);
        Redraw();
    }

    void WeaponUpgrade()
    {
        MovebleObject unit = ((MovebleObject)units[curCheckUnit]);
        if (showRightWeapon)
        {
           // PlayerPrefs.SetInt("WeaponUpgrade" + unit.damage.id.ToString(), 1);
            SpendMoney(150);
            unit.SetWeapon(staticData.GetWeaponByKeyName(unit.rightArm.keyName));
        }
        else
        {
            //PlayerPrefs.SetInt("WeaponUpgrade" + unit.leftArm.id.ToString(), 1);
            SpendMoney(150);
            unit.SetLeftArm(staticData.GetWeaponByKeyName(unit.leftArm.keyName));
        }
        Redraw();
    }

    void ItemUpgrade()
    {
        MovebleObject unit = ((MovebleObject)units[curCheckUnit]);
       // PlayerPrefs.SetInt("ItemUpgrade" + unit.it.id.ToString(), 1);
        SpendMoney(150);
        //unit.SetItem(unit.it.id);
        Redraw();
    }

    void ArmourUpgrade()
    {
        MovebleObject unit = ((MovebleObject)units[curCheckUnit]);
        //PlayerPrefs.SetInt("ArmourUpgrade" + unit.armour.id.ToString(), 1);
        SpendMoney(150);
        //unit.SetArmour(unit.armour.id);
        Redraw();
    }

    GameObject WeaponWindow;
    GameObject ArmourWindow;
    GameObject ItemWindow;

    Text ItemDescrContent;
    Text WeaponDescrContent;
    Text ArmourDescrContent;

    Text ItemDescrLabel;
    Text WeaponDescrLabel;
    Text ArmourDescrLabel;

    GameObject UpgradeWeapon;
    GameObject UpgradeArmour;
    GameObject UpgradeItem;

    GameObject UnitsWindow;
    GameObject WeaponsWindow;
    GameObject ArmoursWindow;
    GameObject ItemsWindow;

    void ChooseUnitClick(int id)
    {
        curCheckUnit = id;
        UnitsWindow.gameObject.SetActive(false);
        Redraw();
    }

    void ChooseWeaponClick(int id)
    {
        if (showRightWeapon)
        {
            ((MovebleObject)units[curCheckUnit]).SetWeapon(staticData.weapons.list[id]);
        }
        else
        {
            ((MovebleObject)units[curCheckUnit]).SetLeftArm(staticData.weapons.list[id]);
        }
        WeaponsWindow.gameObject.SetActive(false);
        Redraw();
    }

    void ChooseArmourClick(int id)
    {
        ((MovebleObject)units[curCheckUnit]).SetArmour(staticData.armours.list[id]);
        ArmoursWindow.gameObject.SetActive(false);
        Redraw();
    }

    void ChooseItemClick(int id)
    {
        ((MovebleObject)units[curCheckUnit]).SetItem(staticData.items.list[id]);
        ItemsWindow.gameObject.SetActive(false);
        Redraw();
    }

    void ExitUnitsList()
    {
        UnitsWindow.SetActive(false);
    }

    List<GameObject> unitButtonsList = new List<GameObject>();
    List<GameObject> weaponButtonsList = new List<GameObject>();
    List<GameObject> armourButtonsList = new List<GameObject>();
    List<GameObject> itemButtonsList = new List<GameObject>();

    public GameObject unitButton;
    public GameObject weaponButton;
    public GameObject armourButton;
    public GameObject itemButton;
    GameObject unitsContent;
    GameObject weaponsContent;
    GameObject armourContent;
    GameObject itemsContent;



    void OpenUnitsList()
    {
        UnitsWindow.SetActive(true);
        foreach (var it in unitButtonsList)
        {
            Destroy(it);
        }
        unitButtonsList.Clear();
        for (int it = 0; it < unitsInBarraks; it++)
        {
            GameObject go = GameObject.Instantiate(unitButton, this.transform.position, this.transform.rotation);
            go.transform.parent = unitsContent.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.GetComponent<UnitButton>().Init((MovebleObject)units[it]);
            int newI = it;
            go.GetComponent<Button>().onClick.AddListener(delegate { ChooseUnitClick(newI); });
            unitButtonsList.Add(go);
        }
    }

    void OpenItemList()
    {
        ItemsWindow.SetActive(true);
        foreach (var it in itemButtonsList)
        {
            Destroy(it);
        }
        itemButtonsList.Clear();
        for (int it = 0; it <= 13; it++)
        {
            //Item a = new Item(it, language);
            //a.Generate(it);
            GameObject go = GameObject.Instantiate(itemButton, this.transform.position, this.transform.rotation);
            go.transform.parent = itemsContent.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            //go.GetComponent<ItemButton>().Init(a);
            int newI = it;
            go.GetComponent<Button>().onClick.AddListener(delegate { ChooseItemClick(newI); });
            itemButtonsList.Add(go);
        }
    }

    void OpenArmourList()
    {
        ArmoursWindow.SetActive(true);
        foreach (var it in armourButtonsList)
        {
            Destroy(it);
        }
        armourButtonsList.Clear();
        for (int it = 0; it <= 4; it++)
        {
            //Armour a = new Armour(it ,language);
            //a.Generate(it);
            GameObject go = GameObject.Instantiate(armourButton, this.transform.position, this.transform.rotation);
            go.transform.parent = armourContent.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            //go.GetComponent<ArmourButton>().Init(a);
            int newI = it;
            go.GetComponent<Button>().onClick.AddListener(delegate { ChooseArmourClick(newI); });
            armourButtonsList.Add(go);
        }
    }

    void OpenWeaponList()
    {
        WeaponsWindow.SetActive(true);
        foreach (var it in weaponButtonsList)
        {
            Destroy(it);
        }
        weaponButtonsList.Clear();
        for (int it = 0; it < staticData.weapons.list.Count; it++)
        {
            Weapon d = staticData.weapons.list[it];
            if (showRightWeapon && (d.handStatus >= -1))
            {
                GameObject go = GameObject.Instantiate(weaponButton, this.transform.position, this.transform.rotation);
                go.transform.parent = weaponsContent.transform;
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
                go.GetComponent<WeaponButton>().Init(d);
               // int newI = d.id;
               // go.GetComponent<Button>().onClick.AddListener(delegate { ChooseWeaponClick(newI); });
                weaponButtonsList.Add(go);
            }
            if(!showRightWeapon && (d.handStatus <= -1))
            {
                GameObject go = GameObject.Instantiate(weaponButton, this.transform.position, this.transform.rotation);
                go.transform.parent = weaponsContent.transform;
                go.transform.localScale = new Vector3(1, 1, 1);
                go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
                go.GetComponent<WeaponButton>().Init(d);
                //int newI = d.id;
                //go.GetComponent<Button>().onClick.AddListener(delegate { ChooseWeaponClick(newI); });
                weaponButtonsList.Add(go);
            }
        }
    }

    void Redraw()
    {
        List<string> values = new List<string>();
        values.Add(gold.ToString());
        ResoursesText.SetValues(values);

        MovebleObject unit = ((MovebleObject)units[curCheckUnit]);

        HPText.text = unit.maxHits.ToString();
        APText.text = unit.maxAP.ToString();
        LevelText.text = unit.level.ToString();

        StatsContent.text = unit.GetEquipStats();
        BattleStatsContent.text = unit.GetBattleStatsDescr();

       // RightHand.sprite = weaponList[((MovebleObject)units[curCheckUnit]).damage.id];
       // LeftHand.sprite = weaponList[((MovebleObject)units[curCheckUnit]).leftArm.id];
        // Debug.Log(((MovebleObject)units[curCheckUnit]).armour.id - 1);
       // if (((MovebleObject)units[curCheckUnit]).armour.id - 1 >= 0)
        {
         //   Armour.sprite = armourList[((MovebleObject)units[curCheckUnit]).armour.id - 1];
        }
       // else
        {
        //    Armour.sprite = null;
        }
        //if (((MovebleObject)units[curCheckUnit]).it.id - 1 >= 0)
        {
          //  Item.sprite = itemList[((MovebleObject)units[curCheckUnit]).it.id - 1];
        }
        //else
        {
            Item.sprite = null;
        }
        //Item.sprite = itemList[((MovebleObject)units[curCheckUnit]).it.id - 1];

        Name.text = unit.name;
        values.Clear();
        values.Add(recrutUpgrade.ToString());
        UpgradeRecrut.SetValues(values);
        values.Clear();
        values.Add("25");
        GainNewRecrut.SetValues(values);

        ClassIcon.sprite = defaultClassIcon;

       // if (unit.forks[1])
        {
            ClassIcon.sprite = classIcons[0];
        }
       // if (unit.forks[2])
        {
            ClassIcon.sprite = classIcons[1];
        }
       // if (unit.forks[4])
        {
            ClassIcon.sprite = classIcons[2];
        }
       // if (unit.forks[5])
        {
            ClassIcon.sprite = classIcons[3];
        }

        //ItemDescrContent.text = unit.it.GetDescr();
       // ArmourDescrContent.text = unit.armour.GetDescr();
        //UpgradeArmour.SetActive(!unit.armour.upgraded);

      //  ItemDescrLabel.text = unit.it.name;    
        ArmourDescrLabel.text = unit.armour.keyName;
       // UpgradeItem.SetActive(!unit.it.upgraded);

        if (showRightWeapon)
        {
           // WeaponDescrContent.text = unit.damage.GetDescr();
            WeaponDescrLabel.text = unit.rightArm.keyName;
         //   UpgradeWeapon.SetActive(!unit.damage.upgraded);
        }
        else
        {
           // WeaponDescrContent.text = unit.leftArm.GetDescr();
            WeaponDescrLabel.text = unit.leftArm.keyName;
           // UpgradeWeapon.SetActive(!unit.leftArm.upgraded);
        }

        LevelUp.SetActive(unit.skillPoints > 0);
        LevelUpText.text = unit.skillPoints.ToString();

        ForkWindow.SetActive(unit.forkCounter <= 0);
        if (unit.GetForks().Count > 0)
        {
            StatFork.sprite = ForksSprites[unit.GetForks()[0]];
            MainFork.sprite = ForksSprites[unit.GetForks()[1]];
        }

        for(int i = 0; i < perksWindows.Count; i++)
        {
            perksWindows[i].gameObject.SetActive(false);
        }

        perksWindows[curFork].SetActive(true);

    }

    List<GameObject> perksWindows = new List<GameObject>();

    Text LevelUpText;
    GameObject LevelUp;

    void UpgradeRecruts()
    {
        SpendMoney(recrutUpgrade);
        PlayerPrefs.SetInt("ExtraLevel", 1 + PlayerPrefs.GetInt("ExtraLevel"));
        PlayerPrefs.SetInt("RecrutUpgradeCost", 100 + (int)(100 * 0.4f * PlayerPrefs.GetInt("ExtraLevel")));
        recrutUpgrade = PlayerPrefs.GetInt("RecrutUpgradeCost");
        Redraw();
    }

    void GainNewRecrutClick()
    {
        if (gold - 25 >= 0)
        {
            unitsInBarraks++;
            //units.Add(new MovebleObject(1, language));
            PlayerPrefs.SetInt("CurSpendMoney", 25 + PlayerPrefs.GetInt("CurSpendMoney"));
            PlayerPrefs.SetInt("Achivment10", 1);
            if (PlayerPrefs.GetInt("CurSpendMoney") >= 300)
            {
                PlayerPrefs.SetInt("Achivment11", 1);
            }
            if (PlayerPrefs.GetInt("CurSpendMoney") >= 1000)
            {
                PlayerPrefs.SetInt("Achivment12", 1);
            }

            PlayerPrefs.SetInt("CurBuyUnits", PlayerPrefs.GetInt("CurBuyUnits") + 1);
            PlayerPrefs.SetInt("Achivment4", 1);
            if (PlayerPrefs.GetInt("CurBuyUnits") >= 5)
            {
                PlayerPrefs.SetInt("Achivment5", 1);
            }
            if (PlayerPrefs.GetInt("CurBuyUnits") >= 25)
            {
                PlayerPrefs.SetInt("Achivment6", 1);
            }
            SpendMoney(25);
            //gold -= 25;
        }
        Redraw();
    }

    void LeftClick()
    {
        curCheckUnit--;
        if(curCheckUnit < 0)
        {
            curCheckUnit = unitsInBarraks - 2;
        }
        Redraw();
    }

    void RightClick()
    {
        curCheckUnit++;
        curCheckUnit %= unitsInBarraks;
        Redraw();
    }

    Text forkDescription;
    Text forkLabel;
    GameObject ApplyFork;

    int curFork = 0;

    List<List<string>> forkDescr = new List<List<string>>();
    List<List<string>> forkLabels = new List<List<string>>();

    void ForkClick(int id)
    {
        curFork = id;
        forkDescription.text = forkDescr[curFork][language];
        forkLabel.text = forkLabels[curFork][language];
       // ApplyFork.SetActive(!((MovebleObject)units[curCheckUnit]).forks[curFork]);
    }

    void ApplyClick()
    {
        //((MovebleObject)units[curCheckUnit]).skillPoints++;
        if (((MovebleObject)units[curCheckUnit]).skillPoints > 0){
            ((MovebleObject)units[curCheckUnit]).GainFork(curFork);
            ApplyFork.SetActive(false);
        }
        Redraw();
    }

    Image StatFork;
    Image MainFork;

    void StatForkClick()
    {
        List<int> forks = ((MovebleObject)units[curCheckUnit]).GetForks();
        IniPerksOfFork(forks[0]);
        curFork = forks[0];
        Redraw();
    }

    void MainForkClick()
    {
        List<int> forks = ((MovebleObject)units[curCheckUnit]).GetForks();
        IniPerksOfFork(forks[1]);
        curFork = forks[1];
        Redraw();
    }

    int curPerk;

    void ApplyPerkClick()
    {
        //((MovebleObject)units[curCheckUnit]).skillPoints++;
        //Debug.Log(((MovebleObject)units[curCheckUnit]).TryPerk(curFork, curPerk));
        if (((MovebleObject)units[curCheckUnit]).TryPerk(curFork, curPerk))
        {
            ((MovebleObject)units[curCheckUnit]).SetPerk(curFork, curPerk);
            ApplyPerk.gameObject.SetActive(false);
            Redraw();
        }
    }

    void IniPerksOfFork(int id)
    {

    }


    void ExitWeaponUnitsList()
    {
        WeaponsWindow.SetActive(false);
    }

    void PerkClick(int fork, int perk)
    {
        curPerk = perk;
        ApplyPerk.gameObject.SetActive(!((MovebleObject)units[curCheckUnit]).HavePerk(fork, perk));
        PerkName.text = ((MovebleObject)units[curCheckUnit]).GetPerkName(curFork, curPerk);
        PerkDescriptionContent.text = ((MovebleObject)units[curCheckUnit]).GetPerkDescr(curFork, curPerk);
    }

    List<Sprite> ForksSprites = new List<Sprite>();

    List<List<Sprite>> PerksSprites = new List<List<Sprite>>();

    Text PerkName;
    Text PerkDescriptionContent;
    GameObject ApplyPerk;

    GameObject ForkWindow;

    Text HPText;
    Text APText;
    Text LevelText;

    void ExitArmoursList()
    {
        ArmoursWindow.SetActive(false);
    }

    void ExitItemsList()
    {
        ItemsWindow.SetActive(false);
    }

    StaticData staticData;

    void Start()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        GameObject.Find("Open").GetComponent<Button>().onClick.AddListener(OpenUnitsList);
        GameObject.Find("ExitUnitsList").GetComponent<Button>().onClick.AddListener(ExitUnitsList);
        GameObject.Find("ExitWeaponWindowList").GetComponent<Button>().onClick.AddListener(ExitWeaponUnitsList);
        GameObject.Find("ExitArmourWindowList").GetComponent<Button>().onClick.AddListener(ExitArmoursList);
        GameObject.Find("ExitItemsWindowList").GetComponent<Button>().onClick.AddListener(ExitItemsList);
        unitsContent = GameObject.Find("UnitsContent");
        weaponsContent = GameObject.Find("WeaponsContent");
        armourContent = GameObject.Find("ArmoursContent");
        itemsContent = GameObject.Find("ItemsContent");
        List<string> buf = new List<string>();
        buf.Add("Fork contains" + '\n' + "strength abilities" + '\n' + "Gives +1 strength per level");
        buf.Add("Ветка содержит" + '\n' + "силовые сопосбности" + '\n' + "Даёт +1 к Силе за уровень");
        forkDescr.Add(buf);
        buf = new List<string>();
        buf.Add("Fork contains" + '\n' + "abilities with shield" + '\n' + "based on strength" + '\n' + "Improve one-handed block");
        buf.Add("Ветка содержит" + '\n' + "способности со щитом" + '\n' + "основанные на Силе" + '\n' + "Улучшает блок" + '\n' + "одной рукой");
        forkDescr.Add(buf);
        buf = new List<string>();
        buf.Add("Fork contains" + '\n' + "abilities with" + '\n' + "two-handed weapons" + '\n' + "based on strength" + '\n' + "Improve two-handed block");
        buf.Add("Ветка содержит" + '\n' + "способности с двуручным" + '\n' + "оружием, основанные" + '\n' + "на Силе" + '\n' + "Улучшает блок" + '\n' + "двуручным оружием");
        forkDescr.Add(buf);
        buf = new List<string>();
        buf.Add("Fork contains" + '\n' + "agility abilities" + '\n' + "Gives +1 agility per level");
        buf.Add("Ветка содержит" + '\n' + "способности ловкости" + '\n' + "Даёт +1 к ловкости за уровень");
        forkDescr.Add(buf);
        buf = new List<string>();
        buf.Add("Fork contains" + '\n' + "abilities wiht" + '\n' + "two weapons" + '\n' + "based on agility" + '\n' + "Improve one-handed parry");
        buf.Add("Ветка содержит" + '\n' + "сппособноти с" + '\n' + "двумя оружиями," + '\n' + "основанные на Ловкости" + '\n' + "Улучшает парирование" + '\n' + "одной рукой");
        forkDescr.Add(buf);
        buf = new List<string>();
        buf.Add("Fork contains" + '\n' + "abilities wiht" + '\n' + "ranged weapons" + '\n' + "based on agility" + '\n' + "Improve accurcy");
        buf.Add("Ветка содержит" + '\n' + "сппособноти с" + '\n' + "стрелковым оружием," + '\n' + "основанные на Ловкости" + '\n' + '\n' + "Увеличивает точность");
        forkDescr.Add(buf);

        buf = new List<string>();
        buf.Add("Strength");
        buf.Add("Сила");
        forkLabels.Add(buf);
        buf = new List<string>();
        buf.Add("Shieldman");
        buf.Add("Щитоносец");
        forkLabels.Add(buf);
        buf = new List<string>();
        buf.Add("Infantry");
        buf.Add("Пехотинец");
        forkLabels.Add(buf);
        buf = new List<string>();
        buf.Add("Agility");
        buf.Add("Ловкость");
        forkLabels.Add(buf);
        buf = new List<string>();
        buf.Add("Dimaher");
        buf.Add("Димахер");
        forkLabels.Add(buf);
        buf = new List<string>();
        buf.Add("Shooter");
        buf.Add("Стрелок");
        forkLabels.Add(buf);

        AbilitiesWindow = GameObject.Find("AbilitiesWindow");
        EquipmentWindow = GameObject.Find("EquipmentWindow");
        StatsContent = GameObject.Find("StatsContent").GetComponent<Text>();
        BattleStatsContent = GameObject.Find("BattleStatsContent").GetComponent<Text>();
        ResoursesText = GameObject.Find("ResoursesText").GetComponent<TextLocalaze>();
        ClassIcon = GameObject.Find("ClassIcon").GetComponent<Image>();
        FaceIcon = GameObject.Find("FaceIcon").GetComponent<Image>();

        HPText = GameObject.Find("HPText").GetComponent<Text>();
        APText = GameObject.Find("APText").GetComponent<Text>();
        LevelText = GameObject.Find("LevelText").GetComponent<Text>();

        RightHand = GameObject.Find("RightHand").GetComponent<Image>();
        LeftHand = GameObject.Find("LeftHand").GetComponent<Image>();
        Armour = GameObject.Find("Armour").GetComponent<Image>();
        Item = GameObject.Find("Item").GetComponent<Image>();

        RightHand.gameObject.GetComponent<Button>().onClick.AddListener(RightHandClick);
        LeftHand.gameObject.GetComponent<Button>().onClick.AddListener(LeftHandClick);
        Armour.gameObject.GetComponent<Button>().onClick.AddListener(ArmourClick);
        Item.gameObject.GetComponent<Button>().onClick.AddListener(ItemClick);

        ItemDescrContent = GameObject.Find("ItemDescrContent").GetComponent<Text>();
        WeaponDescrContent = GameObject.Find("WeaponDescrContent").GetComponent<Text>();
        ArmourDescrContent = GameObject.Find("ArmourDescrContent").GetComponent<Text>();

        ItemDescrLabel = GameObject.Find("ItemDescrLabel").GetComponent<Text>();
        WeaponDescrLabel = GameObject.Find("WeaponDescrLabel").GetComponent<Text>();
        ArmourDescrLabel = GameObject.Find("ArmourDescrLabel").GetComponent<Text>();

        WeaponWindow = GameObject.Find("WeaponDescription");
        ArmourWindow = GameObject.Find("ArmourDescription");
        ItemWindow = GameObject.Find("ItemDescription");

        UpgradeWeapon = GameObject.Find("WeaponUpgrade");
        UpgradeWeapon.GetComponent<Button>().onClick.AddListener(WeaponUpgrade);
        UpgradeArmour = GameObject.Find("ArmourUpgrade");
        UpgradeArmour.GetComponent<Button>().onClick.AddListener(ArmourUpgrade);
        UpgradeItem = GameObject.Find("ItemUpgrade");
        UpgradeItem.GetComponent<Button>().onClick.AddListener(ItemUpgrade);

        Name = GameObject.Find("Name").GetComponent<Text>();

        UnitsWindow = GameObject.Find("UnitsWindow");
        UnitsWindow.SetActive(false);
        WeaponsWindow = GameObject.Find("WeaponsWindow");
        WeaponsWindow.SetActive(false);
        ArmoursWindow = GameObject.Find("ArmoursWindow");
        ArmoursWindow.SetActive(false);
        ItemsWindow = GameObject.Find("ItemsWindow");
        ItemsWindow.SetActive(false);

        GameObject.Find("Abilities").GetComponent<Button>().onClick.AddListener(AbilitiesClick);
        GameObject.Find("Equipment").GetComponent<Button>().onClick.AddListener(EquipmentClick);

        GainNewRecrut = GameObject.Find("GainNewUnit").GetComponent<TextLocalaze>();
        //Debug.Log(GainNewRecrut.gameObject.transform.parent.name);
        GainNewRecrut.gameObject.transform.parent.GetComponent<Button>().onClick.AddListener(GainNewRecrutClick);
        UpgradeRecrut = GameObject.Find("UpgradeUnits").GetComponent<TextLocalaze>();
        UpgradeRecrut.gameObject.transform.parent.GetComponent<Button>().onClick.AddListener(UpgradeRecruts);

        GameObject.Find("Left").GetComponent<Button>().onClick.AddListener(LeftClick);
        GameObject.Find("Right").GetComponent<Button>().onClick.AddListener(RightClick);

        forkDescription = GameObject.Find("ForkDescriptionContent").GetComponent<Text>();
        forkLabel = GameObject.Find("ForkName").GetComponent<Text>();
        ApplyFork = GameObject.Find("ApplyFork");
        ApplyFork.GetComponent<Button>().onClick.AddListener(ApplyClick);

        PerkName = GameObject.Find("PerkName").GetComponent<Text>();
        PerkDescriptionContent = GameObject.Find("PerkDescriptionContent").GetComponent<Text>();
        ApplyPerk = GameObject.Find("ApplyPerk");
        ApplyPerk.GetComponent<Button>().onClick.AddListener(ApplyPerkClick);

        GameObject.Find("Fork0It").GetComponent<Button>().onClick.AddListener(StatForkClick);
        GameObject.Find("Fork1It").GetComponent<Button>().onClick.AddListener(MainForkClick);

        StatFork = GameObject.Find("Fork0It").GetComponent<Image>();
        MainFork = GameObject.Find("Fork1It").GetComponent<Image>();

        LevelUp = GameObject.Find("LevelUp");
        LevelUpText = GameObject.Find("LevelUpText").GetComponent<Text>();

        ForkWindow = GameObject.Find("Forks");

        Texture2D tex = Resources.Load("Sprites/class_icon_default") as Texture2D;
        Rect rec = new Rect(0, 0, tex.width, tex.height);
        defaultClassIcon = Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1);

        for (int i = 0; i < 4; i++)
        {
            tex = Resources.Load("Sprites/class_icon_" + i.ToString()) as Texture2D;
            rec = new Rect(0, 0, tex.width, tex.height);
            classIcons.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
        }

        recrutUpgrade = PlayerPrefs.GetInt("RecrutUpgradeCost");
        au.volume *= PlayerPrefs.GetFloat("Volume");
        //Debug.Log(PlayerPrefs.GetInt("ToBattle"));
        language = PlayerPrefs.GetInt("Language");
        //obj = new MovebleObject(1, language);

        for (int i = 0; i <= staticData.weapons.list.Count - 1; i++)
        {
            //Debug.Log("Sprites/weapon_" + i.ToString() + "_0");
            // Debug.Log(Resources.Load("Sprites/weapon_" + i.ToString() + "_0"));
            tex = Resources.Load("Sprites/weapon_" + i.ToString() + "_0") as Texture2D;
            rec = new Rect(0, 0, tex.width, tex.height);
            weaponList.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));

            //weaponList.Add(Resources.Load("weapon_" + i.ToString() + "_0") as Sprite);
        }

        for (int i = 1; i < 5; i++)
        {
            //Debug.Log(Resources.Load("Sprites/armour_" + i.ToString() + "_0"));
            tex = Resources.Load("Sprites/armour_" + i.ToString() + "_0") as Texture2D;
            rec = new Rect(0, 0, tex.width, tex.height);
            armourList.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
        }

        for (int i = 1; i < 14; i++)
        {
            //Debug.Log(Resources.Load("Sprites/item_" + i.ToString() + "_0"));
            tex = Resources.Load("Sprites/item_" + i.ToString() + "_0") as Texture2D;
            rec = new Rect(0, 0, tex.width, tex.height);
            itemList.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
        }

        for (int i = 1; i < 7; i++)
        {
            //Debug.Log(Resources.Load("Sprites/item_" + i.ToString() + "_0"));
            tex = Resources.Load("Sprites/fork_" + i.ToString()) as Texture2D;
            rec = new Rect(0, 0, tex.width, tex.height);
            ForksSprites.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
        }

        for (int i = 0; i < 6; i++)
        {
            List<Sprite> spr = new List<Sprite>();
            for (int j = 0; j < 4; j++)
            {
                tex = Resources.Load("Sprites/perk_" + i.ToString() + "_" + j.ToString()) as Texture2D;
                if (tex != null)
                {
                    rec = new Rect(0, 0, tex.width, tex.height);
                    spr.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
                }
                else
                {
                    spr.Add(null);
                }
            }
            PerksSprites.Add(spr);
        }
        /*for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                Debug.Log(PerksSprites[i][j]);
            }
        }*/


        for (int i = 0; i < 6; i++)
        {
            GameObject go = GameObject.Find("Fork" + i.ToString());
            if (go != null)
            {
                int newI = i;
                go.GetComponent<Button>().onClick.AddListener(delegate { ForkClick(newI); });
            }
            perksWindows.Add(GameObject.Find("PerkWin" + i.ToString()));
            for (int j = 0; j < 4; j++)
            {
                //Debug.Log(perksWindows[i]);
                Transform g = perksWindows[i].transform.Find("Perk" + j.ToString());
                if (g != null)
                {
                    int newI = i;
                    int newJ = j;
                    g.GetComponent<Button>().onClick.AddListener(delegate { PerkClick(newI, newJ); });
                    g.GetComponent<Image>().sprite = PerksSprites[i][j];
                }
            }
        }

        time = PlayerPrefs.GetFloat("Time");
        gold = PlayerPrefs.GetInt("Gold");
        units = new ArrayList();
        unitsInBarraks = PlayerPrefs.GetInt("UnitsInBarraks");
        curCheckUnit = 0;
        onMission = new bool[unitsInBarraks];
        int cou = 0;
        int c = unitsInBarraks;
        for (int i = 0; i < c; i++)
        {
            if (PlayerPrefs.GetInt("NewGameBattle") == 0)
            {
                //units[i] = new MovebleObject(1, true);
                if (PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()) == 1)
                {
                    if (PlayerPrefs.GetInt("PlayerUnitOnMission" + cou.ToString()) == 1)
                    {
                        MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString());
                        if (b.id != 0)
                        {
                            units.Add(b);
                            // Debug.Log(b.name);
                        }
                    }
                    else
                    {
                        unitsInBarraks--;
                        //Debug.Log(1);
                    }
                    cou++;
                }
                else
                {
                    MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString());
                    if (b.id != 0)
                    {
                        units.Add(b);
                        // Debug.Log(b.id);
                    }
                }
                onMission[i] = false;
            }
            else
            {
                //PlayerPrefs.SetInt("DemonicInfluence", 400);
                //units.Add(new MovebleObject(1));
            }
        }
        PlayerPrefs.SetInt("NewGameBattle", 0);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            ((MovebleObject)units[i]).RecalcFatigue(time);
        }
        Sort();
        curCheckUnit = PlayerPrefs.GetInt("ChosenEquipUnit");

        Redraw();
    }

    MovebleObject recrut;

    void Save()
    {
        PlayerPrefs.SetInt("Gold", gold);
        PlayerPrefs.SetInt("UnitsInBarraks", unitsInBarraks);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            BackClick();
        }
    }

    int language;

    void RedrawUnitStats(MovebleObject unit)
    {
        string s = unit.GetEquipStats();

    }

    string[] fatigueText = { "Fatigued", "Ранение" };

    void FastHealClick()
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        int k = (int)Mathf.Round((float)System.Math.Round(a.valueFatigue - time, 2) * 4F);
        if (gold >= k)
        {
            // gold -= k;
            SpendMoney(k);
            PlayerPrefs.SetInt("CurSpendMoney", k + PlayerPrefs.GetInt("CurSpendMoney"));
            PlayerPrefs.SetInt("Achivment10", 1);
            if (PlayerPrefs.GetInt("CurSpendMoney") >= 300)
            {
                PlayerPrefs.SetInt("Achivment11", 1);
            }
            if (PlayerPrefs.GetInt("CurSpendMoney") >= 1000)
            {
                PlayerPrefs.SetInt("Achivment12", 1);
            }
            PlayerPrefs.SetInt("CurHealedUnit", k + PlayerPrefs.GetInt("CurHealedUnits"));
            PlayerPrefs.SetInt("Achivment7", 1);
            if (PlayerPrefs.GetInt("CurHealedUnits") >= 5)
            {
                PlayerPrefs.SetInt("Achivment8", 1);
            }
            if (PlayerPrefs.GetInt("CurHealedUnits") >= 25)
            {
                PlayerPrefs.SetInt("Achivment9", 1);
            }
            a.fatigueFlag = false;
            a.valueFatigue = -1;
        }
    }

    void RedrawUnitParamenters(MovebleObject unit)
    {
        string s = unit.GetPrameters();
        if (unit.fatigueFlag)
        {
            s += '\n' + fatigueText[language] + ": " + System.Math.Round(unit.valueFatigue - time, 2).ToString();
        }
    }

    void RedrawSkillsWindow(MovebleObject unit)
    {
        List<int> forks = unit.GetForks();
        for (int i = 0; i < 4; i++)
        {
            //ative
        }
        if (forks.Count == 0)
        {
            return;
        }
        for(int i = 0; i < 4; i++)
        {
            //deative
        }
        for(int i = 0; i < forks.Count; i++)
        {

        }
    }

    void ShowFork(MovebleObject unit)
    {

    }

    void SetPerkButton()
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        /*if (a.TryPerk(chosenFork, chosenPerk) && !a.HavePerk(chosenFork, chosenPerk))
        {
            //Debug.Log(a.TryPerk(chosenFork, chosenPerk));
            //Debug.Log(a.skillPoints);
            if (GUI.Button(new Rect(0, Screen.height / 2 - Screen.height / 8 * 2, Screen.width / 2, dx), acceptText[language]))
            {
                if (a.skillPoints > 0)
                {
                    a.SetPerk(chosenFork, chosenPerk);
                    PlayerPrefs.SetInt("CurLevelUp", PlayerPrefs.GetInt("CurLevelUp") + 1);
                    PlayerPrefs.SetInt("Achivment14", 1);
                    if (PlayerPrefs.GetInt("CurLevelUp") >= 10)
                    {
                        PlayerPrefs.SetInt("Achivment15", 1);
                    }
                    if (PlayerPrefs.GetInt("CurLevelUp") >= 25)
                    {
                        PlayerPrefs.SetInt("Achivment16", 1);
                    }
                }
            }
        }
        else
        {
            if (!a.HavePerk(chosenFork, chosenPerk))
            {
                if (a.skillPoints <= 0)
                {
                    if (GUI.Button(new Rect(0, Screen.height / 2f - Screen.height / 8f * 2f - Screen.height / 30f, Screen.width / 2, Screen.height / 8f), notEnSkill[language]))
                    {

                    }
                }
                else
                {
                    if (GUI.Button(new Rect(0, Screen.height / 2f - Screen.height / 8f * 2f - Screen.height / 30f, Screen.width / 2, Screen.height / 8), requirmentsText[language]))
                    {

                    }
                }
            }
        }*/
    }

    void ShowPerk(MovebleObject unit)
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
    }

    int gold;

    float time;

    void RedrawResourses()
    {

    }

    void InitLoadWindow()
    {

    }

    void BackClick()
    {
        Save();
        if (PlayerPrefs.GetInt("ToBattle") == 1)
        {
            //Debug.Log(PlayerPrefs.GetInt("ToBattle"));
            PlayerPrefs.SetInt("BackToBattle", 0);
            SceneManager.LoadSceneAsync("PlaningScene");
        }
        else
        {
            SceneManager.LoadSceneAsync("EventScene");
        }
    }

    int recrutUpgrade;

    void SpendMoney(int g)
    {
        gold -= g;
        PlayerPrefs.SetInt("CurSpendMoney", g + PlayerPrefs.GetInt("CurSpendMoney"));
        PlayerPrefs.SetInt("Achivment10", 1);
        if (PlayerPrefs.GetInt("CurSpendMoney") >= 300)
        {
            PlayerPrefs.SetInt("Achivment11", 1);
        }
        if (PlayerPrefs.GetInt("CurSpendMoney") >= 1000)
        {
            PlayerPrefs.SetInt("Achivment12", 1);
        }
    }
}

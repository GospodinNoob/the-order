﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class WeaponButton : MonoBehaviour {

    public int id;

    Text Name;
    Text Stats;
    Image Icon;

    void Awake()
    {
        Name = this.transform.Find("Name").GetComponent<Text>();
        Icon = this.transform.Find("Icon").GetComponent<Image>();
        Stats = this.transform.Find("Description").GetComponent<Text>();
    }

    public void Init(Weapon d)
    {
        //id = d.id;
        //Debug.Log(id);
        Name.text = d.keyName;
       // Stats.text = d.GetDescrDamage();
    }
}

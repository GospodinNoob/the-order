﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BarrkasController : MonoBehaviour
{
    /*
    public Texture2D levelUp;
    int maxEnemyUnits;
    float dx;
    int curCheckUnit;
    int unitsInBarraks;

    public GUIStyle buttonUp;
    public GUIStyle buttonDown;
    public GUIStyle personStyle;

    Damage[] damageList;// = { new Damage(1), new Damage(2), new Damage(3), new Damage(4), new Damage(5), new Damage(6), new Damage(7) };
    Armour[] armourList;// = { new Armour(1), new Armour(2), new Armour(3) };
    Item[] itemList;

    string baseGround;

    ArrayList units;
    bool[] onMission;
    int eventId;

    bool skillMenu;
    bool equipMenu;
    float verticalScrollBarValue;
    bool rightArm;
    int leftArm;
    bool armourShow;

    int mapHeight;
    int mapWidth;
    int demonicInfluence;

    GameEvent ev;

    bool start;

    void Sort()
    {
        for (int i = 0; i < unitsInBarraks; i++)
        {
            for (int j = 0; j < unitsInBarraks - 1; j++)
            {
                if ((((MovebleObject)units[j]).id == 0) || (((MovebleObject)units[j]).level < ((MovebleObject)units[j + 1]).level) || ((((MovebleObject)units[j]).level == ((MovebleObject)units[j + 1]).level) && (((MovebleObject)units[j]).exp < ((MovebleObject)units[j + 1]).exp)))
                {
                    MovebleObject buf = ((MovebleObject)units[j]);
                    units[j] = units[j + 1];
                    units[j] = buf;
                }
            }
        }
    }

    public AudioSource au;

    MovebleObject obj;

    void Start()
    {
        recrutUpgrade = PlayerPrefs.GetInt("RecrutUpgradeCost");
        dx = Mathf.Min(Screen.width / 3, Screen.height / 6);
        float dy = (Screen.height / 2f) / 3f;
        strengthPerkButton = new float[4][];
        shieldPerkButton = new float[4][];
        twoHandedPerkButton = new float[4][];
        agilityPerkButton = new float[4][];
        doubleHandedPerkButton = new float[4][];

        for (int i = 0; i < 4; i++)
        {
            strengthPerkButton[i] = new float[4];
            shieldPerkButton[i] = new float[4];
            twoHandedPerkButton[i] = new float[4];
            agilityPerkButton[i] = new float[4];
            doubleHandedPerkButton[i] = new float[4];

            strengthPerkButton[i][0] = 0 + Screen.width / 4 - dx * 2 / 6;
            shieldPerkButton[i][0] = 0 + Screen.width / 4 - dx * 2 / 6;
            twoHandedPerkButton[i][0] = 0 + Screen.width / 4 - dx * 2 / 6;
            agilityPerkButton[i][0] = 0 + Screen.width / 4 - dx * 2 / 6;
            doubleHandedPerkButton[i][0] = 0 + Screen.width / 4 - dx * 2 / 6;

            strengthPerkButton[i][1] = Screen.height / 30 + dy / 2 + dy * i * 2 / 3;
            shieldPerkButton[i][1] = Screen.height / 30 + dy / 2 + dy * i * 2 / 3;
            twoHandedPerkButton[i][1] = Screen.height / 30 + dy / 2 + dy * i * 2 / 3;
            agilityPerkButton[i][1] = Screen.height / 30 + dy / 2 + dy * i * 2 / 3;
            doubleHandedPerkButton[i][1] = Screen.height / 30 + dy / 2 + dy * i * 2 / 3;

            strengthPerkButton[i][2] = dx * 2f / 3f;
            shieldPerkButton[i][2] = dx * 2f / 3f;
            twoHandedPerkButton[i][2] = dx * 2f / 3f;
            agilityPerkButton[i][2] = dx * 2f / 3f;
            doubleHandedPerkButton[i][2] = dx * 2f / 3f;

            strengthPerkButton[i][3] = dx * 2f / 3f;
            shieldPerkButton[i][3] = dx * 2f / 3f;
            twoHandedPerkButton[i][3] = dx * 2f / 3f;
            agilityPerkButton[i][3] = dx * 2f / 3f;
            doubleHandedPerkButton[i][3] = dx * 2f / 3f;
        }

        shieldPerkButton[3][0] = 0 + Screen.width / 4 + dx * 2 / 6;
        twoHandedPerkButton[3][0] = 0 + Screen.width / 4 + dx * 2 / 6;
        doubleHandedPerkButton[3][0] = 0 + Screen.width / 4 + dx * 2 / 6;

        shieldPerkButton[3][1] = Screen.height / 30 + dx / 2;
        twoHandedPerkButton[3][1] = Screen.height / 30 + dx / 2 + dx * 1 * 2 / 3;
        doubleHandedPerkButton[3][1] = Screen.height / 30 + dx / 2;


        loadWindow = false;
        au.volume *= PlayerPrefs.GetFloat("Volume");
        //Debug.Log(PlayerPrefs.GetInt("ToBattle"));
        equipMenu = true;
        curID = 1;
        Damage dhelp = new Damage();
        Armour ahelp = new Armour(1, language);
        Item ihelp = new Item();
        language = PlayerPrefs.GetInt("Language");
        obj = new MovebleObject(1, language);
        lHelp = new LoadHelpObject(language);

        //damageList = new Damage[dhelp.rusNames.Length];

        armourList = new Armour[ahelp.rusNames.Length];
        itemList = new Item[ihelp.rusNames.Length];

        weaponUp = new bool[damageList.Length];
        armourUp = new bool[armourList.Length];
        itemUp = new bool[itemList.Length];

        for (int i = 0; i < damageList.Length; i++)
        {
            damageList[i] = new Damage(i, language);
            weaponUp[i] = PlayerPrefs.GetInt("WeaponUpgrade" + i.ToString()) == 1;
        }
            //Debug.Log(language);
            for (int i = 0; i < armourList.Length; i++)
        {
            armourList[i] = new Armour(i, language);
            armourUp[i] = PlayerPrefs.GetInt("ArmourUpgrade" + i.ToString()) == 1;
        }

        for (int i = 0; i < itemList.Length; i++)
        {
            itemList[i] = new Item(i, language);
            itemUp[i] = PlayerPrefs.GetInt("ItemUpgrade" + i.ToString()) == 1;
        }

        int fontSize = 18;

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;


        chosenFork = -1;
        equipMenu = true;
        freeFork = false;
        perks = false;
        time = PlayerPrefs.GetFloat("Time");
        gold = PlayerPrefs.GetInt("Gold");
        units = new ArrayList();
        shiftBar = 0;
        start = true;
        unitsInBarraks = PlayerPrefs.GetInt("UnitsInBarraks");
        skillMenu = false;
        equipMenu = false;
        rightArm = false;
        showMenu = false;
        curCheckUnit = 0;
        onMission = new bool[unitsInBarraks];
        int cou = 0;
        int c = unitsInBarraks;
        for (int i = 0; i < c; i++)
        {
            if (PlayerPrefs.GetInt("NewGameBattle") == 0)
            {
                //units[i] = new MovebleObject(1, true);
                if (PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()) == 1)
                {
                    if (PlayerPrefs.GetInt("PlayerUnitOnMission" + cou.ToString()) == 1)
                    {
                        MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString(), language);
                        if (b.id != 0)
                        {
                            units.Add(b);
                            // Debug.Log(b.name);
                        }
                    }
                    else
                    {
                        unitsInBarraks--;
                        //Debug.Log(1);
                    }
                    cou++;
                }
                else
                {
                    MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString(), language);
                    if (b.id != 0)
                    {
                        units.Add(b);
                        // Debug.Log(b.id);
                    }
                }
                onMission[i] = false;
            }
            else
            {
                //PlayerPrefs.SetInt("DemonicInfluence", 400);
                units.Add(new MovebleObject(1, language));
            }
        }
        //Debug.Log(unitsInBarraks);
        PlayerPrefs.SetInt("NewGameBattle", 0);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            ((MovebleObject)units[i]).RecalcFatigue(time);
        }
        Sort();
        curCheckUnit = PlayerPrefs.GetInt("ChosenEquipUnit");
        curRight = ((MovebleObject)units[curCheckUnit]).damage.id;
        curLeft = ((MovebleObject)units[curCheckUnit]).leftArm.id;
        curArm = ((MovebleObject)units[curCheckUnit]).armour.id;
        curItem = ((MovebleObject)units[curCheckUnit]).it.id;

        skillMenu = true;
    }

    MovebleObject recrut;

    void Save()
    {
        PlayerPrefs.SetInt("Gold", gold);
        PlayerPrefs.SetInt("UnitsInBarraks", unitsInBarraks);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Menu))
        {
            showMenu = !showMenu;
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("EventScene");
        }
    }

    public
        Texture2D okTexture;

    string baseGroundId;

    public
        Texture2D backGroundTexture;

    public
        GUISkin skin;

    int language;

    string[] damageText = {"Damage", "Урон" };
    string[] dmgBonText = {"Bonus damage", "Доп. урон" };
    string[] rangeText = {"Range", "Радиус" };
    string[] armourText = {"Armour", "Броня" };
    string[] speedText = {"Speed", "Скор." };
    string[] critText = { "Crit", "Крит" };
    string[] dodgeText = { "Dodge", "Уклон." };
    string[] blockText = { "Block", "Блок" };
    string[] parryText = { "Parry", "Парир." };
    string[] hitsText = { "Hits", "ОЗ" };
    string[] apText = { "AP", "ОД" };
    string[] levelText = { "Level", "Уровень" };
    string[] skillpointsText = { "Skill points", "Очки навыков" };
    string[] aimText = { "Accuracy", "Точность" };
    string[] attackCostText = { "Attack cost ", "Стоимость атаки" };


    void StatsWindow(int WindowID)
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        string s;
        s = a.damage.keyName;
        if (a.leftArm.id != 0)
        {
            s += '\n' + a.leftArm.keyName;
        }
        s += '\n';
        s += '\n' + a.armour.keyName + '\n'
                + '\n' + hitsText[language] + " " + a.maxHits //);
                + " " + apText[language] + " " + a.maxAP
                + '\n' + attackCostText[language] + " " + a.GetAttackCost()
                + '\n' + dmgBonText[language] + " " + a.bonusDamage //);
                + '\n' + rangeText[language] + " " + a.GetRangeAttack() //);
                + '\n' + a.armour.keyName
                + '\n' + speedText[language] + " " + a.speed//);
                + '\n' + aimText[language] + " " + (100 + a.aim) + "%"
                + '\n' + critText[language] + " " + a.critChance + "% " + '\n' + dodgeText[language] + " " + a.dodgeChance + "%"
                + '\n' + blockText[language] + " " + a.blockChance + "%" + '\n' + parryText[language] + " " + a.parryChance + "%"//);
                ;
        GUI.Box(new Rect(0, 0, Screen.width / 2 - Screen.width / 6, Screen.height / 8f * 5f), s);
           // damageText[language] + " " + a.damage.piercingDamage + " " + a.damage.slashDamage + " " + a.damage.crashDamage + " " + a.damage.axeDamage //);
                                                                                                                                 /*GUI.Box(new Rect(0, high * 2, Screen.width / 2, 2 * high),
                
    }

    string[] fatigueText = { "Fatigued", "Ранение" };

    void NonBattleStatsWindow(int WindowId)
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        string s = strengthText[language] + " " + a.strength

                + '\n' + agilityText[language] + " " + a.agility

                + '\n' + staminaText[language] + " " + a.stamina

                + '\n' + '\n' + levelText[language] + " " + a.level + ' ' + a.exp + "/" + a.nextLevel

                + '\n' + skillpointsText[language] + " " + a.skillPoints + '\n';
        if (a.fatigueFlag)
        {
            s += '\n' + fatigueText[language] + ": " + System.Math.Round(a.valueFatigue - time, 2).ToString();
            int k = (int)Mathf.Round((float)System.Math.Round(a.valueFatigue - time, 2) * 4F);
            if (GUI.Button(new Rect(0, Screen.height / 8 * 5, Screen.width / 2 - Screen.width / 6, Screen.height / 8), fastHeal[language] + "("+ k.ToString() + " " + goldText[language] + ")"))
            {
                if (gold >= k)
                {
                    // gold -= k;
                    SpendMoney(k);
                    PlayerPrefs.SetInt("CurSpendMoney", k + PlayerPrefs.GetInt("CurSpendMoney"));
                    PlayerPrefs.SetInt("Achivment10", 1);
                    if (PlayerPrefs.GetInt("CurSpendMoney") >= 300)
                    {
                        PlayerPrefs.SetInt("Achivment11", 1);
                    }
                    if (PlayerPrefs.GetInt("CurSpendMoney") >= 1000)
                    {
                        PlayerPrefs.SetInt("Achivment12", 1);
                    }
                    PlayerPrefs.SetInt("CurHealedUnit", k + PlayerPrefs.GetInt("CurHealedUnits"));
                    PlayerPrefs.SetInt("Achivment7", 1);
                    if (PlayerPrefs.GetInt("CurHealedUnits") >= 5)
                    {
                        PlayerPrefs.SetInt("Achivment8", 1);
                    }
                    if (PlayerPrefs.GetInt("CurHealedUnits") >= 25)
                    {
                        PlayerPrefs.SetInt("Achivment9", 1);
                    }
                    a.fatigueFlag = false;
                    a.valueFatigue = -1;
                }
            }
        }
        GUI.Box(new Rect(0, 0, Screen.width / 2 - Screen.width / 6, Screen.height / 8f * 5f), s);
    }

    string[] fastHeal = {"Quick cure", "Быстрое лечение" };

    bool freeFork;
    bool perks;

    string description;
    int activeId;

    int chosenFork = 0;
    int chosenPerk = 0;

    string[] strengthText = { "Strength", "Сила" };
    string[] agilityText = { "Agility", "Ловкость" };
    string[] staminaText = { "Stamina", "Выносливость" };

    string[] acceptText = { "Accept", "Принять" };
    string[] notEnSkill = { "Not enough skill points", "Не хватает очков" };

    void ForkWindow(int WindowId)
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        //a.skillPoints = 1;
        GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 2 - Screen.height / 4), description);
        if (!a.forks[chosenFork]) {
            if (a.skillPoints > 0)
            {
                if (GUI.Button(new Rect(0, Screen.height / 2 - Screen.height / 4 - Screen.height / 30, Screen.width / 2, Screen.height / 8), acceptText[language]))
                {
                    a.GainFork(chosenFork);
                    freeFork = false;
                }
            }
            else
            {
                if (GUI.Button(new Rect(0, Screen.height / 2 - Screen.height / 4 - Screen.height / 30, Screen.width / 2, Screen.height / 8), notEnSkill[language]))
                {
                }
            }
        }
    }

    string[] requirmentsText = { "Requirements are not fulfilled", "Требования не выполнены" };

    void SkillWindow(int WindowId)
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        //a.skillPoints = 1;
        GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 2 - Screen.height / 8 * 2), description);
        if (a.TryPerk(chosenFork, chosenPerk) && !a.HavePerk(chosenFork, chosenPerk))
        {
            //Debug.Log(a.TryPerk(chosenFork, chosenPerk));
            //Debug.Log(a.skillPoints);
            if (GUI.Button(new Rect(0, Screen.height / 2 - Screen.height / 8 * 2, Screen.width / 2, dx), acceptText[language]))
            {
                if (a.skillPoints > 0)
                {
                    a.SetPerk(chosenFork, chosenPerk);
                    PlayerPrefs.SetInt("CurLevelUp", PlayerPrefs.GetInt("CurLevelUp") + 1);
                    PlayerPrefs.SetInt("Achivment14", 1);
                    if (PlayerPrefs.GetInt("CurLevelUp") >= 10)
                    {
                        PlayerPrefs.SetInt("Achivment15", 1);
                    }
                    if (PlayerPrefs.GetInt("CurLevelUp") >= 25)
                    {
                        PlayerPrefs.SetInt("Achivment16", 1);
                    }
                }
            }
        }
        else
        {
            if (!a.HavePerk(chosenFork, chosenPerk))
            {
                if (a.skillPoints <= 0)
                {
                    if (GUI.Button(new Rect(0, Screen.height / 2f - Screen.height / 8f * 2f - Screen.height / 30f, Screen.width / 2, Screen.height / 8f), notEnSkill[language]))
                    {

                    }
                }
                else
                {
                    if (GUI.Button(new Rect(0, Screen.height / 2f - Screen.height / 8f * 2f - Screen.height / 30f, Screen.width / 2, Screen.height / 8), requirmentsText[language]))
                    {

                    }
                }
            }
        }
    }

    string[] lockedText = { "Locked", "Заблок." };

    public Texture2D chosen;

    public Texture2D[] bowPerks;

    void SkillsWindow(int WindowID)
    {
        float dx = (Screen.height / 6f);
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        int cou = a.forkCounter;
        if ((cou > 0) && (chosenFork == -1))
        {
            for(int i = 0; i < a.forksNames.Length; i++)
            {
                if (a.forks[i])
                {
                    chosenFork = i;
                    break;
                }
            }
        }
        if (freeFork)
        {
            int coun = 0;
            for (int i = 1; i < a.forks.Length; i++)
            {
                if (!a.forks[i] && (i != 3))
                {
                    if (GUI.Button(new Rect(0 + Screen.width / 4 * (coun % 2), Screen.height / 30 + dx / 2 + dx * (coun - coun % 2) / 2, Screen.width / 4, dx), a.forksNames[i]))
                    {
                        chosenFork = i;
                        perks = false;
                    }
                    coun++;
                }
            }
        }
        else
        {
            if (cou != 0)
            {
                string st = "";
                Texture2D tex;
                int i = 0;
                switch (chosenFork)
                {
                    case 0:
                        for (i = 0; i < 3; i++)
                        {
                            //Debug.Log(chosenFork);
                            //st = a.strengthNames[i];
                            tex = strengthSkills[i];
                          //  Debug.Log(strengthPerkButton[i][0] + " " + strengthPerkButton[i][1] + " " + strengthPerkButton[i][2] + " " + strengthPerkButton[i][3]);
                            if (GUI.Button(new Rect(strengthPerkButton[i][0], strengthPerkButton[i][1], strengthPerkButton[i][2], strengthPerkButton[i][3]), "", personStyle))
                            {
                                chosenPerk = i;
                                perks = true;
                            }
                            GUI.DrawTexture(new Rect(strengthPerkButton[i][0], strengthPerkButton[i][1], strengthPerkButton[i][2], strengthPerkButton[i][3]), tex);
                        }
                        break;

                    case 1:
                        for (i = 0; i < 4; i++)
                        {
                          //  st = a.shieldNames[i];
                            tex = shieldSkills[i];
                            if (GUI.Button(new Rect(shieldPerkButton[i][0], shieldPerkButton[i][1], shieldPerkButton[i][2], shieldPerkButton[i][3]), "", personStyle))
                            {
                                chosenPerk = i;
                                perks = true;
                            }
                            GUI.DrawTexture(new Rect(shieldPerkButton[i][0], shieldPerkButton[i][1], shieldPerkButton[i][2], shieldPerkButton[i][3]), tex);
                        }
                        break;

                    case 2:
                        for (i = 0; i < 4; i++)
                        {
                       //     st = a.twoHandedNames[i];
                            tex = twoHandedSkills[i];
                            if (GUI.Button(new Rect(twoHandedPerkButton[i][0], twoHandedPerkButton[i][1], twoHandedPerkButton[i][2], twoHandedPerkButton[i][3]), "", personStyle))
                            {
                                chosenPerk = i;
                                perks = true;
                            }
                            GUI.DrawTexture(new Rect(twoHandedPerkButton[i][0], twoHandedPerkButton[i][1], twoHandedPerkButton[i][2], twoHandedPerkButton[i][3]), tex);
                        }
                        break;

                    case 3:
                        for (i = 0; i < 3; i++)
                        {
                         //   st = a.agilityNames[i];
                            tex = agilitySkills[i];
                            if (GUI.Button(new Rect(agilityPerkButton[i][0], agilityPerkButton[i][1], agilityPerkButton[i][2], agilityPerkButton[i][3]), "", personStyle))
                            {
                                chosenPerk = i;
                                perks = true;
                            }
                            GUI.DrawTexture(new Rect(agilityPerkButton[i][0], agilityPerkButton[i][1], agilityPerkButton[i][2], agilityPerkButton[i][3]), tex);
                        }
                        break;

                    case 4:
                        for (i = 0; i < 4; i++)
                        {
                        //    st = a.doubleHandedNames[i];
                            tex = doubleHandedSkills[i];
                            if (GUI.Button(new Rect(doubleHandedPerkButton[i][0], doubleHandedPerkButton[i][1], doubleHandedPerkButton[i][2], doubleHandedPerkButton[i][3]), "", personStyle))
                            {
                                chosenPerk = i;
                                perks = true;
                            }
                            GUI.DrawTexture(new Rect(doubleHandedPerkButton[i][0], doubleHandedPerkButton[i][1], doubleHandedPerkButton[i][2], doubleHandedPerkButton[i][3]), tex);
                        }
                        break;
                    case 5:
                        for (i = 0; i < 4; i++)
                        {
                            //    st = a.doubleHandedNames[i];
                            tex = bowPerks[i];
                            if (GUI.Button(new Rect(doubleHandedPerkButton[i][0], doubleHandedPerkButton[i][1], doubleHandedPerkButton[i][2], doubleHandedPerkButton[i][3]), "", personStyle))
                            {
                                chosenPerk = i;
                                perks = true;
                            }
                            GUI.DrawTexture(new Rect(doubleHandedPerkButton[i][0], doubleHandedPerkButton[i][1], doubleHandedPerkButton[i][2], doubleHandedPerkButton[i][3]), tex);
                        }
                        break;
                }
            }
            else
            {
                GUI.Box(new Rect(0, dx / 2, Screen.width / 2, Screen.height / 3 - Screen.height / 8), messEffText[language]);
            }
        }
        int k = 0;
        if (cou == 0)
        {
            if (GUI.Button(new Rect(0, Screen.height / 30, Screen.width / 2, dx / 2), freeForkText[language]))
            {
                freeFork = true;
            }
        }
        else
        {
            for (int i = 0; i < a.forks.Length; i++)
            {
                string st;
                if (k <= cou)
                {
                    if (a.forks[i])
                    {
                        st = a.forksNames[i];
                        k++;
                        if (GUI.Button(new Rect(Screen.width / (cou) * (k - 1) / 2, Screen.height / 30, Screen.width / (cou) / 2, dx / 2), st))
                        {
                            freeFork = false;
                            perks = false;
                            chosenFork = i;
                        }
                    }
                }
            }
        }
        if (showMenu)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }

    float[][] strengthPerkButton;
    float[][] shieldPerkButton;
    float[][] twoHandedPerkButton;
    float[][] agilityPerkButton;
    float[][] doubleHandedPerkButton;

    static string messEffEng = "Choose skills to" + '\n' + "improve your" + '\n' + "battle efficiency";
    static string messEffRus = "Прокачивайте персонажей," + '\n' + "чтобы увеличить" + '\n' + "боевую эффективность";
    string[] messEffText = { messEffEng, messEffRus };

    string[] freeForkText = { "Free fork", "Выберите ветку" };

    public
        Texture2D dark;

    string[] progressText = { "Progress", "Прогресс" };
    string[] eventsText = { "Events", "События" };
    string[] barraksText = { "Barraks", "Казармы" };

    void MenuWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, 80, Screen.width / 2, Screen.height / 8), progressText[language]))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("ProgressScene");
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8, Screen.width / 2, Screen.height / 8), eventsText[language]))
        {
            Save();
            showMenu = false;
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8 * 2, Screen.width / 2, Screen.height / 8), barraksText[language]))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("BarraksScene");
            showMenu = false;
        }
    }
    bool squadStatus;

    bool itemMenu;

    bool showMenu;

    int shiftBar;
    int gold;

    public Texture2D fatigue;
    float time;


    string[] gainNewRecrutText = { "Gain new recrut" + '\n' + "(25 Gold)", "Нанять рекрута" + '\n' + "(25 золота)" };
    string[] emptyUnitText = { "Empty unit slot", "Не выбран юнит" };
    string[] goldText = { "Gold", "Золото" };
    string[] skillsText = { "Skills", "Навыки" };
    string[] equipText = { "Equip", "Экипировка" };
    string[] strenthText = { "Strength", "Сила" };
    string[] shiedText = { "Shield", "Щит" };
    string[] twoHandText = { "Two-handed", "Двуручное" };

    static string strDescEng = "Fork contains" + '\n' + "strength abilities" + '\n' + "Gives +1 strength per level";
    static string strDescRus = "Ветка содержит" + '\n' + "силовые сопосбности" + '\n' + "Даёт +1 к Силе за уровень";
    string[] strDesc = { strDescEng, strDescRus };

    static string shiDescEng = "Fork contains" + '\n' + "abilities with shield" + '\n' + "based on strength" + '\n' + "Improve one-handed block";
    static string shiDescRus = "Ветка содержит" + '\n' + "способности со щитом" + '\n' + "основанные на Силе" + '\n' + "Улучшает блок"+ '\n' + "одной рукой";
    string[] shiDesc = { shiDescEng, shiDescRus };

    static string twoDescEng = "Fork contains" + '\n' + "abilities with" + '\n' + "two-handed weapons" + '\n' + "based on strength" + '\n' + "Improve two-handed block";
    static string twoDescRus = "Ветка содержит" + '\n' + "способности с двуручным" + '\n' + "оружием, основанные" + '\n' + "на Силе" + '\n' + "Улучшает блок"+ '\n' + "двуручным оружием";
    string[] twoDesc = { twoDescEng, twoDescRus };

    string[] agilDesc = {
        "Fork contains" + '\n' + "agility abilities" + '\n' + "Gives +1 agility per level",
        "Ветка содержит" + '\n' + "способности ловкости" + '\n' + "Даёт +1 к ловкости за уровень"
    };

    string[] doubleDesc = {
        "Fork contains" + '\n' + "abilities wiht" + '\n' + "two weapons" + '\n' + "based on agility" + '\n' + "Improve one-handed parry",
        "Ветка содержит" + '\n' + "сппособноти с" + '\n'+ "двумя оружиями," + '\n' + "основанные на Ловкости" + '\n' + "Улучшает парирование" + '\n' + "одной рукой"
    };

    string[] archerDesc =
    {
        "Fork contains" + '\n' + "abilities wiht" + '\n' + "ranged weapons" + '\n' + "based on agility" + '\n' + "Improve accurcy",
        "Ветка содержит" + '\n' + "сппособноти с" + '\n'+ "стрелковым оружием," + '\n' + "основанные на Ловкости" + '\n' + '\n' + "Увеличивает точность"
    };

    string[] backText = { "Back", "Назад" };    

    void ListWindow (int WindowID)
    {
        float dx = Screen.width / 6.0f;
        float dy = Screen.height / 8.0f;
        if (GUI.Button(new Rect(0, 0, dx, dy / 2), "", buttonUp))
        {
            shiftBar = Mathf.Max(0, shiftBar - 1);
        }
        if (GUI.Button(new Rect(0, dy * 5 + dy / 2, dx, dy / 2), "", buttonDown))
        {
            shiftBar = Mathf.Min(unitsInBarraks, shiftBar + 1);
        }
        for (int i = shiftBar; i < Mathf.Min(shiftBar + 5, unitsInBarraks); i++)
        {
            //Debug.Log(((MovebleObject)units[i]).fatigueFlag);
            if (GUI.Button(new Rect(0, (i - shiftBar) * dy + dy / 2, dx, dy), ((MovebleObject)units[i]).name, personStyle))
            {
                curCheckUnit = i;
                armourShow = false;
                rightArm = false;
                perks = false;
                freeFork = false;
                if (((MovebleObject)units[i]).forkCounter > 0)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (((MovebleObject)units[i]).forks[j])
                        {
                            chosenFork = j;
                            //Debug.Log(j);
                            break;
                        }
                    }
                }
                else
                {
                    chosenFork = -1;
                }
                curRight = ((MovebleObject)units[curCheckUnit]).damage.id;
                curLeft = ((MovebleObject)units[curCheckUnit]).leftArm.id;
                curArm = ((MovebleObject)units[curCheckUnit]).armour.id;
                curItem = ((MovebleObject)units[curCheckUnit]).it.id;

                ((MovebleObject)units[i]).SetArmour(curArm);
                ((MovebleObject)units[i]).SetWeapon(curRight);
                ((MovebleObject)units[i]).SetLeftArm(curLeft);
                ((MovebleObject)units[i]).SetItem(curItem);
            }
            GUI.DrawTexture(new Rect(0, (i - shiftBar) * dy + dy / 2, dx, dy), faces[((MovebleObject)units[i]).type], ScaleMode.ScaleAndCrop);
            if (((MovebleObject)units[i]).skillPoints > 0)
            {
                GUI.DrawTexture(new Rect(0, (i - shiftBar) * dy + dy / 2, dx / 5, dx / 5), levelUp, ScaleMode.ScaleAndCrop);
            }
            if (((MovebleObject)units[i]).fatigueFlag)
            {
                float len = (time - ((MovebleObject)units[i]).startFatigue) / (((MovebleObject)units[i]).valueFatigue - ((MovebleObject)units[i]).startFatigue);
                len = 1 - len;
                //Debug.Log(len);
                GUI.DrawTextureWithTexCoords(new Rect(0, (i - shiftBar) * dy + dy / 2 - dy * (-1 + len), dx, dy + dy * (-1 + len)), fatigue, new Rect(0f, 0f, 1f, len));
            }
            if (curCheckUnit == i)
            {
                GUI.DrawTexture(new Rect(0, (i - shiftBar) * dy + dy / 2, dx, dy), chosen, ScaleMode.StretchToFill);
            }
        }
    }

    public Texture2D[] faces;

    string[] resoursesText = { "Resourses", "Ресурсы" };

    void ResoursesWindow(int WindowID)
    {
        GUI.Box(new Rect(0, 0, Screen.width / 3f, Screen.height / 8f), goldText[language] + " " + gold.ToString());
    }

    bool loadWindow;
    LoadHelpObject lHelp;

    string[] loadText = { "Loading", "Загрузка" };

    void LoadWindow(int WindowID)
    {
        int fontSize = 25;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    int recrutUpgrade;

    string[] upgradeRecrutSystem = { "Upgrade recruts", "Улучшить рекрутов" };

    void OnGUI()
    {
        GUI.skin = skin;
        if (loadWindow)
        {
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
        else
        {

            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backGroundTexture, ScaleMode.ScaleAndCrop);
            GUI.Window(3, new Rect(Screen.width / 6.0f * 5, 0, Screen.width / 6.0f, Screen.height / 8f * 6f), ListWindow, "");
            // Debug.Log(unitsInBarraks);
            if (GUI.Button(new Rect(0, Screen.height / 8f * 7f, Screen.width / 2, Screen.height / 8), backText[language]))
            {
                Save();
                if (PlayerPrefs.GetInt("ToBattle") == 1)
                {
                    //Debug.Log(PlayerPrefs.GetInt("ToBattle"));
                    PlayerPrefs.SetInt("BackToBattle", 0);
                    loadWindow = true;
                    SceneManager.LoadSceneAsync("PlaningScene");
                }
                else
                {
                    loadWindow = true;
                    SceneManager.LoadSceneAsync("EventScene");
                }
            }
            if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 8f * 7f, Screen.width / 4, Screen.height / 8), gainNewRecrutText[language]))
            {
                if (gold - 25 >= 0)
                {
                    unitsInBarraks++;
                    units.Add(new MovebleObject(1, language));
                    PlayerPrefs.SetInt("CurSpendMoney", 25 + PlayerPrefs.GetInt("CurSpendMoney"));
                    PlayerPrefs.SetInt("Achivment10", 1);
                    if (PlayerPrefs.GetInt("CurSpendMoney") >= 300)
                    {
                        PlayerPrefs.SetInt("Achivment11", 1);
                    }
                    if (PlayerPrefs.GetInt("CurSpendMoney") >= 1000)
                    {
                        PlayerPrefs.SetInt("Achivment12", 1);
                    }

                    PlayerPrefs.SetInt("CurBuyUnits", PlayerPrefs.GetInt("CurBuyUnits") + 1);
                    PlayerPrefs.SetInt("Achivment4", 1);
                    if (PlayerPrefs.GetInt("CurBuyUnits") >= 5)
                    {
                        PlayerPrefs.SetInt("Achivment5", 1);
                    }
                    if (PlayerPrefs.GetInt("CurBuyUnits") >= 25)
                    {
                        PlayerPrefs.SetInt("Achivment6", 1);
                    }
                    SpendMoney(25);
                    //gold -= 25;
                }
            }
            
            if (GUI.Button(new Rect(Screen.width / 4f * 3f, Screen.height / 8f * 7f, Screen.width / 4, Screen.height / 8), upgradeRecrutSystem[language] + '\n' + "(" + recrutUpgrade.ToString() + " " + goldText[language] + ")") && (gold > recrutUpgrade))
            {
                SpendMoney(recrutUpgrade);
                PlayerPrefs.SetInt("ExtraLevel", 1 + PlayerPrefs.GetInt("ExtraLevel"));
                PlayerPrefs.SetInt("RecrutUpgradeCost", 100 + (int)(100 * 0.4f * PlayerPrefs.GetInt("ExtraLevel")));
                recrutUpgrade = PlayerPrefs.GetInt("RecrutUpgradeCost");
            }
            GUI.Window(4, new Rect(Screen.width / 2f, Screen.height / 8f * 5f, Screen.width / 3f, Screen.height / 8f), ResoursesWindow, resoursesText[language]);
            if (GUI.Button(new Rect(Screen.width / 4f * 3f, Screen.height / 8f * 6f, Screen.width / 4, Screen.height / 8f), equipText[language]))
            {
                equipMenu = true;
                armourShow = false;
                rightArm = false;
                skillMenu = false;
                itemMenu = false;
            }
            if (GUI.Button(new Rect(Screen.width / 2f, Screen.height / 8f * 6f, Screen.width / 4f, Screen.height / 8f), skillsText[language]))
            {
                skillMenu = true;
                armourShow = false;
                rightArm = false;
                itemMenu = false;
                equipMenu = false;
            }
            if (((MovebleObject)units[curCheckUnit]).isEmpty())
            {
                GUI.Label(new Rect(Screen.width / 2, 0, Screen.width / 2, 20), emptyUnitText[language]);
            }
            else
            {
                if (equipMenu)
                {
                    GUI.Window(1, new Rect(Screen.width / 2, 0, Screen.width / 2 - Screen.width / 6, Screen.height / 8f * 5f), StatsWindow, ((MovebleObject)units[curCheckUnit]).name + " battle stats");
                }
                if (skillMenu)
                {
                    GUI.Window(1, new Rect(Screen.width / 2, 0, Screen.width / 2 - Screen.width / 6, Screen.height / 8f * 5f), NonBattleStatsWindow, ((MovebleObject)units[curCheckUnit]).name + " battle stats");

                }
            }
            if (skillMenu)
            {
                if (((MovebleObject)units[curCheckUnit]).isEmpty())
                {
                    GUI.Label(new Rect(Screen.width / 2, 0, Screen.width / 2, 20), emptyUnitText[language]);
                }
                else
                {
                    if (perks)
                    {
                        string st = "";
                        switch (chosenFork)
                        {
                            case 0:
                                st = ((MovebleObject)units[curCheckUnit]).strengthNames[chosenPerk];
                                description = ((MovebleObject)units[curCheckUnit]).descriptionStrengthPeks[chosenPerk];
                                break;

                            case 1:
                                st = ((MovebleObject)units[curCheckUnit]).shieldNames[chosenPerk];
                                description = ((MovebleObject)units[curCheckUnit]).descriptionShieldPeks[chosenPerk];
                                break;

                            case 2:
                                st = ((MovebleObject)units[curCheckUnit]).twoHandedNames[chosenPerk];
                                description = ((MovebleObject)units[curCheckUnit]).descriptionTwoHandedPeks[chosenPerk];
                                break;

                            case 3:
                                st = ((MovebleObject)units[curCheckUnit]).agilityNames[chosenPerk];
                                description = ((MovebleObject)units[curCheckUnit]).agilityDescription[chosenPerk];
                                break;

                            case 4:
                                st = ((MovebleObject)units[curCheckUnit]).doubleHandedNames[chosenPerk];
                                description = ((MovebleObject)units[curCheckUnit]).doubleHandedDescription[chosenPerk];
                                break;
                            case 5:
                                st = ((MovebleObject)units[curCheckUnit]).bowNames[chosenPerk];
                                description = ((MovebleObject)units[curCheckUnit]).bowDescriptionPerks[chosenPerk];
                                break;

                        }
                        GUI.Window(7, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2 - Screen.height / 30f - Screen.height / 8f), SkillWindow, st);
                    }
                    else
                    {

                        switch (chosenFork)
                        {
                            case 0:
                                description = strDesc[language];
                                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30f), ForkWindow, obj.forksNames[0]);
                                break;

                            case 1:
                                description = shiDesc[language];
                                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30f), ForkWindow, obj.forksNames[1]);

                                break;

                            case 2:
                                description = twoDesc[language];
                                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30f), ForkWindow, obj.forksNames[2]);

                                break;

                            case 3:
                                description = agilDesc[language];
                                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30f), ForkWindow, obj.forksNames[3]);

                                break;

                            case 4:
                                description = doubleDesc[language];
                                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30f), ForkWindow, obj.forksNames[4]);

                                break;
                            case 5:
                                description = archerDesc[language];
                                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30f, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30f), ForkWindow, obj.forksNames[5]);

                                break;
                                
                        }
                    }
                    GUI.Window(5, new Rect(0, 0, Screen.width / 2, Screen.height / 2f + Screen.height / 30f), SkillsWindow, ((MovebleObject)units[curCheckUnit]).name + " skills");

                }
            }
            if (equipMenu)
            {
                GUI.Window(5, new Rect(0, 0, Screen.width / 2, Screen.height / 2f + Screen.height / 30), EquipWindow, equipText[language]);
                string st = damageList[curRight].keyName;
                if (curID == 2)
                {
                    st = damageList[curLeft].keyName;
                }
                if (curID == 3)
                {
                    st = armourList[curArm].keyName;
                }
                if (curID == 4)
                {
                    st = itemList[curItem].name;
                }
                GUI.Window(6, new Rect(0, Screen.height / 2f + Screen.height / 30, Screen.width / 2, Screen.height / 2f - Screen.height / 8f - Screen.height / 30), ItemWindow, st);
            }
        }
    }

    public GUIStyle left;
    public GUIStyle right;

    int curRight;
    int curLeft;
    int curItem;
    int curArm;

    public Texture2D[] strengthSkills;
    public Texture2D[] shieldSkills;
    public Texture2D[] twoHandedSkills;
    public Texture2D[] agilitySkills;
    public Texture2D[] doubleHandedSkills;

    void EquipWindow(int WindowID)
    {
        float d = Screen.height / 8;
        if (GUI.Button(new Rect(0, Screen.height / 30, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", left))
        {
            int last = curRight;
            curRight--;
            curRight = Mathf.Max(0, curRight);
            while ((damageList[curRight].handStatus < -1) && (curRight > 0))
            {
                curRight--;
            }
            if (damageList[curRight].handStatus <= -2)
            {
                curRight = last;
            }
            if ((damageList[curRight].handStatus == 2) || (damageList[curRight].handStatus == 3))
            {
                curLeft = 0;
                ((MovebleObject)(units[curCheckUnit])).SetLeftArm(curLeft);
            }
            ((MovebleObject)(units[curCheckUnit])).SetWeapon(curRight);
            curID = 1;
            if (((MovebleObject)(units[curCheckUnit])).damage.handStatus == 3)
            {
                curItem = 10;
                ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
            }
            else
            {
                if (((MovebleObject)(units[curCheckUnit])).it.missle)
                {
                    curItem = 0;
                    ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
                }
            }
        }
        if ((damageList[curRight].handStatus != 2) && (damageList[curRight].handStatus != 3))
        {
            if (GUI.Button(new Rect(0, Screen.height / 30 + Screen.height / 8, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", left))
            {
                int last = curLeft;
                curLeft--;
                curLeft = Mathf.Max(0, curLeft);
                //Debug.Log(curLeft);
                while ((damageList[curLeft].handStatus > -1) && (curLeft > 0))
                {
                    //Debug.Log(curLeft);
                    curLeft--;
                }
                if (damageList[curLeft].handStatus > 0)
                {
                    curLeft = last;
                }
            ((MovebleObject)(units[curCheckUnit])).SetLeftArm(curLeft);
                curID = 2;
            }
        }
        if (GUI.Button(new Rect(0, Screen.height / 30 + Screen.height / 8 * 2, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", left))
        {
            curArm--;
            curArm = Mathf.Max(0, curArm);
            ((MovebleObject)(units[curCheckUnit])).SetArmour(curArm);
            curID = 3;
        }
        if (GUI.Button(new Rect(0, Screen.height / 30 + Screen.height / 8 * 3, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", left))
        {
            int last = curItem;
            curItem--;
            curItem = Mathf.Max(0, curItem);
            while ((damageList[curRight].handStatus == 3) && (curItem > 0) && (!itemList[curItem].missle))
            {
                //Debug.Log(curLeft);
                curItem--;
            }
            if ((damageList[curRight].handStatus == 3) && (!itemList[curItem].missle))
            {
                curItem = last;
            }
            ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
            curID = 4;
        }

        if (GUI.Button(new Rect(Screen.width / 4 + Screen.height / 16, Screen.height / 30, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", right))
        {
            int last = curRight;
            curRight++;
            curRight = Mathf.Min(damageList.Length - 1, curRight);
            while ((damageList[curRight].handStatus < -1) && (curRight < damageList.Length - 1))
            {
                curRight++;
                //Debug.Log(1);
            }
            if (damageList[curRight].handStatus <= -2)
            {
                curRight = last;
            }
            if ((damageList[curRight].handStatus == 2) || (damageList[curRight].handStatus == 3))
            {
                curLeft = 0;
                ((MovebleObject)(units[curCheckUnit])).SetLeftArm(curLeft);
            }
            ((MovebleObject)(units[curCheckUnit])).SetWeapon(curRight);
            curID = 1;
            if (((MovebleObject)(units[curCheckUnit])).damage.handStatus == 3)
            {
                curItem = 10;
                ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
            }
            else
            {
                if (((MovebleObject)(units[curCheckUnit])).it.missle)
                {
                    curItem = 0;
                    ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
                }
            }
        }
        if ((damageList[curRight].handStatus != 2) && (damageList[curRight].handStatus != 3))
        {
            if (GUI.Button(new Rect(Screen.width / 4 + Screen.height / 16, Screen.height / 30 + Screen.height / 8, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", right))
            {
                int last = curLeft;
                curLeft++;
                curLeft = Mathf.Min(damageList.Length - 1, curLeft);
                if (curLeft < damageList.Length - 1)
                {
                    //Debug.Log(damageList[curLeft].handStatus > -1);
                    while ((curLeft < damageList.Length - 1) && (damageList[curLeft].handStatus > -1))
                    {
                        curLeft++;
                        //Debug.Log(curLeft);
                    }
                }
                if (damageList[curLeft].handStatus > 0)
                {
                    curLeft = last;
                }
                ((MovebleObject)(units[curCheckUnit])).SetLeftArm(curLeft);
                curID = 2;
            }
        }
        if (GUI.Button(new Rect(Screen.width / 4 + Screen.height / 16, Screen.height / 30 + Screen.height / 8 * 2, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", right))
        {
            curArm++;
            curArm = Mathf.Min(armourList.Length - 1, curArm);
            ((MovebleObject)(units[curCheckUnit])).SetArmour(curArm);
            curID = 3;
        }
        if (GUI.Button(new Rect(Screen.width / 4 + Screen.height / 16, Screen.height / 30 + Screen.height / 8 * 3, Screen.width / 4 - Screen.height / 16, Screen.height / 8), "", right))
        {
            int last = curItem;
            curItem++;
            curItem = Mathf.Min(itemList.Length - 1, curItem);
            while ((damageList[curRight].handStatus == 3) && (curItem < itemList.Length - 1) && (!itemList[curItem].missle))
            {
                //Debug.Log(curLeft);
                curItem++;
            }
            if ((damageList[curRight].handStatus == 3) && (!itemList[curItem].missle))
            {
                curItem = last;
            }
            ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
            curID = 4;
            ((MovebleObject)(units[curCheckUnit])).SetItem(curItem);
            curID = 4;
        }

        if (GUI.Button(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30, Screen.height / 8, Screen.height / 8), "", personStyle))
        {
            curID = 1;
        }
        GUI.DrawTexture(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30, Screen.height / 8, Screen.height / 8), weaponTex[curRight]);

        if ((damageList[curRight].handStatus != 2) && (damageList[curRight].handStatus != 3))
        {
            if (GUI.Button(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30 + Screen.height / 8, Screen.height / 8, Screen.height / 8), "", personStyle))
            {
                curID = 2;
            }
            GUI.DrawTexture(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30 + Screen.height / 8, Screen.height / 8, Screen.height / 8), weaponTex[curLeft]);
        }
        if (GUI.Button(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30 + Screen.height / 8 * 2, Screen.height / 8, Screen.height / 8), "", personStyle))
        {
            curID = 3;
        }
        GUI.DrawTexture(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30 + Screen.height / 8 * 2, Screen.height / 8, Screen.height / 8), armorTex[curArm]);

        if (GUI.Button(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30 + Screen.height / 8 * 3, Screen.height / 8, Screen.height / 8), "", personStyle))
        {
            curID = 4;
        }
        GUI.DrawTexture(new Rect(Screen.width / 4 - Screen.height / 16, Screen.height / 30 + Screen.height / 8 * 3, Screen.height / 8, Screen.height / 8), itemTex[curItem]);

    }

    public Texture2D[] weaponTex;
    public Texture2D[] armorTex;
    public Texture2D[] itemTex;

    bool[] weaponUp;
    bool[] armourUp;
    bool[] itemUp;

    void SpendMoney(int g)
    {
        gold -= g;
        PlayerPrefs.SetInt("CurSpendMoney", g + PlayerPrefs.GetInt("CurSpendMoney"));
        PlayerPrefs.SetInt("Achivment10", 1);
        if (PlayerPrefs.GetInt("CurSpendMoney") >= 300)
        {
            PlayerPrefs.SetInt("Achivment11", 1);
        }
        if (PlayerPrefs.GetInt("CurSpendMoney") >= 1000)
        {
            PlayerPrefs.SetInt("Achivment12", 1);
        }
    }

    string[] upText = { "Upgrade (150 gold)", "Улучшить (150 золота)" };

    void ItemWindow(int WindowID)
    {
        MovebleObject a = ((MovebleObject)(units[curCheckUnit]));
        string s = "";
        if (curID == 1)
        {

            GUI.Box(new Rect(0, 0, Screen.width / 4, Screen.height / 2 - Screen.height / 4), a.damage.GetDescrDamage());
            GUI.Box(new Rect(Screen.width / 4, 0, Screen.width / 4, Screen.height / 2 - Screen.height / 4), a.damage.GetDescrAnother());
            if (!weaponUp[a.damage.id] && (a.damage.id != 0))
            {
                if (GUI.Button(new Rect(0, Screen.height / 4f - Screen.height / 30f, Screen.width / 2f, Screen.height / 8f), upText[language]) && (gold >= 150))
                {
                    PlayerPrefs.SetInt("WeaponUpgrade" + a.damage.id.ToString(), 1);
                    weaponUp[a.damage.id] = true;
                    SpendMoney(150);
                    a.SetWeapon(curRight);
                }
            }
        }
        if (curID == 2)
        {
            if (a.leftArm.id != -2)
            {
                GUI.Box(new Rect(0, 0, Screen.width / 4, Screen.height / 2 - Screen.height / 4), ((MovebleObject)(units[curCheckUnit])).leftArm.GetDescrDamage());
                GUI.Box(new Rect(Screen.width / 4, 0, Screen.width / 4, Screen.height / 2 - Screen.height / 4), ((MovebleObject)(units[curCheckUnit])).leftArm.GetDescrAnother());
            }
            else
            {
                GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 2 - Screen.height / 4), ((MovebleObject)(units[curCheckUnit])).leftArm.GetDescr());

            }
            if (!weaponUp[a.leftArm.id] && (a.leftArm.id != 0))
            {
                if (GUI.Button(new Rect(0, Screen.height / 4f - Screen.height / 30f, Screen.width / 2f, Screen.height / 8f), upText[language]) && (gold >= 150))
                {
                    PlayerPrefs.SetInt("WeaponUpgrade" + a.leftArm.id.ToString(), 1);
                    weaponUp[a.leftArm.id] = true;
                    SpendMoney(150);
                    a.SetLeftArm(curLeft);
               //     a.leftArm.Upgrade();
                }
            }
        }
        if (curID == 3)
        {
            GUI.Box(new Rect(0, 0, Screen.width / 4, Screen.height / 4), ((MovebleObject)(units[curCheckUnit])).armour.GetDescrArmour());
            GUI.Box(new Rect(Screen.width / 4, 0, Screen.width / 4, Screen.height / 4), ((MovebleObject)(units[curCheckUnit])).armour.GetDescrAnother());
            if (!armourUp[a.armour.id] && (a.armour.id != 0))
            {
                if (GUI.Button(new Rect(0, Screen.height / 4f - Screen.height / 30f, Screen.width / 2f, Screen.height / 8f), upText[language]) && (gold >= 150))
                {
                    PlayerPrefs.SetInt("ArmourUpgrade" + a.armour.id.ToString(), 1);
                    armourUp[a.armour.id] = true;
                    SpendMoney(150);
                    a.SetArmour(curArm);
                //    a.armour.Upgrade();
                }
            }
        }
        if (curID == 4)
        {
            GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 2 - Screen.height / 4), ((MovebleObject)(units[curCheckUnit])).it.GetDescr());
            if (!itemUp[a.it.id] && (a.it.id != 0))
            {
                if (GUI.Button(new Rect(0, Screen.height / 4f - Screen.height / 30f, Screen.width / 2f, Screen.height / 8f), upText[language]) && (gold >= 150))
                {
                    PlayerPrefs.SetInt("ItemUpgrade" + a.it.id.ToString(), 1);
                    itemUp[a.it.id] = true;
                    SpendMoney(150);
                //    a
                    a.SetItem(curItem);
                }
            }
        }
    }

    int curID;

    string[] menuText = { "Menu", "Меню" };
*/

}

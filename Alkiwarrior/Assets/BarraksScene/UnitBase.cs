﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class UnitBase{

    public string keyName;
    public string skeletName;
    public int baseStrength;
    public int baseAgility;
    public int baseStamina;
    public int baseWillpower;
    public int baseIntelligence;
    public float baseIncStrength;
    public float baseIncAgility;
    public float baseIncStamina;
    public float baseIncWillpower;
    public float baseIncIntelligence;
    public string idleAnim;
    public string walkAnim;
    public string blockAnim;
    public string hittedAnim;
    public string closeAnim;
    public string longAnim;
    public string dualAnim;
    public List<string> animList = new List<string>();

    public void Save(string nameParentUnit)
    {
        string s = nameParentUnit + "_base";

        PlayerPrefs.SetString(s + "_keyName", keyName);
        PlayerPrefs.SetString(s + "_skeletName", skeletName);

        PlayerPrefs.SetInt(s + "_strength", baseStrength);
        PlayerPrefs.SetInt(s + "_agility", baseAgility);
        PlayerPrefs.SetInt(s + "_stamina",baseStamina);
        PlayerPrefs.SetInt(s + "_willpower", baseWillpower);
        PlayerPrefs.SetInt(s + "_intelligence", baseIntelligence);

        PlayerPrefs.SetFloat(s + "_strength", baseIncStrength);
        PlayerPrefs.SetFloat(s + "_agility", baseIncAgility);
        PlayerPrefs.SetFloat(s + "_stamina", baseIncStamina);
        PlayerPrefs.SetFloat(s + "_willpower", baseIncWillpower);
        PlayerPrefs.SetFloat(s + "_intelligence", baseIncIntelligence);
    }

    public UnitBase(string nameParentUnit)
    {
        string s = nameParentUnit + "_base";

        keyName = PlayerPrefs.GetString(s + "_keyName");
        skeletName = PlayerPrefs.GetString(s + "_skeletName");

        baseStrength = PlayerPrefs.GetInt(s + "_strength");
        baseAgility = PlayerPrefs.GetInt(s + "_agility");
        baseStamina = PlayerPrefs.GetInt(s + "_stamina");
        baseWillpower = PlayerPrefs.GetInt(s + "_willpower");
        baseIntelligence = PlayerPrefs.GetInt(s + "_intelligence");

        baseIncStrength = PlayerPrefs.GetFloat(s + "_strength");
        baseIncAgility = PlayerPrefs.GetFloat(s + "_agility");
        baseIncStamina = PlayerPrefs.GetFloat(s + "_stamina");
        baseIncWillpower = PlayerPrefs.GetFloat(s + "_willpower");
        baseIncIntelligence = PlayerPrefs.GetFloat(s + "_intelligence");
        Init();
    }

    public UnitBase(UnitBase un)
    {
        if(un == null)
        {
            return;
        }
        keyName = un.keyName;
        skeletName = un.skeletName;
        baseStrength = un.baseStrength;
        baseStamina = un.baseStamina;
        baseAgility = un.baseAgility;
        baseWillpower = un.baseWillpower;
        baseIntelligence = un.baseIntelligence;
        baseIncAgility = un.baseIncAgility;
        baseIncStamina = un.baseIncStamina;
        baseIncStrength = un.baseIncStrength;
        baseIncWillpower = un.baseIncWillpower;
        baseIncIntelligence = un.baseIncIntelligence;
        idleAnim = un.idleAnim;
        walkAnim = un.walkAnim;
        hittedAnim = un.hittedAnim;
        blockAnim = un.blockAnim;

        closeAnim = un.closeAnim;
        longAnim = un.longAnim;
        dualAnim = un.dualAnim;
        Init();
    }

    public void Init()
    {
        animList.Add(idleAnim);
        animList.Add(walkAnim);
        animList.Add(hittedAnim);
        animList.Add(blockAnim);
        animList.Add(closeAnim);
        animList.Add(longAnim);
        animList.Add(dualAnim);
    }
}

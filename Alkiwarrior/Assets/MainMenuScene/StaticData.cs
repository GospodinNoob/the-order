﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class StaticData : MonoBehaviour {

    public static string LoadResourceTextfile(string name)
    {
        string filePath = "JsonData/" + name;

        TextAsset targetFile = Resources.Load<TextAsset>(filePath);

        return targetFile.text;
    }

    [Serializable]
    public class Weapons
    {
        public List<Weapon> list;
    }

    [Serializable]
    public class Items
    {
        public List<Item> list;
    }

    [Serializable]
    public class Armours
    {
        public List<Armour> list;
    }

    [Serializable]
    public class Damages
    {
        public List<Damage> list;
    }

    [Serializable]
    public class Passives
    {
        public List<Passive> list;
    }

    [Serializable]
    public class Actives
    {
        public List<Active> list;
    }

    [Serializable]
    public class ArmourValues
    {
        public List<ArmourValue> list;
    }

    [Serializable]
    public class Squads
    {
        public List<Squad> list;
    }

    [Serializable]
    public class Bases
    {
        public List<UnitBase> list;
    }

    [Serializable]
    public class Units
    {
        public List<MovebleObject> list;
    }

    [Serializable]
    public class Chunks
    {
        public List<Chunk> list;
    }

    Dictionary<string, AI> AIDict = new Dictionary<string, AI>();
    Dictionary<string, Sprite> SpriteDict = new Dictionary<string, Sprite>();

    public Weapon GetWeaponByKeyName(string keyName)
    {
        foreach(var i in weapons.list)
        {
            if(i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public Damage GetDamageByKeyName(string keyName)
    {
        foreach (var i in damages.list)
        {
           // Debug.Log(i.keyName + " "+ keyName);
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public Armour GetArmourByKeyName(string keyName)
    {
        foreach (var i in armours.list)
        {
            // Debug.Log(i.keyName + " "+ keyName);
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public ArmourValue GetArmourValueByKeyName(string keyName)
    {
        foreach (var i in armourValues.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public Passive GetPassiveByKeyName(string keyName)
    {
        foreach (var i in passives.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public Active GetActiveByKeyName(string keyName)
    {
        foreach (var i in actives.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public UnitBase GetBaseByKeyName(string keyName)
    {
        foreach (var i in bases.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public MovebleObject GetUnitByKeyName(string keyName)
    {
        foreach (var i in units.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public Item GetItemByKeyName(string keyName)
    {
        //Debug.Log(keyName);
        foreach (var i in items.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return new Item();
    }

    public AI GetAiByKeyName(string keyName)
    {
        //Debug.Log(keyName);
        if (AIDict.ContainsKey(keyName))
        {
            return AIDict[keyName];
        }
        return null;
    }

    public Chunk GetChunkByKeyName(string keyName)
    {
        foreach (var i in chunks.list)
        {
            if (i.keyName == keyName)
            {
                return i;
            }
        }
        return null;
    }

    public Weapons weapons;
    public Damages damages;
    public Armours armours;
    public Passives passives;
    public Actives actives;
    public ArmourValues armourValues;
    public Squads squads;
    public Bases bases;
    public Units units;
    public Items items;
    public Chunks chunks;

    public bool ArrayHasTag(string[] array, string tag)
    {
        if(array == null)
        {
            return false;
        }
        if(array.Length == 0)
        {
            return false;
        }
        foreach(var i in array)
        {
            if(i.ToLower() == tag.ToLower())
            {
               // Debug.Log(tag);
                return true;
            }
        }
        return false;
    }

    public Squad GetRandomSquadByTag(string tag)
    {
        List<Squad> buf = new List<Squad>();
        foreach(var i in squads.list)
        {
            if(ArrayHasTag(i.types, tag))
            {
                buf.Add(i);
            }
        }
        if(buf.Count == 0)
        {
            return null;
        }
        int rand = UnityEngine.Random.Range(0, buf.Count);
        return buf[rand];
    }

    public Squad GetRandomSquadWithMightByTag(int minMight, int maxMight, string tag)
    {
        List<Squad> buf = new List<Squad>();
        foreach (var i in squads.list)
        {
            if ((i.might >= minMight) && (i.might <= maxMight) && ArrayHasTag(i.types, tag))
            {
                buf.Add(i);
            }
        }
        if(buf.Count == 0)
        {
            return null;
        }
        int rand = UnityEngine.Random.Range(0, buf.Count);
        return buf[rand];
    }

    public MovebleObject GetRandomUnitWithMightByTag(int minMight, int maxMight, string tag)
    {
        List<MovebleObject> buf = new List<MovebleObject>();
        foreach (var i in units.list)
        {
            if ((i.might >= minMight) && (i.might <= maxMight) && ArrayHasTag(i.types, tag))
            {
                buf.Add(i);
            }
        }
        if (buf.Count == 0)
        {
            return null;
        }
        int rand = UnityEngine.Random.Range(0, buf.Count);
        return buf[rand];
    }

    public List<string> GetAnimListByUnit(MovebleObject move)
    {
        //bug.Log(move.keyName + " " + move.rightArm.locked);
        if (move.rightArm.locked)
        {
            return move.unitBase.animList;
        }
        //Debug.Log(move.rightArm.types);
        if (ArrayHasTag(move.rightArm.types, "HAL"))
        {
            return GetBaseByKeyName("ANIMHALB").animList;
        }
        if(ArrayHasTag(move.rightArm.types, "TWOSW"))
        {
            return GetBaseByKeyName("ANIMTWO").animList;
        }
        if (ArrayHasTag(move.leftArm.types, "DOUBLE"))
        {
            return GetBaseByKeyName("ANIMDOUBLE").animList;
        }
        if (ArrayHasTag(move.leftArm.types, "SHIELD"))
        {
            return GetBaseByKeyName("ANIMSHIELD").animList;
        }
        if (ArrayHasTag(move.rightArm.types, "BOW"))
        {
            return GetBaseByKeyName("ANIMBOW").animList;
        }
        return null;
    }

    Sprite LoadSprite(string KeyName)
    {
        Texture2D tex = Resources.Load("Sprites/" + KeyName) as Texture2D;
        if(tex == null)
        {
            return null;
        }
        Rect rec = new Rect(0, 0, tex.width, tex.height);
        return Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1);
    }

    void AddSprite(string KeyName)
    {
        if (SpriteDict.ContainsKey(KeyName))
        {
            return;
        }
        SpriteDict[KeyName] = LoadSprite(KeyName);
    }

    public Sprite GetSpriteByKeyName(string KeyName)
    {
        if (SpriteDict.ContainsKey(KeyName))
        {
            return SpriteDict[KeyName];
        }
        return null;
    }

    // Use this for initialization
    void Awake () {

        AddSprite("DOUBLEATTACK");
        AddSprite("SWORDATTACK");

        AIDict.Add("IMP", new ImpAI());

        string json;
        DontDestroyOnLoad(this.gameObject);

        json = LoadResourceTextfile("Damage");
        json = "{\"list\":" + json + "}";
        damages = JsonUtility.FromJson<Damages>(json);

        json = LoadResourceTextfile("ArmourValue");
        json = "{\"list\":" + json + "}";
        armourValues = JsonUtility.FromJson<ArmourValues>(json);

        json = LoadResourceTextfile("PassiveAbilities");
        json = "{\"list\":" + json + "}";
        passives = JsonUtility.FromJson<Passives>(json);

        json = LoadResourceTextfile("ActiveAbilities");
        json = "{\"list\":" + json + "}";
        actives = JsonUtility.FromJson<Actives>(json);

        json = LoadResourceTextfile("Armour");
        json = "{\"list\":" + json + "}";
        armours = JsonUtility.FromJson<Armours>(json);

        json = LoadResourceTextfile("Weapon");
        json = "{\"list\":" + json + "}";
        weapons = JsonUtility.FromJson<Weapons>(json);

        json = LoadResourceTextfile("Item");
        json = "{\"list\":" + json + "}";
        items = JsonUtility.FromJson<Items>(json);

        json = LoadResourceTextfile("UnitBase");
        json = "{\"list\":" + json + "}";
        bases = JsonUtility.FromJson<Bases>(json);

        json = LoadResourceTextfile("UnitArchtype");
        json = "{\"list\":" + json + "}";
        units = JsonUtility.FromJson<Units>(json);

        json = LoadResourceTextfile("SquadArchtype");
        json = "{\"list\":" + json + "}";
        squads = JsonUtility.FromJson<Squads>(json);

        json = LoadResourceTextfile("Chunk");
        json = "{\"list\":" + json + "}";
        chunks = JsonUtility.FromJson<Chunks>(json);

        foreach(var i in bases.list)
        {
            i.Init();
        }

        foreach (var i in passives.list)
        {
            AddSprite(i.image);
            i.Init();
        }

        foreach(var i in actives.list)
        {
            AddSprite(i.image);
            i.Init();
        }

        foreach(var i in items.list)
        {
            AddSprite(i.image);
            i.Init();
        }

        foreach (var i in weapons.list)
        {
            AddSprite(i.image);
            i.Init();
        }

        foreach (var i in armours.list)
        {
            AddSprite(i.image);
            i.Init();
        }

        foreach(var i in units.list)
        {
            i.Init();
        }

        foreach (var i in squads.list)
        {
            i.Init();
        }

        foreach (var i in chunks.list)
        {
           // Debug.Log(i + " " + i.keyName);
            i.Init();
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//using System.Drawing;
//using System.Drawing.Imaging;
using System;
using System.IO;
//using UnityEngine.Advertisements;

public class MainMenu : MonoBehaviour {

   /* // Use this for initialization

    float len;
    int language;

    string[] newGameText = { "New Game", "Новая игра" };
    string[] continueText = { "Continue", "Продолжить" };

    void GenStartEvent()
    {
        PlayerPrefs.SetInt("PlayerTeamCounter", 6);
        GameEvent ev = new GameEvent(PlayerPrefs.GetInt("DemonicInfluence"), 0, language);
        ev.enemyCounter = 2;
        ev.width = 6;
        ev.height = 6;
        ev.Save("ChosenEvent");
        for (int i = 0; i < 6; i++)
        {
            MovebleObject b = new MovebleObject(1, language);
            b.num = i;
            b.Save("PlayerUnitOnBase" + i.ToString());
            b.Save("PlayerUnit" + i.ToString());
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 1);
        }
        if (bonusesBool[0])
        {
            for (int i = 6; i < 12; i++)
            {
                MovebleObject b = new MovebleObject(1, language);
                b.num = i;
                b.Save("PlayerUnitOnBase" + i.ToString());
                PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            }
        }

    }

  //  public Image gif;
   // GifImage gifImage;
    //Image currentFame;


    void Save()
    {
        GenStartEvent();
        PlayerPrefs.SetInt("Language", language);
        PlayerPrefs.SetFloat("Volume", hSbarValue);
        PlayerPrefs.SetFloat("CameraSize", cameraSizeValue);
        if (confrimAction) {
            PlayerPrefs.SetInt("ConfrimAction", 1);
        }
        else
        {
            PlayerPrefs.SetInt("ConfrimAction", 0);
        }
    }

    Texture2D gifT;

    public static System.Drawing.Image Texture2Image(Texture2D texture)
    {
        if (texture == null)
        {
            return null;
        }
        byte[] bytes = texture.EncodeToPNG();

        MemoryStream ms = new MemoryStream(bytes);

        ms.Seek(0, SeekOrigin.Begin);

        System.Drawing.Image bmp2 = System.Drawing.Bitmap.FromStream(ms);

        ms.Close();
        ms = null;

        return bmp2;
    }

    string filePath;

    public Texture2D[] achivmentsIcon;
    string[] achivmentsNames;
    string[] achivmentsDesc;

    string[] rusAchivmentsNames = {
        "Начало пути",
        "Первая победа",
        "Победитель",
        "Победоносное" +'\n' + "шествие",
        "Первый рекрут",
        "Вербовщик",
        "Имя нам легион",
        "Не болей",
        "Возвращение" +'\n' + "на службу",
        "Прорывная" + '\n' + "медицина",
        "Первые траты",
        "Большие расходы",
        "Транжира",
        "Денег много" +'\n' + "не бывает",
        "Тренировка",
        "Элитный отряд",
        "Легендарная" + '\n' + "армия",
        "Первая кровь",
        "Убийца демонов",
        "Машина смерти",
        "Спасение мира"
    };

    string[] rusAchivmentsDesc =
    {
        "Выиграйте учебный" +'\n' + "бой",
        "Выиграйте сражение",
        "Выиграйте 5" +'\n' + "сражений за 1 игру",
        "Выиграйте 25" +'\n' + "сражений за 1 игру",
        "Наймите рекрута",
        "Наймите 5 рекрутов" +'\n' + "за 1 игру",
        "Наймите 25 рекрутов" +'\n' + "за 1 игру",
        "Быстро вылечите" +'\n' + "бойца",
        "Быстро вылечите" +'\n' + "5 бойцов за 1 игру",
        "Быстро вылечите" +'\n' + "25 бойцов за 1 игру",
        "Потратьте золото",
        "Потратьте 300" +'\n' + "золота за 1 игру",
        "Потратьте 1000" +'\n' + "золота за 1 игру",
        "Накопите 1000" +'\n' + "золота за 1 игру",
        "Повысьте уровень" +'\n' + "бойца",
        "Повысьте уровень" +'\n' + "10 бойцов за 1 игру",
        "Повысьте уровень" +'\n' + "25 бойцов за 1 игру",
        "Убейте демона",
        "Убейте 25 демонов" +'\n' + "за 1 игру",
        "Убейте 70 демонов" +'\n' + "за 1 игру",
        "Убейте предводителя" +'\n' + "демонов"
    };

    string[] engAchivmentsDesc =
    {
        "Win a tranning" +'\n' + "battle",
        "Win a battle",
        "Win 5 battles" +'\n' + "in 1 game",
        "Win 25 battles" +'\n' + "in 1 game",
        "Gain a recrut",
        "Gain 5 recruts" +'\n' + "in 1 game",
        "Gain 25 recruts" +'\n' + "in 1 game",
        "Quickly cure soldier",
        "Quickly cure 5" +'\n' + "soldiers in 1 game",
        "Quickly cure 25" +'\n' + "soldiers in 1 game",
        "Spend gold onse",
        "Spend 300 gold" +'\n' + "in 1 game",
        "Spend 1000 gold" +'\n' + "in 1 game",
        "Collect 1000 gold",
        "Select talent",
        "Select talent 10" +'\n' + "times in 1 game",
        "Select talent 25" +'\n' + "times in 1 game",
        "Kill a demon",
        "Kill 25 demons" +'\n' + "in 1 game",
        "Kill 70 demons" +'\n' + "in 1 game",
        "Kill the demon's" +'\n' + "leader"
    };

    string[] engAchivmentsNames = {
        "The begginig",
        "First win",
        "Win streak",
        "Triumphant" +'\n' + "march",
        "First recrut",
        "Recruter",
        "Our name" +'\n' + "is legion",
        "Be healthy",
        "Return to squad",
        "Breakthrough" +'\n' + "medicine",
        "First spending",
        "Heavy expenses",
        "Spender",
        "Not enough money",
        "Traning",
        "Elite squad",
        "Legendary army",
        "First blood",
        "Demon slayer",
        "War machine",
        "Save the world"
    };

    bool[] achivmentsBool;
    int killedDemons;

    void Start () {
        //PlayerPrefs.DeleteAll();
        //Debug.Log(filePath);
        //gif = Image.FromFile(filePath);
        //   TextAsset textAsset = (TextAsset)Resources.Load("g1", typeof(TextAsset));
        //  byte[] a = textAsset.bytes;
        // MemoryStream ms = new MemoryStream(a);

        //ms.Seek(0, SeekOrigin.Begin);
        //Debug.Log(ms);
        //  gif = Image.FromStream(ms);
        //    Debug.Log(gif);
        //gifImage = new GifImage(gif);
        //    currentFame = gifImage.GetFrame(0);
        //PlayerPrefs.DeleteAll();
        cameraSizeValue = 1;
        //PlayerPrefs.DeleteAll();
        if (PlayerPrefs.HasKey("Language"))
        {
            language = PlayerPrefs.GetInt("Language");
            cameraSizeValue = PlayerPrefs.GetFloat("CameraSize");
            confrimAction = PlayerPrefs.GetInt("ConfrimAction") == 1;
        }
        else
        {
            PlayerPrefs.SetInt("ConfrimAction", 1);
            confrimAction = true;
            for (int i = 0; i < achivmentsIcon.Length; i++)
            {
                PlayerPrefs.SetInt("Achivment" + i.ToString(), 0);
            }
            for(int i = 0; i < bonusesTex.Length; i++)
            {
                PlayerPrefs.SetInt("Bonuses" + i.ToString(), 0);
                PlayerPrefs.SetInt("WatchAdv" + i.ToString(), 0);
            }
            language = 0;
            cameraSizeValue = 1;
            PlayerPrefs.SetInt("Language", 0);
            PlayerPrefs.SetInt("MaxKilledDemons", 0);
            PlayerPrefs.SetFloat("CameraSize", 1);
        }
        killedDemons = Mathf.Max(PlayerPrefs.GetInt("MaxKilledDemons"), PlayerPrefs.GetInt("CurKilledDemons"));
        achivmentsBool = new bool[achivmentsIcon.Length];
        bonusesBool = new bool[bonusesTex.Length];
        for(int i = 0; i < achivmentsIcon.Length; i++)
        {
            achivmentsBool[i] = PlayerPrefs.GetInt("Achivment" + i.ToString()) == 1;
        }
        if (achivmentsBool[6] || (PlayerPrefs.GetInt("WatchAdv0") == 1))
        {
            PlayerPrefs.SetInt("Bonuses0", 1);
        }
        if (achivmentsBool[13] || (PlayerPrefs.GetInt("WatchAdv1") == 1))
        {
            PlayerPrefs.SetInt("Bonuses1", 1);
        }
        if (achivmentsBool[14] || (PlayerPrefs.GetInt("WatchAdv2") == 1))
        {
            PlayerPrefs.SetInt("Bonuses2", 1);
        }
        if (achivmentsBool[19] || (PlayerPrefs.GetInt("WatchAdv3") == 1))
        {
            PlayerPrefs.SetInt("Bonuses3", 1);
        }
        if (achivmentsBool[16] || (PlayerPrefs.GetInt("WatchAdv4") == 1))
        {
            PlayerPrefs.SetInt("Bonuses4", 1);
        }
        if (achivmentsBool[9] || (PlayerPrefs.GetInt("WatchAdv5") == 1))
        {
            PlayerPrefs.SetInt("Bonuses5", 1);
        }
        for (int i = 0; i < bonusesTex.Length; i++)
        {
            bonusesBool[i] = (PlayerPrefs.GetInt("Bonuses" + i.ToString()) == 1) || (PlayerPrefs.GetInt("WatchAdv" + i.ToString()) == 1);
        }
        SetLang();
        achivments = false;
        //len = Screen.height / 2;
        lHelp = new LoadHelpObject(language);
        len = Screen.height / 7f * 4f;
        int fontSize = 25;

        skin.horizontalScrollbarThumb.fixedHeight = Screen.height / 14;
        skin.horizontalScrollbarThumb.fixedWidth = Screen.height / 14;
        hSbarValue = 1f;
        if (PlayerPrefs.HasKey("Volume"))
        {
            hSbarValue = PlayerPrefs.GetFloat("Volume");
        }
        baseVol = au.volume;
        au.volume *= hSbarValue;
        //Debug.Log(au.volume + " " + hSbarValue);

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;
        loadWindow = false;
        //  Debug.Log(len);
    }

    float[] mainIm;
    float[] buttonIm;
	
	// Update is called once per frame
	void Update () {
	}

    public Texture2D backGroundTexture;

    public  UnityEngine.Object obj;

    public GUIStyle buttonStyle;

    public GUISkin skin;
    GUISkin skin2;

    public Texture2D tex;

    Texture2D frame;

    int chosen;

    string[] text = { "English", "Русский" };

    /*  public static Texture2D ImageToTexture(System.Drawing.Image im)
      {
          if (im == null)
          {
              return new Texture2D(4, 4);
          }
          MemoryStream ms = new MemoryStream();
          im.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
          ms.Seek(0, SeekOrigin.Begin);
          Texture2D tex = new Texture2D(im.Width, im.Height); 
          tex.LoadImage(ms.ToArray());
          ms.Close();
          ms = null;

          //
          return tex;
      }

    bool loadWindow;
    LoadHelpObject lHelp;
    string[] loadText = { "Loading", "Загрузка" };
    string[] achivmentsText = { "Achivments", "Достижения" };
    string[] exitText = { "Exit", "Выход" };

    public GUIStyle upArrow;
    public GUIStyle downArrow;

    void LoadWindow(int WindowID)
    {
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    int it = 0;
    void OnGUI()
    {
            return;
        GUI.skin = skin;
        if (loadWindow)
        {
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
        else {
            //currentFame = gif;
            //byte[] ar = imageToByteArray(currentFame);
            //Debug.Log(ar);
            //Debug.Log(ar.Length);
            //frame = new Texture2D(100, 100);
            //frame.LoadRawTextureData(ar);
            // Debug.Log(skin.font.fontSize);
            // Debug.Log(Screen.height);
            //  if (gif != null)
            {
                GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backGroundTexture, ScaleMode.ScaleAndCrop);
                //GUI.Label(new Rect(0, Screen.height / 2, Screen.width, 60), filePath);

                //       currentFame = gifImage.GetFrame(it);
                //     frame = ImageToTexture(currentFame);
                //   it++;
                // GUI.DrawTexture(new Rect(0, 0, 100, 100), frame);
                //currentFame = gifImage.GetNextFrame();
            }
            if (!settings && !achivments)
            {
                GUI.DrawTexture(new Rect(Screen.width / 4, 0, Screen.width / 2, len), tex, ScaleMode.ScaleAndCrop);
                if (GUI.Button(new Rect(0, len, Screen.width, Screen.height / 7), newGameText[language]))
                {
                    loadWindow = true;
                    PlayerPrefs.SetInt("NewGame", 1);
                    PlayerPrefs.SetInt("Boss", 0);
                    PlayerPrefs.SetInt("Win", 0);
                    PlayerPrefs.SetInt("ChosenEquipUnit", 0);
                    PlayerPrefs.SetInt("ToBattle", 0);
                    PlayerPrefs.SetInt("BackToBattle", 1);
                    PlayerPrefs.SetInt("EventCounter", 0);
                    PlayerPrefs.SetInt("DemonicInfluence", 400);
                    PlayerPrefs.SetInt("ExtraLevel", 0);
                    PlayerPrefs.SetInt("RecrutUpgradeCost", 100);
                    PlayerPrefs.SetFloat("Time", 0);
                    PlayerPrefs.SetFloat("GenTimer", UnityEngine.Random.Range(2f, 5f));
                    PlayerPrefs.SetInt("NewGameBattle", 0);
                    Armour ar = new Armour(language);
                    for(int i = 0; i < ar.armourNames.Length; i++)
                    {
                        PlayerPrefs.SetInt("ArmourUpgrade" + i.ToString(), 0);
                    }
                    Damage dmg = new Damage(0, language);
                    for (int i = 0; i < dmg.weaponNames.Length; i++)
                    {
                        PlayerPrefs.SetInt("WeaponUpgrade" + i.ToString(), 0);
                    }
                    Item it = new Item(0, language);
                    for(int i = 0; i < it.itemNames.Length; i++)
                    {
                        PlayerPrefs.SetInt("ItemUpgrade" + i.ToString(), 0);
                    }

                    if (bonusesBool[0])
                    {
                        PlayerPrefs.SetInt("UnitsInBarraks", 12);
                    }
                    else
                    {
                        PlayerPrefs.SetInt("UnitsInBarraks", 6);
                    }
                    if (bonusesBool[1])
                    {
                        PlayerPrefs.SetInt("Gold", 200);
                    }
                    else
                    {
                        PlayerPrefs.SetInt("Gold", 100);
                    }
                    PlayerPrefs.SetInt("CurExpRecruts", PlayerPrefs.GetInt("Bonuses4"));
                    PlayerPrefs.SetFloat("CurExtraGold", -PlayerPrefs.GetInt("Bonuses2") * 0.1f + 1f);
                    PlayerPrefs.SetFloat("CurExtraHeal", 1f - PlayerPrefs.GetInt("Bonuses5") * 0.1f);
                    PlayerPrefs.SetInt("LastGold", 0);
                    //Debug.Log(PlayerPrefs.GetInt("WatchAdv3"));
                    PlayerPrefs.SetInt("CurVeterans", PlayerPrefs.GetInt("Bonuses3"));
                    PlayerPrefs.SetInt("SpecialId", 0);
                    PlayerPrefs.SetInt("SmallBand", 0);
                    PlayerPrefs.SetInt("MassBand", 0);
                    PlayerPrefs.SetInt("Foothold", 0);
                    PlayerPrefs.SetInt("Doomed", 0);
                    PlayerPrefs.SetInt("EventExeption", -1);
                    PlayerPrefs.SetInt("ContinueBattle", 1);
                    PlayerPrefs.SetInt("CurBuyUnits", 0);
                    PlayerPrefs.SetInt("CurSpendMoney", 0);
                    PlayerPrefs.SetInt("CurHealedUnits", 0);
                    PlayerPrefs.SetInt("CurKilledDemons", 0);
                    PlayerPrefs.SetInt("CurWinBattle", 0);
                    PlayerPrefs.SetInt("CurLevelUp", 0);

                    for (int i = 0; i < 6; i++)
                    {
                        PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 0);
                    }

                    Save();

                    //SceneManager.LoadScene("EventScene");
                    SceneManager.LoadSceneAsync("BattleScene");
                }
                if (GUI.Button(new Rect(0, len + Screen.height / 7, Screen.width, Screen.height / 7), continueText[language]))
                {
                    if (PlayerPrefs.GetInt("NewGame") == 0)
                    {
                        loadWindow = true;
                        PlayerPrefs.SetInt("NewGameBattle", 0);
                        if (PlayerPrefs.HasKey("ContinueBattle"))
                        {
                            if (PlayerPrefs.GetInt("ContinueBattle") == 1)
                            {
                                SceneManager.LoadSceneAsync("BattleScene");
                            }
                            else
                            {
                                SceneManager.LoadSceneAsync("EventScene");
                            }
                        }
                        else
                        {
                            SceneManager.LoadSceneAsync("EventScene");
                        }
                    }
                }

                if (GUI.Button(new Rect(Screen.width / 2, len + Screen.height / 7 * 2, Screen.width / 2, Screen.height / 7), settingsText[language]))
                {
                    settings = true;
                }
                if (GUI.Button(new Rect(0, len + Screen.height / 7 * 2, Screen.width / 2, Screen.height / 7), exitText[language]))
                {
                    Application.Quit();
                }
                /*  if (GUI.Button(new Rect(0, len + Screen.height / 7 * 3, Screen.width, Screen.height / 7), achivmentsText[language]))
                  {
                      achivments = true;
                      achiveShift = 0;
                      curBonus = 0;
                  }
            }
            if (settings)
            {
                if (GUI.Button(new Rect(0, 0, Screen.width / 2, Screen.height / 7), "English"))
                {
                    language = 0;
                    int fontSize = 25;

                    skin.button.fontSize = fontSize * Screen.height / 485;
                    skin.box.fontSize = fontSize * Screen.height / 485;
                    skin.window.fontSize = fontSize * Screen.height / 485;
                    skin.label.fontSize = fontSize * Screen.height / 485;
                    SetLang();
                    Save();
                }
                if (GUI.Button(new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height / 7), "Русский"))
                {
                    language = 1;
                    int fontSize = 25;

                    skin.button.fontSize = fontSize * Screen.height / 485;
                    skin.box.fontSize = fontSize * Screen.height / 485;
                    skin.window.fontSize = fontSize * Screen.height / 485;
                    skin.label.fontSize = fontSize * Screen.height / 485;
                    SetLang();
                    Save();
                }
                if (GUI.Button(new Rect(0, Screen.height / 7 * 6, Screen.width / 2, Screen.height / 7), backText[language]))
                {
                    settings = false;
                    Save();
                }
                //if ()

                GUI.Box(new Rect(0, Screen.height / 7, Screen.width, Screen.height / 14), volumeText[language] + " " + ((int)hSbarValue).ToString());
                hSbarValue = GUI.HorizontalScrollbar(new Rect(0, Screen.height / 7 + Screen.height / 14, Screen.width, Screen.height / 14), hSbarValue, 0.0F, 0.0F, 10.0F);
                au.volume = baseVol * hSbarValue;
                //Debug.Log(au.volume + " " + hSbarValue / 10);
                GUI.Box(new Rect(0, Screen.height / 7 * 3, Screen.width, Screen.height / 14), camSizeText[language] + " " + (System.Math.Round(cameraSizeValue, 2)).ToString());
                cameraSizeValue = GUI.HorizontalScrollbar(new Rect(0, Screen.height / 7 * 3 + Screen.height / 14, Screen.width, Screen.height / 14), cameraSizeValue, 0.0F, 0.5F, 1.5F);
                if (confrimAction) {
                    if (GUI.Button(new Rect(0, Screen.height / 7 * 4, Screen.width, Screen.height / 7), secondClick[language] + on[language]))
                    {
                        confrimAction = !confrimAction;
                    }
                }
                else
                {
                    if (GUI.Button(new Rect(0, Screen.height / 7 * 4, Screen.width, Screen.height / 7), secondClick[language] + off[language]))
                    {
                        confrimAction = !confrimAction;
                    }
                }
            }
            if (achivments)
            {
                GUI.Window(2, new Rect(Screen.width / 2, Screen.height / 7 * 5, Screen.width / 2, Screen.height / 7 * 2), StatsWindow, recordText[language]);
                GUI.Window(3, new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height / 7 * 2), BonusWindow, bonusText[language]);
                GUI.Window(4, new Rect(Screen.width / 2, Screen.height / 7 * 2, Screen.width / 2, Screen.height / 7 * 3), CurBonusWindow, bonusNames[curBonus]);
                GUI.Window(1, new Rect(0, 0, Screen.width / 2, Screen.height / 7 * 6), AchivmentsWindow, achivmentsText[language]);
                if (GUI.Button(new Rect(0, Screen.height / 7 * 6, Screen.width / 2, Screen.height / 7), backText[language]))
                {
                    achivments = false;
                    Save();
                }
            }
        }
    }

    bool confrimAction;
    string[] on = { "Second click on block", "Повторное нажатие на блок" };
    string[] off = { "Click on 'Action description' window", "Клик по окну 'Описание действия'" };
    string[] secondClick = { "Confrim action type: ", "Подтверждение действия: " };
    string[] recordText = { "Records", "Рекорды" };
    string[] bonusText = { "Bonuses", "Бонусы" };
    string[] bonusDesc;
    string[] rusBonusDesc = {
        "+6 рекрутов на старте",
        "+100 золота на старте",
        "+10% дохода",
        "Стартовые бойцы" +'\n' + "имеют специализацию",
        "Новые рекруты" + '\n' + "имеют 50 опыта",
        "Бойцы исцеляются" + '\n' + "на 10% быстрее"
    };
    string[] engBonusDesc =
    {
        "+6 recruts in new game",
        "+100 gold in new game",
        "+10% gold income",
        "First squad has" + '\n' + "a specialization",
        "New recruts" + '\n' + "gain 50 experiance",
        "Soldiers cure" + '\n' + "10% faster"
    };

    string[] bonusNames;
    string[] rusBonusNames = {
        "Мобилизация",
        "Сбережения",
        "Процентная ставка",
        "Ветераны",
        "Опытные рекруты",
        "Первая помощь"
    };
    string[] engBonusNames =
    {
        "Mobilization",
        "Deposit",
        "Interest rate",
        "Veterans",
        "Experienced recruits",
        "First aid"
    };

    string[] unlockText;
    string[] rusUnlock = {
        "Получите достижение" +  '\n' + "Имя нам легион, или:",
        "Получите достижение" +  '\n' + "Денег много не бывает, или:",
        "Получите достижение" +  '\n' + "Транжира, или:",
        "Получите достижение" +  '\n' + "Машина смерти, или:",
        "Получите достижение" +  '\n' + "Легендарная армия, или:",
        "Получите достижение" +  '\n' + "Прорывная медицина, или:"
    };
    string[] engUnlock = {
        "Gain achivment" +  '\n' + "Our name is legion, or:",
        "Gain achivment" +  '\n' + "Not enough money, or:",
        "Gain achivment" +  '\n' + "Spender, or:",
        "Gain achivment" +  '\n' + "War machine, or:",
        "Gain achivment" +  '\n' + "Legendary army, or:",
        "Gain achivment" +  '\n' + "Breakthrough medicine, or:",
    };


    public Texture2D[] bonusesTex;

    void SetLang()
    {
        if (language == 0)
        {
            achivmentsDesc = engAchivmentsDesc;
            achivmentsNames = engAchivmentsNames;
            bonusDesc = engBonusDesc;
            bonusNames = engBonusNames;
            unlockText = engUnlock;
        }
        else
        {
            achivmentsDesc = rusAchivmentsDesc;
            achivmentsNames = rusAchivmentsNames;
            bonusNames = rusBonusNames;
            bonusDesc = rusBonusDesc;
            unlockText = rusUnlock;
        }
        lHelp = new LoadHelpObject(language);
    }

    int achiveShift;
    int curBonus;
    bool[] bonusesBool;

    string[] unlockAdv = { "Watch an advertise" +  '\n' + "and unlock bonus", "Посмотрите рекламу" + '\n' + "и разблокируйте бонус" };

    void CurBonusWindow(int WindowID)
    {
        skin.box.fontSize = 20 * Screen.height / 485;
       // GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 7), bonusDesc[curBonus]);
        if (!bonusesBool[curBonus])
        {
            GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 7), bonusDesc[curBonus]);
            GUI.Box(new Rect(0, Screen.height / 7, Screen.width / 2, Screen.height / 7), unlockText[curBonus]);
            if (GUI.Button(new Rect(0, Screen.height / 7 * 2, Screen.width / 2, Screen.height / 7), unlockAdv[language]))
            {
               // StartCoroutine(ShowAdWhenReady());
            }
        }
        else
        {
            GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 7 * 3), bonusDesc[curBonus]);
        }
        skin.box.fontSize = 25 * Screen.height / 485;
    }

    void AchivmentsWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, Screen.height / 7 * 5, Screen.width / 4, Screen.height / 7), "", upArrow))
        {
            achiveShift--;
            achiveShift = Math.Max(achiveShift, 0);
        }
        if(GUI.Button(new Rect(Screen.width / 4, Screen.height / 7 * 5, Screen.width / 4, Screen.height / 7), "", downArrow))
        {
            achiveShift++;
            achiveShift = Math.Min(achiveShift, achivmentsDesc.Length - 4);
        }
        for(int i = achiveShift; i < achiveShift + 4; i++)
        {
            int length = Math.Min(Screen.height / 7, Screen.width / 6);
            GUI.DrawTexture(new Rect(0, Screen.height / 7 * (i - achiveShift + 1) - Screen.height / 14, length, length), icon);
            if (achivmentsBool[i])
            {
                GUI.DrawTexture(new Rect(0, Screen.height / 7 * (i - achiveShift + 1) - Screen.height / 14, length, length), achivmentsIcon[i]);
            }
            skin.box.fontSize = 20 * Screen.height / 485;
            GUI.Box(new Rect(length, Screen.height / 7 * (i - achiveShift + 1) - Screen.height / 14, Screen.width / 2 - length, Screen.height / 14), achivmentsNames[i]);
            skin.box.fontSize = 15 * Screen.height / 485;
            GUI.Box(new Rect(length, Screen.height / 7f * (i - achiveShift + 1) + Screen.height / 14f - Screen.height / 14, Screen.width / 2 - length, Screen.height / 14), achivmentsDesc[i]);
            skin.box.fontSize = 25 * Screen.height / 485;
        }
    }

  /*  IEnumerator ShowAdWhenReady()
    {
        while (!Advertisement.IsReady())
            yield return null;
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                PlayerPrefs.SetInt("WatchAdv" + curBonus.ToString(), 1);
                bonusesBool[curBonus] = true;
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    public Texture2D icon;
    public Texture2D fon;

    string[] killDemons = {"Max. killed demons: ", "Макс. убийств: " };

    void StatsWindow(int WindowID)
    {
        skin.box.fontSize = 20 * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 7 * 2), killDemons[language] + " " + killedDemons.ToString());
        skin.box.fontSize = 25 * Screen.height / 485;
    }

    void BonusWindow(int WindowID)
    {
        for(int i = 0; i < bonusesTex.Length; i++)
        {
            float dx = Math.Min(Screen.width / 6f, Screen.height / 7f) - Screen.height / 60;
            if (GUI.Button(new Rect(Screen.width / 6 * (i % 3), Screen.height / 30 - Screen.height / 60 * (i / 3) + Screen.height / 7 * (i / 3), dx, dx), "", itemStyle))
            {
                curBonus = i;
            }
            if (bonusesBool[i])
            {
                GUI.DrawTexture(new Rect(Screen.width / 6 * (i % 3), Screen.height / 30 - Screen.height / 60 * (i / 3) + Screen.height / 7 * (i / 3), dx, dx), bonusesTex[i]);
            }
            if (i == curBonus)
            {
                GUI.DrawTexture(new Rect(Screen.width / 6 * (i % 3), Screen.height / 30 - Screen.height / 60 * (i / 3) + Screen.height / 7 * (i / 3), dx, dx), chosenTex);
            }
        }
    }

    public GUIStyle itemStyle;

    bool achivments;
    public AudioSource au;
    public Texture2D chosenTex;
    string[] volumeText = { "Sound volume", "Громкость звука" };
    string[] camSizeText = { " Camera Size", "Размер камеры" };
    float hSbarValue;
    float cameraSizeValue;

    bool settings;
    float baseVol;

    string[] backText = { "Back", "Назад" };
    string[] settingsText = { "Settings", "Настройки" };
    */
}

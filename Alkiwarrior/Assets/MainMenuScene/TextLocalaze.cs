﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class TextLocalaze : MonoBehaviour {

    public string[] texts;

	// Use this for initialization
	void Start () {
        SetLang();
	}

    List<string> lastValue;

    public void SetValues(List<string> vals)
    {
        string s = texts[PlayerPrefs.GetInt("Language")];
        if (vals != null)
        {
            for (int i = 0; i < vals.Count; i++)
            {
                s = s.Replace("#VAL" + i.ToString(), vals[i]);
            }
            lastValue = vals;
        }
        this.gameObject.GetComponent<Text>().text = s;
    }
	
	public void SetLang()
    {
        this.gameObject.GetComponent<Text>().text = texts[PlayerPrefs.GetInt("Language")];
        SetValues(lastValue);
        //Debug.Log(texts[PlayerPrefs.GetInt("Language")]);
       // this.gameObject.GetComponent<Text>().text = texts[PlayerPrefs.GetInt("Language")];
    }
}

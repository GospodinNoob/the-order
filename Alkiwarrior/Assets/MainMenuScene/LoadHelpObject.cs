﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadHelpObject : MonoBehaviour {

    string[] helps;
    public string text;
    string[] engHelp = { "Attack from the flank or rear" + '\n' + "increases the damage, crit" + '\n' + "and chance to hit",
        "Make sure that the soldiers were" +'\n' + "equipped according to their class",
        "The weapon has a different range," + '\n' + "but usually longer weighs more",
        "When moving shows who can" +'\n' + "be attacked from selected position",
        "There are 4 types of physical damage," +'\n' + "combine weapons to effectively deal damage",
        "Attack with dual weapons" + '\n' + "cost 1 less," + '\n' + "but not less than 1",
        "Attack cost depends on" + '\n' + "weapon pound",
        "On the bottom panel" + '\n' + "you may choose an ability",
        "Tap again on chosen block" + '\n' + "to confrim an action",
        "Elematal damage may" + '\n' + "affect the target with unique effects",
        "Bows requires points is equal to double pound to attack" + '\n' + "for every missing AP you get a debuff to accuracy",
        "For the attack of the majority weapon" + '\n' + "requires at least one AP",
        "A long weapon (claymore, halberd)" + '\n' + "receive a debuff in accuracy and damage at close range"
    };
    string[] rusHelp = { "Атака с фланга или тыла" + '\n' + "увеличивает урон, шанс крита" + '\n' + "и шанс попадания",
        "Следите, чтобы бойцы были экипированы" +'\n' + "соответственно своему классу",
        "Оружие имеет разную дальность," +'\n' + "но как правило более длинное весит больше",
        "При передвижении показывается кого" + '\n' +"можно будет атаковать с выбранной позиции",
        "Есть 4 типа физического урона, комбинируйте" +'\n' + "вооружение, чтобы эффективно наносить урон",
        "Атаки с оружием в двух руках" + '\n' + "стоят на 1 ОД меньше," + '\n' + "но не меньше 1",
        "Стоимость атак зависит от" + '\n' + "веса оружия",
        "На нажней панели показаны" + '\n' + "способности доступные" + '\n' + "в данный момент",
        "Повторное нажатие на выбранный" + '\n' + "блок, подтверждает действие",
        "Элементальный урон" + '\n' + "имеет свои особые воздействия на цель",
        "Луки требут вдвое больше очков на атаку чем вес" + '\n' + "за каждое недостающее ОД вы получаете штраф к точности",
        "Для атаки большинстовом оружия" + '\n' + "требуется одно ОД",
        "Длинное оружие (клеймор, алебарда)" + '\n' + "получают штраф в точности и уроне на близкой дистанции"
    };


    void SetLang(int la)
    {
        if (la == 0)
        {
            helps = engHelp;
        }
        if (la == 1)
        {
            helps = rusHelp;
        }
    }

	public LoadHelpObject(int la)
    {
        SetLang(la);
        text = helps[Random.Range(0, helps.Length)];
    }
}

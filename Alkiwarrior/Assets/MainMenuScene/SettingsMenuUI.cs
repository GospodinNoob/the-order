﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class SettingsMenuUI : MonoBehaviour {

    Button exitBtn;
    Dropdown language;
    Scrollbar volume;
    public AudioSource au;

    void ExitClick()
    {
        this.gameObject.SetActive(false);
    }

    void SetLang(int lang)
    {
        //Debug.Log(lang);
        PlayerPrefs.SetInt("Language", lang);
        TextLocalaze[] texts = this.gameObject.transform.root.GetComponentsInChildren<TextLocalaze>();

        foreach(var i in texts)
        {
            i.SetLang();
        }
    }

    void VolumeClick(float vol)
    {
        au.volume = vol * 2;
        PlayerPrefs.SetFloat("Volume", vol * 2);
    }

	// Use this for initialization
	void Start () {
		exitBtn = GameObject.Find("SettingsExit").GetComponent<Button>();
        exitBtn.onClick.AddListener(ExitClick);
        language = GameObject.Find("LanguageDropdown").GetComponent<Dropdown>();
        if (PlayerPrefs.HasKey("Language")) {
            language.value = PlayerPrefs.GetInt("Language");
        }
        language.onValueChanged.AddListener(SetLang);
        volume = GameObject.Find("VolumeBar").GetComponent<Scrollbar>();
        if (PlayerPrefs.HasKey("Volume"))
        {
            volume.value = PlayerPrefs.GetFloat("Volume");
        }
        else
        {
            volume.value = 0.5f;
        }
        volume.onValueChanged.AddListener(VolumeClick);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

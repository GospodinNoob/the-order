﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    public AudioSource au;

    void GenStartEvent()
    {
        int language = PlayerPrefs.GetInt("Language");
        GameEvent ev = new GameEvent(PlayerPrefs.GetInt("DemonicInfluence"), 0, language);
        ev.enemyCounter = 2;
        ev.width = 6;
        ev.height = 6;
        ev.Save("ChosenEvent");
        Squad sq = staticData.GetRandomSquadByTag("PLAYER");
        sq.Init();
        for(int i = 0; i < sq.units.Count; i++)
        {
            sq.units[i].NewPlayerUnitInit();
            sq.units[i].Save("PlayerUnitOnBase" + i.ToString());
        }
        sq.Save("BattleSquad");
    }


    void Save()
    {
        GenStartEvent();
        //PlayerPrefs.SetInt("Language", language);
        for (int i = 0; i < 100; i++)
        {
            PlayerPrefs.SetInt("Bonuses" + i.ToString(), 0);
            PlayerPrefs.SetInt("WatchAdv" + i.ToString(), 0);
        }
    }

    GameObject SettingsObj;
    GameObject GameMechanicObj;
    GameObject LoadObj;
    Button newGameBtn;
    Button continueBtn;
    Button exitBtn;
    Button settingsBtn;
    Button gameMechanicBtn;

    StaticData staticData;

    void Start()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetInt("ConfrimAction", 0);
            for (int i = 0; i < 100; i++)
            {
                PlayerPrefs.SetInt("Bonuses" + i.ToString(), 0);
                PlayerPrefs.SetInt("WatchAdv" + i.ToString(), 0);
            }
            PlayerPrefs.SetInt("Language", 0);
            PlayerPrefs.SetInt("MaxKilledDemons", 0);
            PlayerPrefs.SetFloat("CameraSize", 1);
            PlayerPrefs.SetFloat("Volume", 0.5f);
        }
        newGameBtn = GameObject.Find("NewGame").GetComponent<Button>();
        newGameBtn.onClick.AddListener(NewGameClick);
        continueBtn = GameObject.Find("Continue").GetComponent<Button>();
        continueBtn.onClick.AddListener(ContinueClick);
        settingsBtn = GameObject.Find("Settings").GetComponent<Button>();
        settingsBtn.onClick.AddListener(SettingsClick);
        exitBtn = GameObject.Find("ButtonExit").GetComponent<Button>();
        exitBtn.onClick.AddListener(ExitClick);
        gameMechanicBtn = GameObject.Find("GameMechanic").GetComponent<Button>();
        gameMechanicBtn.onClick.AddListener(GameMechanicClick);
        SettingsObj = GameObject.Find("SettingsWindow");
        LoadObj = GameObject.Find("LoadingWindow");
        LoadObj.gameObject.SetActive(false);
        //Debug.Log(SettingsObj);
        SettingsObj.SetActive(false);
        GameMechanicObj = GameObject.Find("GameMechanicWindow");
        GameMechanicObj.SetActive(false);
        au.volume = PlayerPrefs.GetFloat("Volume");
    }

    void Update()
    {
    }

    void NewGameClick()
    {
        int language = PlayerPrefs.GetInt("Language");
        PlayerPrefs.SetInt("GlobalId", 0);
        PlayerPrefs.SetInt("NewGame", 1);
        PlayerPrefs.SetInt("Boss", 0);
        PlayerPrefs.SetInt("Win", 0);
        PlayerPrefs.SetInt("ChosenEquipUnit", 0);
        PlayerPrefs.SetInt("ToBattle", 0);
        PlayerPrefs.SetInt("BackToBattle", 1);
        PlayerPrefs.SetInt("EventCounter", 0);
        PlayerPrefs.SetInt("DemonicInfluence", 400);
        PlayerPrefs.SetInt("ExtraLevel", 0);
        PlayerPrefs.SetInt("RecrutUpgradeCost", 100);
        PlayerPrefs.SetFloat("Time", 0);
        PlayerPrefs.SetFloat("GenTimer", UnityEngine.Random.Range(2f, 5f));
        PlayerPrefs.SetInt("NewGameBattle", 0);
        for (int i = 0; i < staticData.armours.list.Count; i++)
        {
            PlayerPrefs.SetInt("ArmourUpgrade" + i.ToString(), 0);
        }
        for (int i = 0; i < staticData.weapons.list.Count; i++)
        {
            PlayerPrefs.SetInt("WeaponUpgrade" + i.ToString(), 0);
        }

        for (int i = 0; i < staticData.items.list.Count; i++)
        {
            PlayerPrefs.SetInt("ItemUpgrade" + i.ToString(), 0);
        }
        PlayerPrefs.SetInt("LastGold", 0);
        PlayerPrefs.SetInt("SpecialId", 0);
        PlayerPrefs.SetInt("SmallBand", 0);
        PlayerPrefs.SetInt("MassBand", 0);
        PlayerPrefs.SetInt("Foothold", 0);
        PlayerPrefs.SetInt("Doomed", 0);
        PlayerPrefs.SetInt("EventExeption", -1);
        PlayerPrefs.SetInt("ContinueBattle", 1);
        PlayerPrefs.SetInt("CurBuyUnits", 0);
        PlayerPrefs.SetInt("CurSpendMoney", 0);
        PlayerPrefs.SetInt("CurHealedUnits", 0);
        PlayerPrefs.SetInt("CurKilledDemons", 0);
        PlayerPrefs.SetInt("CurWinBattle", 0);
        PlayerPrefs.SetInt("CurLevelUp", 0);

        for (int i = 0; i < 6; i++)
        {
            PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 0);
        }

        Save();

        //SceneManager.LoadScene("EventScene");
        InitLoadWindow();
        SceneManager.LoadSceneAsync("BattleScene");
    }

    void InitLoadWindow()
    {
        LoadObj.SetActive(true);
        LoadHelpObject lHelp = new LoadHelpObject(PlayerPrefs.GetInt("Language"));

        GameObject.Find("MainLoadText").GetComponent<Text>().text = lHelp.text;
        //SetLang();

    }

    void SetLang()
    {
        TextLocalaze[] texts = this.gameObject.transform.root.GetComponentsInChildren<TextLocalaze>();

        foreach (var i in texts)
        {
            i.SetLang();
        }
    }

    void ContinueClick()
    {
        if (PlayerPrefs.GetInt("NewGame") == 0)
        {
            InitLoadWindow();
            PlayerPrefs.SetInt("NewGameBattle", 0);
            if (PlayerPrefs.HasKey("ContinueBattle"))
            {
                if (PlayerPrefs.GetInt("ContinueBattle") == 1)
                {
                    SceneManager.LoadSceneAsync("BattleScene");
                }
                else
                {
                    SceneManager.LoadSceneAsync("EventScene");
                }
            }
            else
            {
                SceneManager.LoadSceneAsync("EventScene");
            }
        }
    }

    void SettingsClick()
    {
        SettingsObj.SetActive(true);
        SetLang();
    }

    void ExitClick()
    {
        Application.Quit();
    }

    void GameMechanicClick()
    {
        GameMechanicObj.SetActive(true);
        SetLang();
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveShow : MonoBehaviour {

    public void Init(Active a)
    {
        //Debug.Log(a.image + " " + a.keyName);
        StaticData staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        this.gameObject.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName(a.image);
    }
}

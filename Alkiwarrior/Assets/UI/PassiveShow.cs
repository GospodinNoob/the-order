﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PassiveShow : MonoBehaviour {

    public void Init(Passive p)
    {
        StaticData staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        Debug.Log(p.image);
        this.gameObject.GetComponent<Image>().sprite = staticData.GetSpriteByKeyName(p.image);
    }
	
}

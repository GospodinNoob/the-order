﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class SquadConstructionUI : MonoBehaviour {

    int maxEnemyUnits;
    float dx;
    int unitsInBarraks;
    public AudioSource au;

    string baseGround;

    ArrayList units;

    Button ClickAuto;
    Button ClickHands;
    Text MissionDescr;
    Text MissionStats;
    Text UnitStats;
    GameObject UnitsWindow;

    Image rightHand;
    Image leftHand;
    Image armour;
    Image item;

    List<GameObject> squadPositions;

    List<Sprite> weaponList = new List<Sprite>();
    List<Sprite> armourList = new List<Sprite>();
    List<Sprite> itemList = new List<Sprite>();

    // Use this for initialization
    void Sort()
    {
        for (int i = 0; i < unitsInBarraks; i++)
        {
            for (int j = 0; j < unitsInBarraks - 1; j++)
            {
                if ((((MovebleObject)units[j]).id == 0) || (((MovebleObject)units[j]).level < ((MovebleObject)units[j + 1]).level) || ((((MovebleObject)units[j]).level == ((MovebleObject)units[j + 1]).level) && (((MovebleObject)units[j]).exp < ((MovebleObject)units[j + 1]).exp)))
                {
                    MovebleObject buf = (MovebleObject)units[j];
                    units[j] = units[j + 1];
                    units[j] = buf;
                }
            }
        }
    }

    float time;

    int language;

    int curCheckUnit;

    void ChooseUnitClick(int id)
    {
        if (((MovebleObject)units[id]).fatigueFlag)
        {
            return;
        }
        if (((MovebleObject)units[id]).chosenInSquad)
        {
            return;
        }
        ((MovebleObject)units[curCheckUnit]).chosenInSquad = false;
        ((MovebleObject)units[id]).chosenInSquad = true;
        unitsInSquad[curCheckUnit] = ((MovebleObject)units[id]);
        curCheckUnit = id;
        UnitsWindow.gameObject.SetActive(false);
        Redraw();
    }

    void SetChosenUnit(int id)
    {
        if(unitsInSquad[id] == null)
        {
            SquadPositionClick(id);
            return;
        }
        curCheckUnit = id;
        Redraw();
    }

    void SquadPositionClick(int i)
    {
        curCheckUnit = i;
        UnitsWindow.SetActive(true);
        foreach(var it in unitButtonsList)
        {
            Destroy(it);
        }
        unitButtonsList.Clear();
        for (int it = 0; it < unitsInBarraks; it++)
        {
            GameObject go = GameObject.Instantiate(unitButton, this.transform.position, this.transform.rotation);
            go.transform.parent = unitsContent.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.GetComponent<UnitButton>().Init((MovebleObject)units[it]);
            int newI = it;
            go.GetComponent<Button>().onClick.AddListener(delegate { ChooseUnitClick(newI); });
            unitButtonsList.Add(go);
            //unitsInSquad[i].chosenInSquad = false;
            //((MovebleObject)units[i]).chosenInSquad = true;

        }
    }

    void ExitUnitsList()
    {
        UnitsWindow.SetActive(false);
    }

    public GameObject unitButton;
    GameObject unitsContent;
    GameEvent ev;
    List<GameObject> unitButtonsList;

    void Start()
    {
        GameObject.Find("ExitUnitsList").GetComponent<Button>().onClick.AddListener(ExitUnitsList);
        unitButtonsList = new List<GameObject>();
        unitsContent = GameObject.Find("UnitsContent");   
        squadPositions = new List<GameObject>();
        unitsInSquad = new List<MovebleObject>();
        squadPositions.Clear();
        unitsInSquad.Clear();
        unitsInBarraks = PlayerPrefs.GetInt("UnitsInBarraks");
        curCheckUnit = -1;
        rightHand = GameObject.Find("RightHand").GetComponent<Image>();
        leftHand = GameObject.Find("LeftHand").GetComponent<Image>();
        armour = GameObject.Find("Armour").GetComponent<Image>();
        item = GameObject.Find("Item").GetComponent<Image>();
        for (int i = 0; i < 6; i++)
        {
            squadPositions.Add(this.transform.Find("SquadUnit" + i.ToString()).gameObject);
            int newI = i;
            this.transform.Find("ChangeUnit" + i.ToString()).gameObject.GetComponent<Button>().onClick.AddListener(delegate { SquadPositionClick(newI); });
            squadPositions[i].GetComponent<Button>().onClick.AddListener(delegate { SetChosenUnit(newI);});
            squadPositions[i].GetComponent<UnitBlock>().num = i;
            unitsInSquad.Add(null);
            //Debug.Log(squadPositions[i].name);
        }
        ClickAuto = this.transform.Find("CreateAuto").GetComponent<Button>();
        ClickAuto.onClick.AddListener(CreateAuto);
        ClickHands = this.transform.Find("CreateHands").GetComponent<Button>();
        ClickHands.onClick.AddListener(CreateByHands);
        MissionDescr = GameObject.Find("MissionDescrContent").GetComponent<Text>();
        MissionStats = GameObject.Find("MissionStatsContent").GetComponent<Text>();
        UnitStats = GameObject.Find("UnitStatsContent").GetComponent<Text>();
        UnitsWindow = this.transform.Find("UnitsWindow").gameObject;
        UnitsWindow.SetActive(false);

        couInSquad = 0;
        au.volume *= PlayerPrefs.GetFloat("Volume");
        language = PlayerPrefs.GetInt("Language");
        ev = new GameEvent("ChosenEvent", language);

        for (int i = 0; i <= 16; i++)
        {
            //Debug.Log("Sprites/weapon_" + i.ToString() + "_0");
            // Debug.Log(Resources.Load("Sprites/weapon_" + i.ToString() + "_0"));
            Texture2D tex = Resources.Load("Sprites/weapon_" + i.ToString() + "_0") as Texture2D;
            Rect rec = new Rect(0, 0, tex.width, tex.height);
            weaponList.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
            
            //weaponList.Add(Resources.Load("weapon_" + i.ToString() + "_0") as Sprite);
        }

        for (int i = 1; i < 5; i++)
        {
            Debug.Log(Resources.Load("Sprites/armour_" + i.ToString() + "_0"));
            Texture2D tex = Resources.Load("Sprites/armour_" + i.ToString() + "_0") as Texture2D;
            Rect rec = new Rect(0, 0, tex.width, tex.height);
            armourList.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
        }

        for (int i = 1; i < 14; i++)
        {
            Debug.Log(Resources.Load("Sprites/item_" + i.ToString() + "_0"));
            Texture2D tex = Resources.Load("Sprites/item_" + i.ToString() + "_0") as Texture2D;
            Rect rec = new Rect(0, 0, tex.width, tex.height);
            itemList.Add(Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 1));
        }

        units = new ArrayList();
        int cou = 0;
        int c = unitsInBarraks;
        int setIn = 0;
        for (int i = 0; i < c; i++)
        {
            if (PlayerPrefs.GetInt("NewGameBattle") == 0)
            {
                //units[i] = new MovebleObject(1, true);
                if (PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()) == 1)
                {
                    if (PlayerPrefs.GetInt("PlayerUnitOnMission" + cou.ToString()) == 1)
                    {
                        MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString());
                        if (b.id != 0)
                        {
                            units.Add(b);
                            // Debug.Log(b.name);
                        }
                    }
                    else
                    {
                        unitsInBarraks--;
                        //Debug.Log(1);
                    }
                    cou++;
                }
                else
                {
                    MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString());
                    if (b.id != 0)
                    {
                        units.Add(b);
                        // Debug.Log(PlayerPrefs.GetInt("PlayerUnitInSquad" + i.ToString()));
                        if (PlayerPrefs.GetInt("BackToBattle") == 0)
                        {
                            //Debug.Log(i);
                            ((MovebleObject)units[i]).chosenInSquad = PlayerPrefs.GetInt("PlayerUnitInSquad" + i.ToString()) == 1;
                            if (((MovebleObject)units[i]).chosenInSquad)
                            {
                                couInSquad++;
                                unitsInSquad[setIn] = b;
                                setIn++;
                            }
                        }
                        else
                        {
                            ((MovebleObject)units[i]).chosenInSquad = false;
                        }
                        PlayerPrefs.SetInt("BackToBattle", 0);
                        // Debug.Log(onMission[i]);
                        // Debug.Log(b.id);
                    }
                }
            }
            else
            {
                //PlayerPrefs.SetInt("DemonicInfluence", 30);
                //units.Add(new MovebleObject(1, language));
            }
        }
        //
        for (int i = 0; i < unitsInBarraks; i++)
        {
            ((MovebleObject)units[i]).RecalcFatigue(time);
        }
        PlayerPrefs.SetInt("NewGameBattle", 0);
        Sort();
        Redraw();
    }

    List<MovebleObject> unitsInSquad;

    void Redraw()
    {
        for(int i = 0; i < 6; i++)
        {
           // Debug.Log(unitsInSquad[i]);
            squadPositions[i].GetComponent<UnitBlock>().Init(unitsInSquad[i]);
        }
       // Debug.Log(MissionDescr + " " + ev);
        MissionDescr.text = ev.GetShortDescr();
        MissionStats.text = ev.GetStatsDesr();
        Debug.Log(curCheckUnit);
        if (curCheckUnit != -1)
        {
            UnitStats.text = ((MovebleObject)units[curCheckUnit]).GetBattleStatsDescr();
            //Debug.Log(weaponList[((MovebleObject)units[curCheckUnit]).damage.id]);
           // rightHand.sprite = weaponList[((MovebleObject)units[curCheckUnit]).damage.id];
           // leftHand.sprite = weaponList[((MovebleObject)units[curCheckUnit]).leftArm.id];
            //armour.sprite = armourList[((MovebleObject)units[curCheckUnit]).armour.id - 1];
           // item.sprite = itemList[((MovebleObject)units[curCheckUnit]).it.id - 1];
        }
        else
        {
            UnitStats.text = "";
        }
    }

    public void DeleteFromSquad(int id)
    {
        unitsInSquad[id].chosenInSquad = false;
        unitsInSquad[id] = null;
        Redraw();
    }

    MovebleObject recrut;

    void Save()
    {
        PlayerPrefs.SetInt("ChosenEquipUnit", curCheckUnit);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            if (((MovebleObject)units[i]).chosenInSquad)
            {
                PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 1);
            }
            else
            {
                PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 0);
            }
            //Debug.Log(onMission[i]);
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
        }
    }

    void BackToEvents()
    {
        Save();
        PlayerPrefs.SetInt("EventExeption", -1);
        SceneManager.LoadSceneAsync("EventScene");
    }

    // Update is called once per fram
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Save();
            SceneManager.LoadSceneAsync("EventScene");
        }
    }

    void BarraksClick()
    {
        Save();
        PlayerPrefs.SetInt("ToBattle", 1);
        PlayerPrefs.SetInt("BackToBattle", 0);
        SceneManager.LoadSceneAsync("BarraksScene");
    }

    void FightClick()
    {
        int counter = 0;
        for (int i = 0; i < unitsInBarraks; i++)
        {
            if (((MovebleObject)units[i]).chosenInSquad)
            {
                counter++;
            }
        }
        if (counter > 0)
        {
            PlayerPrefs.SetInt("PlayerTeamCounter", counter);
            //  Debug.Log(counter);
            counter = 0;
            for (int i = 0; i < unitsInBarraks; i++)
            {
                PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 0);
                ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
                if (((MovebleObject)units[i]).chosenInSquad)
                {
                    PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 1);
                    ((MovebleObject)units[i]).Save("PlayerUnit" + counter.ToString());
                    counter++;
                }
                else
                {
                    PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
                }

                PlayerPrefs.SetInt("ContinueBattle", 1);
                SceneManager.LoadSceneAsync("BattleScene");
            }
        }
    }
    void InitLoadWindow()
    {

    }

    void CreateAuto()
    {
        Sort();
        int inSquad = 0;
        for (int i = 0; i < unitsInBarraks; i++)
        {
            //Debug.Log(((MovebleObject)units[i]).fatigueFlag);
            if (!((MovebleObject)units[i]).fatigueFlag)
            {
                if ((((MovebleObject)units[i]).id != 0) && (inSquad < 6))
                {
                    //    Debug.Log(i);
                    ((MovebleObject)units[i]).chosenInSquad = true;
                    inSquad++;
                    unitsInSquad[i] = (MovebleObject)units[i];
                }
            }
            if (inSquad == maxEnemyUnits)
            {
                break;
            }
        }
        curCheckUnit = 0;
        ClickAuto.gameObject.SetActive(false);
        ClickHands.gameObject.SetActive(false);
        Redraw();
    }

    void CreateByHands()
    {
        ClickAuto.gameObject.SetActive(false);
        ClickHands.gameObject.SetActive(false);
        Redraw();
    }

    int couInSquad;

    LoadHelpObject lHelp;


}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public
class BattleGenerator : MonoBehaviour
{
    /*
    public Texture2D levelUp;
    int maxEnemyUnits;
    float dx;
    int curCheckUnit;
    int unitsInBarraks;
    public AudioSource au;

    bool itemMenu;

    Damage[] damageList;
    Armour[] armourList;
    Item[] itemList;

    string baseGround;

    ArrayList units;
    bool[] onMission;
    int eventId;

    bool skillMenu;
    bool equipMenu;
    float verticalScrollBarValue;
    bool rightArm;
    int leftArm;
    bool armourShow;

    public GUIStyle left;
    public GUIStyle right;

    int mapHeight;
    int mapWidth;
    int gold;

    public
        GUIStyle buttonStyleBig;
    public
        GUIStyle buttonStyleMedium;
    public
        GUIStyle buttonStyleSmall;

    int demonicInfluence;

    GameEvent ev;

    public Texture2D fatigue;

    bool start;

    void Sort()
    {
        for(int i = 0; i < unitsInBarraks; i++)
        {
            for (int j = 0; j < unitsInBarraks - 1; j++)
            {
                if ((((MovebleObject)units[j]).id == 0) || (((MovebleObject)units[j]).level < ((MovebleObject)units[j + 1]).level) || ((((MovebleObject)units[j]).level == ((MovebleObject)units[j + 1]).level) && (((MovebleObject)units[j]).exp < ((MovebleObject)units[j + 1]).exp)))
                {
                    MovebleObject buf = (MovebleObject)units[j];
                    units[j] = units[j + 1];
                    units[j] = buf;
                }
            }
        }
    }

    float time;

    int language;

    void Start()
    {
        couInSquad = 0;
        //Debug.Log(au.volume);
        au.volume *= PlayerPrefs.GetFloat("Volume");
        //Debug.Log(au.volume);
        language = PlayerPrefs.GetInt("Language");
        loadWindow = false;
        lHelp = new LoadHelpObject(language);

        //Debug.Log(language);

        int fontSize = 18;

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;


        damageList = new Damage[7];
        armourList = new Armour[3];
        itemList = new Item[2];

        for(int i = 0; i < 7; i++)
        {
            damageList[i] = new Damage(i + 1, language);
        }

        for(int i = 0; i < 3; i++)
        {
            armourList[i] = new Armour(i + 1);
        }

        for(int i = 0; i < 2; i++)
        {
            itemList[i] = new Item(i, language);
        }

        time = PlayerPrefs.GetFloat("Time");
        gold = PlayerPrefs.GetInt("Gold");
        itemMenu = false;
        shiftBar = 0;
        start = PlayerPrefs.GetInt("BackToBattle") == 1;
        unitsInBarraks = PlayerPrefs.GetInt("UnitsInBarraks");
        ev = new GameEvent("ChosenEvent", language);
        mapWidth = ev.width;
        mapHeight = ev.height;
        maxEnemyUnits = ev.enemyCounter;
        baseGround = ev.baseGround[ev.baseGroundId - 1];
        skillMenu = false;
        equipMenu = false;
        rightArm = false;
        showMenu = false;
        curCheckUnit = 0;
        units = new ArrayList();
        onMission = new bool[unitsInBarraks];
        dx = Screen.width / 6;
        int cou = 0;
        int c = unitsInBarraks;
        for (int i = 0; i < c; i++)
        {
            if (PlayerPrefs.GetInt("NewGameBattle") == 0)
            {
                //units[i] = new MovebleObject(1, true);
                if (PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()) == 1)
                {
                    if (PlayerPrefs.GetInt("PlayerUnitOnMission" + cou.ToString()) == 1)
                    {
                        MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString(), language);
                        if (b.id != 0)
                        {
                            units.Add(b);
                            // Debug.Log(b.name);
                        }
                    }
                    else
                    {
                        unitsInBarraks--;
                        //Debug.Log(1);
                    }
                    cou++;
                }
                else
                {
                    MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString(), language);
                    if (b.id != 0)
                    {
                        units.Add(b);
                        // Debug.Log(PlayerPrefs.GetInt("PlayerUnitInSquad" + i.ToString()));
                        if (PlayerPrefs.GetInt("BackToBattle") == 0)
                        {
                            //Debug.Log(i);
                            onMission[i] = PlayerPrefs.GetInt("PlayerUnitInSquad" + i.ToString()) == 1;
                            if (onMission[i])
                            {
                                couInSquad++;
                            }
                        }
                        else{
                            onMission[i] = false;
                        }
                        PlayerPrefs.SetInt("BackToBattle", 0);
                        // Debug.Log(onMission[i]);
                        // Debug.Log(b.id);
                    }
                }
            }
            else
            {
                //PlayerPrefs.SetInt("DemonicInfluence", 30);
                units.Add(new MovebleObject(1, language));
            }
        }
        //
        for(int i = 0; i < unitsInBarraks; i++)
        {
            ((MovebleObject)units[i]).RecalcFatigue(time);
        }
        PlayerPrefs.SetInt("NewGameBattle", 0);
        Sort();
    }

    MovebleObject recrut;

    void Save()
    {
        PlayerPrefs.SetInt("ChosenEquipUnit", curCheckUnit);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            if (onMission[i])
            {
                PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 1);
            }
            else
            {
                PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 0);
            }
            //Debug.Log(onMission[i]);
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Menu))
        {
            showMenu = !showMenu;
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Save();
            PlayerPrefs.SetInt("SpecialID", 0);
            loadWindow = true;
            SceneManager.LoadSceneAsync("EventScene");
        }
    }

    public
        Texture2D okTexture;

    string baseGroundId;

    public
        Texture2D backGroundTexture;

    public
        GUISkin skin;

    string[] damageText = { "Damage", "Урон" };
    string[] dmgBonText = { "Bonus damage", "Доп. урон" };
    string[] rangeText = { "Range", "Радиус" };
    string[] armourText = { "Armour", "Броня" };
    string[] speedText = { "Speed", "Скор." };
    string[] critText = { "Crit", "Крит" };
    string[] dodgeText = { "Dodge", "Уклон." };
    string[] blockText = { "Block", "Блок" };
    string[] parryText = { "Parry", "Парир." };
    string[] hitsText = { "Hits", "ОЗ" };
    string[] apText = { "AP", "ОД" };
    string[] levelText = { "Level", "Уровень" };
    string[] skillpointsText = { "Skill points", "Очки навыков" };

    string[] aimText = { "Accuracy", "Точность" };


    void StatsWindow(int WindowID)
    {
        MovebleObject a = ((MovebleObject)units[curCheckUnit]);
        GUI.Box(new Rect(0, Screen.height / 40, Screen.width / 2, Screen.height / 2 - Screen.height / 6),
            damageText[language] + " " + a.damage.piercingDamage + " " + a.damage.slashingDamage + " " + a.damage.crushingDamage + " " + a.damage.axingDamage //);
                                                                                                                                                      /*GUI.Box(new Rect(0, high * 2, Screen.width / 2, 2 * high),
                + '\n' + dmgBonText[language] + " " + a.bonusDamage //);
                                                                    /*GUI.Box(new Rect(0, high * 3, Screen.width / 2, 2 * high),
                + ' ' + rangeText[language] + " " + a.damage.attackRange //);
                                                                         /*GUI.Box(new Rect(0, high * 4, Screen.width / 2, 2 * high),
                + '\n' + armourText[language] + " " + a.armour.piercingArmour + " " + a.armour.slashingArmour + " " + a.armour.crushingArmour + " " + a.armour.axingArmour //);
                                                                                                                                                                   /*GUI.Box(new Rect(0, high * 5, Screen.width / 2, 2 * high),
                + '\n' + speedText[language] + " " + a.speed //);

                + '\n' + aimText[language] + " " + (100 + a.aim) + "%"
                                                             /*GUI.Box(new Rect(0, high * 6, Screen.width / 2, 2 * high),
                + '\n' + critText[language] + " " + a.critChance + "% " + " " + dodgeText[language] + " " + a.dodgeChance + "%"

                + '\n' + blockText[language] + " " + a.blockChance + "%" + " " + parryText[language] + " " + a.parryChance + "%"//);
                                                                                                          /*GUI.Box(new Rect(0, high * 7, Screen.width / 2, 2 * high),
                + '\n' + hitsText[language] + " " + a.maxHits //);
                                                              /*GUI.Box(new Rect(0, high * 8, Screen.width / 2, 2 * high),
                + " " + apText[language] + " " + a.maxAP);

        if (GUI.Button(new Rect(0, Screen.height / 2 - Screen.height / 6, Screen.width / 2, Screen.height / 6), equipText[language]))
        {
            Save();
            PlayerPrefs.SetInt("ToBattle", 1);
            PlayerPrefs.SetInt("BackToBattle", 0);
            loadWindow = true;
            SceneManager.LoadSceneAsync("BarraksScene");
        }
        if (showMenu)
        {
       //     GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }

    public
        Texture2D dark;

    string[] emptyEventListText = { "Empty event list", "Не выбрано событие" };
    string[] enemyUnitsText = { "Enemy units:", "Враги:" };
    string[] mapSizeText = { "Map size:", "Размер карты:" };
    string[] baseGroundCoverText = { "Base ground cover:", "Ландшафт:" };


    void StatsWindow3(int WindowID)
    {
        GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 2 - Screen.height / 6), enemyUnitsText[language] + " " + maxEnemyUnits.ToString() + '\n' + mapSizeText[language] + " " + mapWidth.ToString() + "*" + mapHeight.ToString() + '\n'
                + baseGroundCoverText[language] + " " + baseGround);
        if (showMenu)
        {
            // GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }

    string[] progressText = { "Progress", "Прогресс" };
    string[] eventsText = { "Events", "События" };
    string[] barraksText = { "Barraks", "Казармы" };

    void MenuWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, 80, Screen.width / 2, Screen.height / 8), progressText[language]))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("ProgressScene");
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8, Screen.width / 2, Screen.height / 8), eventsText[language]))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("EventScene");
            showMenu = false;
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8 * 2, Screen.width / 2, Screen.height / 8), barraksText[language]))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("BarraksScene");
            showMenu = false;
        }
    }
    bool squadStatus;

    bool showMenu;

    int shiftBar;

    string[] missionBriffingText = {"Mission briefing", "Описание миссии" };
    string[] fightText = { "Fight!", "В бой!" };
    string[] createAuto = { "Create squad auto", "Автосбор отряда" };
    string[] createHand = { "Create squad by hand", "Ручной сбор" };
    string[] addText = { "Add", "Добавить" };
    string[] deleteText = { "Delete", "Удалить" };

    public Texture2D chosenTex;

    public Texture2D fonTex;

    void ListWindow(int WindowID)
    {
        if (!start)
        {
            if (GUI.Button(new Rect(0, Screen.height / 6 * 5, Screen.width / 2, Screen.height / 6), fightText[language]))
            {
                int counter = 0;
                for (int i = 0; i < unitsInBarraks; i++)
                {
                    if (onMission[i])
                    {
                        counter++;
                    }
                }
                if (counter > 0)
                {
                    PlayerPrefs.SetInt("PlayerTeamCounter", counter);
                    //  Debug.Log(counter);
                    counter = 0;
                    for (int i = 0; i < unitsInBarraks; i++)
                    {
                        PlayerPrefs.SetInt("PlayerUnitInSquad" + i.ToString(), 0);
                        ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
                        if (onMission[i])
                        {
                            ((MovebleObject)units[i]).num = counter;
                            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 1);
                            ((MovebleObject)units[i]).Save("PlayerUnit" + counter.ToString());
                            counter++;
                        }
                        else
                        {
                            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
                        }
                    }
                    loadWindow = true;
                    PlayerPrefs.SetInt("ContinueBattle", 1);
                    SceneManager.LoadSceneAsync("BattleScene");
                }
            }
        }

        int coun = 0;
        float dy = Screen.height / 8f;

        if (start)
        {
            if (GUI.Button(new Rect(0, 0, Screen.width / 2, Screen.height / 2 - Screen.height / 12), createAuto[language]))
            {
                int inSquad = 0;
                for (int i = 0; i < unitsInBarraks; i++)
                {
                    //Debug.Log(((MovebleObject)units[i]).fatigueFlag);
                    if (!((MovebleObject)units[i]).fatigueFlag)
                    {
                        if ((((MovebleObject)units[i]).id != 0) && (inSquad < 6))
                        {
                        //    Debug.Log(i);
                            onMission[i] = true;
                            inSquad++;
                        }
                    }
                    if (inSquad == maxEnemyUnits)
                    {
                        break;
                    }
                }
                start = false;
            }
            if (GUI.Button(new Rect(0, Screen.height / 2 - Screen.height / 12, Screen.width / 2, Screen.height / 2 - Screen.height / 12), createHand[language]))
            {
                start = false;
            }
        }
        else
        {
            for(int i = 0; i < 6; i++)
            {
                GUI.DrawTexture(new Rect(0, i * dy + dy / 2, dx, dy), fonTex);
            }
            for (int i = 0; i < unitsInBarraks; i++)
            {
                if (onMission[i])
                {
                    if (GUI.Button(new Rect(0, coun * dy + dy / 2, dx, dy), "", personStyle))
                    {
                        curCheckUnit = i;
                    }
                 //   Debug.Log(((MovebleObject)units[i]).type);
                    GUI.DrawTexture(new Rect(0, coun * dy + dy / 2, dx, dy), faces[((MovebleObject)units[i]).type], ScaleMode.ScaleAndCrop);
                    if (((MovebleObject)units[i]).skillPoints > 0)
                    {
                        GUI.DrawTexture(new Rect(0, coun * dy + dy / 2, dx / 5, dx / 5), levelUp, ScaleMode.ScaleAndCrop);
                    }
                    coun++;
                }
                if (curCheckUnit == i)
                {
                    if (!onMission[curCheckUnit])
                    {
                        if (!((MovebleObject)units[curCheckUnit]).fatigueFlag)
                        {
                            if (couInSquad < 6)
                            {
                                if ((i - shiftBar >= 0) && (i - shiftBar < 5))
                                {
                                    if (GUI.Button(new Rect(Screen.width / 6, (i - shiftBar) * dy + dy / 2, dx, dy), "", left))
                                    {
                                        couInSquad++;
                                        onMission[curCheckUnit] = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (GUI.Button(new Rect(Screen.width / 6, (coun - 1) * dy + dy / 2, dx, dy), "", right))
                        {
                            couInSquad--;
                            onMission[curCheckUnit] = false;
                        }
                        GUI.DrawTexture(new Rect(0, (coun - 1) * dy + dy / 2, dx, dy), chosenTex);
                    }
                }
            }

            coun = 0;
            if (GUI.Button(new Rect(Screen.width / 6 * 2, 0, dx, dy / 2), "", up))
            {
                shiftBar = Mathf.Max(0, shiftBar - 1);
            }
            if (GUI.Button(new Rect(Screen.width / 6 * 2, dy / 2 + 5 * dy, dx, dy / 2), "", down))
            {
                shiftBar = Mathf.Min(unitsInBarraks, shiftBar + 1);
            }
            for (int i = shiftBar; i < Mathf.Min(shiftBar + 5, unitsInBarraks); i++)
            {
                if (!onMission[i])
                {
                    if (GUI.Button(new Rect(Screen.width / 6 * 2, (i - shiftBar) * dy + dy / 2, dx, dy), "", personStyle))
                    {
                        curCheckUnit = i;
                        armourShow = false;
                        rightArm = false;
                        skillMenu = false;
                        itemMenu = false;
                    }
                    GUI.DrawTexture(new Rect(Screen.width / 6 * 2, (i - shiftBar) * dy + dy / 2, dx, dy), faces[((MovebleObject)units[i]).type], ScaleMode.ScaleAndCrop);

                    if (((MovebleObject)units[i]).skillPoints > 0)
                    {
                        GUI.DrawTexture(new Rect(Screen.width / 6 * 2, i * dy + dy / 2, dx / 5, dx / 5), levelUp, ScaleMode.ScaleAndCrop);
                    }
                    if (((MovebleObject)units[i]).fatigueFlag)
                    {
                        float len = (time - ((MovebleObject)units[i]).startFatigue) / (((MovebleObject)units[i]).valueFatigue - ((MovebleObject)units[i]).startFatigue);
                       // len = 0;
                        len = 1 - len;
                        //Debug.Log(len);
                        GUI.DrawTextureWithTexCoords(new Rect(Screen.width / 6 * 2, (i - shiftBar) * dy - dy * (-1 + len) + dy / 2, dx, dy + dy * (-1 + len)), fatigue, new Rect(0f, 0f, 1f, len));
                    }
                    if (curCheckUnit == i)
                    {
                        GUI.DrawTexture(new Rect(Screen.width / 6 * 2, (i - shiftBar) * dy + dy / 2, dx, dy), chosenTex);
                    }
                }
                else
                {
                    if(GUI.Button(new Rect(Screen.width / 6 * 2, (i - shiftBar) * dy + dy / 2, dx, dy), "", personStyle))
                    {

                    }
                    GUI.DrawTexture(new Rect(Screen.width / 6 * 2, (i - shiftBar) * dy + dy / 2, dx, dy), locked, ScaleMode.ScaleAndCrop);                 
                }
                coun++;
            }
        }   
        
    }

    public Texture2D locked;

    public GUIStyle personStyle;
    public GUIStyle up, down;
    public Texture2D[] faces;

    int couInSquad;

    bool loadWindow;
    LoadHelpObject lHelp;
    string[] loadText = { "Loading", "Загрузка" };

    void LoadWindow(int WindowID)
    {
        int fontSize = 25;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    void OnGUI()
    {
        GUI.skin = skin;
        if (loadWindow)
        {
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
        else
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backGroundTexture, ScaleMode.ScaleAndCrop);
            GUI.Window(5, new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height), ListWindow, "");
            if (!start)
            {
                GUI.Window(3, new Rect(0, Screen.height / 2, Screen.width / 2, Screen.height / 2 - Screen.height / 6), StatsWindow3, missionBriffingText[language]);
            }
            if (GUI.Button(new Rect(0, Screen.height / 6 * 5, Screen.width / 2, Screen.height / 6), backText[language]))
            {
                Save();
                PlayerPrefs.SetInt("EventExeption", -1);
                loadWindow = true;
                SceneManager.LoadSceneAsync("EventScene");
            }

            //  if (GUI.Button(new Rect(Screen.width / 4 * 3, Screen.height / 3 * 2 - dx * 2, Screen.width / 4, dx), equipText[language]))
            {
                //     equipMenu = !equipMenu;
                //    armourShow = false;
                //    rightArm = false;
                //    skillMenu = false;
                //    itemMenu = false;
            }
            if (!start && (unitsInBarraks > 0))
            {

                GUI.Window(1, new Rect(0, 0, Screen.width / 2, Screen.height / 2), StatsWindow, ((MovebleObject)units[curCheckUnit]).name);

            }
        }
    }

    string[] emptyText = { "Empty unit slot", "Юнит не выбран" };
    string[] lockedText = { "Locked", "Заблок." };
    string[] menuText = { "Menu", "Меню" };
    string[] backText = { "Back", "Назад" };
    string[] equipText = { "Equip", "Экипировка" };*/
}

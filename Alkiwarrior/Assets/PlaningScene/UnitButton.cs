﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class UnitButton : MonoBehaviour {

    Text Name;
    Image ClassIcon;
    Image UnitIcon;
    Text HP;
    Text AP;
    Text Level;
    Text Stats;
    Image Locked;

    // Use this for initialization
    void Awake()
    {
        Name = this.transform.Find("Name").GetComponent<Text>();
        ClassIcon = this.transform.Find("ClassIcon").GetComponent<Image>();
        UnitIcon = this.transform.Find("UnitIcon").GetComponent<Image>();
        Locked = this.transform.Find("Locked").GetComponent<Image>();
        HP = this.transform.Find("HP").GetComponent<Text>();
        AP = this.transform.Find("AP").GetComponent<Text>();
        Level = this.transform.Find("Lvl").GetComponent<Text>();
        Stats = this.transform.Find("Description").GetComponent<Text>();
    }

    public void Init(MovebleObject unit)
    {
        HP.text = unit.maxHits.ToString();
        AP.text = unit.maxAP.ToString();
        Level.text = unit.level.ToString();
        Name.text = unit.name;
        Stats.text = unit.GetBattleStatsDescr();
        Locked.gameObject.SetActive(unit.chosenInSquad || unit.fatigueFlag);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class UnitBlock : MonoBehaviour, IPointerClickHandler
{

    public int num;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            this.gameObject.transform.parent.GetComponent<SquadConstructionUI>().DeleteFromSquad(num);
        }
            
    }
    Text Name;
    Image ClassIcon;
    Image UnitIcon;
    Text HP;
    Text AP;
    Text Level;

    List<PassiveShow> passives = new List<PassiveShow>();

	// Use this for initialization
	void Awake () {
        Name = this.transform.Find("Name").GetComponent<Text>();
        ClassIcon = this.transform.Find("ClassIcon").GetComponent<Image>();
        UnitIcon = this.transform.Find("UnitIcon").GetComponent<Image>();
        HP = this.transform.Find("HP").GetComponent<Text>();
        AP = this.transform.Find("AP").GetComponent<Text>();
        Level = this.transform.Find("Level").GetComponent<Text>();

        PassiveShow[] passive = this.transform.GetComponentsInChildren<PassiveShow>();

        for(int i = 0; i < passive.Length; i++)
        {
            foreach (var j in passive)
            {
                if (j.gameObject.name == "Passive" + i.ToString())
                {
                    passives.Add(j);
                }
            }
        }
    }

    public void Init(MovebleObject unit)
    {
        if(unit == null)
        {
            Name.gameObject.SetActive(false);
            ClassIcon.gameObject.SetActive(false);
            UnitIcon.gameObject.SetActive(false);
            HP.gameObject.SetActive(false);
            AP.gameObject.SetActive(false);
            Level.gameObject.SetActive(false);
            foreach(var i in passives)
            {
                i.gameObject.SetActive(false);
            }
            return;
        }
        else
        {
            Name.gameObject.SetActive(true);
            ClassIcon.gameObject.SetActive(true);
            UnitIcon.gameObject.SetActive(true);
            HP.gameObject.SetActive(true);
            AP.gameObject.SetActive(true);
            Level.gameObject.SetActive(true);
            foreach (var i in passives)
            {
                i.gameObject.SetActive(true);
            }
        }
        HP.text = unit.curHits.ToString();
        AP.text = unit.curAP.ToString();
        Level.text = unit.level.ToString();
        for(int i = 0; i < unit.passive.Count; i++)
        {
            if(i >= passives.Count)
            {
                break;
            }
            passives[i].Init(unit.passive[i]);
        }
        for(int i = unit.passive.Count; i < passives.Count; i++)
        {
            passives[i].gameObject.SetActive(false);
        }
        Name.text = unit.name;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

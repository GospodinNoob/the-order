﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ProgressMenu : MonoBehaviour {

    int demonicInfluence;
    bool showMenu;
    int x, y;

    bool smallBand;
    bool massedBand;
    bool foothold;
    bool doomed;

    GameEvent chosenEvent;
    ArrayList smallEv, massEv, footHoldEv, doomedEv;

    int increasingDemonicInfluence;

    int language;

    int smallBandCounter;
    int massBandCounter;
    int footholdCounter;
    int doomedCounter;
    float time;
    public AudioSource au;

    // Use this for initialization
    void Start () {
        au.volume *= PlayerPrefs.GetFloat("Volume");
        int fontSize = 18;

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;

        smallEv = new ArrayList();
        massEv = new ArrayList();
        footHoldEv = new ArrayList();
        doomedEv = new ArrayList();

        smallBandCounter = PlayerPrefs.GetInt("SmallBand");
        massBandCounter = PlayerPrefs.GetInt("MassBand");
        footholdCounter = PlayerPrefs.GetInt("Foothold");
        doomedCounter = PlayerPrefs.GetInt("Doomed");

        time = PlayerPrefs.GetFloat("Time");

        language = PlayerPrefs.GetInt("Language");

        lHelp = new LoadHelpObject(language);

        increasingDemonicInfluence = 0;

        increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
            PlayerPrefs.GetInt("MassBand") * 5 +
            PlayerPrefs.GetInt("Foothold") * 10 +
            PlayerPrefs.GetInt("Doomed") * 15;

        chosenEvent = new GameEvent(0, language, demonicInfluence, time);

        for(int i = 0; i < smallBandCounter; i++)
        {
            smallEv.Add(new GameEvent("SmallBandEvent" + i.ToString(), language));
        }

        for (int i = 0; i < massBandCounter; i++)
        {
            massEv.Add(new GameEvent("MassBandEvent" + i.ToString(), language));
        }

        for (int i = 0; i < footholdCounter; i++)
        {
            footHoldEv.Add(new GameEvent("FootholdEvent" + i.ToString(), language));
        }

        for (int i = 0; i < doomedCounter; i++)
        {
            doomedEv.Add(new GameEvent("DoomedEvent" + i.ToString(), language));
        }

        demonicInfluence = PlayerPrefs.GetInt("DemonicInfluence");
        //Debug.Log(demonicInfluence);
        showMenu = false;
        x = Screen.width;
        y = Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Menu))
        {
            showMenu = !showMenu;
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenuScene");
        }
    }

    string[] progressText = { "Progress", "Прогресс" };
    string[] eventsText = { "Events", "События" };
    string[] barraksText = { "Barraks", "Казармы" };

    void MenuWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, 80, Screen.width / 2, Screen.height / 8), progressText[language]))
        {
            SceneManager.LoadScene("ProgressScene");
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8, Screen.width / 2, Screen.height / 8), eventsText[language]))
        {
            showMenu = false;
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8 * 2, Screen.width / 2, Screen.height / 8), barraksText[language]))
        {
            SceneManager.LoadScene("BarraksScene");
            showMenu = false;
        }
    }

    public GUISkin skin;
    public Texture2D backGroundTexture;
    public Texture2D dark;

    string[] endGameText = { "Demons took your world, resistance is futile", "Ваш мир обречён, сопротивление бесполезно..." };
    string[] almostEndGameText = { "Your world is doomed", "Ваш мир обречён..." };
    string[] footHoldText = { "Demons have created a foothold in your world", "Демоны закрепились в вашем мире" };
    string[] massBandText = { "The massed band of demons invaded our world", "Большие группы демонов разгуливают по вашему миру" };
    string[] normal = { "The demons invaded our world", "Демоны вторгаются в ваш мир" };

    void Progress(int WindowID)
    {
        string s;
        if(demonicInfluence >= 900)
        {
            s = almostEndGameText[language];
        }
        else
        {
            if (demonicInfluence >= 700)
            {
                s = footHoldText[language];
            }
            else
            {
                if (demonicInfluence >= 500)
                {
                    s = massBandText[language];
                }
                else
                {
                    s = normal[language];
                }
            }
        }

        GUI.Box(new Rect(0, 20, Screen.width, Screen.height / 8), s);
        GUI.Box(new Rect(0, Screen.height / 6, Screen.width, 30), demonicText[language] + " " + demonicInfluence + "/1000" + " +" + increasingDemonicInfluence.ToString());
        if (showMenu)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }

    string[] demonicText = { "Demonic influence", "Демоническое влияние" };

    string[] emptyEventListText = { "Empty event list", "Не выбрано событие" };
    string[] enemyUnitsText = { "Enemy units:", "Враги:" };
    string[] mapSizeText = { "Map size:", "Размер карты:" };
    string[] baseGroundCoverText = { "Base ground cover:", "Ландшафт:" };

    string[] fightText = { "Fight", "В бой" };


    void SpecialMission(int WindowID)
    {
        if (chosenEvent.eventId != 0)
        {
            //Debug.Log(enemyUnitsText[language] + " " + chosenEvent.enemyCounter.ToString() + '\n' + mapSizeText[language] + " " + chosenEvent.width.ToString() + "*" + chosenEvent.height.ToString());
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height / 2 - 2 * Screen.height / 8), enemyUnitsText[language] + " " + chosenEvent.enemyCounter.ToString() + '\n' + mapSizeText[language] + " " + chosenEvent.width.ToString() + "*" + chosenEvent.height.ToString() + '\n'
                + baseGroundCoverText[language] + " " + chosenEvent.baseGround[chosenEvent.baseGroundId - 1]);
        }
        else
        {
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height / 2), "Empty special mission");
        }
        if (showMenu)
        {
            // GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }

    void DescMission(int WindowID)
    {
        if (chosenEvent.eventId != 0)
        {
            //Debug.Log(enemyUnitsText[language] + " " + chosenEvent.enemyCounter.ToString() + '\n' + mapSizeText[language] + " " + chosenEvent.width.ToString() + "*" + chosenEvent.height.ToString());
            GUI.Box(new Rect(0, 0, Screen.width, Screen.height / 2 - 2 * Screen.height / 8), chosenEvent.desc);
        }
    }

    public GUIStyle box;
    public GUIStyle left;
    public GUIStyle right;

    int specialId;

    string[] smallBandText = { "Small band", "Группа рейдеров" };
    string[] massedBandText = { "Massed band", "Группа террора" };
    string[] footholdTextname = { "Foothold", "Плацдарм" };
    string[] doomedText = { "Doomed", "Армия демонов" };
    string[] missionBriffingText = { "Mission briefing", "Описание миссии" };

    string[] progress = { "Progress", "Прогресс" };

    public Texture2D smallTex, massTex, footholdTex, doomedTex, chosen;
    string[] descMissionText = { "Mission description", "Описание миссии" };

    

    int counter;

    void OnGUI()
    {
        GUI.skin = skin;
        if (!loadwindow)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backGroundTexture, ScaleMode.ScaleAndCrop);
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height / 4), Progress, progress[language]);
            //GUI.Box(new Rect(20, Screen.height / 6 + 30, Screen.width - 40, 30), "Demonic Influence " + demonicInfluence);
            if (smallBandCounter > 0)
            {
                if (GUI.Button(new Rect(0, Screen.height / 4, Screen.width / 4, Screen.width / 4), "", box))
                {
                    counter = 0;
                    chosenEvent = (GameEvent)smallEv[0];
                    specialId = 1;
                    num = 0;
                }
                GUI.DrawTexture(new Rect(0, Screen.height / 4, Screen.width / 4, Screen.width / 4), smallTex);
                GUI.Box(new Rect(Screen.width / 16 * 3, Screen.height / 4, Screen.width / 12, Screen.width / 12), smallBandCounter.ToString());
                if (specialId == 1)
                {
                    GUI.DrawTexture(new Rect(0, Screen.height / 4, Screen.width / 4, Screen.width / 4), chosen);
                }
            }
            if (massBandCounter > 0)
            {
                if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 4, Screen.width / 4), "", box))
                {
                    counter = 0;
                    chosenEvent = (GameEvent)massEv[0];
                    specialId = 2;
                    num = 0;
                }
                GUI.DrawTexture(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 4, Screen.width / 4), massTex);
                GUI.Box(new Rect(Screen.width / 4 + Screen.width / 16 * 3, Screen.height / 4, Screen.width / 12, Screen.width / 12), massBandCounter.ToString());
                if (specialId == 2)
                {
                    GUI.DrawTexture(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 4, Screen.width / 4), chosen);
                }
            }
            if (footholdCounter > 0)
            {

                if (GUI.Button(new Rect(Screen.width / 2, Screen.height / 4, Screen.width / 4, Screen.width / 4), "", box))
                {
                    counter = 0;
                    chosenEvent = (GameEvent)footHoldEv[0];
                    specialId = 3;
                    num = 0;
                }
                GUI.DrawTexture(new Rect(Screen.width / 4 * 2, Screen.height / 4, Screen.width / 4, Screen.width / 4), footholdTex);
                GUI.Box(new Rect(Screen.width / 2 + Screen.width / 16 * 3, Screen.height / 4, Screen.width / 12, Screen.width / 12), footholdCounter.ToString());
                if (specialId == 3)
                {
                    GUI.DrawTexture(new Rect(Screen.width / 4 * 2, Screen.height / 4, Screen.width / 4, Screen.width / 4), chosen);
                }
            }
            if (doomedCounter > 0)
            {
                //GUI.Box(new Rect(Screen.width / 4 * 3 + Screen.width / 16 * 3, Screen.height / 4, Screen.width / 16, Screen.width / 16), doomedCounter.ToString());
                if (GUI.Button(new Rect(Screen.width / 4 * 3, Screen.height / 4, Screen.width / 4, Screen.width / 4), "", box))
                {
                    counter = 0;
                    chosenEvent = (GameEvent)doomedEv[0];
                    specialId = 4;
                    num = 0;
                }
                GUI.DrawTexture(new Rect(Screen.width / 4 * 3, Screen.height / 4, Screen.width / 4, Screen.width / 4), doomedTex);
                GUI.Box(new Rect(Screen.width / 4 * 3 + Screen.width / 16 * 3, Screen.height / 4, Screen.width / 12, Screen.width / 12), doomedCounter.ToString());
                if (specialId == 4)
                {
                    GUI.DrawTexture(new Rect(Screen.width / 4 * 3, Screen.height / 4, Screen.width / 4, Screen.width / 4), chosen);
                }
            }
            //Debug.Log(chosenEvent.eventId);
            if (chosenEvent.eventId != 0)
            {
                string st = "";
                if (GUI.Button(new Rect(0, Screen.height / 2, Screen.width / 12, Screen.height / 8), "", left))
                {
                    counter--;
                    counter = Mathf.Max(0, counter);
                }
                if (specialId == 1)
                {
                    st = smallBandText[language];
                    if (GUI.Button(new Rect(Screen.width - Screen.width / 12, Screen.height / 2, Screen.width / 12, Screen.height / 8), "", right))
                    {
                        counter++;
                        counter = Mathf.Min(counter, smallBandCounter);
                        num = counter;
                    }
                    for (int i = counter; i < Mathf.Min(counter + 5, smallBandCounter); i++)
                    {
                        if (GUI.Button(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), "", box))
                        {
                            //Debug.Log(counter);
                            chosenEvent = ((GameEvent)smallEv[i]);
                            num = i;
                        }
                        GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), smallTex);
                        if (i == num)
                        {
                            GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), chosen);
                        }
                    }
                }
                if (specialId == 2)
                {
                    st = massedBandText[language];
                    if (GUI.Button(new Rect(Screen.width - Screen.width / 12, Screen.height / 2, Screen.width / 12, Screen.height / 8), "", right))
                    {
                        counter++;
                        counter = Mathf.Min(counter, massBandCounter);
                        num = counter;
                    }
                    for (int i = counter; i < Mathf.Min(counter + 5, massBandCounter); i++)
                    {
                        if (GUI.Button(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), "", box))
                        {
                            chosenEvent = ((GameEvent)massEv[i]);
                            num = i;
                        }
                        GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), massTex);
                        if (i == num)
                        {
                            GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), chosen);
                        }
                    }
                }
                if (specialId == 3)
                {
                    st = footholdTextname[language];
                    if (GUI.Button(new Rect(Screen.width - Screen.width / 12, Screen.height / 2, Screen.width / 12, Screen.height / 8), "", right))
                    {
                        counter++;
                        counter = Mathf.Min(counter, footholdCounter);
                        num = counter;
                    }
                    for (int i = counter; i < Mathf.Min(counter + 5, footholdCounter); i++)
                    {
                        if (GUI.Button(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), "", box))
                        {
                            chosenEvent = ((GameEvent)footHoldEv[i]);
                            num = i;
                        }
                        GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), footholdTex);
                        if (i == num)
                        {
                            GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), chosen);
                        }
                    }
                }
                if (specialId == 4)
                {
                    st = doomedText[language];
                    if (GUI.Button(new Rect(Screen.width - Screen.width / 12, Screen.height / 2, Screen.width / 12, Screen.height / 8), "", right))
                    {
                        counter++;
                        counter = Mathf.Min(counter, doomedCounter);
                    }
                    for (int i = counter; i < Mathf.Min(counter + 5, doomedCounter); i++)
                    {
                        if (GUI.Button(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), "", box))
                        {
                            chosenEvent = ((GameEvent)doomedEv[i]);
                        }
                        GUI.DrawTexture(new Rect(Screen.width / 12 + Screen.width / 6 * (i - counter), Screen.height / 2, Screen.width / 6, Screen.height / 8), doomedTex);
                    }
                }
                GUI.Window(2, new Rect(0, Screen.height / 2 + Screen.height / 8, Screen.width/ 2, Screen.height / 2 - Screen.height / 8 * 2), SpecialMission, st);
                GUI.Window(3, new Rect(Screen.width / 2, Screen.height / 2 + Screen.height / 8, Screen.width / 2, Screen.height / 2 - Screen.height / 8 * 2), DescMission, descMissionText[language]);

                if (chosenEvent.eventId != 0)
                {
                    if (GUI.Button(new Rect(Screen.width / 2, Screen.height - Screen.height / 8, Screen.width / 2, Screen.height / 8), fightText[language]))
                    {
                        PlayerPrefs.SetInt("SpecialID", specialId);
                        if ((specialId == 3) || (specialId == 4))
                        {
                            PlayerPrefs.SetInt("Boss", 1);
                        }
                        chosenEvent.Save("ChosenEvent");
                        SceneManager.LoadSceneAsync("PlaningScene");
                        loadwindow = true;
                    }
                }
            }
            if (GUI.Button(new Rect(0, Screen.height - Screen.height / 8, Screen.width / 2, Screen.height / 8), backText[language]))
            {
                SceneManager.LoadSceneAsync("EventScene");
                loadwindow = true;
            }
        }
        else
        {
            GUI.Window(2, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
    }

    string[] loadText = {"Loading", "Загрузка" };

    int num;
    bool loadwindow;
    LoadHelpObject lHelp;

    void LoadWindow(int WindowID)
    {
        int fontSize = 25;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    string[] backText = { "Back", "Назад" };
    string[] menuText = { "Menu", "Меню" };
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Damage {

    public string keyName;
    public int piercingDamage;
    public int slashingDamage;
    public int crushingDamage;
    public int axingDamage;
    public int fireDamage, frostDamage, shockDamage;

    public Damage()
    {
        piercingDamage = 0;
        slashingDamage = 0;
        crushingDamage = 0;
        axingDamage = 0;
        fireDamage = 0;
        frostDamage = 0;
        shockDamage = 0;
    }

    public void Clear()
    {
        piercingDamage = 0;
        slashingDamage = 0;
        crushingDamage = 0;
        axingDamage = 0;
        fireDamage = 0;
        frostDamage = 0;
        shockDamage = 0;
    }

    public Damage(Damage d)
    {
        keyName = d.keyName;
        piercingDamage = d.piercingDamage;
        slashingDamage = d.slashingDamage;
        crushingDamage = d.crushingDamage;
        axingDamage = d.axingDamage;
        fireDamage = d.fireDamage;
        frostDamage = d.frostDamage;
        shockDamage = d.shockDamage;
    }

    public Damage(Damage d, float coef)
    {
        keyName = d.keyName;
        piercingDamage = (int)(d.piercingDamage * coef);
        slashingDamage = (int)(d.slashingDamage * coef);
        crushingDamage = (int)(d.crushingDamage * coef);
        axingDamage = (int)(d.axingDamage * coef);
        fireDamage = (int)(d.fireDamage * coef);
        frostDamage = (int)(d.frostDamage * coef);
        shockDamage = (int)(d.shockDamage * coef);
    }

    public Damage(Damage dmg1, Damage dmg2)
    {
        piercingDamage = dmg1.piercingDamage + dmg2.piercingDamage;
        slashingDamage = dmg1.slashingDamage + dmg2.slashingDamage;
        crushingDamage = dmg2.crushingDamage + dmg1.crushingDamage;
        axingDamage = dmg1.axingDamage + dmg2.axingDamage;
        fireDamage = dmg1.fireDamage + dmg2.fireDamage;
        frostDamage = dmg1.frostDamage + dmg2.frostDamage;
        shockDamage = dmg1.shockDamage + dmg2.shockDamage;
    }

    public int GetSum()
    {
        return piercingDamage
            + slashingDamage
            + crushingDamage
            + axingDamage
            + shockDamage
            + fireDamage
            + frostDamage;
    }

    public void Add(Damage d)
    {
        if(d == null)
        {
            return;
        }
        piercingDamage += d.piercingDamage;
        slashingDamage += d.slashingDamage;
        crushingDamage += d.crushingDamage;
        axingDamage += d.axingDamage;
        fireDamage += d.fireDamage;
        frostDamage += d.frostDamage;
        shockDamage += d.shockDamage;
    }

    public Damage(Damage d, ArmourValue a)
    {
        if(d == null)
        {
            d = new Damage();
        }
        piercingDamage = Mathf.Max(0, d.piercingDamage - a.piercingArmour);
        slashingDamage = Mathf.Max(0, d.slashingDamage - a.slashingArmour);
        axingDamage = Mathf.Max(0, d.axingDamage - a.axingArmour);
        crushingDamage = Mathf.Max(0, d.crushingDamage - a.crushingArmour);
        fireDamage = Mathf.Max(0, d.fireDamage - a.fireArmour);
        frostDamage = Mathf.Max(0, d.frostDamage - a.frostArmour);
        shockDamage = Mathf.Max(0, d.shockDamage - a.shockArmour);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EstimateDamageType{
    E,
    ABS
}

public class EstimateDamage{

    public int damage;
    public int potentialDamage;
    public int Edamage;
    public float hit;
    public float crit;
    public int activationCost;
    int cou;

    List<Passive> passive = new List<Passive>();

    public void ReduceToCurhealth(int targetHealth)
    {
        potentialDamage = damage;
        damage = Mathf.Min(damage, targetHealth);
        Edamage = (int)(hit * damage * (1 + crit));
    }
    
    public bool GreaterThan(EstimateDamage e, EstimateDamageType type)
    {
        if(type == EstimateDamageType.E)
        {
            return Edamage > e.Edamage;
        }
        if(type == EstimateDamageType.ABS)
        {
            if(damage > e.damage)
            {
                return true;
            }
            else
            {
                if(potentialDamage > e.potentialDamage)
                {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public EstimateDamage()
    {
        damage = 0;
        Edamage = 0;
        hit = 0;
        crit = 0;
        cou = 0;
        activationCost = 0;
    }

    public EstimateDamage(Damage d, MovebleObject parent, MovebleObject attacker)
    {
        Edamage = 0;
        hit = 0;
        crit = 0;
        cou = 0;
        activationCost = 0;
        damage = d.piercingDamage + d.shockDamage + d.slashingDamage + d.fireDamage + d.crushingDamage + d.axingDamage + d.frostDamage;
        damage -= parent.reducedDamage;
        if(damage > 0)
        {
            damage += attacker.bonusDamage;
        }
        damage = Mathf.Max(0, damage);
        Edamage = (int)(hit * damage * (1 + crit));
    }

    public EstimateDamage(Active act, ArmourValue a, float damageCoef, MovebleObject parent, MovebleObject attacker)
    {
        damage = 0;
        Edamage = 0;
        hit = 0;
        crit = 0;
        cou = 0;
        activationCost = act.activationCost;
        Add(new EstimateDamage(new Damage(new Damage(act.damageAdd, a), damageCoef), parent, attacker));
        foreach(var i in act.actives)
        {
            Add(new EstimateDamage(new Damage(new Damage(i.damageAdd, a), damageCoef), parent, attacker));
        }
    }

    public void Add(EstimateDamage e)
    {
        damage += e.damage;
        Edamage += e.Edamage;
        hit = (hit * cou + e.hit) / (cou + 1f);
        crit = (crit * cou + e.crit) / (cou + 1f);
        activationCost += e.activationCost;
    }
}

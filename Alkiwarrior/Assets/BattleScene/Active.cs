﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Active{

    public string keyName;
    public string perkReq;
    public string[] targetTypes;
    public int healthRestore;
    public float range;
    public string[] attackTypes;
    public string type;
    public string[] passiveAbilities;
    public string damage;
    public string armour;
    public float bashIfHit;
    public float rightExtraAim;
    public float leftExtraAim;
    public int activationCost;
    public int cooldown;
    public int useCounter;
    public string animation;
    public string image;
    public bool isList;
    public bool combination;

    StaticData staticData;
    public Damage damageAdd;
    public ArmourValue armourAdd;

    public List<Active> actives = new List<Active>();
    public List<Passive> passives = new List<Passive>();

    public void Update(MovebleObject parent)
    {
        foreach (var i in parent.passive)
        {
            if(staticData.ArrayHasTag(i.targetTypes, type))
            {
                if((damageAdd != null) && (i.damageAdd != null))
                {
                    damageAdd.Add(i.damageAdd);
                }
            }
        }
        foreach (var i in actives)
        {
            i.Update(parent);
        }
    }

    public void ActiveAbility(MovebleObject parent, MovebleObject move, float coef)
    {
        //Debug.Log(keyName);

        if (staticData.ArrayHasTag(targetTypes, "FRIENDLY"))
        {
            if (parent.team != move.team)
            {
                return;
            }
        }
        if (staticData.ArrayHasTag(targetTypes, "ENEMY"))
        {
            if (parent.team == move.team)
            {
                return;
            }
        }
        if (damageAdd != null)
        {
            //Debug.Log(damage);
            move.DealDamage(parent, new Damage(damageAdd, coef));
        }

        foreach (var i in passives)
        {
            Debug.Log(i.keyName + " Add");
            move.passive.Add(new Passive(i));
        }
        move.RecalcBattleParams();
        if (this.actives.Count == 0)
        {
            return;
        }
        foreach (var i in actives)
        {
            i.ActiveAbility(parent, move, coef);
        }
    }

    public Active(Active a)
    {
        keyName = a.keyName;
        type = a.type;
        perkReq = a.perkReq;
        targetTypes = a.targetTypes;
        healthRestore = a.healthRestore;
        range = a.range;
        attackTypes = a.attackTypes;
        damage = a.damage;
        armour = a.armour;
        bashIfHit = a.bashIfHit;
        rightExtraAim = a.rightExtraAim;
        leftExtraAim = a.leftExtraAim;
        activationCost = a.activationCost;
        cooldown = a.cooldown;
        useCounter = a.useCounter;
        animation = a.animation;
        image = a.image;
        passiveAbilities = a.passiveAbilities;
        combination = a.combination;
        Init();
        foreach(var i in a.actives)
        {
            actives.Add(new Active(i));
        }
    }

    public List<Active> GetAbilityList()
    {
        List<Active> ans = new List<Active>();
        //Debug.Log(keyName + actives.Count);
        if(actives.Count > 0)
        {
            foreach(var i in actives)
            {
                //List<Active> buf = i.GetAbilityList();
                //foreach(var j in buf)
                //{
                    ans.Add(i);
                //}
            }
        }
        else
        {
            ans.Add(this);
        }
        return ans;
    }

    public Active(Active a, Active b, UnitBase ba)
    {
        actives.Add(new Active(a));
        actives.Add(new Active(b));
        keyName = a.keyName + b.keyName;
        image = "DOUBLEATTACK";
        attackTypes = new string[1];
        attackTypes[0] = "WEAPON";
        animation = ba.animList[6];
        //Debug.Log(a.keyName + " " + a.range);
        //Debug.Log(b.keyName + " " + b.range);
        targetTypes = new string[a.targetTypes.Length + b.targetTypes.Length];
        for(int i = 0; i < a.targetTypes.Length; i++)
        {
            targetTypes[i] = a.targetTypes[i];
        }
        for (int i = 0; i < b.targetTypes.Length; i++)
        {
            targetTypes[i + a.targetTypes.Length] = a.targetTypes[i];
        }
        range = Mathf.Min(a.range, b.range);
        activationCost = Mathf.Max(1, a.activationCost + b.activationCost - 1);
        actives[0].activationCost = 0;
        actives[1].activationCost = 0;
        combination = true;
    }

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        damageAdd = null;
        armourAdd = null;
        if (!combination)
        {
            if (damage != "-")
            {
                damageAdd = new Damage(staticData.GetDamageByKeyName(damage));
            }
            if (armour != "-")
            {
                armourAdd = new ArmourValue(staticData.GetArmourValueByKeyName(armour));
            }
            foreach (var i in passiveAbilities)
            {
                if (i.Length > 2)
                {
                    passives.Add(new Passive(staticData.GetPassiveByKeyName(i)));
                }
            }
        }
    }

}

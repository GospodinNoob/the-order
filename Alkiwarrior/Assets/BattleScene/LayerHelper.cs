﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerHelper : MonoBehaviour {

    public int idLayer;
    public bool blocked;

	// Use this for initialization
	void Start () {
        if (!blocked)
        {
            SpriteRenderer[] ar = this.transform.GetComponentsInChildren<SpriteRenderer>();
            foreach (var i in ar)
            {
                i.sortingOrder = idLayer;
            }
        }
	}
	
	public void ActiveSprite(string keyName)
    {
        //Debug.Log(keyName);
        SpriteRenderer[] ar = this.transform.GetComponentsInChildren<SpriteRenderer>();
        foreach (var i in ar)
        {
            i.gameObject.SetActive(i.gameObject.name == keyName);
            i.sortingOrder = idLayer;
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class Effect {

    public int id;
    public float value;
    public int turns;
    public bool valueble;

    int lang;

    string[] effectNames;
    string[] rusNames = { "Оглушён", "Огненная смазка", "Морозная смазка", "Шоковая смазка", "Замедление", "Огонь", "Обморожение", "Шок" };
    string[] engNames = { "Stunned", "Fire oil", "Frost oil", "Shock oil", "Slowed", "Fire", "Frostite", "Shock" };

    public string name;

    void SetLang()
    {
        // Debug.Log(lang);
        if (lang == 0)
        {
            effectNames = engNames;
        }
        if (lang == 1)
        {
            effectNames = rusNames;
        }
    }

    public Effect(int a, int b, int la)
    {
        this.id = a;
        SetLang();
        this.name = effectNames[a - 1];
        this.turns = b;
        lang = la;
        valueble = false;
    }

    public Effect(int a, float val, int t, int la)
    {
        this.id = a;
        SetLang();
        this.name = effectNames[a - 1];
        this.turns = t;
        lang = la;
        value = val;
        if (a == 6)
        {
            valueble = true;
        }
    }

    public void AddEffect(int c)
    {
        this.turns += c;
    }

    public void Turn()
    {
        this.turns--;
    }

    public void PassiveActive(MovebleObject move)
    {
        if (this.id == 2)
        {
         //   move.extraDmg.fireDamage += 2;
        }
        if (id == 3)
        {
           // move.extraDmg.frostDamage += 2;
        }
        if (id == 4)
        {
            //move.extraDmg.shockDamage += 2;
        }
        if (id == 5)
        {
            move.slowedValue += value;
        }
        if (id == 6)
        {
           // move.extraArmour.frostArmour += 2;
        }
        if (id == 7)
        {
          //  move.extraArmour.fireArmour += 2;
          //  move.extraArmour.piercingArmour += 1;
          //  move.extraArmour.slashingArmour += 2;
            move.frozen = true;
        }
        if (id == 8)
        {
            move.shocked = true;
            move.shockAim = -15;
            move.shockDodge = -15;
        }
    }

    public void Active(MovebleObject move)
    {
        if (this.id == 1)
        {
            int a = Mathf.Min(turns, move.curAP);
            move.curAP -= a;
            turns -= a;
        }
        if (id == 6)
        {
         //   Damage dmg = new Damage(0, lang);
          //  dmg.fireDamage = Random.Range(1, 3);
            move.fired = true;
          //  move.DealDamage(dmg);
        }
    }

    public void PassiveActive(Object move)
    {
        if (id == 6)
        {
            move.fired = true;
     //       move.extraArmour.frostArmour += 2;
        }
        if (id == 7)
        {
       //     move.extraArmour.fireArmour += 2;
        //    move.extraArmour.piercingArmour += 1;
         //   move.extraArmour.slashingArmour += 2;
            move.frozen = true;
        }
    }

    public void Active(Object move)
    {
        if (id == 6)
        {
        //    Damage dmg = new Damage(0, lang);
        //    dmg.fireDamage = Random.Range(1, 3);
            move.fired = true;
        //    move.DealDamage(dmg);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpAI : AI {

    public override PointXY GetTarget(Active act, MovebleObject attacker, MapTemplate map, PointXY pos)
    {
        MovebleObject target = null;
        int minusDmg = -1;
        float hit = -1;
        bool kill = false;
        EstimateDamage est;
        EstimateDamage global = new EstimateDamage();
        List<MovebleObject> targets = map.RecalcTargets(attacker, pos, act);
        if(targets.Count == 0)
        {
            return null;
        }
        foreach (var i in targets)
        {
            PointXY aRot = new PointXY(i.point.x - attacker.point.x, i.point.y - attacker.point.y);
            est = i.EstimateDamage(aRot, i.rotation, attacker, act);
            est.ReduceToCurhealth(i.curHits);
            if (kill)
            {
                if (est.damage == i.curHits)
                {
                    if (est.hit > hit)
                    {
                        target = i;
                        minusDmg = est.damage;
                        hit = est.hit;
                        global = est;
                    }
                }
            }
            else
            {
                if (est.damage == i.curHits)
                {
                    if (est.hit * est.hit * 3 > hit * minusDmg)
                    {
                        target = i;
                        minusDmg = est.damage;
                        hit = est.hit;
                        kill = true;
                        global = est;
                    }
                }
                else
                {
                    if (est.damage * est.hit > minusDmg * hit)
                    {
                        target = i;
                        minusDmg = est.damage;
                        hit = est.hit;
                        kill = false;
                        global = est;
                    }
                }
            }
        }
        PointXY ans = new PointXY(target.point.x, target.point.y);
        ans.est = global;
        ans.abilityKeyName = act.keyName;
        return ans;
    }

    public override PointXY GetBestPoint(MovebleObject attacker, MapTemplate map)
    {
        int INF = 10000000;
        PointXY ans = new PointXY(0, 0);
        ans.abilityKeyName = "MOVE";
        List<List<float>> costMatrix = new List<List<float>>();
        for (int i = 0; i < map.dist.Count; i++)
        {
            costMatrix.Add(new List<float>());
            for (int j = 0; j < map.dist[0].Count; j++)
            {
                if (Mathf.CeilToInt(map.dist[i][j] / attacker.GetSpeed()) > attacker.curAP)
                {
                    costMatrix[i].Add(-INF);
                }
                else
                {
                    costMatrix[i].Add(0);
                }
            }
        }
        if ((attacker.curAP == 1) && (attacker.lastAttack))
        {
            float value = -INF;
            for (int i = 0; i < map.dist.Count; i++)
            {
                for (int j = 0; j < map.dist[0].Count; j++)
                {
                    foreach(var un in map.units)
                    {
                        if ((un.isAlive()) && (un.team != attacker.team))
                        {
                            costMatrix[i][j] += Mathf.Sqrt((un.point.x - i) * (un.point.x - i) + (un.point.y - j) * (un.point.y - j));
                        }
                    }
                    if (costMatrix[i][j] > value)
                    {
                        ans = new PointXY(i, j);
                        ans.abilityKeyName = "MOVE";
                        value = costMatrix[i][j];
                    }
                }
            }
            //Debug.Log(attacker.curAP);
            return ans;
        }
        EstimateDamage max = new EstimateDamage();
        Active active = null;
        List<Active> abilities = attacker.GetLinkToReadyAbiitiesWithAttackTag("WEAPON");
        //Debug.Log("count " + abilities.Count);
        foreach (var i in abilities)
        {
            for (int j = 0; j < map.dist.Count; j++)
            {
                for (int k = 0; k < map.dist[0].Count; k++)
                {
                    if (Mathf.CeilToInt(map.dist[j][k] / attacker.GetSpeed()) < attacker.curAP)
                    {
                        PointXY targetPoint = GetTarget(i, attacker, map, new PointXY(j, k));
                        if (targetPoint != null)
                        {
                            //Debug.Log(i.keyName + " " + targetPoint.est.damage + " " + max.damage + " " + targetPoint.est.GreaterThan(max, EstimateDamageType.ABS));
                            if (targetPoint.est.GreaterThan(max, EstimateDamageType.ABS))
                            {
                                max = targetPoint.est;
                                ans = new PointXY(j, k);
                                ans.abilityKeyName = i.keyName;
                            }
                            else
                            {
                                if(targetPoint.est.damage == max.damage)
                                {
                                    int a = Mathf.CeilToInt(map.dist[ans.x][ans.y] / attacker.GetSpeed());
                                    int b = Mathf.CeilToInt(map.dist[targetPoint.x][targetPoint.y] / attacker.GetSpeed());
                                    if(b < a)
                                    {
                                        max = targetPoint.est;
                                        ans = new PointXY(j, k);
                                        ans.abilityKeyName = i.keyName;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return ans;
            /*
        for (int j = 0; j < map.dist.Count; j++)
        {
            for (int k = 0; k < map.dist[0].Count; k++)
            {
                PointXY target = GetTarget(, new PointXY(j, k));
                if (target != null)
                {
                    estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                    estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                    //  Debug.Log(map);
                    if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                    {
                        //Debug.Log(j + " " + k);
                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP)
                        {
                            if (estHelp.realD * (estHelp.realH) > est.realD * (est.realH))
                            {
                                //Debug.Log(j + " " + k);
                                tfMI = true;
                                p2 = new PointXY(j, k);
                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                est = new PointXY(estHelp);
                            }
                        }
                    }
                }
            }
        }
        /*
        if (tfMI)
        {
            if (enemyUn.UseAbility(Ability.DoubleStrike))
            {
                p2.ability = Ability.DoubleStrike;
            }
            return p2;
        }

        if (!tfMI)
        {
            if (enemyUn.curAP > 1)
            {
                est = new PointXY(-1, -1);
                int cost = 10000;
                bool tf = false;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                        Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                        estHelp = new PointXY(0, 0);
                        if (target != null)
                        {
                            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(target.point, new PointXY(j, k))));
                            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                            if (map[j][k].movebleObject.isEmpty() && map[j][k].obj.isEmpty())
                            {
                                if (p.x < 0)
                                {
                                    tf = true;
                                    tfMI = true;
                                    p = new PointXY(j, k);
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                                else
                                {
                                    if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                    {
                                        tf = true;
                                        tfMI = true;
                                        p = new PointXY(j, k);
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                        est = new PointXY(estHelp);
                                    }
                                    else
                                    {
                                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) == Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                        {
                                            if (estHelp.realD * (estHelp.realH) > est.realD * (est.realH))
                                            {
                                                tf = true;
                                                tfMI = true;
                                                p = new PointXY(j, k);
                                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                                est = new PointXY(estHelp);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                if (!tf)
                {
                    return null;
                }
                if (enemyUn.UseAbility(Ability.DoubleStrike))
                {
                    p2.ability = Ability.DoubleStrike;
                }
                return p;
            }
        }*/
    }   
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Messenger : MonoBehaviour {

    public GameObject textGO;

    ArrayList messadges;

    float startTimer;

    float dx, dy;

    GameObject canvas;

    public void ShowMessage(string text)
    {
        if (messadges.Count == 0)
        {
            startTimer = 0.6f + Time.time;
        }
        messadges.Add(text);
    }

    void Show()
    {
        string text = (string)messadges[0];
        //Vector3 pos = new Vector3(this.transform.position.x, this.transform.position.y, -0.5f);
        GameObject newText = (GameObject)GameObject.Instantiate(textGO, this.transform.position, this.transform.rotation);
        //newText.transform.parent = canvas.transform;
        //newText.transform.position = pos;
        //newText.transform.position = new Vector3(newText.transform.position.x, newText.transform.position.y, -0.5f);
        newText.transform.rotation = new Quaternion(0, 0, 0, 1);
        newText.GetComponentInChildren<Text>().text = text;
        //float scale = newText.transform.localScale.x * dx;
        //newText.transform.localScale = new Vector3(scale, scale, scale);
        newText.GetComponent<Rigidbody2D>().velocity = new Vector3(0, 25, 0);
        Destroy(newText, 3);
    }

    void Awake()
    {
        canvas = GameObject.Find("Canvas");
        messadges = new ArrayList();
    }

	// Use this for initialization
	void Start () {
        dx = Screen.width / 6;
        dy = Screen.height / 8;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }
        startTimer = -1;
    }
	
	// Update is called once per frame
	void Update () {
	    if ((messadges.Count > 0) && (Time.time - startTimer > 0.6f))
        {
            Show();
            messadges.Remove(messadges[0]);
            startTimer = Time.time;
        }
	}
}

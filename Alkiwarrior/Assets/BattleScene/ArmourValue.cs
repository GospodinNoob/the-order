﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArmourValue {

    public string keyName;
    public int piercingArmour;
    public int slashingArmour;
    public int crushingArmour;
    public int axingArmour;
    public int fireArmour;
    public int frostArmour;
    public int shockArmour;
    public bool vulnerability;

    public void Add(ArmourValue a)
    {
        if(a == null)
        {
            return;
        }
        if (a.vulnerability)
        {
            piercingArmour += a.piercingArmour;
            slashingArmour += a.slashingArmour;
            crushingArmour += a.crushingArmour;
            axingArmour += a.axingArmour;
            fireArmour += a.fireArmour;
            frostArmour += a.frostArmour;
            shockArmour += a.shockArmour;
        }
        else
        {
            piercingArmour = Mathf.Max(piercingArmour +  a.piercingArmour);
            slashingArmour = Mathf.Max(slashingArmour + a.slashingArmour);
            crushingArmour = Mathf.Max(crushingArmour + a.crushingArmour);
            axingArmour = Mathf.Max(axingArmour + a.axingArmour);
            fireArmour = Mathf.Max(fireArmour + a.fireArmour);
            frostArmour = Mathf.Max(frostArmour + a.frostArmour);
            shockArmour = Mathf.Max(shockArmour + a.shockArmour);
        }
    }

    public ArmourValue()
    {
        piercingArmour = 0;
        slashingArmour = 0;
        crushingArmour = 0;
        axingArmour = 0;
        fireArmour = 0;
        frostArmour = 0;
        shockArmour = 0;
        vulnerability = false;
    }

    public void Clear()
    {
        piercingArmour = 0;
        slashingArmour = 0;
        crushingArmour = 0;
        axingArmour = 0;
        fireArmour = 0;
        frostArmour = 0;
        shockArmour = 0;
        vulnerability = false;
    }

    public ArmourValue(ArmourValue a, ArmourValue b)
    {
        piercingArmour = 0;
        slashingArmour = 0;
        crushingArmour = 0;
        axingArmour = 0;
        fireArmour = 0;
        frostArmour = 0;
        shockArmour = 0;
        vulnerability = false;
        Add(a);
        Add(b);
    }

    public ArmourValue(ArmourValue arm)
    {
        keyName = arm.keyName;
        piercingArmour = arm.piercingArmour;
        slashingArmour = arm.slashingArmour;
        crushingArmour = arm.crushingArmour;
        axingArmour = arm.axingArmour;
        fireArmour = arm.fireArmour;
        frostArmour = arm.frostArmour;
        shockArmour = arm.frostArmour;
        vulnerability = arm.vulnerability;
    }
}

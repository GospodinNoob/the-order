﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitMove : MonoBehaviour {

    List<PointXY> newWay;
    GameObject unit;
    GameObject animatorGO;
    public GameObject camera;
    public GameObject main;

    bool flag;
    bool attack;

    float attackTimer;

	// Use this for initialization
	void Start () {
        flag = false;
        attack = false;
        attackTimer = Time.time;
        newWay = new List<PointXY>();
	}

    int segment = 0;
    float timer;

    public void SetMain(GameObject go, GameObject cam, MovebleObject un)
    {
        main = go;
        camera = cam;
        unitObject = un;
        unit = this.gameObject;
        animatorGO = this.gameObject.GetComponent<HelperScript>().GetRenderGO();
    }

    GameObject enemy;

    float lenAttack;

    int animId;

    public void SetAttack(int vx, int vy, GameObject en, float len)
    {
        attack = true;
        //Debug.Log(un.unit.name + " " + go.name);
        //animatorGO = go.GetComponent<HelperScript>().GetRenderGO();
        attackTimer = Time.time;
        Rotation(vx, vy);
        wait = true;
        enemy = en;
        lenAttack = len;
        var a = 5;
        if ((lenAttack > 1.5) && (unitObject.rightArm.handStatus != 3))
        {
            a = 6;
        }
        animId = a;
    }

    public void SetAbility(int vx, int vy, GameObject en, Ability abilityId)
    {
        attack = true;
        //Debug.Log(un.unit.name + " " + go.name);
        //animatorGO = go.GetComponent<HelperScript>().GetRenderGO();
        attackTimer = Time.time;
        Rotation(vx, vy);
        wait = true;
        enemy = en;
        lenAttack = len;
        int val = 0;
        switch (abilityId)
        {
            case Ability.Attack:
                val = -1;
                break;
            case Ability.ShieldBash:
                val = 0;
                break;
            case Ability.Cleave:
                val = 1;
                break;
            case Ability.DoubleStrike:
                val = 2;
                break;
            case Ability.CurvedTrajectoryShot:
                val = 3;
                break;
        }
        animId = 6 + val + 1;
    }

    float cameraTimer;
    float cameraSpeed;
    float startCameraMove;
    bool cameraMove;

    float dx;

    public void SetDx(float a)
    {
        dx = a;
    }

    void Rotation(int vx, int vy)
    {
        float angle = Mathf.Atan2(vx, -vy);
        angle *= 57.2958f;
        this.gameObject.GetComponent<HelperScript>().renderGO.gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        //Debug.Log(angle);
    }

    MovebleObject unitObject;

    public void SetWay(List<PointXY> way)
    {
        newWay = new List<PointXY>();
        for (int i = way.Count - 1; i >= 0; i--)
        {
            newWay.Add(way[i]);
        }
        segment = 1;
        timer = 0;
        flag = true;
        musicChange = false;
        
    }

    bool damageFlag;
    float damageDelay;

    Unit attacker;
    PointXY pTarget;

    public float GetDelay(Unit m, PointXY p)
    {
        return 0;// animatorGO.GetComponent<AnimatorControllerHelp>().GetDealDamageDelay(new PointXY(m.point, p));
    }

    public void DamageTakenDelay(Unit m, PointXY p, float delay)
    {
        //float delay = GetDelay(m, p); //animatorGO.GetComponent<AnimatorControllerHelp>().GetDealDamageDelay(new PointXY(m.point, p));
        damageFlag = true;
        damageDelay = Time.time + delay;
        attacker = m;
        pTarget = new PointXY(p);
    }

    float len;

    int ChooseSoundStep(PointXY p)
    {
        return main.GetComponent<BattleSceneUI>().GetGround(p);
    }

    bool musicChange;
    Vector2 speed;
    bool wait;
	
	// Update is called once per frame
	void Update () {
        if (damageFlag)
        {
            if (Time.time > damageDelay)
            {
                damageFlag = false;
                //main.GetComponent<BattleSceneUI>().DealDamage(attacker, pTarget.x, pTarget.y);
            }
        }
        if (flag)
        {
            //Debug.Log(camera);
            Vector3 newPosition = new Vector3(unit.gameObject.transform.position.x, unit.gameObject.transform.position.y, camera.gameObject.transform.position.z);
          //  if (unitObject.pla)
            {
                camera.transform.position = newPosition;
            }
        }
	    if (flag)
        {
            //Debug.Log(newWay.Count);
            PointXY a = (PointXY)newWay[segment - 1];
            PointXY b = new PointXY(0, 0);
            if (segment < newWay.Count)
            {
                b = (PointXY)newWay[segment];
            }
            if (segment < newWay.Count)
            {
                if (Time.time - timer > len)
                {
                    this.gameObject.GetComponent<AudioGround>().SetActive(ChooseSoundStep(b));
                    musicChange = false;
                    //Transform tr = this.gameObject.transform;
                    //Debug.Log((b.x - a.x).ToString() + " " + (b.y - a.y).ToString());
                    //unitObject.SetRotation(new PointXY(b.x - a.x, b.y - a.y));
                    //Debug.Log(unitObject.rotation.x.ToString() + " " + unitObject.rotation.y.ToString());
                    Rotation(b.x - a.x, b.y - a.y);
                    len = Mathf.Sqrt((b.x - a.x) * (b.x - a.x) + (b.y - a.y) * (b.y - a.y));
                    float shift = Screen.height / 8;
                    if (Screen.height / 8 <= Screen.width / 6)
                    {
                        shift = 0;
                    }
                    //Debug.Log(unit.name);
                    //unit.gameObject.transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * a.x, tr.position.y - dx / 2 + shift - dx + dx * a.y, tr.position.z);
                    segment++;
                    timer = Time.time;
                    speed = new Vector2((b.x - a.x) / len, (b.y - a.y) / len) * dx;
                    this.gameObject.gameObject.GetComponent<Rigidbody2D>().velocity = speed;
                    //Debug.Log(speed.sqrMagnitude);
                    animatorGO.GetComponent<AnimatorControllerHelp>().SetSpeed(speed.sqrMagnitude);
                    //Debug.Log(a + " " + b);
                    main.GetComponent<BattleSceneUI>().Swap(a, b);
                }
                if ((Time.time - timer > len / 2) && (!musicChange))
                {
                    this.gameObject.GetComponent<AudioGround>().SetActive(ChooseSoundStep(a));
                    musicChange = true;
                }
            }
            else
            {
                if(Time.time - timer > len)
                {
                    animatorGO.GetComponent<AnimatorControllerHelp>().SetSpeed(0);
                }
                if (Time.time - timer > len)
                {
                    flag = false;
                    //Transform tr = this.gameObject.transform;
                    Vector2 speed = new Vector2(0, 0);
                    this.gameObject.gameObject.GetComponent<Rigidbody2D>().velocity = speed;
                    float shift = Screen.height / 8;
                    this.gameObject.GetComponent<AudioGround>().Mute();
                    if (Screen.height / 8 <= Screen.width / 6)
                    {
                        shift = 0;
                    }
                    //unit.gameObject.transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * a.x, tr.position.y - dx / 2 - dx + shift + dx * a.y, tr.position.z);
                    main.GetComponent<BattleSceneUI>().SetBlock(false, false);
                }
                }
        }
        for (int i = 0; i < pauseTimer.Count; i++)
        {
            //Debug.Log(i + 2f + " " + Time.time);
            if(pauseTimer[i] + 2f < Time.time)
            {
                main.GetComponent<BattleSceneUI>().SetBlock(false, false);
                pauseTimer.RemoveAt(i);
                i--;
            }
        }
	}

    List<float> pauseTimer = new List<float>();

    public void PlayAnim(string name, int cou)
    {
        if (cou <= 0)
        {
            animatorGO.GetComponent<AnimatorControllerHelp>().PlayAnim(name);
            //pauseTimer.Add(Time.time);
        }
        else
        {
            main.GetComponent<BattleSceneUI>().AddBlock();
            animatorGO.GetComponent<AnimatorControllerHelp>().PlayAnim(name, 1);
            pauseTimer.Add(Time.time);
        }
    }
}

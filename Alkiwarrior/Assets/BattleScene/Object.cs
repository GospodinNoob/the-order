﻿using UnityEngine;
using System.Collections;

public class Object{

    public int id;
    public Armour armour;
    public int curHits;
    public int maxHits;
    public string name;
    bool flamable;

    int lang;

    string[] names;

    string[] engNames = { "Box", "Barrels", "Wall", "Boulder" };
    string[] rusNames = { "Ящик", "Бочки", "Стена", "Валун" };

    void SetLang()
    {
        lang = PlayerPrefs.GetInt("Language");
        if (lang == 0)
        {
            names = engNames;

        }
        if (lang == 1)
        {
            names = rusNames;
        }
    }

    public bool blocked;
    public bool smallBlock;
    public bool permanent;

    public Object (int a)
    {
        this.permanent = false;
        SetLang();
        if (a != 0)
        {
            Generate(a);
}
        else
        {
            this.id = 0;
            Generate(0);
            //this.armour = new Armour(lang);
        }
    }

    public GameObject go;

    string[] damageText = { "Damage", "Урон" };
    string[] pariedText = { "Paried", "Парировано" };
    string[] dodgeText = { "Miss", "Промах" };
    string[] blockedText = { "Blocked", "Блок" };
    string[] critText = { "Crit", "Крит" };

    void OneHandDamage(MovebleObject move, Damage dmg)
    {
        int rand = Random.Range(0, 101);
        int coef = 1;
        bool crit = false;
        bool dodge = false;
        if (rand <= move.GetCrit())
        {
            coef = 2;
            crit = true;
        }
        rand = Random.Range(0, 101);
       // Debug.Log(100 + move.GetAim());
        if (rand >= 100 + move.GetAim())
        {
            coef = 0;
            crit = false;
        }
        Damage helpDmg = new Damage(dmg, move.extraDmg);
        Damage reducedDamage = null;// new Damage(coef, helpDmg, this.armour, true, this);
        //int sum = reducedDamage.Sum();
    }

    public void DealDamage(MovebleObject move)
    {
       // OneHandDamage(move, move.damage);
        //if ((move.leftArm.handStatus >= -1) && (move.leftArm.id != 0))
        {
         //   OneHandDamage(move, move.leftArm);
        }
        if (this.curHits <= 0)
        {
            this.id = 0;
            //go.GetComponent<HelperScript>().SetBurned(false);
            //go.GetComponent<HelperScript>().SetFrozen(false);
        }
    }

    public Armour extraArmour;

    public void DealDamage(Damage dmg)
    {
        Damage reducedDamage = null;// new Damage(1, dmg, new ArmourValue(this.armour, extraArmour), false, this);

    }

    PointXY HelpEstimatedDamage(MovebleObject move, Damage dmg)
    {
        int rand = Random.Range(0, 101);
        int coef = 1;
        bool crit = false;
        bool dodge = false;
        Damage helpDmg = new Damage(dmg, move.extraDmg);
        Damage reducedDamage = null;// new Damage(coef, helpDmg, this.armour, false, this);
        //Debug.Log(move.GetAim());
        return null;
    }

    public PointXY EstimatedDamage(MovebleObject move)
    {
        //PointXY ans = new PointXY(HelpEstimatedDamage(move, move.damage));
        PointXY ans2 = new PointXY(0, 0);
       // if ((move.leftArm.id != -2) && (move.leftArm.id != 0))
        {
        //    ans2 = new PointXY(HelpEstimatedDamage(move, move.leftArm));
        }
        //ans.realH = 100f - ans.realH;
        //ans.realD += ans2.realD;
        return null;// ans;
    }

    public void Generate(int a)
    {
        /*SetLang();
        this.id = a;
        extraArmour = new Armour(0, lang);
        effects = new ArrayList();
        if (a == 1)
        {
          //  SetBurnedStatus(3);
            this.armour = new Armour(10, 6, 0, 0, lang);
            this.maxHits = 10;
            this.curHits = this.maxHits;
            this.name = names[this.id - 1];
            this.blocked = true;
            this.smallBlock = true;
            this.permanent = false;
            flamable = true;
          //  Debug.Log(this.armour.piercingArmour);
        }
        if (a == 2)
        {
            this.armour = new Armour(10, 6, 0, 0, lang);
            this.maxHits = 20;
            this.curHits = this.maxHits;
            this.name = names[this.id - 1];
            this.blocked = true;
            this.smallBlock = true;
            this.permanent = false;
            flamable = true;
        }
        if (a == 3)
        {
            this.armour = new Armour(0, 0, 0, 0, lang);
            this.maxHits = 1;
            this.curHits = this.maxHits;
            this.name = names[this.id - 1];
            this.blocked = true;
            this.smallBlock = false;
            this.permanent = true;
            flamable = false;
        }
        if (a == 4)
        {
            this.armour = new Armour(0, 0, 0, 0, lang);
            this.maxHits = 1;
            this.curHits = this.maxHits;
            this.name = names[this.id - 1];
            this.blocked = true;
            this.smallBlock = true;
            this.permanent = true;
            flamable = false;
        }*/
    }

    public Object()
    {
        this.permanent = false;
        SetLang();
        this.id = 0;
        //this.armour = new Armour(lang);
        Generate(0);
    }

    public bool isEmpty()
    {
        return id == 0;
    }

    public void Clear()
    {
        this.id = 0;
    }

    ArrayList effects;
    string[] burnedText = { "Fire ", "Огонь " };
    string[] frozenText = { "Frostbite ", "Обмрожение " };

    public void SetBurnedStatus(int turns)
    {
        go.GetComponent<Messenger>().ShowMessage(burnedText[lang] + "(" + turns + ")");
        if (frozen)
        {
            for (int i = 0; i < effects.Count; i++)
            {
                if (((Effect)effects[i]).id == 7)
                {
                    ((Effect)effects[i]).turns -= turns;
                    int a = ((Effect)effects[i]).turns;
                    if (((Effect)effects[i]).turns <= 0)
                    {
                        frozen = false;
                        fired = true;
                       // go.GetComponent<HelperScript>().SetBurned(fired);
                       // go.GetComponent<HelperScript>().SetFrozen(frozen);
                        effects.Remove(effects[i]);
                        if (a < 0)
                        {
                            effects.Add(new Effect(6, -a, lang));
                        }
                        RecalcSecondaryNewTurn();
                    }
                    break;
                }
            }
        }
       // go.GetComponent<HelperScript>().SetBurned(true);
        for (int i = 0; i < effects.Count; i++)
        {
            if (((Effect)effects[i]).id == 6)
            {
                ((Effect)effects[i]).turns += turns;
                break;
            }
        }
        effects.Add(new Effect(6, turns, lang));
        fired = true;
        frozen = false;
       // go.GetComponent<HelperScript>().SetBurned(fired);
        //go.GetComponent<HelperScript>().SetFrozen(frozen);
        //go.GetComponent<Messenger>().ShowMessage(burnedText[lang] + "(" + turns + ")");
    }

    public void SetFrozenStatus(int turns)
    {
        go.GetComponent<Messenger>().ShowMessage(frozenText[lang] + "(" + turns + ")");
        if (frozen)
        {
            for (int i = 0; i < effects.Count; i++)
            {
                if (((Effect)effects[i]).id == 6)
                {
                    ((Effect)effects[i]).turns -= turns;
                    int a = ((Effect)effects[i]).turns;
                    if (((Effect)effects[i]).turns <= 0)
                    {
                        frozen = true;
                        fired = false;
                        //go.GetComponent<HelperScript>().SetBurned(fired);
                        //go.GetComponent<HelperScript>().SetFrozen(frozen);
                        effects.Remove(effects[i]);
                        if (a < 0)
                        {
                            effects.Add(new Effect(7, -a, lang));
                        }
                        RecalcSecondaryNewTurn();
                    }
                    break;
                }
            }
        }
        //go.GetComponent<HelperScript>().SetBurned(true);
        for (int i = 0; i < effects.Count; i++)
        {
            if (((Effect)effects[i]).id == 7)
            {
                ((Effect)effects[i]).turns += turns;
                break;
            }
        }
        effects.Add(new Effect(7, turns, lang));
        fired = false;
        frozen = true;
        //go.GetComponent<HelperScript>().SetBurned(fired);
        //go.GetComponent<HelperScript>().SetFrozen(frozen);
       // effects.Add(new Effect(7, turns, lang));
    }

    public bool frozen;
    public bool fired;

    void RecalcSecondaryNewTurn()
    {
        frozen = false;
        fired = false;
        //extraArmour = new Armour(0, lang);
       // for (int i = 0; i < effects.Count; i++)
        //{
         //   ((Effect)effects[i]).PassiveActive(this);
       // }
    }

    public void calculateNewTurn()
    {
        
       // for (int i = 0; i < effects.Count; i++)
        //{
            //    ((Effect)effects[i]).Turn();
          //      ((Effect)effects[i]).Turn();
           // if (((Effect)effects[i]).turns == 0)
            //{
             //   effects.Remove(effects[i]);
              //  i--;
           // }
       // }
        RecalcSecondaryNewTurn();
       // for (int i = 0; i < effects.Count; i++)
        //{
         //   ((Effect)effects[i]).Active(this);
       // }
        //Debug.Log(go);
        //go.GetComponent<HelperScript>().SetBurned(fired);
        //go.GetComponent<HelperScript>().SetFrozen(frozen);
    }
}

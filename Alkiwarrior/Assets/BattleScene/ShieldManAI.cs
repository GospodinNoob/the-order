﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldManAI
{

    public Unit[] units;
    public float[][] dist;
    public float[][] costMatrix;
    public ChunkController[][] map;
    public RangeAttack RA;

    public ShieldManAI()
    {
        RA = new RangeAttack();
    }

    public Unit GetTarget(MovebleObject un, PointXY p, Ability activeAction)
    {
        Unit target = null;
        int minusDmg = -1;
        float hit = -1;
        bool kill = false;
        // Debug.Log("Target");
        int maxunits = units.Length;
        for (int i = 0; i < maxunits; i++)
        {
            if (!units[i].unit.isEmpty() && units[i].unit.player && RA.InRangeAttack(p, units[i].point, un, activeAction, null))
            {
                // Debug.Log(i);
                PointXY estDmg = null;// new PointXY(units[i].unit.EstimatedDamage(un, units[i].rotation, new PointXY(units[i].point.x - p.x, units[i].point.y - p.y), RA.LenPoints(p, units[i].point)));
                estDmg.realD = Mathf.Min(estDmg.realD, units[i].unit.curHits);
                if (kill)
                {
                    if (estDmg.realD == units[i].unit.curHits)
                    {
                        if (estDmg.realH > hit)
                        {
                            target = units[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;

                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    if (estDmg.realD == units[i].unit.curHits)
                    {
                        if (estDmg.realH * estDmg.realH * 3 > hit * minusDmg)//un.unit.archTypeValues[6] > hit * minusDmg)
                        {
                            target = units[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;
                            kill = true;
                        }
                    }
                    else
                    {
                        if (estDmg.realD * estDmg.realH > minusDmg * hit)
                        {
                            target = units[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;
                            kill = false;
                        }
                    }
                }
            }
        }
        if (minusDmg <= 0)
        {
            return null;
        }
        return target;
    }

    bool Flag(MovebleObject enemyUn, int j, int k)
    {
        if (enemyUn.maxAP == 1)
        {
            return Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) <= enemyUn.curAP;
        }
        return Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP;
    }

    public PointXY GetBestPoint(MovebleObject move)
    {
        //    Debug.Log("*");
        return null;
        MovebleObject enemyUn = move;
        int mapWidth = dist.Length;
        int mapHeight = dist[0].Length;
        int INF = 10000000;
        int maxunits = units.Length;

        PointXY yourPoint = null;

        for (int i = 0; i < maxunits; i++)
        {
            if (units[i].unit == move)
            {
                yourPoint = new PointXY(units[i].point);
            }
        }
        PointXY p = new PointXY(-1, -1);
        PointXY p2 = new PointXY(-1, -1);
        PointXY est, estHelp;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        int costMI = 10000;
        int needAp = enemyUn.curAP - 1;
        bool tfMI = false;// ((Mathf.Abs(units[r].point.x - j) <= 1) && (Mathf.Abs(units[r].point.y - k)) <= 1)
        tfMI = false;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        int val = -INF;
            /*
        for (int j = 0; j < mapWidth; j++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                //  Debug.Log(mapWidth);
                //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                if (target != null)
                {
                    estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                    estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                    //  Debug.Log(map);
                    if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                    {
                        //Debug.Log(j + " " + k);
                        if (Flag(enemyUn, j, k))
                        {
                            if (estHelp.realD * (estHelp.realD) > est.realH * (est.realH))
                            {
                                //Debug.Log(j + " " + k);
                                tfMI = true;
                                p2 = new PointXY(j, k);
                                //       costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                //  Debug.Log(costMI);
                                val = Mathf.Max(val, new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point))).realD);
                                est = new PointXY(estHelp);
                            }
                        }
                    }
                }
            }
        }
        if (tfMI)
        {
            Unit target = GetTarget(enemyUn, new PointXY(p2.x, p2.y), Ability.Attack);
            float value = (val * (est.realH) / 100f);
            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - p2.x, target.point.y - p2.y), RA.LenPoints(new PointXY(p2.x, p2.y), target.point)));
            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
            if (target != null)
            {
                //Debug.Log(move.UseAbility(0) + " " + value + " " + target.unit.curHits);
                if (move.UseAbility(0) && (value < target.unit.curHits) && (RA.InRangeAttack(enemyUn.point, new PointXY(p2.x, p2.y), enemyUn, Ability.ShieldBash, new PointXY(p2.x, p2.y))))
                {
                    p2.ability = Ability.ShieldBash;
                }
            }
            return p2;
        }

        if (!tfMI)
        {
            if ((enemyUn.curAP > 1) || ((enemyUn.maxAP == 1) && (enemyUn.curAP == 1)))
            {
                est = new PointXY(-1, -1);
                int cost = 10000;
                bool tf = false;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                       // enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                        Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                        estHelp = new PointXY(0, 0);
                        if (target != null)
                        {
                            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(target.point, new PointXY(j, k))));
                            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                            if (map[j][k].movebleObject.isEmpty() && map[j][k].obj.isEmpty())
                            {
                                if (p.x < 0)
                                {
                                    tf = true;
                                    tfMI = true;
                                    p = new PointXY(j, k);
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                                else
                                {
                                    if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                    {
                                        tf = true;
                                        tfMI = true;
                                        p = new PointXY(j, k);
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                        est = new PointXY(estHelp);
                                    }
                                    else
                                    {
                                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) == Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                        {
                                            if (estHelp.realD * (  estHelp.realH) > est.realD * (  est.realH))
                                            {
                                                tf = true;
                                                tfMI = true;
                                                p = new PointXY(j, k);
                                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                                est = new PointXY(estHelp);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                if (!tf)
                {
                    return null;
                }
                return p;
            }
        }
        return null;
        */
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTemplate{

    public List<string> grounds = new List<string>();
    public List<float> groundsChance = new List<float>();
    public float objectChance;
    public List<string> objects = new List<string>();
    public int height = 6;
    public int width = 6;

    int INF = 10000000;

    public List<List<GameObject>> chunks;

    public List<PointXY> way;

    public List<MovebleObject> units = new List<MovebleObject>();

    public void DrawWay()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                chunks[i][j].GetComponent<SpriteActive>().SetWay(new PointXY(0, 0));
            }
        }
        if (way == null)
        {
            return;
        }
        if (way.Count < 2)
        {
            return;
        }
        chunks[((PointXY)way[0]).x][((PointXY)way[0]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[0], (PointXY)way[1]));
        int c = way.Count - 1;
        chunks[((PointXY)way[c]).x][((PointXY)way[c]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[c], (PointXY)way[c - 1]));
        for (int i = 1; i < way.Count - 1; i++)
        {
            chunks[((PointXY)way[i]).x][((PointXY)way[i]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[i], (PointXY)way[i - 1]));
            chunks[((PointXY)way[i]).x][((PointXY)way[i]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[i], (PointXY)way[i + 1]));
        }
    }

    public Chunk playerChunk;
    public PointXY playerPoint;

    public List<MovebleObject> RecalcTargets(MovebleObject m, PointXY pos, Active a)
    {
        List<MovebleObject> targets = new List<MovebleObject>();
        for(int i = 0; i < width; i++)
        {
            for(int j = 0; j < height; j++)
            {
                //Debug.Log(a.range);
                if (LenPoints(pos, new PointXY(i, j)) < a.range)
                {
                    if((map[i][j].movebleObject != null) && (map[i][j].movebleObject.isAlive()))
                    {
                        if(staticData.ArrayHasTag(a.targetTypes, "ENEMY"))
                        {
                           // Debug.Log(map[i][j].movebleObject.team + " " + m.team);
                            if (map[i][j].movebleObject.team != m.team)
                            {
                                targets.Add(map[i][j].movebleObject);
                            }
                        }
                        if (staticData.ArrayHasTag(a.targetTypes, "FRIENDLY"))
                        {
                            if (map[i][j].movebleObject.team == m.team)
                            {
                                targets.Add(map[i][j].movebleObject);
                            }
                        }
                    }
                }
            }
        }
        return targets;
    }

    public void ClearMapInterface()
    {
        //Debug.Log(way);
        way.Clear();
        DrawWay();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-9, false);
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-8, false);
                chunks[i][j].GetComponent<SpriteActive>().SetText("");
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-5, false);
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
            }
        }
    }

    public void RedrawObjects()
    {

    }

    public void RedrawMoveArea()
    {
        ClearMapInterface();
        if(playerChunk == null)
        {
            return;
        }
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                int k = Mathf.CeilToInt(dist[i][j] / playerChunk.movebleObject.GetSpeed());
                if ((k > 0) && (k <= playerChunk.movebleObject.curAP))
                {
                    chunks[i][j].GetComponent<SpriteActive>().SetText(k.ToString());
                }
                else
                {
                    chunks[i][j].GetComponent<SpriteActive>().SetText("");
                }
                if (dist[i][j] <= playerChunk.movebleObject.curAP * playerChunk.movebleObject.GetSpeed())
                {
                    if (!Valid(i - 1, j) || (dist[i - 1][j] > playerChunk.movebleObject.curAP * playerChunk.movebleObject.GetSpeed()))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                    }
                    if (!Valid(i + 1, j) || (dist[i + 1][j] > playerChunk.movebleObject.curAP * playerChunk.movebleObject.GetSpeed()))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
                    }
                    if (!Valid(i, j - 1) || (dist[i][j - 1] > playerChunk.movebleObject.curAP * playerChunk.movebleObject.GetSpeed()))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                    }
                    if (!Valid(i, j + 1) || (dist[i][j + 1] > playerChunk.movebleObject.curAP * playerChunk.movebleObject.GetSpeed()))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                    }
                }
            }
        }
    }

    public void RedrawAttackArea()
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
            }
        }
    }

    public void RedrawSpawnArea()
    {
        ClearMapInterface();
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if ((dist[i][j] < 2))
                {
                    if (!Valid(i - 1, j) || (dist[i - 1][j] > 2))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                    }
                    if (!Valid(i + 1, j) || (dist[i + 1][j] > 2))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
                    }
                    if (!Valid(i, j - 1) || (dist[i][j - 1] > 2))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                    }
                    if (!Valid(i, j + 1) || (dist[i][j + 1] > 2))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                    }
                }
            }
        }
    }

    public void CheckTextures()
    {
        RedrawMoveArea();
        RedrawAttackArea();
    }

    bool InMap(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= width)
        {
            return false;
        }
        if (b >= height)
        {
            return false;
        }
        return true;
    }

    bool Valid(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= width)
        {
            return false;
        }
        if (b >= height)
        {
            return false;
        }
        if ((map[a][b].obj == null) && ((map[a][b].movebleObject == null) || (!map[a][b].movebleObject.isAlive())))
        {
            return true;
        }
        return false;
    }

    float Len(int a, int b, int c, int d, float v1, float v2)
    {
        return Mathf.Sqrt((a - c) * (a - c) + (b - d) * (b - d)) * (v1 + v2) / 2;
    }

    public void Dijkstra(PointXY p1)
    {
        PointXY p = new PointXY(p1);
        int n = width;
        int k = height;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < k; j++)
            {
                dist[i][j] = INF;
                flags[i][j] = false;
            }
        }

        dist[p.x][p.y] = 0;
        int step = 0;
        while ((dist[p.x][p.y] < INF - 1) && (step < 500))
        {
            step++;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (!((i != 0) && (j != 0)))
                    {
                        if (Valid(p.x + i, p.y + j) && (dist[p.x + i][p.y + j] > dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].speed, map[p.x + i][p.y + j].speed)))
                        {

                            dist[p.x + i][p.y + j] = dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].speed, map[p.x + i][p.y + j].speed);
                            parents[p.x + i][p.y + j] = new PointXY(p);
                        }
                    }
                    else
                    {
                        if (Valid(p.x + i, p.y + j) && (map[p.x + i][p.y].movebleObject == null) && (map[p.x][p.y + j].movebleObject == null) && (map[p.x + i][p.y].obj == null) && (map[p.x][p.y + j].obj == null) && (dist[p.x + i][p.y + j] > dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].speed, map[p.x + i][p.y + j].speed)))
                        {

                            dist[p.x + i][p.y + j] = dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].speed, map[p.x + i][p.y + j].speed);
                            parents[p.x + i][p.y + j] = new PointXY(p);
                        }
                    }
                }
            }

            flags[p.x][p.y] = true;
            float min = INF;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    if ((dist[i][j] < min) && (!flags[i][j]))
                    {
                        p.x = i;
                        p.y = j;
                        min = dist[i][j];
                    }
                }
            }

        }
    }

    StaticData staticData;

    public List<List<Chunk>> map;
    public List<List<float>> dist;
    List<List<bool>> flags;
    List<List<PointXY>> parents; 

    public MovebleObject GetNextNonLockedUnitInTeam(string team)
    {
        foreach(var i in units)
        {
           // Debug.Log(i.team + " " + i.curAP);
            if((i.team == team) && (i.curAP > 0))
            {
                return i;
            }
        }
        return null;
    }

    public MapTemplate()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        grounds.Add("GRASS");
        grounds.Add("MUD");
        objectChance = 0;
    }

    float LenPoints(PointXY a, PointXY b)
    {
        return Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    public bool FreeSpaceForSpawn(PointXY p)
    {
        bool flag = true;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if ((LenPoints(new PointXY(i, j), p) <= 1.42f) && ((map[i][j].movebleObject != null) || (map[i][j].obj != null)))
                {
                    flag = false;
                }
            }
        }
        return flag;
    }

    bool SpaseWithoutUnits(PointXY p)
    {
        bool flag = true;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if ((LenPoints(new PointXY(i, j), p) <= 1.42f) && (map[i][j].movebleObject != null))
                {
                    flag = false;
                }
            }
        }
        return flag;
    }
    void ClearSpaceForSquad(PointXY p)
    {
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                if ((LenPoints(new PointXY(i, j), p) <= 1.42f))
                {
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(map[i][j].obj.id, false);
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(map[i][j].obj.id, false, false, false, false);
                    map[i][j].obj.Clear();
                }
            }
        }
    }

    public List<PointXY> GetWay(PointXY p)
    {
        List<PointXY> newWay = new List<PointXY>();
        newWay.Add(p);
        while (dist[p.x][p.y] > 0)
        {
            p = parents[p.x][p.y];
            newWay.Add(p);
        }
        return newWay;
    }

    public void CreateMap()
    {
        int n = 8;
        int k = 6;
        float dx = Screen.width / n;
        float dy = Screen.height / k;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }

        map = new List<List<Chunk>>();
        dist = new List<List<float>>();
        flags = new List<List<bool>>();
        parents = new List<List<PointXY>>();
        chunks = new List<List<GameObject>>();
        for (int i = 0; i < width; i++)
        {
            List<Chunk> m = new List<Chunk>();
            List<float> d = new List<float>();
            List<bool> b = new List<bool>();
            List<PointXY> p = new List<PointXY>();
            List<GameObject> c = new List<GameObject>();
            for (int j = 0; j < height; j++)
            {
                m.Add(new Chunk(staticData.GetChunkByKeyName("GRASS")));
                d.Add(INF);
                b.Add(false);
                p.Add(new PointXY(0, 0));
                c.Add(null);
            }
            chunks.Add(c);
            map.Add(m);
            dist.Add(d);
            flags.Add(b);
            parents.Add(p);
        }

        GameObject mapObj = GameObject.Find("Map");
        GameObject chunkPrefab = Resources.Load("ChunkPrefab") as GameObject;
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                Transform tr = mapObj.transform;
                chunks[i][j] = (GameObject)GameObject.Instantiate(chunkPrefab, tr);
                chunks[i][j].GetComponent<SpriteActive>().setGround(1);
                //map[i][j].obj.go = chunks[i][j];
                chunks[i][j].GetComponent<CoordinatsHelper>().point = new PointXY(i, j);
                chunks[i][j].transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * i, tr.position.y - dx / 2 - dx + dx * j, tr.position.z);
                float scale = chunks[i][j].transform.localScale.x * dx;
                chunks[i][j].transform.localScale = new Vector3(scale, scale, scale);
                chunks[i][j].transform.parent = mapObj.transform;
            }
        }
        int cou = 0;
        for (int i = 1; i < width - 1; i++)
        {
            if (FreeSpaceForSpawn(new PointXY(i, 0)))
            {
                cou++;
            }
            if (FreeSpaceForSpawn(new PointXY(i, height - 1)))
            {
                cou++;
            }
        }
        for (int j = 1; j < height - 1; j++)
        {
            if (FreeSpaceForSpawn(new PointXY(0, j)))
            {
                cou++;
            }
            if (FreeSpaceForSpawn(new PointXY(width - 1, j)))
            {
                cou++;
            }
        }
        int len = (width * 2 + height * 2 - 4);
        int step = 0;
        while (cou <= (width * 2 + height * 2 - 4) * 0.3f)
        {
            cou = 0;
            step++;
            if (step > 50)
            {
                break;
            }
            for (int i = 1; i < width - 1; i++)
            {
                if (SpaseWithoutUnits(new PointXY(i, 0)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(i, 0));
                }
                if (SpaseWithoutUnits(new PointXY(i, height - 1)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(i, height - 1));
                }
            }
            for (int j = 1; j < height - 1; j++)
            {
                if (SpaseWithoutUnits(new PointXY(0, j)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(0, j));
                }
                if (SpaseWithoutUnits(new PointXY(width - 1, j)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(width - 1, j));
                }
            }
            for (int i = 1; i < width - 1; i++)
            {
                if (FreeSpaceForSpawn(new PointXY(i, 0)))
                {
                    cou++;
                }
                if (FreeSpaceForSpawn(new PointXY(i, height - 1)))
                {
                    cou++;
                }
            }
            for (int j = 1; j < height - 1; j++)
            {
                if (FreeSpaceForSpawn(new PointXY(0, j)))
                {
                    cou++;
                }
                if (FreeSpaceForSpawn(new PointXY(width - 1, j)))
                {
                    cou++;
                }
            }
        }
        SecondaryGroundGeneration(dist.Count * dist[0].Count / 10 / 2, 6, "MUD");
        way = new List<PointXY>();
    }


    void SecondaryGroundGeneration(int areas, int pointsInArea, string createGround)
    {
        Queue<PointXY> que;
        que = new Queue<PointXY>();
        PointXY p;
        while (areas > 0)
        {
            int localMax = pointsInArea;
            que.Clear();
            p = (new PointXY((int)Random.Range(0, dist.Count), (int)Random.Range(0, dist[0].Count)));
            //Debug.Log(p.x + " " + (p.y));
            while (!Valid(p.x, p.y))
            {
                p = (new PointXY((int)Random.Range(0, dist.Count), (int)Random.Range(0, dist[0].Count)));
                //   Debug.Log(p.x + " " + (p.y));
            }
            que.Enqueue(p);
            int step = 0;
            //Debug.Log(p.x + " " + (p.y));
            while ((localMax > 0) && (step < 50) && (que.Count != 0))
            {
                step++;
                p = que.Dequeue();
                if (map[p.x][p.y].keyName != createGround)
                {
                    //   Debug.Log(p.x + " " + p.y);
                    //Debug.Log(name);
                    map[p.x][p.y] = staticData.GetChunkByKeyName(createGround);
                    chunks[p.x][p.y].GetComponent<SpriteActive>().SetGround(createGround);
                    //Destroy(this.gameObject.transform.root.gameObject);
                    //Debug.Log(name);
                    float rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x + 1, p.y) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x + 1, p.y));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x - 1, p.y) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x - 1, p.y));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x, p.y - 1) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x, p.y - 1));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x, p.y + 1) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x, p.y + 1));
                    }
                    localMax--;
                }
            }
            areas--;
        }
        for (int i = 0; i < dist.Count; i++)
        {
            for (int j = 0; j < dist[0].Count; j++)
            {
                if (map[i][j].keyName == createGround)
                {
                    if (InMap(i - 1, j))
                    {
                        if (map[i - 1][j].keyName != map[i][j].keyName)
                        {
                            chunks[i - 1][j].GetComponent<SpriteActive>().SetGroundBorder(2, map[i][j].keyName);
                        }
                    }
                    if (InMap(i + 1, j))
                    {
                        if (map[i + 1][j].keyName != map[i][j].keyName)
                        {
                            chunks[i + 1][j].GetComponent<SpriteActive>().SetGroundBorder(1, map[i][j].keyName);
                        }
                    }
                    if (InMap(i, j + 1))
                    {
                        if (map[i][j + 1].keyName != map[i][j].keyName)
                        {
                            chunks[i][j + 1].GetComponent<SpriteActive>().SetGroundBorder(0, map[i][j].keyName);
                        }
                    }
                    if (InMap(i, j - 1))
                    {
                        if (map[i][j - 1].keyName != map[i][j].keyName)
                        {
                            chunks[i][j - 1].GetComponent<SpriteActive>().SetGroundBorder(3, map[i][j].keyName);
                        }
                    }
                }
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowManAI{

    public Unit[] units;
    public float[][] dist;
    public float[][] costMatrix;
    public ChunkController[][] map;
    public RangeAttack RA;

    public BowManAI()
    {
        RA = new RangeAttack();
    }

    public Unit GetTarget(MovebleObject un, PointXY p, Ability activeAction)
    {
        Unit target = null;
        int minusDmg = -1;
        float hit = -1;
        bool kill = false;
        // Debug.Log("Target");
      //  Debug.Log(p);
        int maxunits = units.Length;
        for (int i = 0; i < maxunits; i++)
        {
            if (!units[i].unit.isEmpty() && units[i].unit.player && RA.InRangeAttack(p, units[i].point, un, activeAction, null))
            {
                un.SetExtraAim(RA.LenPoints(p, units[i].point), RA.LineMinusAim(p, units[i].point));
                //un.EmulateAbility(activeAction, Mathf.CeilToInt(dist[p.x][p.y] / un.GetSpeed()));
                // Debug.Log(i);
                PointXY estDmg = null;// new PointXY(units[i].unit.EstimatedDamage(un, units[i].rotation, new PointXY(units[i].point.x - p.x, units[i].point.y - p.y), RA.LenPoints(p, units[i].point)));
                //  Debug.Log("*");
                estDmg.realD = Mathf.Min(estDmg.realD, units[i].unit.curHits);
                //Debug.Log(estDmg.realH + " " + (estDmg.realH < 100));
                if (estDmg.realH > 0)
                {


               //     un.EmulateAbility(-1, Mathf.CeilToInt(dist[p.x][p.y] / un.GetSpeed()));
                    if (activeAction == Ability.CurvedTrajectoryShot)
                    {
                        //       un.EmulateAbility(3, Mathf.CeilToInt(dist[p.x][p.y] / un.GetSpeed()));
                        //  Debug.Log(un);
                        // Debug.Log(units[i]);
                        //Debug.Log(target.unit.EstimatedDamage(un, units[i].rotation, new PointXY(units[i].point, p), RA.LenPoints(p, units[i].point)));
                        PointXY estHelp2 = null;// new PointXY(units[i].unit.EstimatedDamage(un, units[i].rotation, new PointXY(units[i].point, p), RA.LenPoints(p, units[i].point)));
                        estHelp2.realD = Mathf.Min(estHelp2.realD, units[i].unit.curHits);
                        if (estHelp2.realD * (estHelp2.realH) > estDmg.realD * (estDmg.realH))
                        {
                            estDmg = new PointXY(estHelp2);
                        }
                    }
                    if (kill)
                    {
                        if (estDmg.realD == units[i].unit.curHits)
                        {
                            if (estDmg.realH > hit)
                            {
                                target = units[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;

                            }
                            else
                            {

                            }
                        }
                    }
                    else
                    {
                        if (estDmg.realD == units[i].unit.curHits)
                        {
                            if ((estDmg.realH) * estDmg.realH * 3 > (hit) * minusDmg)//un.unit.archTypeValues[6] > hit * minusDmg)
                            {
                                target = units[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = true;
                            }
                        }
                        else
                        {
                            if (estDmg.realD * (estDmg.realH) > minusDmg * (hit))
                            {
                                target = units[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = false;
                            }
                        }
                    }
                }
            }
        }
        if (minusDmg <= 0)
        {
            return null;
        }
        return target;
    }

    public PointXY GetBestPoint(MovebleObject move)
    {
        MovebleObject enemyUn = move;
        int mapWidth = dist.Length;
        int mapHeight = dist[0].Length;
        int INF = 10000000;
        int maxunits = units.Length;

        PointXY yourPoint = null;

        yourPoint = move.point;
        PointXY p = new PointXY(-1, -1);
        PointXY p2 = new PointXY(-1, -1);
        PointXY est, estHelp;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        int costMI = 10000;
        int needAp = enemyUn.curAP - 1;
        bool tfMI = false;// ((Mathf.Abs(units[r].point.x - j) <= 1) && (Mathf.Abs(units[r].point.y - k)) <= 1)
        bool archeryShot = false;
        bool resultArcheryShot = false;
        /*
        for (int j = 0; j < mapWidth; j++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                if (RA.FreeAround(enemyUn, new PointXY(j, k)))
                {
                    ///enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                    Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                    if (target != null)
                    {
                        estHelp = null;// new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                        estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                        if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                        {
                            //Debug.Log(j + " " + k);
                            if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP)
                            {
                                if (RA.LenPoints(yourPoint, new PointXY(j, k)) > enemyUn.GetRangeAttack() / 2f)
                                {
                                    //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                                    if (estHelp.realD * (estHelp.realH) > est.realD * (est.realH))
                                    {
                                        //Debug.Log(j + " " + k);
                                        tfMI = true;
                                        p2 = new PointXY(j, k);
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                        est = new PointXY(estHelp);
                                    }
                                }
                            }
                        }
                    }
                    if (enemyUn.UseAbility(Ability.CurvedTrajectoryShot))
                    {
                        target = GetTarget(enemyUn, new PointXY(j, k), Ability.CurvedTrajectoryShot);
                        if (target != null)
                        {
                            //estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                            //estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                            if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                            {
                                //Debug.Log(j + " " + k);
                                if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP)
                                {
                                    if (RA.LenPoints(yourPoint, new PointXY(j, k)) > enemyUn.GetRangeAttack() / 2f)
                                    {
                                       // enemyUn.EmulateAbility(Ability.CurvedTrajectoryShot, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                                        PointXY estHelp2 = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                                        estHelp2.realD = Mathf.Min(estHelp2.realD, target.unit.curHits);
                                        estHelp = new PointXY(estHelp2);
                                        if (estHelp.realD * ( estHelp.realH) > est.realD * ( est.realH))
                                        {
                                            tfMI = true;
                                            resultArcheryShot = true;
                                            p2 = new PointXY(j, k);
                                            costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                            est = new PointXY(estHelp);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if (tfMI)
        {
            if (resultArcheryShot)
            {
                p2.ability = Ability.CurvedTrajectoryShot;
            }
            return p2;
        }
        resultArcheryShot = false;
        tfMI = false;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        for (int j = 0; j < mapWidth; j++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                if (target != null)
                {
                    estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                    estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                    //  Debug.Log(map);
                    if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                    {
                        //Debug.Log(j + " " + k);
                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP)
                        {
                            if (estHelp.realD * ( estHelp.realH) > est.realD * ( est.realH))
                            {
                                //Debug.Log(j + " " + k);
                                tfMI = true;
                                p2 = new PointXY(j, k);
                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                est = new PointXY(estHelp);
                            }
                        }
                    }
                }
                if (enemyUn.UseAbility(Ability.CurvedTrajectoryShot) && (enemyUn.rightArm.handStatus == 3))
                {
                    target = GetTarget(enemyUn, new PointXY(j, k), Ability.CurvedTrajectoryShot);
                    if (target != null)
                    {
                        estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                        estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                        if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                        {
                            //Debug.Log(j + " " + k);
                            if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP)
                            {
                                //enemyUn.EmulateAbility(Ability.CurvedTrajectoryShot, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                                PointXY estHelp2 = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                                estHelp2.realD = Mathf.Min(estHelp2.realD, target.unit.curHits);
                                estHelp = new PointXY(estHelp2);
                                if (estHelp.realD * ( estHelp.realH) > est.realD * ( est.realH))
                                {
                                    resultArcheryShot = true;
                                    tfMI = true;
                                    p2 = new PointXY(j, k);
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (tfMI)
        {
            if (resultArcheryShot)
            {
                p2.ability = Ability.CurvedTrajectoryShot;
            }
            return p2;
        }

        if (!tfMI)
        {
            if (enemyUn.curAP > 1)
            {
                est = new PointXY(-1, -1);
                int cost = 10000;
                bool tf = false;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                        Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                        estHelp = new PointXY(0, 0);
                        if (target != null)
                        {
                            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(target.point, new PointXY(j, k))));
                            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                        }
                        Unit target2 = GetTarget(enemyUn, new PointXY(j, k), Ability.CurvedTrajectoryShot);
                        // Debug.Log(target);
                        if ((target == null) && (target2 != null))
                        {
                            target = target2;
                        }
                        if (target != null)
                        {
                            archeryShot = false;

                            if (enemyUn.UseAbility(Ability.CurvedTrajectoryShot) && (enemyUn.rightArm.handStatus == 3))
                            {
                                if (target2 != null)
                                {
                                        //enemyUn.EmulateAbility(Ability.CurvedTrajectoryShot, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                                        PointXY estHelp2 = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                                        estHelp2.realD = Mathf.Min(estHelp2.realD, target.unit.curHits);
                                        if (estHelp2.realD * ( estHelp2.realH) > estHelp.realD * ( estHelp.realH))
                                        {
                                            //           archeryShot = true;
                                            estHelp = new PointXY(estHelp2);
                                        }
                                    
                                }
                            }

                            if (map[j][k].movebleObject.isEmpty() && map[j][k].obj.isEmpty())
                            {
                                if (p.x < 0)
                                {
                                    tf = true;
                                    tfMI = true;
                                    p = new PointXY(j, k);
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                                else
                                {
                                    if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                    {
                                        tf = true;
                                        tfMI = true;
                                        p = new PointXY(j, k);
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                        est = new PointXY(estHelp);
                                    }
                                    else
                                    {
                                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) == Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                        {
                                            if (estHelp.realD * ( estHelp.realH) > est.realD * ( est.realH))
                                            {
                                                tf = true;
                                                tfMI = true;
                                                p = new PointXY(j, k);
                                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                                est = new PointXY(estHelp);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                
                if (!tf)
                {
                    return null;
                }
                if (resultArcheryShot)
                {
                    p2.ability = Ability.CurvedTrajectoryShot;
                }
                return p;
            }
        }*/
        return null;
    }
}

﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpriteActive : MonoBehaviour {

    public GameObject[] grass;
    public GameObject[] mud;
    public GameObject[] road;
    public GameObject box;
    public GameObject top, bot, left, right, attack, playerTrue, chosen, text, barrels, stone;
    public GameObject wallTop, wallBot, wallLeft, wallRight;
    public GameObject posAttack, roadLU, roadLD, roadRU, roadRD;
    public GameObject[] topBorder, leftBorder, botBorder, rightBorder;
    public GameObject leftWay, rightWay, topWay, downWay, leftUpWay, rightUpWay, rightDownWay, leftDownWay;

	// Use this for initialization
	void Start () {
        string id = PlayerPrefs.GetString("BaseGroundStyle");
	}

    public void SetWay(PointXY direction)
    {
        switch (direction.x)
        {
            case 0:
                switch (direction.y)
                {
                    case 0:
                        leftWay.SetActive(false);
                        rightWay.SetActive(false);
                        topWay.SetActive(false);
                        downWay.SetActive(false);
                        leftUpWay.SetActive(false);
                        rightUpWay.SetActive(false);
                        rightDownWay.SetActive(false);
                        leftDownWay.SetActive(false);
                        break;
                    case 1:
                        topWay.SetActive(true);
                        break;
                    case -1:
                        downWay.SetActive(true);
                        break;
                }
                break;
            case 1:
                switch (direction.y)
                {
                    case 0:
                        rightWay.SetActive(true);
                        break;
                    case 1:
                        rightUpWay.SetActive(true);
                        break;
                    case -1:
                        rightDownWay.SetActive(true);
                        break;
                }
                break;
            case -1:
                switch (direction.y)
                {
                    case 0:
                        leftWay.SetActive(true);
                        break;
                    case 1:
                        leftUpWay.SetActive(true);
                        break;
                    case -1:
                        leftDownWay.SetActive(true);
                        break;
                }
                break;
        }
    }

    public void SetText(string st)
    {
        text.GetComponent<Text>().text = st;
    }

    public void SetGroundBorder(int id, string keyName)
    {
        if(keyName == "MUD")
        {
            setGroundBorder(id, 1);
        }
    }

    public void setGroundBorder(int id, int type)
    {
        if (id == 0)
        {
            botBorder[type].active = true;
        }
        if (id == 1)
        {
            leftBorder[type].active = true;
        }
        if (id == 2)
        {
            rightBorder[type].active = true;
        }
        if (id == 3)
        {
            topBorder[type].active = true;
        }
    }

    public void setGround(int id, int help)
    {
        if (help == 0)
        {
            roadLD.active = true;
        }
        if (help == 1)
        {
            roadLU.active = true;
        }
        if (help == 2)
        {
            roadRD.active = true;
        }
        if (help == 3)
        {
            roadRU.active = true;
        }
    }

    public void SetGround(string KeyName)
    {
        if(KeyName == "MUD")
        {
            setGround(2);
        }
    }

    public void setGround(int id)
    {
        //Debug.Log(id);
        road[0].SetActive(false);
        for (int i = 0; i < mud.Length; i++)
        {
            mud[i].active = false;
        }
        for (int i = 0; i < grass.Length; i++)
        {
            grass[i].SetActive(false);
        }
        if (id == 1)
        {
            for (int i = 0; i < mud.Length; i++)
            {
                mud[i].active = false;
            }
            int a = Random.Range(0, grass.Length);
            for (int i = 0; i < grass.Length; i++)
            {
                if (i == a)
                {
                    grass[i].active = true;
                }
                else
                {
                    grass[i].active = false;
                }
            }
        }
        if (id == 2)
        {
            for (int i = 0; i < grass.Length; i++)
            {
                grass[i].SetActive(false);
            }
            int a = Random.Range(0, mud.Length);
            for (int i = 0; i < mud.Length; i++)
            {
                if (i == a)
                {
                    mud[i].active = true;
                }
                else
                {
                    mud[i].active = false;
                }
            }
        }
        if (id == 3)
        {
            roadLD.active = false;
            roadLU.active = false;
            roadRD.active = false;
            roadRU.active = false;
            road[0].SetActive(true);
        }
    }

    public GameObject friendlyActive;

    public void setIdActive(int id, bool tf)
    {
        if (id == -10)
        {
            friendlyActive.active = tf;
        }
        if (id == -9)
        {
            posAttack.active = tf;
        }
        if (id == -8)
        {
            //Debug.Log(tf);
            chosen.active = tf;
        }
        if (id == -6)
        {
            playerTrue.active = tf;
        }
        if (id == -5)
        {
            attack.active = tf;
        }
        if (id == -1)
        {
            top.active = tf;
        }
        if (id == -2)
        {
            bot.active = tf;
        }
        if (id == -3)
        {
            left.active = tf;
        }
        if (id == -4)
        {
            right.active = tf;
        }
        if (id == 1)
        {
            box.active = tf;
        }
        if (id == 2)
        {
            barrels.active = tf;
        }
        if (id == 4)
        {
           // Debug.Log(id);
            stone.active = tf;
        }

    }

    public  void setIdActive(int id, bool l, bool r, bool b, bool t)
    {
        if (id == 3)
        {
            wallBot.active = b;
            wallLeft.active = l;
            wallRight.active = r;
            wallTop.active = t;
        }
    }
	
	// Update is called once per frame
	void Update () {
	    
	}
}

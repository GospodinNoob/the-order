﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AI {

    public abstract PointXY GetTarget(Active act, MovebleObject attacker, MapTemplate map, PointXY pos);

    public abstract PointXY GetBestPoint(MovebleObject attacker, MapTemplate map);
	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour {

    bool hide;
    float hideTimer;

	// Use this for initialization
	void Start () {
        hide = false;
        hideTimer = -1;
	}

    public void Active()
    {
        hide = true;
        hideTimer = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
        if (hide)
        {
            if(hideTimer + 1.2f < Time.time)
            {
                Transform[] trs = this.GetComponentsInChildren<Transform>();
                foreach (var i in trs)
                {
                    if (i != this.transform)
                    {
                        i.gameObject.SetActive(false);
                    }
                }
                this.enabled = false;
            }
        }
	}
}

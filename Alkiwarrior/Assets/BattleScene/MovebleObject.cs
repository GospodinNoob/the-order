﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class MovebleObject {

    public string keyName;
    public string baseUnit;
    public string rightArmToken;
    public string leftArmToken;
    public string armourToken;
    public string itemToken;
    public int might;
    public string aiToken;
    public string[] types;

    public string team;

    public float extraParry;
    public float extraBlock;

    public int globalId;

    public Armour armour;
    public Weapon rightArm;
    public Weapon leftArm;
    public UnitBase unitBase;

    public PointXY rotation;
    public PointXY point;

    public UnitBlock unitButton = null;

    StaticData staticData;

    public List<Passive> passive;// = new List<Passive>();
    public List<Active> active;// = new List<Active>();

    public Dictionary<string, bool> state = new Dictionary<string, bool>();

    public List<Active> GetLinkToReadyAbiitiesWithAttackTag(string tag)
    {
        List<Active> ans = new List<Active>();
        foreach(var i in active)
        {
            if(staticData.ArrayHasTag(i.attackTypes, tag))
            {
                ans.Add(i);
            }
        }
        return ans;
    }

    void RedrawButton()
    {
        if (unitButton != null)
        {
            unitButton.Init(this);
        }
    }

    public MovebleObject(MovebleObject obj)
    {
        keyName = obj.keyName;
        baseUnit = obj.baseUnit;
        rightArmToken = obj.rightArmToken;
        leftArmToken = obj.leftArmToken;
        armourToken = obj.armourToken;
        itemToken = obj.itemToken;
        might = obj.might;
        types = obj.types;
        aiToken = obj.aiToken;
        Init();
    }

    public void NewPlayerUnitInit()
    {
        globalId = PlayerPrefs.GetInt("GlobalId");
        PlayerPrefs.SetInt("GlobalId", globalId + 1);
    }

    public MovebleObject(string loadKey)
    {
        string s = loadKey + "_unit";

        keyName = PlayerPrefs.GetString(s + "_keyName");
        rightArmToken = PlayerPrefs.GetString(s + "_rightArm");
        leftArmToken = PlayerPrefs.GetString(s + "_leftArm");
        armourToken = PlayerPrefs.GetString(s + "_armour");
        itemToken = PlayerPrefs.GetString(s + "_item");
        aiToken = PlayerPrefs.GetString(s + "_aiToken");
        baseUnit = "";
       // Debug.Log(itemToken);
        Init();
        unitBase = new UnitBase(s);
        baseUnit = unitBase.keyName;

        globalId = PlayerPrefs.GetInt(s + "_globalId");
    }

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        unitBase = new UnitBase(staticData.GetBaseByKeyName(baseUnit));
        armour = new Armour(staticData.GetArmourByKeyName(armourToken));
        rightArm = new Weapon(staticData.GetWeaponByKeyName(rightArmToken));
        leftArm = new Weapon(staticData.GetWeaponByKeyName(leftArmToken));
        it = new Item(staticData.GetItemByKeyName(itemToken));
        IniAI();
        state = new Dictionary<string, bool>();
        state.Add("PERMANENT", true);
        state.Add("WHILEMOVE", false);
        state.Add("ATTACK", false);
        state.Add("ATTACKING", false);
        state.Add("BLOCKING", false);
        state.Add("PARRYING", false);
        state.Add("DODGING", false);
        state.Add("TAKINGDAMAGE", false);
        state.Add("WOUNDED", false);
        state.Add("ATTACKEDFALSE", false);
        state.Add("ATTACKFALSE", false);
        state.Add("STARTTURN", false);
        state.Add("ENDTURN", false);
        //state.Add("WOUNDED", false);

        extraDamageRightHand = new Damage();
        extraDamageLeftHand = new Damage();
        extraArmour = new ArmourValue();

        active = new List<Active>();
        passive = new List<Passive>();

        extraDamageRightHand = new Damage();

        //passive.Add(new Passive(staticData.GetPassiveByKeyName("AMULHEALTH")));

        foreach (var i in rightArm.passive)
        {
            passive.Add(i);
        }

        foreach (var i in leftArm.passive)
        {
            passive.Add(i);
        }

        foreach(var i in it.passive)
        {
           // Debug.Log(i.keyName);
            passive.Add(i);
        }

        foreach(var i in it.active)
        {
            active.Add(i);
        }

        foreach (var i in armour.passive)
        {
            passive.Add(i);
        }
        foreach (var i in rightArm.active)
        {
            active.Add(i);
        }

        foreach (var i in leftArm.active)
        {
            active.Add(i);
        }

        foreach (var i in armour.active)
        {
            active.Add(i);
        }
        string s = rightArm.keyName;
        /*foreach(var i in rightArm.types)
        {
            s += i + " ";
        }
        s += leftArm.keyName;
        foreach (var i in leftArm.types)
        {
            s += i + " ";
        }*/
        UpdateAnims();
        //Debug.Log(s);
        if (staticData.ArrayHasTag(rightArm.types, "ONE") && staticData.ArrayHasTag(leftArm.types, "DOUBLE")) {
            foreach (var i in rightArm.active)
            {
                foreach (var j in leftArm.active)
                {
                    if ((i.type == "WEAPON") && (j.type == "WEAPON"))
                    {
                        active.Add(new Active(i, j, unitBase));
                        //Debug.Log(i.range + " " + j.range + " " + active[active.Count - 1].range);
                    }
                }
            }
        }
        active.Add(staticData.GetActiveByKeyName("DEFENSE"));

        InitSecondaryParams();
    }

    public void SetStatus(string status, bool active)
    {
        state[status] = active;
        RecalcBattleParams();
    }

    public Active GetLinkActiveByKeyName(string KeyName)
    {
        foreach(var i in active)
        {
            if(i.keyName == KeyName)
            {
                return i;
            }
        }
        return null;
    }

    public void ActiveAbility(PointXY attackDir, PointXY targetDir, MovebleObject attacker, Active ability)
    {
        //Debug.Log(ability.animation);
        if (ability.animation == "WEAPON")
        {
            if(new PointXY(attacker.point, this.point).len() > 1.41f)
            {
                attacker.go.GetComponent<UnitMove>().PlayAnim(attacker.unitBase.animList[5], 1);
            }
            else
            {
               // Debug.Log(attacker.unitBase.animList[4]);
                attacker.go.GetComponent<UnitMove>().PlayAnim(attacker.unitBase.animList[4], 1);
            }
        }
        else
        {
            attacker.go.GetComponent<UnitMove>().PlayAnim(ability.animation, 1);
        }
        attacker.SpendAP(ability.activationCost);
        attacker.SetStatus("ATTACK", true);
        this.SetStatus("ATTACKED", true);
        attacker.RecalcBattleParams();
        RecalcBattleParams();

        Active act = new Active(ability);
        act.Update(attacker);

        float aim = attacker.GetAim();
        float block = this.GetBlock(attacker, ability);
        float parry = this.GetParry(attacker, ability);

        double angle = Mathf.PI - Angle(targetDir, attackDir);
        double resangle = angle;
        resangle /= Mathf.PI;


        float crit = attacker.GetCrit() * (1f + (float)resangle);
        float damageCoef = (1f + (float)resangle);
        if (angle > Mathf.PI / 2 + 0.001)
        {
            angle /= Mathf.PI;
            parry *= 0;
            block *= 0;
        }
        else
        {
            angle = Mathf.PI - angle;
            angle /= Mathf.PI;
            parry *= (float)angle;
            block *= (float)angle;
        }

        act.ActiveAbility(attacker, this, damageCoef);
        attacker.SetStatus("ATTACKED", false);
        attacker.SetStatus("ATTACK", false);
        attacker.RecalcBattleParams();
        RecalcBattleParams();
    }

    public EstimateDamage EstimateDamage(PointXY attackDir, PointXY targetDir, MovebleObject attacker, Active ability)
    {
        attacker.SetStatus("ATTACKFALSE", true);
        this.SetStatus("ATTACKEDFALSE", true);
        EstimateDamage ans = new EstimateDamage();
        attacker.RecalcBattleParams();
        RecalcBattleParams();

        float aim = attacker.GetAim();
        float block = this.GetBlock(attacker, ability);
        float parry = this.GetParry(attacker, ability);

        double angle = Mathf.PI - Angle(targetDir, attackDir);
        double resangle = angle;
        resangle /= Mathf.PI;


        float crit = attacker.GetCrit() * (1f + (float)resangle);
        float damageCoef = (1f + (float)resangle);
        if (angle > Mathf.PI / 2 + 0.001)
        {
            angle /= Mathf.PI;
            parry *= 0;
            block *= 0;
        }
        else
        {
            angle = Mathf.PI - angle;
            angle /= Mathf.PI;
            parry *= (float)angle;
            block *= (float)angle;
        }

        List<Active> incomingActives = ability.GetAbilityList();

        List<EstimateDamage> est = new List<EstimateDamage>();
        //Debug.Log("list");
        foreach(var i in incomingActives)
        {
            //Debug.Log(i.keyName);
            Active buf = new Active(i);
            buf.Update(attacker);
            EstimateDamage add = new EstimateDamage(buf, this.extraArmour, damageCoef, this, attacker);
            //Debug.Log(i.keyName + " " + add.damage);
            ans.Add(add);
        }
        if (ability.combination)
        {
            ans.activationCost += ability.activationCost;
        }
        ans.hit = aim - Mathf.Max(dodgeChance, block, parry);
        ans.crit = crit;
        this.SetStatus("ATTACKEDFALSE", false);
        attacker.SetStatus("ATTACKFALSE", false);
        attacker.RecalcBattleParams();
        RecalcBattleParams();
        return ans;
    }

    public void SpendAP(int ap)
    {
        curAP -= ap;
        RedrawButton();
    }

    public void Save(string saveKey)
    {
        string s = saveKey + "_unit";

        PlayerPrefs.SetString(s + "_keyName", keyName);
        PlayerPrefs.SetString(s + "_rightArm", rightArm.keyName);
        PlayerPrefs.SetString(s + "_leftArm", leftArm.keyName);
        PlayerPrefs.SetString(s + "_armour", armour.keyName);
        PlayerPrefs.SetString(s + "_item", it.keyName);
        PlayerPrefs.SetString(s + "_aiToken", aiToken);

        PlayerPrefs.SetInt(s + "_globalId", globalId);
        unitBase.Save(s);
    }

    int lang;
    public int type;

    public RangeAttack RA;

    public bool wait;

    public HornedDemonAI hornedAI;

    public string GetMoveDescr(int id)
    {
        string s = "";
        if (id == 0)
        {
            return "";// moveText[lang];
        }
        return s;
    }

    public List<int> GetForks()
    {
        List<int> ans = new List<int>();
       // for(int i = 0; i < forks.Length; i++)
       // {
       //     if (forks[i])
       //     {
       //         ans.Add(i);
        //    }
       // }
        return ans;
    }

    public string GetBattleStatsDescr()
    {
        string s = "";
        /*MovebleObject a = this;
        int language = lang;
        s = a.damage.keyName + " (" + a.damage.piercingDamage + " " + a.damage.slashingDamage + " " + a.damage.crushingDamage + " " + a.damage.axingDamage + " "+ a.damage.fireDamage + " " + a.damage.frostDamage + " " + a.damage.shockDamage + ")"
            + '\n' + a.leftArm.keyName + " (" + a.leftArm.piercingDamage + " " + a.leftArm.slashingDamage + " " + a.leftArm.crushingDamage + " " + a.leftArm.axingDamage + " " + a.leftArm.fireDamage + " " + a.leftArm.frostDamage + " " + a.leftArm.shockDamage + ")"//);
            + '\n' + a.armour.keyName + " (" + a.armour.piercingArmour + " " + a.armour.slashingArmour + " " + a.armour.crushingArmour + " " + a.armour.axingArmour + " " + a.armour.fireArmour + " " + a.armour.frostArmour + " " + a.armour.shockArmour + ")"                                                                                                                                         /*GUI.Box(new Rect(0, high * 2, Screen.width / 2, 2 * high),
                + '\n' + '\n' + dmgBonText[language] + " " + a.bonusDamage
                + '\n' + attackCostText[language] + " " + a.GetAttackCost()//);
                                                                           /*GUI.Box(new Rect(0, high * 3, Screen.width / 2, 2 * high),
                + '\n' + rangeText[language] + " " + a.damage.realRange //);
                                                                         /*GUI.Box(new Rect(0, high * 4, Screen.width / 2, 2 * high),                                                                                                                                                               /*GUI.Box(new Rect(0, high * 5, Screen.width / 2, 2 * high),
                + '\n' + speedText[language] + " " + a.speed //);

                + '\n' + aimText[language] + " " + (100 + a.aim) + "%"
                /*GUI.Box(new Rect(0, high * 6, Screen.width / 2, 2 * high),
                + '\n' + critText[language] + " " + a.critChance + "% " + " " + dodgeText[language] + " " + a.dodgeChance + "%"

                + '\n' + blockText[language] + " " + a.blockChance + "%" + " " + parryText[language] + " " + a.parryChance + "%";*/
        return s;
    }

    public string GetEquipStats()
    {
        MovebleObject a = this;
        int language = lang;
        string s = "";
       // string s =
         //       strengthText[lang] + " " + strength
           //     + '\n' + agilityText[lang] + " " + agility
             //   + '\n' + staminaText[lang] + " " + stamina;
        return s;
    }

    public string GetPrameters()
    {
        int language = lang;
        MovebleObject a = this;
      /*  string s = strengthText[language] + " " + a.strength

                + '\n' + agilityText[language] + " " + a.agility

                + '\n' + staminaText[language] + " " + a.stamina

                + '\n' + '\n' + levelText[language] + " " + a.level + ' ' + a.exp + "/" + a.nextLevel

                + '\n' + skillpointsText[language] + " " + a.skillPoints + '\n';*/
        return "";
    }

    public bool chosenInSquad;

    public int id;

    public ArmourValue extraArmour;

    /*1 - stun
    */
    public ArrayList effects;
    public bool stunned;

    public string[] bowDescriptionPerks;
    public string[] bowNames;

    public Item it;

    public int level;
    public int exp;
    public int nextLevel;
    public int skillPoints;

    public float aim;

    public int forkCounter;

    public float extraCrit;

    public float GetCrit()
    {
        //Debug.Log(critChance);
        return extraCrit + critChance;
    }

    public void SetValueInTheLine(int a)
    {
    }

    public void GainHealth(int a)
    {
        string st;
        int b = this.curHits;
        this.curHits += a;
        this.curHits = Mathf.Min(this.curHits, this.maxHits);
        b = this.curHits - b;
        //st = healText[lang] + " " + b.ToString();
        if (go != null)
        {
           // go.GetComponent<Messenger>().ShowMessage(st);
        }
    }

    public int defense;

    int expCost;
    public int curHits;
    public int maxHits;
    public float speed;
    public int maxAP;
    public int curAP;
    public bool player;
    public string name;
    public int strength;
    public int agility;
    public int stamina;
    public int bonusDamage;
    public float critChance;
    public float dodgeChance;
    public float blockChance;
    public float parryChance;

    public GameObject go;

    public int sumPoundEquip;

    public void SetWeapon(Weapon d)
    {
        this.rightArm = new Weapon(d);
        if (this.rightArm.handStatus > 1)
        {
            SetLeftArm(staticData.GetWeaponByKeyName("FREEARM"));
        }
        if (this.rightArm.handStatus != -3)
        {
            SetItem(staticData.GetItemByKeyName("EMPTYIT"));
        }
        this.InitSecondaryParams();
        UpdateAnims();
    }

    public void SetLeftArm(Weapon d)
    {
        this.leftArm = new Weapon(d);
        if(this.leftArm.handStatus <= -2)
        {
            SetWeapon(staticData.GetWeaponByKeyName("FREEARM"));
        }
        if((this.leftArm.handStatus == -1) && (this.rightArm.handStatus > 1))
        {
            SetWeapon(staticData.GetWeaponByKeyName("FREEARM"));
        }
        this.InitSecondaryParams();
        UpdateAnims();
    }

    public void SetArmour(Armour a)
    {
        this.armour = new Armour(a);
        this.InitSecondaryParams();
    }

    public void SetItem(Item it)
    {
        this.it = new Item(it);
        this.InitSecondaryParams();
    }

    public bool TryPerk(int fork, int perk)
    {
        return false;
    }

    public bool HavePerk(int fork, int perk)
    {
        return false;
    }

    public void ExtraPoint()
    {

    }

    public string GetPerkDescr(int fork, int perk)
    {

        return "";
    }

    public string GetPerkName(int fork, int perk)
    {

        return "";
    }

    void UpLevel()
    {
        //Debug.Log(level);
        this.level++;
        this.nextLevel = 100 + (int)(100 * 0.4f * level);
    }

    public void SetExp(int a)
    {
        this.exp += a;
        while(this.exp >= this.nextLevel)
        {
            this.exp -= this.nextLevel;
            UpLevel();
        }
        if (a > 0)
        {
            if (go != null)
            {
                //go.GetComponent<Messenger>().ShowMessage(st);
            }
        }
    }

    public int sumPound;

    public void GainFork(int id)
    {

    }

    public float valueFatigue;
    public float startFatigue;
    public bool fatigueFlag;

    public void RecalcFatigue(float time)
    {
        if (time > this.valueFatigue)
        {
            this.valueFatigue = -1;
            this.fatigueFlag = false;
        }
    }

    public void SetFatigue(float time)
    {
        if (this.curHits < this.maxHits)
        {
            this.fatigueFlag = true;
            this.startFatigue = time;
            this.valueFatigue = time + 2 * (this.maxHits - this.curHits) * PlayerPrefs.GetFloat("CurExtraHeal");
        }
        else
        {
            this.valueFatigue = -1;
            this.fatigueFlag = false;
            this.curHits = this.maxHits;
        }
    }

    float GetBlock(MovebleObject move, Active ability)
    {
        return Mathf.Max(rightArm.baseBlock, leftArm.baseBlock) + extraBlock;
    }

    public bool lastAttack;

    float GetParry(MovebleObject move, Active ability)
    {
        return Mathf.Max(rightArm.baseParry, leftArm.baseParry) + extraParry;
    }

    bool plusSpeedAfterAttack;

    public bool slowed;
    public float slowedValue;
    public bool fired;

    public float GetSpeed()
    {
        float sp;
        if (plusSpeedAfterAttack)
        {
            sp = this.speed + 0.5f;
        }
        else
        {
            sp = speed;
        }
        if (frozen)
        {
            sp /= 2f;
        }
        return Mathf.Max(0, sp - slowedValue);
    }

    bool stickyTrick;
    bool sprint;
    bool killSeries;
    bool lightArmour;
    bool denied;
    bool hitAndRun;
    public int reducedDamage;

    void UpdateAnims()
    {
        unitBase.animList = staticData.GetAnimListByUnit(this);
    }

    public Damage extraDamageRightHand;
    public Damage extraDamageLeftHand;

    public void RecalcBattleParams()
    {

        this.extraAim = 0;
        extraParry = 0;
        extraBlock = 0;
        reducedDamage = 0;

        this.speed = 1.5f + unitBase.baseAgility / 20f + unitBase.baseStamina / 20f;
        this.sumPoundEquip = this.rightArm.pound + this.armour.pound + this.leftArm.pound;
        this.sumPound = unitBase.baseStamina + (int)(unitBase.baseStrength / 5);
        int pou = this.sumPound - this.sumPoundEquip;
        this.maxAP = 2;
        this.dodgeChance = Mathf.Max(0, pou * unitBase.baseAgility / 3);
        this.maxAP += (int)(pou / 5f);
        maxAP = Mathf.Max(1, maxAP);
        if (pou > 0)
        {
            this.speed += 0.2f * pou;
        }
        else
        {
            this.speed += 0.4f * pou;
        }
        this.speed = Mathf.Max(0.7f, this.speed);
        this.bonusDamage = unitBase.baseStrength / 5;
        //Debug.Log(bonusDamage);
        this.maxHits = unitBase.baseStamina + unitBase.baseStrength / 3;
        this.aim = this.agility * pou / 3 + unitBase.baseStrength / 3;
        this.blockChance = unitBase.baseAgility / 7 + unitBase.baseStrength / 4;
        this.parryChance = unitBase.baseAgility / 2;
        this.critChance = unitBase.baseAgility;
        //Debug.Log(critChance);       

        extraDamageRightHand.Clear();
        extraDamageLeftHand.Clear();
        extraArmour.Clear();

        foreach(var i in passive)
        {
            i.Active(this, this);
        }
    }

    public void DealDamage(MovebleObject attacker, Damage d)
    {
        Damage deal = new Damage(d, new ArmourValue(extraArmour));
        int sum = deal.GetSum();
        sum -= this.reducedDamage;
        if(sum > 0)
        {
            sum += attacker.bonusDamage;
        }
        sum = Mathf.Max(sum, 0);
        if(sum > 0)
        {
            state["WOUNDED"] = true;
        }
        //Debug.Log("DAMAGE " + sum);
        go.GetComponent<Messenger>().ShowMessage("DAMAGE " + sum);
        curHits -= sum;
        RecalcBattleParams();
        if (go != null)
        {
            go.GetComponentInChildren<UIUnitController>().UpdateByUnit(this);
        }
        if(curHits <= 0)
        {
            Death();
        }
    }

    bool alive = true;

    public bool isAlive()
    {
        return alive;
    }


    void Death()
    {
        //this.go.SetActive(false);
        this.go.GetComponent<Hide>().Active();
        alive = false;
    }

    public void InitSecondaryParams()
    {
        RecalcBattleParams();
        this.curAP = maxAP;
        curHits = maxHits;
        normalAP = maxAP;
        SetValueInTheLine(0);
    }

    public void SetPerk(int fork, int perk)
    {
    }

    int damageLength;
    int armourLength;

    void GenerateArchType(int type)
    {
        this.armour = new Armour(staticData.GetArmourByKeyName("FREEARM"));
        this.rightArm = new Weapon(staticData.GetWeaponByKeyName("FREEARM"));
        this.leftArm = new Weapon(staticData.GetWeaponByKeyName("FREEARM"));
        this.it = new Item(staticData.GetItemByKeyName("EMPTYIT"));
    }

    public float[] archTypeValues;

    public float GetRangeAttack()
    {
        return 0;// Mathf.Max(damage.realRange, leftArm.realRange);
    }

    public int GetRangFromRange(float range)
    {
        int atr = 1;
        if (range > 2.02)
        {
            atr = 4;
        }
        else
        {
            if (range > 1.5)
            {
                atr = 3;
            }
            else
            {
                if (range > 1.2)
                {
                    atr = 2;
                }
            }
        }
        return atr;
    }

    public int GetRangAttack()
    {
        //Debug.Log(rightArm + " " + leftArm);   
        return Mathf.Max(GetRangFromRange(rightArm.attackRange), GetRangFromRange(leftArm.attackRange));
    }

    int extraAim;
    int controlledValue;

    public float GetAim()
    {
        return 100 + aim + extraAim;
    }

    public int GetAttackCost()
    {
        if (rightArm.handStatus == 3)
        {
            return Mathf.Max(1, rightArm.pound * 2);
        }
        if (rightArm.handStatus == 2)
        {
            return Mathf.Max(1, rightArm.pound);
        }
        if (leftArm.handStatus == -1)
        {
            return Mathf.Max(1, rightArm.pound + leftArm.pound - 1);
        }
        else
        {
            return Mathf.Max(1, rightArm.pound);
        }
    }

    public int showType;

    double Angle(PointXY vectorTarget, PointXY vectorAttack)
    {
        double a1 = Mathf.Atan2(vectorAttack.x, vectorAttack.y);
        if (a1 < 0)
        {
            a1 += Mathf.PI * 2;
        }
        double a2 = Mathf.Atan2(vectorTarget.x, vectorTarget.y);
        if (a2 < 0)
        {
            a2 += Mathf.PI * 2;
        }
        double resultAngle = (a2 - a1);
        if (resultAngle < 0)
        {
            resultAngle += Mathf.PI * 2;
        }
        if (resultAngle > Mathf.PI)
        {
            resultAngle = Mathf.PI * 2 - resultAngle;
        }
        return resultAngle;
    }

    public Damage extraDmg;

    public float GetDefense(float val)
    {
        return 0;
    }

    int lenAim;

    public void SetExtraAim(float v, float v2)
    {
        //Debug.Log(v + " " + v2);
        float val = 50f;
        lenAim = (int)(-val * v / GetRangeAttack());
        extraAim = -(int)v2;
    }

    public bool frozen;
    public bool shocked;

    public float GetDodge()
    {
        return dodgeChance + shockDodge;
    }

    public bool isEmpty()
    {
        return id == 0;
    }

    int resultAim;

    int littleStrength;

    public List<MovebleObject> units;

    public AI ai = null;

    public void IniAI()
    {
        //Debug.Log(ai);
        ai = staticData.GetAiByKeyName(aiToken);
    }

    public int shockAim;
    public int shockDodge;
    public int waitAP;

    void RecalcSecondaryNewTurn()
    {
        slowedValue = 0;
        shockAim = 0;
        shockDodge = 0;
        frozen = false;
        fired = false;
        shocked = false;
        plusSpeedAfterAttack = false;
        lastAttack = false;
    }

    public void calculateNewTurn()
    {
        maxAP = 2;
        curAP = maxAP;
        return;
    }

    int normalAP;
}

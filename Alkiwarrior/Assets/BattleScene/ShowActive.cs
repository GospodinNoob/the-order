﻿using UnityEngine;
using System.Collections;

public class ShowActive : MonoBehaviour {

    public GameObject renderGO;
    //public GameObject playerID;

    public void Show (bool f)
    {
        renderGO.active = f;
        //playerID.active = f;
    }

    public void IniRender(MovebleObject move)
    {
        renderGO.GetComponent<AnimatorControllerHelp>().Ini(move);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

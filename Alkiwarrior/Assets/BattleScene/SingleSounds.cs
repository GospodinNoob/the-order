﻿using UnityEngine;
using System.Collections;

public class SingleSounds : MonoBehaviour {

    public AudioSource strikeSource;
    public AudioSource screamStrike;
    public AudioClip strike1;
    public AudioClip scream1;

	// Use this for initialization
	void Start () {
        strikeSource.volume *= PlayerPrefs.GetFloat("Volume");
        if (screamStrike != null)
        {
            screamStrike.volume *= PlayerPrefs.GetFloat("Volume");
        }
    }

    public void Play(int id)
    {
        //Debug.Log(id);
        if (id == 1)
        {
            //Debug.Log(1);
            strikeSource.PlayOneShot(strike1, 0.5f);
        }
        if (id == 2)
        {
           // Debug.Log(2);
            screamStrike.PlayOneShot(scream1, 0.23f);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

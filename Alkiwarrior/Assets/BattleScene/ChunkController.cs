﻿using UnityEngine;
using System.Collections;

public class ChunkController{

    string[] rusCover = {"Трава", "Грязь", "Мостовая"};
    string[] engCover = { "Grass", "Mud", "Roadway" };

    string[] cover;

    public bool canMove(MovebleObject check)
    {
        if (this.obj.isEmpty() && (this.movebleObject.isEmpty() || (this.movebleObject == check)))
        {
            return true;
        }
        return false;
    }

    public bool isSmallObj()
    {
        if((this.obj.smallBlock) || (this.movebleObject.player))
        {
            return true;
        }
        return false;
    }

    public Object obj;
    public MovebleObject movebleObject;
    public string landCover;
    public int landCoverId;
    public float moveCost;

    void Generate()
    {
        int lang = PlayerPrefs.GetInt("Language");
        if (lang == 0)
        {
            cover = engCover;
        }
        if (lang == 1)
        {
            cover = rusCover;
        }
    }

    public ChunkController()
    {
        this.obj = new Object();
        //this.movebleObject = new MovebleObject();
        landCover = "Grass";
        Generate();
    }

    public ChunkController(int a)
    {
        this.obj = new Object(a);
        //this.movebleObject = new MovebleObject();
        landCover = "Grass";
        Generate();
    }

    public ChunkController(int a, int b, bool tf)
    {
        this.obj = new Object(a);
        //this.movebleObject = new MovebleObject(b, tf, PlayerPrefs.GetInt("Language"), new GameEvent("ChosenEvent", PlayerPrefs.GetInt("Langugage")));
        landCover = "Grass";
        Generate();
    }

    public void SetLandCover(int s)
    {
        landCoverId = s;
        landCover = cover[landCoverId - 1];
        if (s == 1)
        {
            moveCost = 1;
        }
        if (s == 2)
        {
            moveCost = 1.4f;
        }
        if (s == 3)
        {
            moveCost = 0.7f;
        }
    }
}

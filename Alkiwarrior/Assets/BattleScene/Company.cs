﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company {

    List<Squad> squads;
    public float speed;
    public MapTemplate map;

    StaticData staticData;

    public float GetSpeed()
    {
        float ans = 1000000000000;
        foreach(var i in squads)
        {
            ans = Mathf.Min(i.GetSpeed());
        }
        return ans;
    }

    public Company(int might, string tag)
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        Squad sq = staticData.GetRandomSquadWithMightByTag(0, might, tag);
        while (sq != null)
        {
            squads.Add(sq);
            sq = staticData.GetRandomSquadWithMightByTag(0, might, tag);
        }
        speed = GetSpeed();
        map = new MapTemplate();
    }

    public void Save()
    {



    }

}

﻿using UnityEngine;
using System.Collections;

public class CameraSize : MonoBehaviour {

    private Camera cameraGO;

	// Use this for initialization
	void Start () {
        cameraGO = this.gameObject.GetComponent<Camera>();
        int n = 6;
        int k = 8;
        float dx = Screen.width / n;
        float dy = Screen.height / k;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }
        dx = Screen.width / 6;
        float orthographicSize = Screen.height / 2;
        if (orthographicSize != cameraGO.orthographicSize)
        {
            cameraGO.orthographicSize = orthographicSize;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

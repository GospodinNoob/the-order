﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderArmatueHelper : MonoBehaviour {

    public GameObject[] objects;

    void Awake()
    {
        for(int i = 0; i < objects.Length; i++)
        {
            objects[i].active = true;
        }
        SpriteRenderer[] sp = this.transform.GetComponentsInChildren<SpriteRenderer>();
        foreach (var i in sp)
        {
            i.sortingLayerName = "Units";
           // Debug.Log(i.sortingLayerName);
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleParticles : MonoBehaviour
{
    public float ScaleSize = 1.0f;
    public float myScaleConst; //write your own const, which you need in 
    List<float> initialSizes = new List<float>();
    List<float> initialLength = new List<float>();
    List<float> initialVelocity= new List<float>();

    void Awake()
    {

        ParticleSystem[] particles = gameObject.GetComponentsInChildren<ParticleSystem>();
        foreach (ParticleSystem particle in particles)
{
            initialSizes.Add(particle.startSize);
            ParticleSystemRenderer renderer = particle.GetComponent<ParticleSystemRenderer>();
            if(renderer)
{
                initialLength.Add(renderer.lengthScale);
                initialVelocity.Add(renderer.velocityScale);
            }
        }
    }

    void Start()
    {
        this.gameObject.transform.localScale = new Vector3(ScaleSize, ScaleSize, ScaleSize);
        int arrayIndex = 0;
        ParticleSystem[] particles = gameObject.GetComponentsInChildren<ParticleSystem>();
        Debug.Log(this.gameObject.transform.parent.localScale.magnitude * myScaleConst);
        foreach (ParticleSystem particle in particles)
        {
            particle.startSize = initialSizes[arrayIndex] * gameObject.transform.parent.localScale.magnitude * myScaleConst;
            ParticleSystemRenderer renderer = particle.GetComponent<ParticleSystemRenderer>();
            if (renderer)
            {
                renderer.lengthScale = initialLength[arrayIndex] * this.gameObject.transform.parent.localScale.magnitude * myScaleConst;
                renderer.velocityScale = initialVelocity[arrayIndex] * this.gameObject.transform.parent.localScale.magnitude * myScaleConst;
            }
            arrayIndex++;
        }
    }

}
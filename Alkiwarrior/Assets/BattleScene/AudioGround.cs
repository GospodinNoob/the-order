﻿using UnityEngine;
using System.Collections;

public class AudioGround : MonoBehaviour {

    public AudioSource grass;
    public AudioSource mud;
    public AudioSource road;

	// Use this for initialization
	void Start () {
        Mute();
        road.pitch = 0.52f;
        grass.volume *= PlayerPrefs.GetFloat("Volume");
        mud.volume *= PlayerPrefs.GetFloat("Volume");
        road.volume *= PlayerPrefs.GetFloat("Volume");
    }

    public void Mute()
    {
        grass.mute = true;
        mud.mute = true;
        road.mute = true;
    }

    public void SetActive(int id)
    {
        Mute();
        //Debug.Log(i);
        if (id == 1)
        {
            grass.mute = false;
        }
        if (id == 2)
        {
            mud.mute = false;
        }
        if (id == 3)
        {
            road.mute = false;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

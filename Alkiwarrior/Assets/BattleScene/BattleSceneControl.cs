﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
//using UnityEngine.Advertisements;

public class BattleSceneControl : MonoBehaviour {
    /*

    public Texture2D defense, healhPotion;

    public GameObject humanGO;
    GameObject[][] chunks;
    public GameObject chunkPrefab;
    int n, k;
    float deltaX;
    int magicConstant = 53;

    int maxPlayerUnits;
    int maxEnemyUnits;
    int maxUnits;

    public GameObject cam;

    Vector3 defaultCameraPosition;
    public AudioSource au;

    bool gameResult;
    /*
    IEnumerator ShowAdWhenReady()
    {
        while (!Advertisement.IsReady())
            yield return null;
        var options = new ShowOptions { resultCallback = HandleShowResult };
        Advertisement.Show(options);
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                loadWindow = true;
                Application.LoadLevelAsync("BattleScene");
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }





    ArrayList way;

    bool enemyTurn;


    Chunk[][] map;

    Chunk chosenChunk;
    PointXY chosenPoint;
    bool move = false;
    bool attack = false;

    float dx;
    float dy;

    public GUIStyle coverButtonStyle;

    //GameObject[] playerUnitsGO;
    //Unit[] playerUnits;

    //GameObject[] enemyUnitsGO;
    //Unit[] enemyUnits;
    Unit[] units;
    GameObject[] unitsGO;

    float[][] dist;
    //List<List<float>> dist;
    float[][] costMatrix;
    PointXY[][] parents;
    bool[][] flags;

    Unit moveO;

    int mapHeight;
    int mapWidth;
    int shiftX;
    int shiftY;

    GameEvent ev;

    void Rotation(GameObject go, int vx, int vy)
    {
        // Debug.Log(vx.ToString() + " " + vy.ToString());
        float angle = Mathf.Atan2(vx, -vy);
        angle *= 57.2958f;
        go.GetComponent<HelperScript>().renderGO.gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        //Debug.Log(angle);
    }

    bool InMap(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= mapWidth)
        {
            return false;
        }
        if (b >= mapHeight)
        {
            return false;
        }
        return true;
    }

    bool Valid(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= mapWidth)
        {
            return false;
        }
        if (b >= mapHeight)
        {
            return false;
        }
        if (map[a][b].obj.isEmpty() && map[a][b].movebleObject.isEmpty())
        {
            return true;
        }
        return false;
    }

    float Len(int a, int b, int c, int d, float v1, float v2)
    {
        return Mathf.Sqrt((a - c) * (a - c) + (b - d) * (b - d)) * (v1 + v2) / 2;
    }

    public void DealDamage(Unit move, int x, int y)
    {
        blockCounter--;
        block = blockCounter != 0;
        // Debug.Log(blockCounter + " " + 1);
        move.SetRotation(new PointXY(x - move.point.x, y - move.point.y));
     /*   if (activeAction == 1)
        {
            for(int i = 0; i < mapWidth; i++)
            {
                for(int j = 0; j < mapHeight; j++)
                {
                    if (RA.InRangeAttack(move.point, new PointXY(x, y), move.unit, activeAction))
                    {
                        if (!map[x][y].obj.isEmpty())
                        {
                            map[x][y].obj.DealDamage(move.unit);
                            if (map[x][y].obj.id == 0)
                            {
                                chunks[x][y].GetComponent<SpriteActive>().setIdActive(1, false);
                            }
                        }
                        if (!map[x][y].movebleObject.isEmpty())
                        {
                            GetUnit(new PointXY(x, y)).unit.DealDamage(move.unit, GetUnit(new PointXY(x, y)).rotation, move.rotation, LenPoints(move.point, new PointXY(x, y)));
                            // map[x][y].movebleObject.DealDamage(move, map[x][y].movebleObject.);
                        }
                    }
                }
            }
        }
        else
        {

        
        
            if (!map[x][y].obj.isEmpty())
            {
                map[x][y].obj.DealDamage(move.unit);
                if (map[x][y].obj.id == 0)
                {
                    chunks[x][y].GetComponent<SpriteActive>().setIdActive(1, false);
                }
            }
            if (!map[x][y].movebleObject.isEmpty())
            {
                GetUnit(new PointXY(x, y)).unit.DealDamage(move.unit, GetUnit(new PointXY(x, y)).rotation, move.rotation, LenPoints(move.point, new PointXY(x, y)));
                // map[x][y].movebleObject.DealDamage(move, map[x][y].movebleObject.);
            }
        }
        if (!enemyTurn)
        {
            CheckNextTurn();
        }
    }

    int INF = 10000000;


    void Dijkstra(PointXY p1)
    {
        PointXY p = new PointXY(p1);
        int n = mapWidth;
        int k = mapHeight;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < k; j++)
            {
                dist[i][j] = INF;
                flags[i][j] = false;
            }
        }

        dist[p.x][p.y] = 0;
        int step = 0;
        while ((dist[p.x][p.y] < INF - 1) && (step < 500))
        {
            step++;
            // Dep.yug.Log(dist[p.x][p.y]);
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    // Dep.yug.Log(Vp.xlid(p.x + i, p.y + j));
                    if (!((i != 0) && (j != 0)))
                    {
                        if (Valid(p.x + i, p.y + j) && (dist[p.x + i][p.y + j] > dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost)))
                        {

                            dist[p.x + i][p.y + j] = dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost);
                            //Debug.Log(p.x + " " + p.y);
                            parents[p.x + i][p.y + j] = new PointXY(p);
                        }
                    }
                    else
                    {
                        if (Valid(p.x + i, p.y + j) && (map[p.x + i][p.y].movebleObject.isEmpty()) && (map[p.x][p.y + j].movebleObject.isEmpty()) && (map[p.x + i][p.y].obj.isEmpty()) && (map[p.x][p.y + j].obj.isEmpty()) && (dist[p.x + i][p.y + j] > dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost)))
                        {

                            dist[p.x + i][p.y + j] = dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost);
                            //Debug.Log(p.x + " " + p.y);
                            parents[p.x + i][p.y + j] = new PointXY(p);
                        }
                    }
                }
            }

            flags[p.x][p.y] = true;

            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
               //     if ((Mathf.Abs(p1.x - i) <= 1) && (Mathf.Abs(p1.y - j) <= 1) && map[i][j].movebleObject.isEmpty() && map[i][j].obj.isEmpty())
                //    {
                 //       if (!p1.isEqual(new PointXY(i, j)))
                  //      {
                   //         dist[i][j] = 0.5f;
                    //    }
                //  /  }
                }
            }
            float min = INF;
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if ((dist[i][j] < min) && (!flags[i][j]))
                    {
                        p.x = i;
                        p.y = j;
                        min = dist[i][j];
                    }
                }
            }

        }



    }

    public float GetScale()
    {
        return unitsGO[0].transform.localScale.x;
    }

    void GenEnemyUnit(int i, int a, int b)
    {
        i += maxPlayerUnits;
        units[i] = new Unit(a, b, false, new PointXY(0, -1), ev);
        Transform tr = this.gameObject.transform;
        unitsGO[i] = (GameObject)GameObject.Instantiate(humanGO, tr);
        unitsGO[i].GetComponent<UnitMove>().SetDx(dx);
        unitsGO[i].GetComponent<UnitMove>().SetMain(this.gameObject, cam, units[i]);
        float shift = Screen.height / 8;
        if (Screen.height / 8 <= Screen.width / 6)
        {
            shift = 0;
        }
        unitsGO[i].GetComponent<HelperScript>().SetPlayerInd(false);
        unitsGO[i].name = units[i].unit.name;
        unitsGO[i].transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * a, tr.position.y - dx / 2 + shift - dx + dx * b, tr.position.z);
        ////Debug.Log('*');
        Rotation(unitsGO[i], units[i].rotation.x, units[i].rotation.y);
        //Debug.Log('*');
        float scale = unitsGO[i].transform.localScale.x * dx;
        unitsGO[i].transform.localScale = new Vector3(scale, scale, scale);
        map[a][b].movebleObject = units[i].unit;
        map[a][b].movebleObject.un = units[i];
        units[i].unit.units = units;
        units[i].unit.dist = dist;
        units[i].unit.costMatrix = costMatrix;
        units[i].unit.map = map;
        units[i].unit.IniAI();
        units[i].unit.go = unitsGO[i];
        unitsGO[i].GetComponent<ShowActive>().IniRender(units[i].unit);
        units[i].unit.curAP = 0;
        units[i].unit.RecalcHp();
        units[i].unit.RecalcAP();
        //Debug.Log('*');
    }

    void GenerateEnemySquad(int a, int b, bool rotation)
    {
        int squadUnitCount = 0;
        if (curEnemyUnits <= 6)
        {
            squadUnitCount = curEnemyUnits;
        }
        else
        {
            squadUnitCount = Random.Range(4, 7);
        }
        curEnemyUnits -= squadUnitCount;
        if (rotation)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenEnemyUnit(curEnemyUnits + squadUnitCount - 1, a + i, b + j);
                        squadUnitCount--;
                        // curEnemyUnits--;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenEnemyUnit(curEnemyUnits + squadUnitCount - 1, a + i, b + j);
                        squadUnitCount--;
                        //  curEnemyUnits--;
                    }
                }
            }
        }
    }

    int curEnemyUnits;

    bool EmptyChunkForGen(int x, int y)
    {
        if (InMap(x, y))
        {
            return map[x][y].movebleObject.isEmpty() && (!map[x][y].obj.permanent);
        }
        else
        {
            return false;
        }
    }

    bool EmptySpaceForSquad(int a, int b, bool tf)
    {
        bool flag = true;
        if (tf)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    flag = flag && EmptyChunkForGen(a + i, b + j);
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    flag = flag && EmptyChunkForGen(a + i, b + j);
                }
            }
        }
        return flag;
    }

    void GenerateEnemies()
    {
        int a = Random.Range(0, mapWidth);
        int b = Random.Range(0, mapHeight);
        bool rotation = true;
        if (Random.Range(0, 2) == 0)
        {
            rotation = false;
        }
        int step = 0;
        curEnemyUnits = maxEnemyUnits;
        while ((curEnemyUnits > 0) && (step < 50))
        {
            step = 0;
            while (!EmptySpaceForSquad(a, b, rotation) && (step < 50))
            {
                a = Random.Range(0, mapWidth);
                b = Random.Range(0, mapHeight);
                rotation = true;
                if (Random.Range(0, 2) == 0)
                {
                    rotation = false;
                }
                step++;
            }
            //Debug.Log(step);
            if (step < 50)
            {
                if (rotation)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        for (int j = 0; j <= 2; j++)
                        {
                            map[a + i][b + j].obj.Clear();
                        }
                    }
                }
                else
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        for (int j = 0; j <= 1; j++)
                        {
                            map[a + i][b + j].obj.Clear();
                        }
                    }
                }
                //Debug.Log(rotation);
                GenerateEnemySquad(a, b, rotation);
            }
            //curEnemyUnits--;
        }
    }

    void SecondaryGroundGeneration(int a, int b, int id)
    {
        int curMax = a / b;
        Queue<PointXY> que;
        que = new Queue<PointXY>();
        PointXY p;
        while (a > 6)
        {
            int localMax = curMax;
            que.Clear();
            p = (new PointXY((int)Random.Range(0, mapWidth), (int)Random.Range(0, mapHeight)));
            //Debug.Log(p.x + " " + (p.y));
            while (!Valid(p.x, p.y))
            {
                p = (new PointXY((int)Random.Range(0, mapWidth), (int)Random.Range(0, mapHeight)));
                //   Debug.Log(p.x + " " + (p.y));
            }
            que.Enqueue(p);
            int step = 0;
            //Debug.Log(p.x + " " + (p.y));
            while ((a > 0) && (localMax > 0) && (step < 50) && (que.Count != 0))
            {
                step++;
                p = que.Dequeue();
                if (map[p.x][p.y].landCoverId != id)
                {
                    //   Debug.Log(p.x + " " + p.y);
                    //Debug.Log(name);
                    map[p.x][p.y].SetLandCover(id);
                    chunks[p.x][p.y].GetComponent<SpriteActive>().setGround(id);
                    //Destroy(this.gameObject.transform.root.gameObject);
                    //Debug.Log(name);
                    float rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x + 1, p.y) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x + 1, p.y));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x - 1, p.y) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x - 1, p.y));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x, p.y - 1) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x, p.y - 1));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x, p.y + 1) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x, p.y + 1));
                    }
                    a--;
                    localMax--;
                }
            }
        }
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if (map[i][j].landCoverId == id)
                {
                    if (InMap(i - 1, j))
                    {
                        if (map[i - 1][j].landCoverId != map[i][j].landCoverId)
                        {
                            chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(2, map[i][j].landCoverId - 1);
                        }
                    }
                    if (InMap(i + 1, j))
                    {
                        if (map[i + 1][j].landCoverId != map[i][j].landCoverId)
                        {
                            chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(1, map[i][j].landCoverId - 1);
                        }
                    }
                    if (InMap(i, j + 1))
                    {
                        if (map[i][j + 1].landCoverId != map[i][j].landCoverId)
                        {
                            chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(0, map[i][j].landCoverId - 1);
                        }
                    }
                    if (InMap(i, j - 1))
                    {
                        if (map[i][j - 1].landCoverId != map[i][j].landCoverId)
                        {
                            chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(3, map[i][j].landCoverId - 1);
                        }
                    }
                }
            }
        }
    }

    string[] baseGround = { "Grass", "Mud" };

    float coef = (Screen.height / Screen.width);
    float dc;

    void Awake()
    {
        n = 6;
        k = 8;
        dx = Screen.width / n;
        dy = Screen.height / k;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }
        // Debug.Log(dx);
        deltaX = Screen.width - 6 * dx;
        deltaX /= 2;
        dc = -dx / 2 * 3; ;
        /* if ((int)(Screen.height / 8) != (int)dx)
         {
             dc += dx * (Screen.height / 8) / dx;
         }

        //Screen.SetResolution((int)(6 * dx), (int)(8 * dx), false);
    }

    // Use this for initialization

    int language;

    void WallGenerator()
    {
        int start = Random.Range(0, 4);
        int finish = Random.Range(0, 4);
        PointXY pS = new PointXY(0, 0);
        PointXY vec = new PointXY(0, 0);
        if (start == 0)
        {
            pS.x = 0;
            pS.y = Random.Range(0, mapHeight);
            vec.x = 1;
        }
        if (start == 1)
        {
            pS.x = mapWidth - 1;
            pS.y = Random.Range(0, mapHeight);
            vec.x = -1;
        }
        if (start == 2)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = 0;
            vec.y = 1;
        }
        if (start == 3)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = mapHeight - 1;
            vec.y = -1;
        }
        int t = 0;
        // Debug.Log((pS.x + vec.x * t).ToString() + " " + (pS.y + vec.y * t).ToString());
        //Debug.Log((vec.x).ToString() + " " + (vec.y).ToString());
        bool flag = false;
        while (InMap(pS.x + vec.x * t, pS.y + vec.y * t))
        {
            if (map[pS.x + vec.x * t][pS.y + vec.y * t].landCoverId != 3)
            {
                //  chunks[pS.x + vec.x * t][pS.y + vec.y * t].GetComponent<SpriteActive>().setIdActive(map[pS.x + vec.x * t][pS.y + vec.y * t].obj.id, false);
                //map[pS.x + vec.x * t][pS.y + vec.y * t].obj.Clear();
                map[pS.x + vec.x * t][pS.y + vec.y * t].obj = new Object(3);
                map[pS.x + vec.x * t][pS.y + vec.y * t].obj.go = chunks[pS.x + vec.x * t][pS.y + vec.y * t];
                //chunks[pS.x + vec.x * t][pS.y + vec.y * t].GetComponent<SpriteActive>().setIdActive(map[pS.x + vec.x * t][pS.y + vec.y * t].obj.id, true);
                //Debug.Log((pS.x + vec.x * t).ToString() + " " + (pS.y + vec.y * t).ToString());
            }
            else
            {
                flag = true;
            }
            ++t;
        }
        int cou = 0;
        int len = t;
        int step = 0;
        t = 0;
        while (InMap(pS.x + vec.x * t, pS.y + vec.y * t))
        {
            //Debug.Log(t);
            if (map[pS.x + vec.x * t][pS.y + vec.y * t].obj.id == 3)
            {

                bool l = false;
                if (InMap(pS.x + vec.x * t - 1, pS.y + vec.y * t))
                {
                    l = map[pS.x + vec.x * t - 1][pS.y + vec.y * t].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapWidth - 1)) && (vec.x != 0))
                    {
                        l = true;
                    }
                }
                bool r = false;
                if (InMap(pS.x + vec.x * t + 1, pS.y + vec.y * t))
                {
                    r = map[pS.x + vec.x * t + 1][pS.y + vec.y * t].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapWidth - 1)) && (vec.x != 0))
                    {
                        r = true;
                    }
                }
                bool b = false;
                if (InMap(pS.x + vec.x * t, pS.y + vec.y * t - 1))
                {
                    b = map[pS.x + vec.x * t][pS.y + vec.y * t - 1].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapHeight - 1)) && (vec.y != 0))
                    {
                        b = true;
                    }
                }
                bool to = false;
                if (InMap(pS.x + vec.x * t, pS.y + vec.y * t + 1))
                {
                    to = map[pS.x + vec.x * t][pS.y + vec.y * t + 1].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapHeight - 1)) && (vec.y != 0))
                    {
                        to = true;
                    }
                }
                chunks[pS.x + vec.x * t][pS.y + vec.y * t].GetComponent<SpriteActive>().setIdActive(3, l, r, b, to);
                // Debug.Log(t);
                cou++;
            }
            ++t;
        }
        t = 0;
        step = 0;
        while ((!flag) && (step < 2000))
        {
            t = 0;
            step++;
            while (InMap(pS.x + vec.x * t, pS.y + vec.y * t))
            {
                if (Random.Range(0f, 1f) < (2f / len))
                {
                    map[pS.x + vec.x * t][pS.y + vec.y * t].obj = new Object(0);
                    map[pS.x + vec.x * t][pS.y + vec.y * t].obj.go = chunks[pS.x + vec.x * t][pS.y + vec.y * t];
                    chunks[pS.x + vec.x * t][pS.y + vec.y * t].GetComponent<SpriteActive>().setIdActive(3, false, false, false, false);
                    //   Debug.Log(t);
                    cou++;
                    flag = true;
                }
                ++t;
            }
            //   Debug.Log(flag);
        }
    }

    RangeAttack RA;

    public Texture2D fonTex;
    public Texture2D overTex;

    void Start() {
        IniTech();
        IniMap();
        StartIni();
        shiftAbilities = Screen.width / 2 - ((Screen.height - dy * 6.0f) / 4.0f) * 5;
    }

    bool ini;

    void IniTech()
    {
        activity = Activity.Myself;
        RA = new RangeAttack();
        confrimAction = PlayerPrefs.GetInt("ConfrimAction") == 1;
        activeAction = Ability.Defense;
        returnedToUnit = false;
        //move = true;
        ///Debug.Log(au.volume);
        au.volume *= PlayerPrefs.GetFloat("Volume");
        // Debug.Log(au.volume);
        showEnemyList = false;
        showYourList = false;
        returnWait = false;
        cameraMove = false;
        cameraTimer = -1;
        cameraSpeed = -1;

        language = PlayerPrefs.GetInt("Language");

        loadWindow = false;
        lHelp = new LoadHelpObject(language);

        extraUnit = new Unit();
        extraUnit.unit.id = -100;
        p = new PointXY(0, 0);

        int fontSize = 18;

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;

        ev = new GameEvent("ChosenEvent", language);
        mapHeight = ev.height;
        mapWidth = ev.width;
        //Debug.Log(mapWidth + " " + mapHeight);
        enemyTurn = false;
        defaultCameraPosition = new Vector3(cam.transform.position.x, cam.transform.position.y, cam.transform.position.z);
        maxPlayerUnits = PlayerPrefs.GetInt("PlayerTeamCounter");
        maxEnemyUnits = ev.enemyCounter;
        maxUnits = maxPlayerUnits + maxEnemyUnits;
        units = new Unit[maxUnits];
        //Debug.Log((Screen.height / 8) / dx);
        dist = new float[mapWidth][];
        //dist = new List<List<float>>();
        costMatrix = new float[mapWidth][];
        timer = new float[mapWidth][];
        parents = new PointXY[mapWidth][];
        flags = new bool[mapWidth][];
        map = new Chunk[mapWidth][];
        chunks = new GameObject[mapWidth][];
        unitsGO = new GameObject[maxUnits];
    }

    void IniMap()
    {
        int groundId = ev.baseGroundId;
        for (int i = 0; i < mapWidth; i++)
        {
            map[i] = new Chunk[mapHeight];
            timer[i] = new float[mapHeight];
            dist[i] = new float[mapHeight];
            //dist.Add(new List<float>());
            costMatrix[i] = new float[mapHeight];
            parents[i] = new PointXY[mapHeight];
            flags[i] = new bool[mapHeight];
            chunks[i] = new GameObject[mapHeight];
            for (int j = 0; j < mapHeight; j++)
            {
                //dist[i].Add(0);
                map[i][j] = new Chunk();
                Transform tr = this.gameObject.transform;
                chunks[i][j] = (GameObject)GameObject.Instantiate(chunkPrefab, tr);
                chunks[i][j].GetComponent<SpriteActive>().setGround(groundId);
                chunks[i][j].GetComponent<ObjectEffects>().SetMain(this.gameObject);
                map[i][j].SetLandCover(groundId);
                map[i][j].obj.go = chunks[i][j];
                float shift = Screen.height / 8;
                if (Screen.height / 8 <= Screen.width / 6)
                {
                    shift = 0;
                }
                chunks[i][j].GetComponent<CoordinatsHelper>().point = new PointXY(i, j);
                chunks[i][j].transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * i, tr.position.y - dx / 2 + shift - dx + dx * j, tr.position.z);
                //Debug.Log(dx);
                float scale = chunks[i][j].transform.localScale.x * dx;
                chunks[i][j].transform.localScale = new Vector3(scale, scale, scale);
                chunks[i][j].GetComponent<SpriteActive>().setIdActive(1, false);
            }
        }

        int objectNumber = (int)Random.Range(0.1f * mapHeight * mapWidth, 0.3f * mapWidth * mapHeight);

        int secondaryTypePoint = (int)Random.Range(0.1f * mapHeight * mapWidth, (0.35f * mapHeight * mapWidth));

        int s = Random.Range(1, baseGround.Length + 1);

        int step = 0;
        while (((s == groundId)) && (step < 50))
        {
            step++;
            s = Random.Range(1, baseGround.Length + 1);
        }
        int secondaryGenerationsPoint = Random.Range(1, secondaryTypePoint / 6);


        SecondaryGroundGeneration(secondaryTypePoint, secondaryGenerationsPoint, s);

        if (Random.Range(0f, 1f) < 0.4f)
        {
            RoadGeneration();
            if (Random.Range(0f, 1f) < 0.2f)
            {
                RoadGeneration();
            }
            {
                WallGenerator();
            }
        }
        for (int i = 0; i < objectNumber; i++)
        {
            int a = Random.Range(0, mapWidth);
            int b = Random.Range(0, mapHeight);
            if (map[a][b].obj.isEmpty() && (map[a][b].landCoverId != 3))
            {
                if (Random.Range(0f, 1f) < 0.2f)
                {
                    map[a][b].obj.Generate(4);
                    map[a][b].obj.go = chunks[a][b];
                    //Debug.Log(a + " " + b);
                }
                else
                {
                    map[a][b].obj.Generate(Random.Range(1, 3));
                    map[a][b].obj.go = chunks[a][b];
                }
                chunks[a][b].GetComponent<SpriteActive>().setIdActive(map[a][b].obj.id, true);
                map[a][b].SetObjectLink(chunks[a][b]);
                map[a][b].obj.go = chunks[a][b];
                //map[a][b].obj.SetBurnedStatus(3);
            }
        }
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                dist[i][j] = INF;
            }
        }
        int cou = 0;
        for (int i = 1; i < mapWidth - 1; i++)
        {
            if(FreeSpaceForSpawn(new PointXY(i, 0)))
            {
                cou++;
            }
            if (FreeSpaceForSpawn(new PointXY(i, mapHeight - 1)))
            {
                cou++;
            }
        }
        for(int j = 1; j < mapHeight - 1; j++)
        {
            if (FreeSpaceForSpawn(new PointXY(0, j)))
            {
                cou++;
            }
            if (FreeSpaceForSpawn(new PointXY(mapWidth - 1, j)))
            {
                cou++;
            }
        }
        int len = (mapWidth * 2 + mapHeight * 2 - 4);
        step = 0;
        while (cou <= (mapWidth * 2 + mapHeight * 2 - 4) * 0.3f)
        {
            cou = 0;
            step++;
            if(step > 50)
            {
                break;
            }
            for (int i = 1; i < mapWidth - 1; i++)
            {
                if (SpaseWithoutUnits(new PointXY(i, 0)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(i, 0));
                }
                if (SpaseWithoutUnits(new PointXY(i, mapHeight - 1)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(i, mapHeight - 1));
                }
            }
            for (int j = 1; j < mapHeight - 1; j++)
            {
                if (SpaseWithoutUnits(new PointXY(0, j)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(0, j));
                }
                if (SpaseWithoutUnits(new PointXY(mapWidth - 1, j)) && (Random.Range(0, len) == 0))
                {
                    ClearSpaceForSquad(new PointXY(mapWidth - 1, j));
                }
            }
            for (int i = 1; i < mapWidth - 1; i++)
            {
                if (FreeSpaceForSpawn(new PointXY(i, 0)))
                {
                    cou++;
                }
                if (FreeSpaceForSpawn(new PointXY(i, mapHeight - 1)))
                {
                    cou++;
                }
            }
            for (int j = 1; j < mapHeight - 1; j++)
            {
                if (FreeSpaceForSpawn(new PointXY(0, j)))
                {
                    cou++;
                }
                if (FreeSpaceForSpawn(new PointXY(mapWidth - 1, j)))
                {
                    cou++;
                }
            }
            Debug.Log(cou);
        }
    }

    bool play;

    void Spawn(PointXY p)
    {
        Debug.Log(p.x + " " + p.y);
        int width = 0;
        if((p.y == 0) || (p.y == mapHeight - 1))
        {
            width = 3;
        }
        else
        {
            width = 2;
        }
        int height = 5 - width;
        int pointX = p.x;
        int pointY = p.y;
        if(p.x == 0)
        {
            pointY--;
        }
        else
        {
            if(p.y == 0)
            {
                pointX--;
            }
            else
            {
                pointX--;
                pointY--;
            }
        }
      //  Debug.Log(pointX + " " + pointY);
        //Shift(pointX + (int)(width / 2), pointY);
      //  Debug.Log(maxPlayerUnits);
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            int a = Random.Range(pointX, pointX + width);
            int b = Random.Range(pointY, pointY + height);
            while (!map[a][b].obj.isEmpty() || !map[a][b].movebleObject.isEmpty())
            {
                a = Random.Range(pointX, pointX + width);
                b = Random.Range(pointY, pointY + height);
            }
           // Debug.Log(i);
            //playerUnits[i] = new Unit(a, b, "PlayerUnit" + i.ToString(), true, new PointXY(0, 1));
            units[i] = new Unit(a, b, "PlayerUnit" + i.ToString(), true, new PointXY(0, 1));
            //  Debug.Log(units[i].unit.name + " " + units[i].unit.abilities[0] + " " +
            //            units[i].unit.abilities[1] + " " +
            //            units[i].unit.abilities[2] + " " +
            //           units[i].unit.abilities[3]);
            //Debug.Log(2);
            //Debug.Log(playerUnits[i]);
            Transform tr = this.gameObject.transform;
            unitsGO[i] = (GameObject)GameObject.Instantiate(humanGO, tr);
            //playerUnitsGO[i] = (GameObject)GameObject.Instantiate(humanGO, tr);
            float shift = Screen.height / 8;
            if (Screen.height / 8 <= Screen.width / 6)
            {
                shift = 0;
            }
            unitsGO[i].GetComponent<HelperScript>().SetPlayerInd(true);
            unitsGO[i].name = units[i].unit.name;
            unitsGO[i].transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * a, tr.position.y - dx / 2 + shift - dx + dx * b, tr.position.z);
            float scale = unitsGO[i].transform.localScale.x * dx;
            Rotation(unitsGO[i], units[i].rotation.x, units[i].rotation.y);
            unitsGO[i].transform.localScale = new Vector3(scale, scale, scale);
            map[a][b].movebleObject = units[i].unit;
            map[a][b].movebleObject.un = units[i];
            //Debug.Log(3);
            units[i].unit.go = unitsGO[i];
            unitsGO[i].GetComponent<ShowActive>().IniRender(units[i].unit);
            units[i].unit.RecalcHp();
            units[i].unit.RecalcHp();
            unitsGO[i].GetComponent<UnitMove>().SetDx(dx);
            unitsGO[i].GetComponent<UnitMove>().SetMain(this.gameObject, cam, units[i]);
            //playerUnits[i].unit.SetBurnedStatus(3);
            //Debug.Log(playerUnits[i].unit.freeSpace);
        }
        CheckInTheLine();
        RecalcPlayerTurn();
        play = true;
    }

    void StartIni()
    {
        play = false;

        //Debug.Log(4);
        GenerateEnemies();

        chosenChunk = map[0][0];
        cam.GetComponent<Camera>().orthographicSize /= PlayerPrefs.GetFloat("CameraSize");
        RA.map = map;
        //   Debug.Log('-');
    }

    bool EmptyChunksForGenPlayer(int a, int b, int x, int y)
    {
        bool ans = true;
        x += a;
        y += b;
        for (int i = a; i < x; i++)
        {
            for (int j = b; j < y; j++)
            {
                ans = ans && map[i][j].movebleObject.isEmpty() && (!map[i][j].obj.permanent);
            }
        }
        return ans;
    }

    void RoadGeneration()
    {
        int start = Random.Range(0, 4);
        int finish = Random.Range(0, 4);
        PointXY pS = new PointXY(0, 0);
        PointXY pF = new PointXY(0, 0);
        if (start == 0)
        {
            pS.x = 0;
            pS.y = Random.Range(0, mapHeight);
        }
        if (start == 1)
        {
            pS.x = mapWidth;
            pS.y = Random.Range(0, mapHeight);
        }
        if (start == 2)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = 0;
        }
        if (start == 3)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = mapHeight;
        }


        if (finish == 0)
        {
            pF.x = 0;
            pF.y = Random.Range(0, mapHeight);
        }
        if (finish == 1)
        {
            pF.x = mapWidth;
            pF.y = Random.Range(0, mapHeight);
        }
        if (finish == 2)
        {
            pF.x = Random.Range(0, mapWidth);
            pF.y = 0;
        }
        if (finish == 3)
        {
            pF.x = Random.Range(0, mapWidth);
            pF.y = mapHeight;
        }
        int step = 0;
        while (((LengthVec(pS.x, pS.y, pF.x, pF.y) < 6) || (start == finish)) && (step < 50))
        {
            step++;
            start = Random.Range(0, 4);
            finish = Random.Range(0, 4);
            pS = new PointXY(0, 0);
            pF = new PointXY(0, 0);
            if (start == 0)
            {
                pS.x = 0;
                pS.y = Random.Range(0, mapHeight - 1);
            }
            if (start == 1)
            {
                pS.x = mapWidth;
                pS.y = Random.Range(0, mapHeight - 1);
            }
            if (start == 2)
            {
                pS.x = Random.Range(0, mapWidth - 1);
                pS.y = 0;
            }
            if (start == 3)
            {
                pS.x = Random.Range(0, mapWidth - 1);
                pS.y = mapHeight;
            }


            if (finish == 0)
            {
                pF.x = 0;
                pF.y = Random.Range(0, mapHeight - 1);
            }
            if (finish == 1)
            {
                pF.x = mapWidth;
                pF.y = Random.Range(0, mapHeight - 1);
            }
            if (finish == 2)
            {
                pF.x = Random.Range(0, mapWidth - 1);
                pF.y = 0;
            }
            if (finish == 3)
            {
                pF.x = Random.Range(0, mapWidth - 1);
                pF.y = mapHeight;
            }
        }

        if (step < 50)
        {
            if (pS.x > pF.x)
            {
                PointXY ph = new PointXY(pF);
                pF = new PointXY(pS);
                pS = new PointXY(ph);
                //Debug.Log(2);
            }

            //Debug.Log(pS.x + " " + pS.y);
            // Debug.Log(pF.x + " " + pF.y);

            PointXY vec = new PointXY(pS, pF);
            vec.norm();
            vec.xf /= 10f;
            vec.yf /= 10f;
            int t = 0;
            //Debug.Log(1);
            step = 0;
            while ((pS.x + vec.xf * t <= pF.x) && (step < 1000))
            {
                step++;
                for (int i = 0; i < mapWidth; i++)
                {
                    for (int j = 0; j < mapHeight; j++)
                    {
                        if (LengthVec(pS.x + vec.xf * t, pS.y + vec.yf * t, i, j) <= 0.8)
                        {
                            map[i][j].SetLandCover(3);
                            map[i][j].GenerateObject(0);
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(map[i][j].obj.id, false);
                        }
                    }
                }
                t++;
            }
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if ((map[i][j].landCoverId == 3))
                    {
                        bool tf = false;
                        if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i + 1, j) == 2)
                        {
                            if (!InMap(i - 1, j) && (GetRoadCounAround(i, j - 1) == 0))
                            {
                                if (GetRoadCounAround(i, j - 1) + GetRoadCounAround(i, j + 1) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i, j - 1))
                                    {
                                        chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(0, 4);
                                    }
                                    if (InMap(i, j + 1))
                                    {
                                        chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(3, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 3);
                                }
                                tf = true;
                            }
                            if (!InMap(i + 1, j) && (GetRoadCounAround(i, j + 1) == 0))
                            {
                                tf = true;
                                if (GetRoadCounAround(i, j - 1) + GetRoadCounAround(i, j + 1) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i, j - 1))
                                    {
                                        chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(0, 4);
                                    }
                                    if (InMap(i, j + 1))
                                    {
                                        chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(3, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 0);
                                }
                            }
                            if (!tf)
                            {
                                tf = true;
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                if (InMap(i, j - 1))
                                {
                                    chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(0, 4);
                                }
                                if (InMap(i, j + 1))
                                {
                                    chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(3, 4);
                                }
                            }

                        }
                        if ((GetRoadCounAround(i, j - 1) + GetRoadCounAround(i, j + 1) == 2) && (!tf))
                        {
                            if (!InMap(i, j - 1) && (GetRoadCounAround(i - 1, j) == 0))
                            {
                                if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i + 1, j) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i - 1, j))
                                    {
                                        chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(1, 4);
                                    }
                                    if (InMap(i + 1, j))
                                    {
                                        chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(2, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 3);
                                }
                                tf = true;
                            }
                            if (!InMap(i, j + 1) && (GetRoadCounAround(i + 1, j) == 0))
                            {
                                if (GetRoadCounAround(i + 1, j) + GetRoadCounAround(i - 1, j) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i - 1, j))
                                    {
                                        chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(1, 4);
                                    }
                                    if (InMap(i + 1, j))
                                    {
                                        chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(2, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 0);
                                }
                                tf = true;
                            }
                            if (!tf)
                            {
                                tf = true;
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                if (InMap(i - 1, j))
                                {
                                    chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(1, 4);
                                }
                                if (InMap(i + 1, j))
                                {
                                    chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(2, 4);
                                }
                            }

                        }
                        if (!tf)
                        {
                            if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i, j - 1) == 2)
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 0);

                            }
                            if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i, j + 1) == 2)
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 1);


                            }

                            if (GetRoadCounAround(i + 1, j) + GetRoadCounAround(i, j + 1) == 2)
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 3);

                            }

                            if (GetRoadCounAround(i + 1, j) + GetRoadCounAround(i, j - 1) == 2)
                            {

                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 2);

                            }
                        }
                    }
                }
            }
        }
    }

    int GetRoadCounAround(int i, int j)
    {
        if (InMap(i, j))
        {
            if (map[i][j].landCoverId == 3)
            {
                return 1;
            }
            return 0;
        }
        return 1;
    }

    float LengthVec(float xf, float yf, int x, int y)
    {
        return Mathf.Sqrt((x - xf) * (x - xf) + (y - yf) * (y - yf));
    }

    int enemyUnitNow;
    bool reGame;

    void EndGame(bool tf)
    {
        if (tf && !(PlayerPrefs.GetInt("NewGame") == 1))
        {
            PlayerPrefs.SetInt("CurWinBattle", PlayerPrefs.GetInt("CurWinBattle") + 1);
            PlayerPrefs.SetInt("Achivment1", 1);
            if (PlayerPrefs.GetInt("CurWinBattle") >= 5)
            {
                PlayerPrefs.GetInt("Achivment2", 1);
            }
            if (PlayerPrefs.GetInt("CurWinBattle") >= 25)
            {
                PlayerPrefs.GetInt("Achivment3", 1);
            }
        }
        int cur = 0;
        for(int i = 0; i < units.Length; i++)
        {
            if (units[i].unit.isEmpty() && !units[i].unit.player)
            {
                cur++;
            }
        }
        PlayerPrefs.SetInt("Achivment17", 1);
        PlayerPrefs.SetInt("CurKilledDemons", PlayerPrefs.GetInt("CurKilledDemons") + cur);
        if (PlayerPrefs.GetInt("CurKilledDemons") >= 25)
        {
            PlayerPrefs.SetInt("Achivment18", 1);
        }
        if (PlayerPrefs.GetInt("CurKilledDemons") >= 70)
        {
            PlayerPrefs.SetInt("Achivment19", 1);
        }
        PlayerPrefs.SetInt("NewGame", 0);
        if (tf)
        {
            PlayerPrefs.SetInt("Achivment0", 1);
            //Debug.Log("win");
            int a = PlayerPrefs.GetInt("DemonicInfluence");
            if ((ev.eventId != 3))
            {
                a -= maxEnemyUnits;
                a = Mathf.Max(0, a);

            }
            if (PlayerPrefs.GetInt("SpecialID") == 1)
            {
                PlayerPrefs.SetInt("SmallBand", PlayerPrefs.GetInt("SmallBand") - 1);
            }
            if (PlayerPrefs.GetInt("SpecialID") == 2)
            {
                PlayerPrefs.SetInt("MassBand", PlayerPrefs.GetInt("MassBand") - 1);
            }
            if (PlayerPrefs.GetInt("SpecialID") == 3)
            {
                PlayerPrefs.SetInt("Foothold", PlayerPrefs.GetInt("Foothold") - 1);
            }
            if (PlayerPrefs.GetInt("SpecialID") == 4)
            {
                PlayerPrefs.SetInt("Doomed", PlayerPrefs.GetInt("Doomed") - 1);
            }

            PlayerPrefs.SetInt("DemonicInfluence", a);
            PlayerPrefs.SetInt("Lose", 0);
            PlayerPrefs.SetInt("ExtraGold", maxEnemyUnits * 10);
            if (PlayerPrefs.GetInt("Gold") + PlayerPrefs.GetInt("ExtraGold") >= 1000)
            {
                PlayerPrefs.SetInt("Achivment13", 1);
            }
        }
        else
        {
            int a = PlayerPrefs.GetInt("DemonicInfluence");
            if ((ev.eventId != 3))
            {
                a += maxEnemyUnits;
                a = Mathf.Min(1000, a);
            }
            PlayerPrefs.SetInt("DemonicInfluence", a);
            PlayerPrefs.SetInt("Lose", 1);
            PlayerPrefs.SetInt("ExtraGold", 0);
            float coef = 1;
            switch (ev.eventId)
            {
                case 1:
                    coef = 1;
                    break;
                case 2:
                    coef = 2;
                    break;
                case 3:
                    coef = 0;
                    break;
            }
            if (UnityEngine.Random.Range(0f, 1000f) * coef < PlayerPrefs.GetInt("DemonicInfluence"))
            {
                PlayerPrefs.SetInt("SmallBand", PlayerPrefs.GetInt("SmallBand") + 1);
                GameEvent ev = new GameEvent(1, language, PlayerPrefs.GetInt("DemonicInfluence"), PlayerPrefs.GetInt("Time"));

                ev.Save("SmallBandEvent" + PlayerPrefs.GetInt("SmallBand").ToString());
            }
        }
        //Debug.Log(PlayerPrefs.GetInt("ExtraGold"));
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            if (units[i].unit.id == 0)
            {
                PlayerPrefs.SetInt("PlayerUnitOnMission" + units[i].unit.num.ToString(), 0);
            }
            else
            {
                units[i].unit.SetExp(80);
                units[i].unit.SetFatigue(PlayerPrefs.GetFloat("Time"));
                units[i].unit.Save("PlayerUnitOnBase" + units[i].unit.num.ToString());
                units[i].unit.Save("PlayerUnit" + i);
                PlayerPrefs.SetInt("PlayerUnitOnMission" + units[i].unit.num.ToString(), 1);
            }
        }
        loadWindow = true;
        SceneManager.LoadSceneAsync("ResultsScene");
    }

    void RecalcPlayerTurn()
    {
        activity = Activity.Myself;
        activeAction = Ability.Defense;
        for (int i = 0; i < maxUnits; i++)
        {
            // Debug.Log(playerUnits[i].unit.id);
            if (!units[i].unit.isEmpty() && units[i].unit.player)
            {
                // Debug.Log(units[i].unit.id);
                units[i].unit.calculateNewTurn();
                if (units[i].unit.curAP > 0)
                {
                    //Debug.Log(units[i].point);
                    //  Debug.Log('?');
                    chosenPoint = new PointXY(units[i].point);
                    playerPoint = new PointXY(units[i].point);
                    chosenChunk = map[chosenPoint.x][chosenPoint.y];
                    playerChunk = map[playerPoint.x][playerPoint.y];
                    // Debug.Log(chosenPoint.x + " " + chosenPoint.y);
                }
            }
        }
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
             //   Debug.Log(map[i][j].obj.go);
                map[i][j].obj.calculateNewTurn();
            }
        }
        //Debug.Log(chosenPoint.x + " " + chosenPoint.y);
       //   Debug.Log(playerPoint);
        Dijkstra(playerPoint);
        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
        move = true;
        attack = true;
        //Debug.Log(1);
        CameraMove(chosenPoint.x, chosenPoint.y);
    }

     float GetEstimateValueInTheLine(Unit un, PointXY pos)
     {
         float cou = 0;
         if (un.unit.isEmpty())
         {
             for (int j = -1; j <= 1; j++)
             {
                 for (int k = -1; k <= 1; k++)
                 {
                     if (Mathf.Abs(j + k) == 1)
                     {
                         if (InMap(pos.x + j, pos.y + k) && (!map[pos.x + j][pos.y + k].movebleObject.isEmpty()) && (map[pos.x + j][pos.y + k].movebleObject.player == un.unit.player) && (map[pos.x + j][pos.y + k].movebleObject != un.unit))
                         {
                             cou += 1;
                         }
                     }
                 }
             }
         }
         if (un.unit.inTheLinePerk)
         {
             cou = 2.5f * cou;
         }
         else
         {
             cou = 0;
         }
         return cou;
     }

     float GetEstimateDefenseFromPoint(Unit un, PointXY pos)
     {
         return un.unit.GetDefense(GetEstimateValueInTheLine(un, pos) * 0.01f) * un.unit.curHits;
     }

     float GetEstimateDamageTakenInNextTurn(Unit un, PointXY pos)
     {
         float ans = 0;
         ArrayList ar = new ArrayList();
         PointXY resultVec;
         if (!pos.isEqual(un.point)) {
             ar = GetWay(pos);
             resultVec = new PointXY(((PointXY)ar[0]).x - ((PointXY)ar[1]).x, ((PointXY)ar[0]).y - ((PointXY)ar[1]).y);
         }
         else
         {
             resultVec = new PointXY(un.rotation);
         }
         for (int i = 0; i < maxUnits; i++)
         {
             if (!units[i].unit.isEmpty() && units[i].unit.player)
             {
                 PointXY at = new PointXY(pos.x - units[i].point.x, pos.y - units[i].point.y);
                 //  PointXY estH = un.unit.EstimatedDamage(units[i].unit, resultVec, at);
                 // ans += estH.realD * (1 + estH.realC * 0.01f) * estH.realH * 0.01f;
             }
         }
         return ans;
     }

    // Update is called once per frame

    void GoNearestPoint(Unit enemyUn, Unit targetUnit)
    {
        PointXY p = enemyUn.unit.GetBestPoint();
       // Debug.Log(p);
        if (p == null)
        {
          //  Debug.Log("#");
            enemyUn.unit.doAction(1);
            return;
        }
       // Debug.Log(p.x + " " + p.y + " " + p.ability);
        if (Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.unit.GetSpeed()) < enemyUn.unit.curAP)
        {
          //  Debug.Log("-");
            if (enemyUn.point.isEqual(p))
            {
                activeAction = p.ability;
                if (activeAction == Ability.ShieldBash)
                {
                    targetUnit = enemyUn.unit.GetTarget(p, Ability.Attack);
                }
                else
                {
                    targetUnit = enemyUn.unit.GetTarget(p, activeAction);
                }
                if (targetUnit != null)
                {
                    enemyUn.unit.EmulateAbility(activeAction);
                    targetPoint = new PointXY(targetUnit.point);
                    TryAttack(enemyUn.point, map[enemyUn.point.x][enemyUn.point.y], targetPoint.x, targetPoint.y);
                }
                else
                {
                   // Debug.Log("-");
                    enemyUn.unit.doAction(1);
                }
            }
            else
            {
                int costMI2 = Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.unit.GetSpeed());
                enemyUn.unit.doAction(2, costMI2);
                block = true;
                blockCounter++;
               // Debug.Log(p);
                GoTo(enemyUn.point, p, false);
            }
        }
        else
        {
            if ((enemyUn.unit.curAP > 1) || ((enemyUn.unit.maxAP == 1) && (enemyUn.unit.curAP == 1)))
            {
                if ((dist[p.x][p.y] == INF))
                {
                    //  Debug.Log(p.x + " " + p.y);
                    Debug.Log(1);
                    enemyUn.unit.doAction(1);
                }
                else
                {
                    ArrayList longWay = GetWay(p);
                    int i = 0;
                    if ((enemyUn.unit.maxAP == 1) && (enemyUn.unit.curAP == 1))
                    {
                        Debug.Log('-');
                        while ((Mathf.CeilToInt(dist[((PointXY)longWay[i]).x][((PointXY)longWay[i]).y] / enemyUn.unit.GetSpeed()) > enemyUn.unit.curAP)
                            && (Mathf.CeilToInt(dist[((PointXY)longWay[i + 1]).x][((PointXY)longWay[i + 1]).y] / enemyUn.unit.GetSpeed()) != 0))
                        {
                     //       Debug.Log(dist[((PointXY)longWay[i]).x][((PointXY)longWay[i]).y]);
                            i++;
                        }
                       // Debug.Log(Mathf.CeilToInt(dist[((PointXY)longWay[i]).x][((PointXY)longWay[i]).y] / enemyUn.unit.GetSpeed()));
                    }
                    else
                    {
                        while ((Mathf.CeilToInt(dist[((PointXY)longWay[i]).x][((PointXY)longWay[i]).y] / enemyUn.unit.GetSpeed()) >= enemyUn.unit.curAP)
                            && (Mathf.CeilToInt(dist[((PointXY)longWay[i + 1]).x][((PointXY)longWay[i + 1]).y] / enemyUn.unit.GetSpeed()) != 0))
                        {
                            i++;
                        }
                    }
                    if (Mathf.CeilToInt(dist[((PointXY)longWay[i]).x][((PointXY)longWay[i]).y] / enemyUn.unit.GetSpeed()) == 0)
                    {
                        enemyUn.unit.doAction(4);
                        return;
                    }
                    p = (PointXY)longWay[i];
                    int cost = Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.unit.GetSpeed());
                    //Debug.Log(cost);
                 //   if (cost <= enemyUn.unit.curAP)
                    {
                        enemyUn.unit.doAction(2, cost);
                        block = true;
                        blockCounter++;
                        GoTo(enemyUn.point, p, false);
                    }
                   // else
                    //{
                     //   enemyUn.unit.doAction(4);
                    //}
                }
            }
            else
            {
             //   Debug.Log("@");
                enemyUn.unit.doAction(1);
            }
        }
    }

    void EnemyAI(Unit enemyUn, GameObject enemyGO)
    {
        Dijkstra(enemyUn.point);
        // Debug.Log(enemyUn);
        bool tf = false;
        Unit targetUnit = new Unit();
        if (enemyUn.unit.it.active && (enemyUn.unit.it.id == 2) && ((enemyUn.unit.maxHits - enemyUn.unit.curHits) * enemyUn.unit.archTypeValues[5] >= 3) && (enemyUn.unit.it.id != 0) && (enemyUn.unit.curAP >= enemyUn.unit.it.activationCost))
        {
            if (enemyUn.unit.it.id == 1)
            {
                enemyUn.unit.ActivateItem(enemyUn.unit);
            }
        }
        else
        {
            GoNearestPoint(enemyUn, targetUnit);
        }

    }

    void Update()
    {
        CheckTextures();
        int counter1 = 0;
        int counter2 = 0;
        if (!loadWindow && play)
        {
            if (!reGame)
            {
                for (int i = 0; i < maxUnits; i++)
                {
                    if ((units[i].unit.id == 0) && (units[i].unit.player))
                    {
                        unitsGO[i].GetComponent<ShowActive>().Show(false);
                        unitsGO[i].GetComponent<HelperScript>().SetPlayerInd(false);
                        counter1++;
                    }
                }
                // Debug.Log(maxEnemyUnits);
                for (int i = 0; i < units.Length; i++)
                {
                    //   Debug.Log("i " + i.ToString());
                    if ((units[i].unit.id == 0) && (!units[i].unit.player))
                    {
                        unitsGO[i].GetComponent<ShowActive>().Show(false);
                        counter2++;
                    }
                }

                if (counter1 == maxPlayerUnits)
                {
                    reGame = true;
                    gameResult = false;
                    // EndGame(false);
                }
                if (counter2 == maxEnemyUnits)
                {
                    reGame = true;
                    gameResult = true;
                    //  EndGame(true);
                }


                if (enemyTurn && !block && !cameraMove)
                {
                    if (enemyUnitNow == maxUnits)
                    {
                        enemyTurn = false;
                      //  Debug.Log("*");
                        RecalcPlayerTurn();
                    }
                    if (enemyTurn)
                    {
                        int i = enemyUnitNow;

                     //   Debug.Log(i);

                        if ((units[i].unit.curAP == 0) || (units[i].unit.isEmpty()) || units[i].unit.player)
                        {
                            //enemyUnitNow = i;
                            enemyUnitNow++;
                            //while ((enemyUnitNow != maxUnits) && (units[enemyUnitNow].unit.isEmpty() || units[enemyUnitNow].unit.player))
                            //{
                             //   enemyUnitNow++;
                            //}
                            if ((enemyUnitNow != maxUnits) && !units[enemyUnitNow].unit.isEmpty() && !units[enemyUnitNow].unit.player)
                            {
                                CameraMove(units[enemyUnitNow].point.x, units[enemyUnitNow].point.y);
                            }
                        }
                        else
                        {
                            if (!units[i].unit.isEmpty() && !units[i].unit.player)
                            {
                                EnemyAI(units[i], unitsGO[i]);
                                //MachineAI(enemyUnits[i], enemyUnitsGO[i]);
                            }
                        }
                    }
                }
            }
        }

    }

    Unit extraUnit;

    Unit GetUnit(PointXY p)
    {
        bool tf = true;
        Unit un = extraUnit;
        for (int i = 0; i < maxUnits; i++)
        {
            if ((units[i].point.x == p.x) && (units[i].point.y == p.y) && (units[i].unit.id != 0))
            {
                un = units[i];
                tf = false;
                break;
            }
        }
        return un;
    }

    GameObject GetUnitGO(PointXY p)
    {
        GameObject un = null;
        bool tf = true;
        for (int i = 0; i < maxUnits; i++)
        {
            if ((units[i].point.x == p.x) && (units[i].point.y == p.y) && (units[i].unit.id != 0))
            {
                un = unitsGO[i];
                tf = false;
                break;
            }
        }
        return un;
    }

    ArrayList GetWay(PointXY p)
    {
        ArrayList newWay = new ArrayList();
        newWay.Add(p);
        int step = 0;
        //Debug.Log(dist[p.x][p.y] + " " + (dist[p.x][p.y] > 0));
        while (dist[p.x][p.y] > 0)
        {
            p = parents[p.x][p.y];
            newWay.Add(p);
            //Debug.Log(dist[p.x][p.y] + " " + (dist[p.x][p.y] > 0));
        }
        //if (step > 50)
        //{
        //  Debug.Log(dist[p.x][p.y]);
        //}
        return newWay;
    }

    bool block = false;

    void CheckInTheLine()
    {
        for (int i = 0; i < maxUnits; i++)
        {
            int cou = 0;
            if (!units[i].unit.isEmpty())
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (!((j == 0) && (k == 0)))
                        {
                            // Debug.Log(InMap(units[i].point.x + j, units[i].point.y + k));// && (!map[playerUnits[i].point.x + j][playerUnits[i].point.y + k].movebleObject.isEmpty()) && (!map[units[i].point.x + j][units[i].point.y + k].movebleObject.player))
                            // );
                            if (InMap(units[i].point.x + j, units[i].point.y + k) && (!map[units[i].point.x + j][units[i].point.y + k].movebleObject.isEmpty()) && (map[units[i].point.x + j][units[i].point.y + k].movebleObject.player == map[units[i].point.x][units[i].point.y].movebleObject.player))
                            {
                                cou++;
                            }
                        }
                    }
                }
                units[i].unit.SetValueInTheLine(cou);
                if ((cou != 0) && (units[i].unit.inTheLinePerk))
                {
                    unitsGO[i].GetComponent<HelperScript>().SetInTheLineId(true);
                  //  Debug.Log(units[i].unit.inTheLinePerk);
                }
                else
                {
                    unitsGO[i].GetComponent<HelperScript>().SetInTheLineId(false);
                }
            }
        }
    }

    public void SetBlock(bool tf, bool attack)
    {
        FullStop();
        if (!tf)
        {
            blockCounter--;
        }
        //Debug.Log(blockCounter);
        block = blockCounter != 0;
       // Debug.Log(blockCounter + " " + 2);
        if (!tf)
        {
            returnedToUnit = false;
        }
        if (!enemyTurn && playerChunk.movebleObject.curAP == 0)
        {
            CheckNextTurn();
        }
        if (enemyTurn)
        {
            if ((units[enemyUnitNow].unit.curAP == 0))
            {
                enemyUnitNow++;
                //CheckNextTurn();
                if (enemyUnitNow != maxUnits)
                {
                    CameraMove(units[enemyUnitNow].point.x, units[enemyUnitNow].point.y);
                }
            }
        }
        else
        {
            if (chosenChunk.movebleObject.curAP == 0 && !enemyTurn)
            {
                CheckNextTurn();
            }
            else
            {

                Dijkstra(playerPoint);
                chosenPoint = playerPoint;
                chosenChunk = playerChunk;
                move = true;
                attack = true;
                //CheckTextures();
            }
            // Debug.Log(2);
            if (!enemyTurn)
            {
                CheckNextTurn();
            }
        }
    }

    Unit attacker;

    public void Swap(PointXY p1, PointXY p2)
    {
        Unit playerUn = GetUnit(p1);
        Unit second = GetUnit(p2);
        second.point = new PointXY(p1);
        //Debug.Log(playerUn.unit.id);
        playerUn.point = new PointXY(p2);
        //chosenPoint = p2;
        //chosenChunk = map[p2.x][p2.y];
        map[p1.x][p1.y].movebleObject = map[p2.x][p2.y].movebleObject;
        map[p2.x][p2.y].movebleObject = playerUn.unit;
        playerChunk = map[p2.x][p2.y];
        playerPoint = p2;
        playerUn.SetRotation(new PointXY(p2.x - p1.x, p2.y - p1.y));
        CheckInTheLine();
        //playerPoint = new PointXY(p2);
        //playerChunk = map[p2.x][p2.y];
        // Debug.Log(playerUn.unit.name);
        // Debug.Log(map[p2.x][p2.y].movebleObject.curAP);
        //Debug.Log(p1.x + " " + p1.y + " -> " + p2.x + " " + p2.y);
    }

    void GoTo(PointXY p1, PointXY p2, bool tf)
    {
        if (p1.isEqual(p2))
        {
            return;
        }
        Unit playerUn = GetUnit(p1);
        //Debug.Log(p1.x + " " + p1.y + " -> " + p2.x + " " + p2.y);
        //Debug.Log(playerUn.unit.name);
        //playerUn.point = new PointXY(p2);
        way = GetWay(p2);
     //   Debug.Log(p1.x + " " + p1.y + " -> " + p2.x + " " + p2.y);
        //Debug.Log(GetUnit(p2).unit.name);
            // enemyUnitNow = 0;
            for (int i = 0; i < maxUnits; i++)
            {
                if ((units[i] == playerUn) && (!units[i].unit.isEmpty()))
                {
                    //Transform tr = this.gameObject.transform;
                    //  Debug.Log(playerUnits[i].x);
                    block = true;
                    //enemyUnitNow = 0;
                    unitsGO[i].GetComponent<UnitMove>().SetWay(way);
                    //unitsGO[i].transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * units[i].point.x, tr.position.y - dx / 2 - dx + dx * units[i].point.y, tr.position.z);
                }
            }
    }

    void FullStop()
    {
        for (int i = 0; i < maxUnits; i++)
        {
            unitsGO[i].gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }

    void CheckChosenStatus()
    {
        /*     for (int i = 0; i < mapWidth; i++)
             {
                 for (int j = 0; j < mapHeight; j++)
                 {
                     chunks[i][j].GetComponent<SpriteActive>().setIdActive(-8, false);
                 }
             }
             if (chosenPoint != null) { 
                 chunks[chosenPoint.x][chosenPoint.y].GetComponent<SpriteActive>().setIdActive(-8, true);
             }
    }

    int blockCounter;

    void DrawWay()
    {
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                chunks[i][j].GetComponent<SpriteActive>().SetWay(new PointXY(0, 0));
            }
        }
        if (activity != Activity.Move)
        {
            return;
        }
        if(way == null)
        {
            return;
        }
        if (way.Count < 2)
        {
            return;
        }
        chunks[((PointXY)way[0]).x][((PointXY)way[0]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[0], (PointXY)way[1]));
        int c = way.Count - 1;
        chunks[((PointXY)way[c]).x][((PointXY)way[c]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[c], (PointXY)way[c - 1]));
        for (int i = 1; i < way.Count - 1; i++)
        {
            chunks[((PointXY)way[i]).x][((PointXY)way[i]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[i], (PointXY)way[i - 1]));
            chunks[((PointXY)way[i]).x][((PointXY)way[i]).y].GetComponent<SpriteActive>().SetWay(new PointXY((PointXY)way[i], (PointXY)way[i + 1]));
        }
    }

    void CheckTextures()
    {
        DrawWay();
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if (playerChunk != null)
                {
                    if ((activeAction == Ability.Item) && (!map[i][j].movebleObject.isEmpty()) && (map[i][j].movebleObject.player == playerChunk.movebleObject.it.friendly) && (LenPoints(new PointXY(i, j), playerPoint) < playerChunk.movebleObject.GetRangeItem()))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                    }
                    if (new PointXY(i, j).isEqual(chosenPoint) && !enemyTurn)
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-8, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-8, false);
                    }
                    if (!enemyTurn && RA.InRangeAttack(playerPoint, new PointXY(i, j), playerChunk.movebleObject, Ability.Attack, null) && ((!map[i][j].obj.isEmpty()) || (!map[i][j].movebleObject.isEmpty() && !map[i][j].movebleObject.player)))
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-5, true);
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-5, false);
                    }
                    if (!playerPoint.isEqual(chosenPoint) && !enemyTurn && move && (dist[chosenPoint.x][chosenPoint.y] <= GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed()) && ((!map[i][j].obj.isEmpty()) || (!map[i][j].movebleObject.isEmpty() && !map[i][j].movebleObject.player)))
                    {
                        if (RA.InRangeAttack(chosenPoint, new PointXY(i, j), playerChunk.movebleObject, Ability.Attack, null))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-9, true);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-9, false);
                        }
                    }
                    else
                    {
                        if (activeAction == Ability.Cleave)
                        {
                            if (!enemyTurn && RA.InRangeAttack(playerPoint, new PointXY(i, j), playerChunk.movebleObject, activeAction, chosenPoint))
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-9, true);
                            }
                            else
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setIdActive(-9, false);
                            }
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-9, false);
                        }
                    }
                    if (!enemyTurn && move && (dist[i][j] <= GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed()))
                    {
                        if (!Valid(i - 1, j) || (dist[i - 1][j] > GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed()))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, true && !block && !reGame);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                        }
                        if (!Valid(i + 1, j) || (dist[i + 1][j] > GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed()))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, true && !block && !reGame);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
                        }
                        if (!Valid(i, j - 1) || (dist[i][j - 1] > GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed()))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, true && !block && !reGame);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                        }
                        if (!Valid(i, j + 1) || (dist[i][j + 1] > GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed()))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, true && !block && !reGame);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                        }
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
                    }
                }
                if (!play)
                {
                    //Debug.Log(dist[i][j]);
                    if ((dist[i][j] < 2))
                    {
                        if (!Valid(i - 1, j) || (dist[i - 1][j] > 2))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, true);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                        }
                        if (!Valid(i + 1, j) || (dist[i + 1][j] > 2))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, true);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
                        }
                        if (!Valid(i, j - 1) || (dist[i][j - 1] > 2))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, true);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                        }
                        if (!Valid(i, j + 1) || (dist[i][j + 1] > 2))
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, true);
                        }
                        else
                        {
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                        }
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-1, false);
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-2, false);
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-3, false);
                        chunks[i][j].GetComponent<SpriteActive>().setIdActive(-4, false);
                    }
                }
                /* if (!map[i][j].movebleObject.isEmpty() && (map[i][j].movebleObject.player))
                 {
                     chunks[i][j].GetComponent<SpriteActive>().setIdActive(-6, true);
                 }
                 else
                 {
                     chunks[i][j].GetComponent<SpriteActive>().setIdActive(-6, false);
                 }
                if (map[i][j].obj.isEmpty())
                {
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(1, false);
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(2, false);
                }
                if (map[i][j].movebleObject.isEmpty() || !map[i][j].movebleObject.player)
                {
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(-6, false);
                }
            }
        }
    }

    Ability activeAction;

    void CheckNextTurn()
    {
        activity = Activity.Myself;
        activeAction = Ability.Defense;
        returnedToUnit = false;
        if (!enemyTurn && playerChunk.movebleObject.curAP > 0)
        {
            Dijkstra(playerPoint);
            CameraMove(playerPoint.x, playerPoint.y);
            chosenPoint = playerPoint;
            chosenChunk = playerChunk;
            playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
            return;
        }
        Unit un = units[0];
        bool tf = true;
        for (int i = 0; i < maxUnits; i++)
        {
            if ((!units[i].unit.isEmpty()) && (units[i].unit.curAP > 0) && units[i].unit.player)
            {
                tf = false;
                un = units[i];
                chosenPoint = new PointXY(units[i].point);
                playerPoint = new PointXY(units[i].point);
                chosenChunk = map[chosenPoint.x][chosenPoint.y];
                playerChunk = map[playerPoint.x][playerPoint.y];
            }
        }
        //Debug.Log(tf);
        if (tf)
        {
            // Debug.Log(tf);
            enemyTurn = true;
            for (int i = 0; i < maxUnits; i++)
            {
                if (!units[i].unit.isEmpty() && !units[i].unit.player)
                {
                    units[i].unit.calculateNewTurn();
                }
            }
            for (int i = 0; i < maxUnits; i++)
            {
                if (!units[i].unit.isEmpty() && !units[i].unit.player)
                {
                    enemyUnitNow = i;
                    chosenPoint = new PointXY(units[i].point);
                    playerPoint = new PointXY(units[i].point);
                    chosenChunk = map[chosenPoint.x][chosenPoint.y];
                    playerChunk = map[playerPoint.x][playerPoint.y];
                    break;
                }
            }
        }
        else
        {
            attack = true;
            move = true;
            Dijkstra(playerPoint);
            playerChunk.movebleObject.SetShowMinusAp(playerChunk.movebleObject.curAP);
        }
        //Debug.Log(1);
        CameraMove(chosenPoint.x, chosenPoint.y);
        //Debug.Log(chosenPoint.isEqual(playerPoint));
        //Shift(un.point.x, un.point.y);
        if (!block)
        {
            //GameObject hit = GetUnitGO(chosenPoint);
            //Vector3 newPosition = new Vector3(hit.transform.position.x, hit.transform.position.y - (Screen.height - dy * 6) / 2, defaultCameraPosition.z);
            //cam.transform.position = newPosition;
        }
    }

    float[][] timer;

    int butttonPressed = -1;

    public Texture2D backGroundTexture;

    public GUISkin skin;
    public Texture2D list;

    public int GetGround(PointXY p)
    {
        return map[p.x][p.y].landCoverId;
    }

    bool FreeAround(MovebleObject m, PointXY a)
    {
        for(int i = -1; i <= 1; i++)
        {
            for(int j = -1; j <= 1; j++)
            {
                if (!((i == 0) && (j == 0)))
                {
                    if (InMap(a.x + i, a.y + j))
                    {
                        if ((!map[a.x + i][a.y + j].movebleObject.isEmpty()) && (map[a.x + i][a.y + j].movebleObject.player != m.player))
                        {
                          //  Debug.Log(i + " " + j);
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    string[] hitsText = { "Hits", "ОЗ" };
    string[] apText = { "AP", "ОД" };
    string[] armText = { "A", "Б" };
    string[] dmgText = { "D", "У" };

    void StatsWindow(int WindowID)
    {
        string s = "";
        if (playerChunk.obj.id != 0)
        {
            s += playerChunk.obj.name + ' ';
            if (!playerChunk.obj.permanent)
            {
                s += hitsText[language] + " " + playerChunk.obj.curHits + "/" + playerChunk.obj.maxHits + '\n';
                s += armText[language] + " " + playerChunk.obj.armour.piercingArmour + " " + playerChunk.obj.armour.slashingArmour + " " + playerChunk.obj.armour.crushingArmour + " " + playerChunk.obj.armour.axingArmour;
            }
        }
        if (playerChunk.movebleObject.id != 0)
        {
            s += playerChunk.movebleObject.name + '\n';
            s += playerChunk.movebleObject.damage.keyName;
            if (playerChunk.movebleObject.leftArm.id != 0)
            {
                s += '\n' + playerChunk.movebleObject.leftArm.keyName;
            }
            s += '\n';
            s += playerChunk.movebleObject.armour.keyName;
            //s += hitsText[language] + " " + playerChunk.movebleObject.curHits + "/" + playerChunk.movebleObject.maxHits + ' ';
            //s += apText[language] + " "  + playerChunk.movebleObject.curAP + "/" + playerChunk.movebleObject.maxAP + '\n';
            //s += dmgText[language] + " " + playerChunk.movebleObject.damage.piercingDamage + " " + playerChunk.movebleObject.damage.slashDamage + " " + playerChunk.movebleObject.damage.crashDamage + " " + playerChunk.movebleObject.damage.axeDamage + ' ';
            //s += armText[language] + " " + playerChunk.movebleObject.armour.piercingArmour + " " + playerChunk.movebleObject.armour.slashArmour + " " + playerChunk.movebleObject.armour.crashArmour + " " + playerChunk.movebleObject.armour.axeArmour + '\n';
        }
        GUI.Box(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), s);
        GUI.DrawTexture(new Rect(Screen.width / 4 - (Screen.height - (dy * 6)) / 8, (Screen.height - (dy * 6)) / 2 - (Screen.height - (dy * 6)) / 8, (Screen.height - (dy * 6)) / 8, (Screen.height - (dy * 6)) / 8), arrowDown);
        if (GUI.Button(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), "", freeStyle))
        {
            //Debug.Log(1);
            showYourList = !showYourList;
        }
        Vector2 newPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        if (new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2).Contains(newPos))
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), chosen);
        }
    }

    void StatsWindow2(int WindowID)
    {
        string s = "";
        if (chosenChunk.obj.id != 0)
        {
            s += chosenChunk.obj.name + ' ';
            if (!chosenChunk.obj.permanent)
            {
                s += hitsText[language] + " " + chosenChunk.obj.curHits + "/" + chosenChunk.obj.maxHits + '\n';
                s += armText[language] + " " + chosenChunk.obj.armour.piercingArmour + " " + chosenChunk.obj.armour.slashingArmour + " " + chosenChunk.obj.armour.crushingArmour + " " + chosenChunk.obj.armour.axingArmour;
            }
        }
       // Debug.Log(chosenChunk.movebleObject.id);
        if (!chosenChunk.movebleObject.isEmpty())
        {
            s += chosenChunk.movebleObject.name + '\n';
            s += chosenChunk.movebleObject.damage.keyName;
            if (chosenChunk.movebleObject.leftArm.id != 0)
            {
                s += '\n' + chosenChunk.movebleObject.leftArm.keyName;
            }
            s += '\n';
            s += chosenChunk.movebleObject.armour.keyName;
            //s += hitsText[language] + " " + chosenChunk.movebleObject.curHits + "/" + chosenChunk.movebleObject.maxHits + ' ';
            //s += apText[language] + " " + chosenChunk.movebleObject.curAP + "/" + chosenChunk.movebleObject.maxAP + '\n';
            //s += dmgText[language] + " " + chosenChunk.movebleObject.damage.piercingDamage + " " + chosenChunk.movebleObject.damage.slashDamage + " " + chosenChunk.movebleObject.damage.crashDamage + " " + chosenChunk.movebleObject.damage.axeDamage + ' ';
            //s += armText[language] + " " + chosenChunk.movebleObject.armour.piercingArmour + " " + chosenChunk.movebleObject.armour.slashArmour + " " + chosenChunk.movebleObject.armour.crashArmour + " " + chosenChunk.movebleObject.armour.axeArmour + '\n';
        }
        GUI.Box(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), s);
        GUI.DrawTexture(new Rect(0, (Screen.height - (dy * 6)) / 2 - (Screen.height - (dy * 6)) / 8, (Screen.height - (dy * 6)) / 8, (Screen.height - (dy * 6)) / 8), arrowDown);

        if (GUI.Button(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), "", freeStyle))
        {
            //Debug.Log(1);
            showEnemyList = !showEnemyList;
        }
        Vector2 newPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
        if (new Rect(Screen.width / 4 * 3, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2).Contains(newPos))
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), chosen);
        }
    }

    void Shift(int it, int jt)
    {
        if (((it < 3) || (it > mapWidth - 3)))
        {
            if (it < 3)
            {
                shiftX = 3;
            }
            else
            {
                shiftX = mapWidth - 3;
            }
        }
        else
        {
            shiftX = it;
        }
        if (((mapHeight - jt < 3) || (mapHeight - jt > mapHeight - 3)))
        {
            if (jt < 3)
            {
                shiftY = mapHeight - 3;
            }
            else
            {
                shiftY = 3;
            }
        }
        else
        {
            shiftY = jt;
        }
    }

    Chunk playerChunk;
    PointXY playerPoint;
    PointXY attackPoint;

    public Texture2D longTapInd;

    PointXY targetPoint;

    PointXY p;
    bool pressed;

    float cameraTimer;
    float cameraSpeed;
    float startCameraMove;
    bool cameraMove;

    public float GetOneChunkLen()
    {
        float x1 = chunks[0][0].transform.position.x;
        float y1 = chunks[0][0].transform.position.y;
        float x2 = chunks[1][0].transform.position.x;
        float y2 = chunks[1][0].transform.position.y;
        return Mathf.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }

    void CameraMove(int nx, int ny)
    {
        //Debug.Log(block);
        //Debug.Log(blockCounter);
        if (!block)
        {
            cameraMove = true;
            startCameraMove = Time.time;
            cameraSpeed = dx * 3.8f;
            float vx = chunks[nx][ny].transform.position.x - cam.transform.position.x;
            float vy = chunks[nx][ny].transform.position.y - cam.transform.position.y;

            float len = Mathf.Sqrt(vx * vx + vy * vy);

            cam.GetComponent<Rigidbody2D>().velocity = new Vector2(vx / len, vy / len) * cameraSpeed;

            //Debug.Log(cam.GetComponent<Rigidbody2D>().velocity);
            cameraTimer = len / cameraSpeed;
        }
        //Debug.Log(cameraTimer);
    }

    bool returnWait;

    PointXY lastPoint;

    string[] attackTarget = { "Attack target", "Атаковать цель" };

    PointXY estimatedDamage;

    void TryAttack(PointXY playerPoint, Chunk playerChunk, int it, int jt)
    {
        if (playerChunk.movebleObject.damage.handStatus == 3)
        {
            GetUnit(playerPoint).unit.SetExtraAim(LenPoints(playerPoint, new PointXY(it, jt)), LineMinusAim(playerPoint, new PointXY(it, jt)));
        }
        if (!playerPoint.isEqual(new PointXY(it, jt)) && (activeAction != Ability.Item))
        {
            if (activeAction != Ability.Attack)
            {
                //Debug.Log(playerChunk.movebleObject.curAbilityCulldown[activeAction]);
                if (!playerChunk.movebleObject.IsAbilityReloaded(activeAction))
                {
                    return;
                }
            }
            if (activeAction == Ability.Cleave)
            {
                if ((playerChunk.movebleObject.damage.handStatus == 3) && (playerChunk.movebleObject.it.id == 0))
                {
                    return;
                }
                if ((playerChunk.movebleObject.damage.handStatus == 3))
                {
                    playerChunk.movebleObject.it.Active(playerChunk.movebleObject);
                }
                GetUnit(playerPoint).SetRotation(new PointXY(it - playerPoint.x, jt - playerPoint.y));
                Rotation(GetUnitGO(playerPoint), it - playerPoint.x, jt - playerPoint.y);
                chosenPoint = new PointXY(it, jt);
                blockCounter++;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        //Debug.Log(playerChunk.movebleObject.GetRangAttack());
                        //Debug.Log(RA.InRangeAttack(playerPoint, new PointXY(j, k), playerChunk.movebleObject, activeAction));
                       // Debug.Log((Mathf.PI / 8f * 3f + 0.01f) + " " + (Mathf.PI * 2 - Mathf.PI / 4f - 0.01f));
                        if (RA.InRangeAttack(playerPoint, new PointXY(j, k), playerChunk.movebleObject, activeAction, chosenPoint))
                        {
                            if (!map[j][k].movebleObject.isEmpty())
                            {
                                GetUnitGO(new PointXY(j, k)).GetComponent<UnitMove>().DamageTakenDelay(GetUnit(playerPoint), new PointXY(j, k),
                                    GetUnitGO(playerPoint).GetComponent<UnitMove>().GetDelay(GetUnit(playerPoint), new PointXY(j, k)));
                            }
                            else
                            {
                                if (!map[j][k].obj.isEmpty())
                                {
                                    chunks[j][k].GetComponent<ObjectEffects>().DamageTakenDelay(GetUnit(playerPoint), new PointXY(j, k),
                                        GetUnitGO(playerPoint).GetComponent<UnitMove>().GetDelay(GetUnit(playerPoint), new PointXY(j, k))
                                        );
                                }
                            }
                            blockCounter++;
                            //DealDamage(GetUnit(playerPoint), j, k);
                         //   GetUnitGO(playerPoint).GetComponent<UnitMove>().SetAttack(-GetUnit(playerPoint).point.x + it, -GetUnit(playerPoint).point.y + jt, chunks[it][jt]);
                        }
                    }
                }
                PointXY p3 = new PointXY(-GetUnit(playerPoint).point.x + it, -GetUnit(playerPoint).point.y + jt);
                GetUnitGO(playerPoint).GetComponent<UnitMove>().SetAbility(p3.x, p3.y, chunks[it][jt], activeAction);
                playerChunk.movebleObject.ActivateAbility(activeAction);
                block = true;
                return;
            }
            //Debug.Log((map[it][jt].movebleObject.player == enemyTurn));
            if ((GetUnit(playerPoint).unit.curAP > 0) && RA.InRangeAttack(playerPoint, new PointXY(it, jt), GetUnit(playerPoint).unit, activeAction, null) && ((!map[it][jt].obj.isEmpty()) || ((!map[it][jt].movebleObject.isEmpty()) && (map[it][jt].movebleObject.player == enemyTurn))))
            {
                if ((playerChunk.movebleObject.damage.handStatus == 3) && (playerChunk.movebleObject.it.id == 0))
                {
                    return;
                }
                if ((playerChunk.movebleObject.damage.handStatus == 3))
                {
                    playerChunk.movebleObject.it.Active(playerChunk.movebleObject);
                }
                //if (GUI.Button(new Rect(Screen.width / 4, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 4, (Screen.height - dy * 6) / 2), attackTarget[language]))
                {
                    //Debug.Log(chosenPoint.x + " " + it + " " + chosenPoint.y + " " + jt);
                    GetUnit(playerPoint).unit.EmulateAbility(Ability.Attack);
                    block = true;
                    blockCounter += 2;
                    attacker = GetUnit(playerPoint);
                    GetUnit(new PointXY(it, jt)).unit.SetShowMinusHP(false, 0);
                    playerChunk.movebleObject.SetShowMinusAp(false);
                    PointXY p3 = new PointXY(-GetUnit(playerPoint).point.x + it, -GetUnit(playerPoint).point.y + jt);
                    if (activeAction == Ability.Attack)
                    {
                        GetUnit(playerPoint).unit.doAction(3);
                        GetUnitGO(playerPoint).GetComponent<UnitMove>().SetAttack(p3.x, p3.y, chunks[it][jt], p3.len());
                    }
                    else
                    {
                        GetUnit(playerPoint).unit.EmulateAbility(activeAction);
                        GetUnit(playerPoint).unit.ActivateAbility(activeAction);
                        GetUnitGO(playerPoint).GetComponent<UnitMove>().SetAbility(p3.x, p3.y, chunks[it][jt], activeAction);
                    }
                    targetPoint = new PointXY(it, jt);
                    // Debug.Log(-GetUnit(playerPoint).point.y + " " + jt);
                    //Debug.Log((-GetUnit(playerPoint).point.x + it) + " " + (-GetUnit(playerPoint).point.y + jt));
                    if (!map[targetPoint.x][targetPoint.y].movebleObject.isEmpty())
                    {
                        GetUnitGO(targetPoint).GetComponent<UnitMove>().DamageTakenDelay(attacker, targetPoint, GetUnitGO(attacker.point).GetComponent<UnitMove>().GetDelay(attacker, targetPoint));
                    }
                    else
                    {
                        if (!map[targetPoint.x][targetPoint.y].obj.isEmpty())
                        {
                            chunks[targetPoint.x][targetPoint.y].GetComponent<ObjectEffects>().DamageTakenDelay(attacker, targetPoint,
                                GetUnitGO(attacker.point).GetComponent<UnitMove>().GetDelay(attacker, targetPoint));
                        }
                    }
                    attackPoint = new PointXY(playerPoint);
                    attack = false;
                    //CheckNextTurn();
                }
            }
        }
    }

    void TryMove(int it, int jt) {
        if (map[it][jt].obj.isEmpty() && map[it][jt].movebleObject.isEmpty() && (GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed() >= dist[it][jt]))
        {
            //if (GUI.Button(new Rect(0, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 4, (Screen.height - dy * 6) / 2), moveHere[language]))
            {
                blockCounter++;
                int k = Mathf.CeilToInt(dist[it][jt] / GetUnit(playerPoint).unit.GetSpeed());
                GetUnit(playerPoint).unit.doAction(2, k);
                moveO = GetUnit(playerPoint);
                move = false;
                returnedToUnit = true;
                CameraMove(playerPoint.x, playerPoint.y);
                lastPoint = playerPoint;
                returnWait = true;
                chosenPoint = new PointXY(it, jt);
                chosenChunk = map[it][jt];
                playerPoint = new PointXY(it, jt);
                playerChunk = map[it][jt];
                //Dijkstra(playerPoint);
                move = false;
                attack = false;

                if (moveO.unit.curAP == 0)
                {
                    //    CheckNextTurn();
                }
            }
        }
    }

    public Texture2D wait;

    public void TryFriendlyAction(int it, int jt)
    {
        if (!block && !cameraMove)
        {
            nowActive = false;
            if ((activeAction == Ability.Defense) && (playerPoint.isEqual(new PointXY(it, jt))))
            {
                playerChunk.movebleObject.doAction(1);
                chosenChunk.movebleObject.SetShowMinusHP(false, 0);
                playerChunk.movebleObject.SetShowMinusAp(false);
                CheckNextTurn();
              //  Debug.Log(chosenPoint.isEqual(playerPoint));
            }
            if ((activeAction == Ability.Wait) && (playerPoint.isEqual(new PointXY(it, jt))))
            {
                playerChunk.movebleObject.doAction(4);
                chosenChunk.movebleObject.SetShowMinusHP(false, 0);
                playerChunk.movebleObject.SetShowMinusAp(false);
                CheckNextTurn();
                //  Debug.Log(chosenPoint.isEqual(playerPoint));
            }
            if ((activeAction == Ability.Item) && (LenPoints(playerPoint, chosenPoint) < playerChunk.movebleObject.GetRangeItem()))
            {
                if (playerChunk.movebleObject.it.activationCost <= playerChunk.movebleObject.curAP)
                {
                    playerChunk.movebleObject.ActivateItem(chosenChunk.movebleObject);
                }
                if (playerChunk.movebleObject.curAP == 0)
                {
                    chosenChunk.movebleObject.SetShowMinusHP(false, 0);
                    playerChunk.movebleObject.SetShowMinusAp(false);
                    CheckNextTurn();
                //    Debug.Log(chosenPoint.isEqual(playerPoint));
                }
                else
                {
                    Dijkstra(playerPoint);
                    activeAction = Ability.Attack;
                  //  CameraMove(playerPoint.x, playerPoint.y);
                }

            }
        }
    }

    bool returnedToUnit;

    bool loadWindow;
    LoadHelpObject lHelp;
    string[] loadText = { "Loading", "Загрузка" };
    bool nowActive;

    void LoadWindow(int WindowID)
    {
        int fontSize = 25;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    string[] nextText = { "Return to base", "Вернуться на базу" };
    string[] reText = { "Restart battle", "Переиграть бой" };

    PointXY GetLinesCross(float a, float b, float c, float a2, float b2, float c2)
    {
        PointXY ans = new PointXY(0, 0);
        ans.xf = -(c*b2 - c2*b) / (a*b2 - a2*b);
        ans.yf = -(a*c2 - a2*c) / (a*b2 - a2*b);
        return ans;
    }

    bool LineInSquare(int i, int j, float a, float b, float c)
    {
        PointXY left = new PointXY(GetLinesCross(1, 0, -(i - 0.5f), a, b, c));
        PointXY right = new PointXY(GetLinesCross(1, 0, -(i + 0.5f), a, b, c));
        PointXY top = new PointXY(GetLinesCross(0, 1, -(j + 0.5f), a, b, c));
        PointXY bot = new PointXY(GetLinesCross(0, 1, -(j - 0.5f), a, b, c));
        if ((Mathf.Abs(left.yf - j) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(right.yf - j) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(top.xf - i) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(bot.xf - i) <= 0.5f))
        {
            return true;
        }
        return false;
    }

    float LineMinusAim(PointXY p1, PointXY p2)
    {
        float res = 0;
        float a = p1.y - p2.y;
        float b = p2.x - p1.x;
        float c = p1.x * p2.y - p1.y * p2.x;

        for(int i = 0; i < mapWidth; i++)
        {
            for(int j = 0; j < mapHeight; j++)
            {
                float d = Mathf.Abs(a * i + b * j + c) / Mathf.Sqrt(a * a + b * b);
                PointXY p = new PointXY(i, j);
                if (!p.isEqual(p1) && !p.isEqual(p2)) {
                    if (LineInSquare(i, j, a, b, c))
                    {
                        if ((Mathf.Abs(i - p1.x) + Mathf.Abs(i - p2.x) == Mathf.Abs(p1.x - p2.x)) && (Mathf.Abs(j - p1.y) + Mathf.Abs(j - p2.y) == Mathf.Abs(p1.y - p2.y))) {
                            float help = 0;
                           // chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, true);
                            if (!map[i][j].movebleObject.isEmpty())
                            {
                                help += 20;
                            }
                            if (!map[i][j].obj.isEmpty())
                            {
                                if (map[i][j].obj.smallBlock)
                                {
                                    help += 20;
                                }
                                else
                                {
                                    help += 60;
                                }
                            }
                            //Debug.Log(d);
                            if (d > 0.6f)
                            {
                                help /= 2f;
                            }
                            if (d > 0.9f)
                            {
                             //   chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                                help = 0;
                            }
                            res += help;
                        }         
                    }
                    else
                    {
                      //  chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                    }
                }
                else
                {
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                }
            //    if (d <= 0.5f)
              //  {
                //    chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, true);
                //}
            }
        }
        return res;
    }

    Activity activity;

    bool FreeSpaceForSpawn(PointXY p)
    {
        bool flag = true;
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if ((LenPoints(new PointXY(i, j), p) <= 1.42f) && (!map[i][j].movebleObject.isEmpty() || !map[i][j].obj.isEmpty()))
                {
                    flag = false;
                }
            }
        }
        return flag;
    }

    bool SpaseWithoutUnits(PointXY p)
    {
        bool flag = true;
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if ((LenPoints(new PointXY(i, j), p) <= 1.42f) && !map[i][j].movebleObject.isEmpty())
                {
                    flag = false;
                }
            }
        }
        return flag;
    }

    void ClearSpaceForSquad(PointXY p)
    {
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if ((LenPoints(new PointXY(i, j), p) <= 1.42f))
                {
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(map[i][j].obj.id, false);
                    chunks[i][j].GetComponent<SpriteActive>().setIdActive(map[i][j].obj.id, false, false, false, false);
                    map[i][j].obj.Clear();
                }
            }
        }
    }

    string[] spawnTex = { "Spawn squad here", "Появить отряд здесь" };

    void OnGUI()
    {
        GUI.skin = skin;
        bool camBound = false;
        if (!cameraMove)
        {
            float vx = 0;
            float vy = 0;
            float len = GetOneChunkLen();
                if ((Input.mousePosition.x < Screen.height / 50) && (cam.transform.position.x > chunks[0][0].transform.position.x))
                {
                    vx = -len;
                    camBound = true;
                }
                if ((Input.mousePosition.y < Screen.height / 50) && (cam.transform.position.y > chunks[0][0].transform.position.y))
                {
                    vy = -len;
                    camBound = true;
                }
                if ((Input.mousePosition.x > Screen.width - Screen.width / 50) && (cam.transform.position.x < chunks[mapWidth - 1][mapHeight - 1].transform.position.x))
                {
                    vx = len;
                    camBound = true;
                }
                if ((Input.mousePosition.y > Screen.height - Screen.width / 50) && (cam.transform.position.y < chunks[mapWidth - 1][mapHeight - 1].transform.position.y))
                {
                    vy = len;
                    camBound = true;
                }   
            vx *= 2;
            vy *= 2;
            cam.GetComponent<Rigidbody2D>().velocity = new Vector2(vx, vy);
        }
        if ((Time.time - startCameraMove >= cameraTimer) && (!camBound))
        {
            cam.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            cameraMove = false;
            if (returnWait)
            {
                returnWait = false;
                GoTo(lastPoint, playerPoint, true);
            }
        }
        if (loadWindow)
        {
            GUI.skin = skin;
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
        else
        {
            //Debug.Log("*");
            GUI.DrawTexture(new Rect(0, (Screen.height - dx * 6) / 2 + dx * 6, Screen.width, (Screen.height - dx * 6) / 2), backGroundTexture, ScaleMode.ScaleAndCrop);
            GUI.DrawTexture(new Rect(0, 0, Screen.width, (Screen.height - dx * 6) / 2), backGroundTexture, ScaleMode.ScaleAndCrop);

            if (reGame)
            {
                if (GUI.Button(new Rect(0, (Screen.height - dx * 6) / 2 + dx * 6, Screen.width / 2, (Screen.height - dx * 6) / 2), reText[language]))
                {
                    SceneManager.LoadSceneAsync("BattleScene");
                    //  StartCoroutine(ShowAdWhenReady());
                }
                if (GUI.Button(new Rect(Screen.width / 2, (Screen.height - dx * 6) / 2 + dx * 6, Screen.width / 2, (Screen.height - dx * 6) / 2), nextText[language]))
                {
                    EndGame(gameResult);
                }
            }
            else
            {
                //Debug.Log(play);
                if (!play)
                {
                    if ((chosenPoint != null)) {
                        if ((dist[chosenPoint.x][chosenPoint.y] < INF))
                        {
                            if (GUI.Button(new Rect(Screen.width / 2, (Screen.height - dx * 6) / 2 + dx * 6, Screen.width / 2, (Screen.height - dx * 6) / 2), spawnTex[language]))
                            {
                                //Debug.Log("-");
                                Spawn(chosenPoint);
                            }
                        }
                    }
                    if (Input.GetMouseButton(0) && !block)
                    {
                        if (Input.GetMouseButtonDown(0))
                        {
                            //Debug.Log("+");
                            RaycastHit hit;
                            pressed = true;
                            nowActive = true;
                            if ((Input.mousePosition.y > (Screen.height - dy * 6) / 2) && (Input.mousePosition.y < Screen.height - (Screen.height - dy * 6) / 2))
                            {
                                for (int i = 0; i < mapWidth; i++)
                                {
                                    for (int j = 0; j < mapHeight; j++)
                                    {
                                        dist[i][j] = INF;
                                    }
                                }
                                //Debug.Log("+");
                                //Debug.Log(Input.mousePosition);
                                Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                                //Debug.Log(start);
                                Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                                start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                                ray.origin = start;
                               // Debug.Log(ray);
                                if (Physics.Raycast(ray, out hit))
                                {
                                    p = hit.transform.gameObject.GetComponent<CoordinatsHelper>().point;
                                    butttonPressed = p.x * mapWidth + p.y;
                                    int it;
                                    int jt;
                                    it = p.x;
                                    jt = p.y;
                                    chosenPoint = new PointXY(it, jt);
                                    //Debug.Log(it + " " + jt);
                                    if (((it == 0) && (jt == 0)) || ((it == mapWidth - 1) && (jt == 0))
                                            || ((it == 0) && (jt == mapHeight - 1)) || ((it == mapWidth - 1) && (jt == mapHeight - 1))){
                                        return;
                                    }
                                    if((it > 0) && (it < mapWidth - 1) && (jt > 0) && (jt < mapHeight - 1))
                                    {
                                        return;
                                    }
                                    for (int i = 0; i < mapWidth; i++)
                                    {
                                        for (int j = 0; j < mapHeight; j++)
                                        {
                                            if (FreeSpaceForSpawn(new PointXY(it ,jt))) {
                                                //Debug.Log(LenPoints(new PointXY(i, j), new PointXY(it, jt)));
                                                if (LenPoints(new PointXY(i, j), new PointXY(it, jt)) <= 1.42f)
                                                {
                                                    dist[i][j] = 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {

                    int it = p.x;
                    int jt = p.y;
                    //          GUI.DrawTexture(new Rect(0, (Screen.height - dx * 6) / 2 + dx * 6, Screen.width, (Screen.height - dx * 6) / 2), backGroundTexture, ScaleMode.ScaleAndCrop);
                    //        GUI.DrawTexture(new Rect(0, 0, Screen.width, (Screen.height - dx * 6) / 2), backGroundTexture, ScaleMode.ScaleAndCrop);
                    //GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height - (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2), healhPotion, ScaleMode.ScaleToFit);
                    //GUI.DrawTexture(new Rect(0, 0, deltaX, dx * 6), backGroundTexture, ScaleMode.ScaleAndCrop);
                    //GUI.DrawTexture(new Rect(Screen.width - deltaX, 0, deltaX, dx * 6), backGroundTexture, ScaleMode.ScaleAndCrop);
                    float shift = 6 * Screen.height / 8 - 6 * dx;
                    shift = (float)Screen.height / dx;
                    //Debug.Log(block + " " + blockCounter);
                    if (Input.GetMouseButton(0) && !block)
                    {
                        if (Input.GetMouseButtonDown(0) && !pressed)
                        {
                            RaycastHit hit;
                            pressed = true;
                            nowActive = true;
                            if ((Input.mousePosition.y > (Screen.height - dy * 6) / 2) && (Input.mousePosition.y < Screen.height - (Screen.height - dy * 6) / 2))
                            {
                                //Debug.Log(Input.mousePosition);
                                Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                                //Debug.Log(start);
                                Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                                start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                                ray.origin = start;
                                //Debug.Log(ray);
                                if (Physics.Raycast(ray, out hit))
                                {
                                    p = hit.transform.gameObject.GetComponent<CoordinatsHelper>().point;
                                    butttonPressed = p.x * mapWidth + p.y;
                                    it = p.x;
                                    jt = p.y;
                                    //Debug.Log(returnedToUnit);
                                    playerChunk.movebleObject.SetShowMinusAp(false);
                                    if (activity == Activity.Rotate)
                                    {
                                        GetUnit(playerPoint).SetRotation(new PointXY(it - playerPoint.x, jt - playerPoint.y));
                                        Rotation(GetUnitGO(playerPoint), it - playerPoint.x, jt - playerPoint.y);
                                    }
                                    if ((it == chosenPoint.x) && (jt == chosenPoint.y))
                                    {
                                        if ((!cameraMove) && (confrimAction))
                                        {
                                            //Debug.Log(chosenPoint.x + " " + it + " " + chosenPoint.y + " " + jt);
                                            TryAttack(playerPoint, playerChunk, it, jt);
                                            //Debug.Log(chosenPoint.x + " " + it + " " + chosenPoint.y + " " + jt);
                                            TryMove(it, jt);
                                            TryFriendlyAction(it, jt);
                                        }
                                    }
                                    else
                                    {
                                        if (map[it][jt].obj.isEmpty() && map[it][jt].movebleObject.isEmpty() && (GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed() >= dist[it][jt]))
                                        {
                                            int k = Mathf.CeilToInt(dist[it][jt] / GetUnit(playerPoint).unit.GetSpeed());
                                            playerChunk.movebleObject.SetShowMinusAp(true, k);
                                            //Debug.Log(k);
                                        }
                                    }
                                    if (!block && !cameraMove)
                                    {
                                        //Debug.Log(activeAction + " " + playerChunk.movebleObject.it.rangable + " " + ((activeAction == -2) && (playerChunk.movebleObject.it.rangable)));

                                        if (!(activity == Activity.Rotate) &&
                                            nowActive && !map[it][jt].movebleObject.isEmpty() && map[it][jt].movebleObject.player && (map[it][jt].movebleObject.curAP > 0) && !((activeAction == Ability.Item) && (playerChunk.movebleObject.it.rangable)))
                                        {
                                            playerChunk = map[it][jt];
                                            playerPoint = new PointXY(it, jt);
                                            playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                                            Dijkstra(playerPoint);
                                            activeAction = Ability.Defense;
                                            activity = Activity.Myself;
                                        }
                                        else
                                        {
                                            if ((!map[it][jt].movebleObject.isEmpty() && !map[it][jt].movebleObject.player) || (!map[it][jt].obj.isEmpty() && !map[it][jt].obj.permanent))
                                            {
                                                activeAction = Ability.Attack;
                                            }
                                        }
                                    }
                                    if (!block && !cameraMove)
                                    {
                                        if (!(new PointXY(it, jt)).isEqual(playerPoint))
                                        {
                                            GetUnit(playerPoint).SetRotation(new PointXY(it - playerPoint.x, jt - playerPoint.y));
                                            Rotation(GetUnitGO(playerPoint), it - playerPoint.x, jt - playerPoint.y);
                                            if (playerChunk.movebleObject.damage.handStatus == 3)
                                            {
                                                GetUnit(playerPoint).unit.SetExtraAim(LenPoints(playerPoint, new PointXY(it, jt)), LineMinusAim(playerPoint, new PointXY(it, jt)));
                                            }
                                        }
                                        if (!((it == chosenPoint.x) && (jt == chosenPoint.y)))
                                        {
                                            CameraMove(it, jt);
                                        }
                                    }
                                    move = true;
                                    attack = true;
                                    //Debug.Log(block + " " + cameraMove);
                                    chosenChunk.movebleObject.SetShowMinusHP(false, 0);

                                    showYourList = false;
                                    showEnemyList = false;
                                    if (nowActive)
                                    {
                                        chosenChunk = map[it][jt];
                                        chosenPoint = new PointXY(it, jt);
                                    }
                                    if (map[it][jt].movebleObject.isEmpty() && map[it][jt].obj.isEmpty())
                                    {
                                        if (map[it][jt].obj.isEmpty() && (Mathf.CeilToInt(dist[it][jt] / GetUnit(playerPoint).unit.GetSpeed()) <= playerChunk.movebleObject.curAP))
                                        {
                                            way = GetWay(chosenPoint);
                                            activity = Activity.Move;
                                        }
                                        else
                                        {
                                            activity = Activity.NoMove;
                                        }
                                    }

                                    if (!map[it][jt].obj.isEmpty() || (!map[it][jt].movebleObject.isEmpty() && !map[it][jt].movebleObject.player))
                                    {
                                        activity = Activity.Enemy;
                                    }
                                    if (!map[it][jt].movebleObject.isEmpty() && map[it][jt].movebleObject.player)
                                    {
                                        if (!chosenPoint.isEqual(playerPoint))
                                        {
                                            activity = Activity.Ally;
                                        }
                                        else
                                        {
                                            activity = Activity.Myself;
                                        }
                                        if (!(activeAction == Ability.Item &&
                                            playerChunk.movebleObject.it.active &&
                                            (playerChunk.movebleObject.GetRangeItem() < LenPoints(chosenPoint, playerPoint))))
                                        {
                                            activity = Activity.Myself;
                                        }
                                    }
                                    //if (activeAction == Ability.Attack)
                                    {
                                        if (!map[it][jt].movebleObject.isEmpty() && !map[it][jt].movebleObject.player)
                                        {
                                            playerChunk.movebleObject.EmulateAbility(Ability.Attack);
                                            estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                            if (playerChunk.movebleObject.curAP > 0)
                                            {
                                                chosenChunk.movebleObject.SetShowMinusHP(true, (int)(0.8f * estimatedDamage.realD));
                                                playerChunk.movebleObject.SetShowMinusAp(true);
                                            }
                                        }
                                        if (!map[it][jt].obj.isEmpty())
                                        {
                                            playerChunk.movebleObject.EmulateAbility(Ability.Attack);
                                            estimatedDamage = map[it][jt].obj.EstimatedDamage(playerChunk.movebleObject);
                                            if (playerChunk.movebleObject.curAP > 0)
                                            {
                                                playerChunk.movebleObject.SetShowMinusAp(true);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        pressed = false;
                    }
                    it = p.x;
                    jt = p.y;
                    //Debug.Log(returnedToUnit);
                    for (int i = 0; i < mapWidth; i++)
                    {
                        for (int j = 0; j < mapHeight; j++)
                        {
                            //Debug.Log(returnedToUnit);
                            if (move && !block && !returnedToUnit)
                            {
                                int k = Mathf.CeilToInt(dist[i][j] / GetUnit(playerPoint).unit.GetSpeed());
                                if (!enemyTurn && k <= GetUnit(playerPoint).unit.curAP)
                                {
                                    if (k > 0)
                                    {
                                        chunks[i][j].GetComponent<SpriteActive>().SetText(k.ToString());
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().SetText("");
                                }
                            }
                            else
                            {
                                chunks[i][j].GetComponent<SpriteActive>().SetText("");
                            }
                        }
                    }
                    if (!block)
                    {
                        DrawItems(activity == Activity.Myself || activity == Activity.Ally);
                        DrawMyself(activity == Activity.Myself);
                        DrawAbilities(activity == Activity.Enemy || activity == Activity.Ally);
                        DrawMove(activity == Activity.Move);
                        if (!enemyTurn && chosenChunk != null)
                        {
                            GUI.Window(0, new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), StatsWindow, "");
                            //   Vector2 newPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
                            // if (new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2).Contains(newPos))
                            // {
                            // /   GUI.DrawTexture(new Rect(0, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2), chosen);
                            // }
                            if (showEnemyList && !playerPoint.isEqual(chosenPoint))
                            {
                                GUI.Window(3, new Rect(Screen.width / 4 * 3, (Screen.height - (dy * 6)) / 2, Screen.width / 4, Screen.height / 2 - (Screen.height - (dy * 6)) / 2), EnemyFullStatsWindow, effectsList[language]);
                            }
                            if (showYourList)
                            {
                                GUI.Window(4, new Rect(0, (Screen.height - (dy * 6)) / 2, Screen.width / 4, Screen.height / 2 - (Screen.height - (dy * 6)) / 2), YourFullStatsWindow, effectsList[language]);
                            }
                            //if ((!chosenChunk.movebleObject.isEmpty() && !chosenChunk.movebleObject.player) || !chosenChunk.obj.isEmpty())
                            {
                                GUI.skin = skin;
                                GUI.Window(2, new Rect(Screen.width / 4, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2), EstimatedDamageWindow, estDamageText[language]);
                            }
                            if (!playerPoint.isEqual(chosenPoint))
                            {
                                GUI.Window(1, new Rect(Screen.width / 4 * 3, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), StatsWindow2, "");
                            }
                        }
                        CheckChosenStatus();
                    }
                }
            }
        }
    }


    public void DrawItems(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        GUI.DrawTexture(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), fonTex);
        if ((playerChunk != null))
        {
            if (playerChunk.movebleObject.it.active || playerChunk.movebleObject.it.showen)
            {
                Texture2D tex = null;
                if (playerChunk.movebleObject.it.id == 1)
                {
                    tex = healhPotion;
                }
                if (playerChunk.movebleObject.it.id == 6)
                {
                    tex = itemsTexures[0];
                }
                if (playerChunk.movebleObject.it.id == 7)
                {
                    tex = itemsTexures[1];
                }
                if (playerChunk.movebleObject.it.id == 8)
                {
                    tex = itemsTexures[2];
                }
                if (playerChunk.movebleObject.it.id == 9)
                {
                    tex = itemsTexures[3];
                }
                if (playerChunk.movebleObject.it.id == 10)
                {
                    tex = itemsTexures[4];
                }
                if (playerChunk.movebleObject.it.id == 11)
                {
                    tex = itemsTexures[4];
                }
                if (playerChunk.movebleObject.it.id == 12)
                {
                    tex = itemsTexures[4];
                }
                if (playerChunk.movebleObject.it.id == 13)
                {
                    tex = itemsTexures[4];
                }
                //GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height - (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2), tex, ScaleMode.ScaleAndCrop);
                if (GUI.Button(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len, len), tex, itemStyle) && playerChunk.movebleObject.it.active)
                {
                    if (activeAction == Ability.Item && !confrimAction)
                    {
                        TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                        if (chosenPoint.isEqual(playerPoint))
                        {
                            activeAction = Ability.Defense;
                        }
                    }
                    else
                    {
                        activeAction = Ability.Item;
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.it.activationCost);
                    }
                }
                GUI.Box(new Rect(shiftAbilities + len * 2 + len  /2,
                    Screen.height - (Screen.height - dy * 6) / 2 + len /2,
                    len / 2, len / 2), playerChunk.movebleObject.it.count.ToString());
                if (activeAction == Ability.Item)
                {
                    GUI.DrawTexture(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                }

            }
        }
        if ((playerChunk != null) && (!cameraMove) && !playerPoint.isEqual(chosenPoint))
        {
            if ((playerChunk.movebleObject.id != 0) && (playerChunk.movebleObject.player) && (playerChunk.movebleObject.curAP > 0))
            {
                if ((playerChunk.movebleObject.it.active) && (LenPoints(playerPoint, chosenPoint) <= playerChunk.movebleObject.GetRangeItem()) && (playerChunk.movebleObject.it.friendly == playerChunk.movebleObject.player))
                {
                    Texture2D tex = null;
                    if (playerChunk.movebleObject.it.id == 6)
                    {
                        tex = itemsTexures[0];
                    }
                    //GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height - (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2), tex, ScaleMode.ScaleAndCrop);
                    if (GUI.Button(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 6, (Screen.height - dy * 6) / 2), tex, itemStyle))
                    {
                        activeAction = Ability.Item;
                        //    Debug.Log(playerChunk.movebleObject.it.activationCost);
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.it.activationCost);
                    }
                    if (activeAction == Ability.Item)
                    {
                        GUI.DrawTexture(new Rect(shiftAbilities +0, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 6, (Screen.height - dy * 6) / 2), chosen, ScaleMode.StretchToFill);
                    }
                }
                if (GUI.Button(new Rect(shiftAbilities + Screen.width / 6 * 5, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 6, (Screen.height - dy * 6) / 2), cancel, itemStyle))
                {
                    GetUnit(chosenPoint).unit.SetShowMinusHP(false, 0);
                    activeAction = Ability.Defense;
                    activity = Activity.Myself;
                    playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                    chosenChunk = playerChunk;
                    chosenPoint = playerPoint;
                    CameraMove(chosenPoint.x, chosenPoint.y);
                }
            }
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), overTex);
        }
    }

    float shiftAbilities;// = Screen.width / 2 - ((Screen.height - dy * 6.0f) / 4.0f) * 6;

    public void DrawMyself(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        GUI.DrawTexture(new Rect(shiftAbilities, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), fonTex);
        if ((playerChunk != null))
        {
            if ((playerChunk.movebleObject.id != 0) && (playerChunk.movebleObject.player) && (playerChunk.movebleObject.curAP > 0))
            {
                if (GUI.Button(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, len, len), defense, itemStyle))
                {
                    if (activeAction == Ability.Defense && !confrimAction)
                    {
                        TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                        if (chosenPoint.isEqual(playerPoint))
                        {
                            activeAction = Ability.Defense;
                        }
                    }
                    else
                    {
                        activeAction = Ability.Defense;
                        activity = Activity.Myself;
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                    }
                }
                if (activity == Activity.Myself)
                {
                    if (activeAction == Ability.Defense)
                    {
                        GUI.DrawTexture(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                    }
                }
                if (GUI.Button(new Rect(shiftAbilities + len, Screen.height - (Screen.height - dy * 6) / 2, len, len), wait, itemStyle))
                {
                    if (activeAction == Ability.Wait && !confrimAction)
                    {
                        TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                        if (chosenPoint.isEqual(playerPoint))
                        {
                            activeAction = Ability.Defense;
                        }
                    }
                    else
                    {
                        activeAction = Ability.Wait;
                        activity = Activity.Myself;
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                    }
                }
                if (activeAction == Ability.Wait)
                {
                    GUI.DrawTexture(new Rect(shiftAbilities + len, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                }
            }
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), overTex);
        }
    }

    public void DrawAbilities(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        int it = chosenPoint.x;
        int jt = chosenPoint.y;
        GUI.DrawTexture(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len * 4, len * 2), fonTex);
        if (!enemyTurn && (playerChunk != null) && (!playerChunk.movebleObject.isEmpty()) && (!block))
        {
            int cou = 0;
            //if (RA.InRangeAttack(playerPoint, chosenPoint, playerChunk.movebleObject, Ability.Attack, null))
            {
                if (GUI.Button(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len, len), attackTex, itemStyle))
                {
                    if (!enemyTurn && (playerChunk != null) && (!cameraMove) && !chosenChunk.movebleObject.player && (!chosenChunk.movebleObject.isEmpty() || !chosenChunk.obj.isEmpty()))
                    {
                        if (RA.InRangeAttack(playerPoint, chosenPoint, playerChunk.movebleObject, Ability.Attack, null))
                        {
                            //Debug.Log(activeAction);
                            if (activeAction == Ability.Attack && !confrimAction)
                            {
                                TryAttack(playerPoint, playerChunk, chosenPoint.x, chosenPoint.y);
                               // Debug.Log("TryAttack");
                            }
                            else
                            {
                                activeAction = Ability.Attack;

                                //Debug.Log(2);
                                playerChunk.movebleObject.EmulateAbility(Ability.Attack);

                                estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                playerChunk.movebleObject.SetShowMinusAp(true);
                            }
                        }
                    }
                }
                //Debug.Log(activeAction + " " + activity);
                if ((activeAction == Ability.Attack) && (activity == Activity.Enemy))
                {
                    GUI.DrawTexture(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                }
                cou++;
            }
            for (int i = 0; i < spellTex.Length; i++)
            {
                Ability ability = Ability.Attack;
                switch (i)
                {
                    case 0:
                        ability = Ability.ShieldBash;
                        break;
                    case 1:
                        ability = Ability.Cleave;
                        break;
                    case 2:
                        ability = Ability.DoubleStrike;
                        break;
                    case 3:
                        ability = Ability.CurvedTrajectoryShot;
                        break;
                }
             //   Debug.Log(playerChunk.movebleObject.shieldPerks[3] + " " +
              //      playerChunk.movebleObject.abilities[1] + " " +
               //     playerChunk.movebleObject.abilities[2] + " " +
                //    playerChunk.movebleObject.abilities[3]);
                if ((playerChunk.movebleObject.HaveAbility(ability)))// && (RA.InRangeAttack(playerPoint, chosenPoint, GetUnit(playerPoint).unit, activeAction, chosenPoint)))
                {
                    //Debug.Log(ability);
                    if (GUI.Button(new Rect(shiftAbilities + len * 4 + len * cou, Screen.height - (Screen.height - dy * 6) / 2, len, len), spellTex[i], itemStyle))
                    {
                        
                        if ((activity == Activity.Enemy)&&
                            (playerChunk.movebleObject.abilities[i]) && 
                            (RA.InRangeAttack(playerPoint, chosenPoint, GetUnit(playerPoint).unit, activeAction, chosenPoint)))
                        {
                            if (activeAction == ability && !confrimAction)
                            {
                                TryAttack(playerPoint, playerChunk, chosenPoint.x, chosenPoint.y);
                            }
                            else
                            {
                                activeAction = ability;
                                //Debug.Log(i);
                                playerChunk.movebleObject.EmulateAbility(activeAction);
                                playerChunk.movebleObject.SetShowMinusAp(i);
                                if (i == 0)
                                {
                                    playerChunk.movebleObject.shieldBashAbility = true;
                                    //Debug.Log(1);
                                    estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                    playerChunk.movebleObject.shieldBashAbility = false;
                                }
                                if (i == 1)
                                {
                                    estimatedDamage = new PointXY(0, 0);
                                    PointXY help;
                                    for (int j = 0; j < mapWidth; j++)
                                    {
                                        for (int k = 0; k < mapHeight; k++)
                                        {
                                            if (RA.InRangeAttack(playerPoint, new PointXY(j, k), playerChunk.movebleObject, activeAction, chosenPoint))
                                            {
                                                if (!map[j][k].movebleObject.isEmpty() && !map[j][k].movebleObject.player)
                                                {
                                                    help = map[j][k].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                                    estimatedDamage.realD += help.realD;
                                                    estimatedDamage.x += (int)(0.8f * help.realD);
                                                    map[j][k].movebleObject.SetShowMinusHP(true, (int)(0.8f * help.realD));
                                                }
                                                if (!map[j][k].obj.isEmpty())
                                                {
                                                    help = map[j][k].obj.EstimatedDamage(playerChunk.movebleObject);
                                                    estimatedDamage.realD += help.realD;
                                                    estimatedDamage.x += (int)(0.8f * help.realD);
                                                }
                                            }
                                        }
                                    }
                                }
                                if (i == 2)
                                {
                                    // playerChunk.movebleObject.doubleHandedStrike = true;
                                    estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                }
                                if (i == 3)
                                {
                                    estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));

                                }
                            }
                            //Debug.Log(((float)playerChunk.movebleObject.curAbilityCulldown[i]) / (float)playerChunk.movebleObject.abilityCulldowns[i]);
                            
                        }
                    }
                    GUI.DrawTexture(new Rect(len * 4 + len * cou, Screen.height - (Screen.height - dy * 6) / 2, len, (len) * ((float)playerChunk.movebleObject.curAbilityCulldown[i]) / (float)playerChunk.movebleObject.abilityCulldowns[i]), reload);
                    if (activeAction == ability)
                    {
                        GUI.DrawTexture(new Rect(shiftAbilities + len * 4 + len * cou, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                    }
                    cou++;
                }
            }
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len * 4, len * 2), overTex);
        }
    }

    public void DrawMove(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), fonTex);
        int it = chosenPoint.x;
        int jt = chosenPoint.y;
        if(GUI.Button(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len, len), moveTex, itemStyle)){
            if(activity == Activity.Move && !confrimAction)
            {
                TryMove(chosenPoint.x, chosenPoint.y);
            }
        }
        if (activity == Activity.Move)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen);
        }
        if (GUI.Button(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2 + len, len, len), rotateTex, itemStyle))
        {
            activity = Activity.Rotate;
        }
        if (activity == Activity.Rotate)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2 + len, len, len), chosen);
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), overTex);
        }
    }

    public Texture2D arrowDown;

    public Texture2D moveTex;

    public bool confrimAction;

    public Texture2D cancel;

    float LenPoints(PointXY a, PointXY b)
    {
        return Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y)*(a.y - b.y));
    }

    public Texture2D[] itemsTexures;

    public Texture2D reload;

    public Texture2D[] spellTex;

    public Texture2D attackTex;
    public Texture2D chosen;

    public GUIStyle freeStyle;

    bool showYourList;
    bool showEnemyList;

    string[] estDamageText = {"Action description", "Описание действия"};
    string[] damageText = { "Damage: ", "Урон: " };
    string[] hitChanceText = { "Hit chance: ", "Шанс попадания: " };
    string[] critChanceText = { "Crit :", "Крит: " };
    string[] effectsList = { "Effects list", "Список эффектов" };

    public GUIStyle itemStyle;

    void EnemyFullStatsWindow(int WindowID)
    {
        string s = "";
        if (chosenChunk.movebleObject.id != 0)
        {
            if (!chosenChunk.movebleObject.it.active && !chosenChunk.movebleObject.it.isEmpty())
            {
                s += chosenChunk.movebleObject.it.itemNames[chosenChunk.movebleObject.it.id] + '\n' + chosenChunk.movebleObject.it.itemStats[chosenChunk.movebleObject.it.id] + chosenChunk.movebleObject.it.value + '\n';
            }
            if (chosenChunk.movebleObject.defense > 0)
            {
                int val = 20;
                if (chosenChunk.movebleObject.extraDefense)
                {
                    val += 15;
                }
                s += defenseText[language] + ": " + val + "(" + chosenChunk.movebleObject.defense + ")" + '\n';
            }
            if (chosenChunk.movebleObject.inTheLinePerk)
            {
                if (chosenChunk.movebleObject.valueInTheLine > 0)
                {
                    s += inTheLineText[language] + ": " + chosenChunk.movebleObject.valueInTheLine + '\n';
                }
            }
            for(int i = 0; i < chosenChunk.movebleObject.effects.Count; i++)
            {
                s += ((Effect)chosenChunk.movebleObject.effects[i]).name + " (" + ((Effect)chosenChunk.movebleObject.effects[i]).turns + ")";
            }
            if (chosenChunk.movebleObject.GetCalculatedShot())
            {
                s += '\n' + chosenChunk.movebleObject.bowNames[4];
            }
        }
        GUI.Box(new Rect(0, 0, Screen.width / 4, Screen.height / 2 - (Screen.height - (dy * 6)) / 2), s);
    }

    string[] inTheLineText = { "In the line", "В строю" };

    void YourFullStatsWindow(int WindowID)
    {
        string s = "";
        if (playerChunk.movebleObject.id != 0)
        {
            if (!playerChunk.movebleObject.it.active && !playerChunk.movebleObject.it.isEmpty())
            {
                s += playerChunk.movebleObject.it.itemNames[playerChunk.movebleObject.it.id] + '\n' + playerChunk.movebleObject.it.itemStats[playerChunk.movebleObject.it.id] + playerChunk.movebleObject.it.value + '\n';
            }
            if (playerChunk.movebleObject.defense > 0)
            {
                int val = 20;
                if (playerChunk.movebleObject.extraDefense)
                {
                    val += 15;
                }
                s += defenseText[language] + ": " + val + "(" + playerChunk.movebleObject.defense + ")" + '\n';
            }
            if (playerChunk.movebleObject.valueInTheLine > 0)
            {
                s += inTheLineText[language] + ": " + playerChunk.movebleObject.valueInTheLine + '\n';
            }
            for (int i = 0; i < chosenChunk.movebleObject.effects.Count; i++)
            {
                s += ((Effect)playerChunk.movebleObject.effects[i]).name + " (" + ((Effect)playerChunk.movebleObject.effects[i]).turns + ")";
            }
            if (playerChunk.movebleObject.GetCalculatedShot())
            {
                s += '\n' + playerChunk.movebleObject.bowNames[4];
            }
        }
        GUI.Box(new Rect(0, 0, Screen.width / 4, Screen.height / 2 - (Screen.height - (dy * 6)) / 2), s);
    }

    public Texture2D rotateTex;

    void EstimatedDamageWindow(int WindowID)
    {
        if (!enemyTurn)
        {
            string s = "";
            if ((!chosenChunk.movebleObject.isEmpty() && !chosenChunk.movebleObject.player) || (!chosenChunk.obj.isEmpty() && !chosenChunk.obj.permanent))
            {
                if (activeAction != Ability.Cleave)
                {
                    if (chosenChunk.obj.id != 0)
                    {
                        s += damageText[language] + (int)(estimatedDamage.realD * 0.8f) + "-" + estimatedDamage.realD + ' ' + critChanceText[language] + System.Math.Round(estimatedDamage.realC, 1) + "%" + '\n';
                        s += hitChanceText[language] + System.Math.Round((estimatedDamage.realH), 2) + "%";
                    }
                    if (chosenChunk.movebleObject.id != 0)
                    {
                        //Debug.Log(estimatedDamage);
                        s += damageText[language] + (int)(estimatedDamage.realD * 0.8f) + "-" + estimatedDamage.realD + ' ' + critChanceText[language] + System.Math.Round(estimatedDamage.realC, 1) + "%" + '\n';
                        s += hitChanceText[language] + System.Math.Round((estimatedDamage.realH), 2) + "%";
                    }
                }
                else
                {
                    s += damageText[language] + estimatedDamage.x + "-" + estimatedDamage.realD;
                }
                if (activity == Activity.Rotate)
                {
                    s = rotateText[language];
                    GUI.Box(new Rect(0, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2), s);
                }
                else
                {
                    GUI.Box(new Rect(0, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), s);
                    s = playerChunk.movebleObject.GetDescrAbility(activeAction);
                    GUI.Box(new Rect(Screen.width / 4, 0, Screen.width / 4, (Screen.height - (dy * 6)) / 2), s);
                }
            }
            else
            {
                if (chosenChunk.movebleObject.isEmpty() && chosenChunk.movebleObject.isEmpty())
                {
                    if (Mathf.CeilToInt((dist[chosenPoint.x][chosenPoint.y] / playerChunk.movebleObject.GetSpeed())) <= playerChunk.movebleObject.curAP)
                    {
                        s = playerChunk.movebleObject.GetMoveDescr(moveAction) + '\n';
                        s += moveCost[language] + Mathf.CeilToInt((dist[chosenPoint.x][chosenPoint.y] / playerChunk.movebleObject.GetSpeed())).ToString();
                    }
                    else
                    {
                        s = moveTextToFar[language];
                    }
                }
                else {
                    if ((activeAction == Ability.Defense) && (playerPoint.isEqual(chosenPoint)))
                    {
                        s = playerChunk.movebleObject.GetFriendlyActionDescr(activeAction);
                        //  Debug.Log(chosenPoint.isEqual(playerPoint));
                    }
                    if ((activeAction == Ability.Wait) && (playerPoint.isEqual(chosenPoint)))
                    {
                        s = playerChunk.movebleObject.GetFriendlyActionDescr(activeAction);
                        //  Debug.Log(chosenPoint.isEqual(playerPoint));
                    }
                    if ((activeAction == Ability.Item) && (LenPoints(playerPoint, chosenPoint) < playerChunk.movebleObject.GetRangeItem()))
                    {
                        s = playerChunk.movebleObject.GetFriendlyActionDescr(activeAction);

                    }
                }
                if(activity == Activity.Rotate)
                {
                    s = rotateText[language];
                }
                GUI.Box(new Rect(0, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2), s);
            }
          /*  if (!confrimAction && !cameraMove && !enemyTurn && !block)
            {
                if (GUI.Button(new Rect(0, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2), "", freeStyle))
                {
                    TryAttack(playerPoint, playerChunk, chosenPoint.x, chosenPoint.y);
                    TryMove(chosenPoint.x, chosenPoint.y);
                    TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                }
                Vector2 newPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
                if (new Rect(Screen.width / 4, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2).Contains(newPos))
                {
                    GUI.DrawTexture(new Rect(0, 0, Screen.width / 2, (Screen.height - (dy * 6)) / 2), chosen);
                }
            }
        }
    }

    int moveAction = 0;

    string[] rotateText = { "Rotate" + '\n' + "Rotate character to the point", "Поворот" +'\n'+ "Поверните человека на выбранную точку" };

    string[] moveTextToFar = { "To far", "Слишком далеко" };
    string[] moveCost = { "Movement cost: ", "Стоимость передвижения: " };

    string[] defenseText = {"Defense", "Оборона" };
    string[] attackText = {"Attack", "Атака" };
    string[] moveText = {"Move", "Идти" };
    string[] moveHere = { "Move here", "Идти сюда" };*/
}

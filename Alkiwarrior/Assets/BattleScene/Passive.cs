﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Passive{

    //extraHP extraAP extraAim healthRegeneration  damageReduce time    reducePerOverwight extraApIfKill   extraCritForEachWound extraCrit   extraDodgeWithoutOwrweight extraTimeDefence    mobileDefence image

    public string keyName;
    public string[] type;
    public float incStrength;
    public float incAgility;
    public float incStamina;
    public float incWillpower;
    public float incInteligence;
    public int useCounter;
    public string damage;
    public string armour;
    public float bashIfHit;
    public float improveOneHand;
    public float improveTwoHand;
    public float improveOneParry;
    public float improveTwoParry;
    public float extraSpeed;
    public float extraDodge;
    public int extraBonusDamage;
    public int extraHP;
    public int extraAP;
    public float extraAim;
    public int healthRegeneration;
    public int damageReduce;
    public int time;
    public bool reducePerOverwight;
    public bool extraApIfKill;
    public bool extraCritForEachWound;
    public float extraCrit;
    public bool extraDodgeWithoutOwrweight;
    public int extraTimeDefence;
    public bool mobileDefence;
    public string image;
    public float extraParry;
    public float extraBlock;
    public string[] targetTypes;
    public string[] useCounterConditions;
    public string[] removeConditions;

    StaticData staticData;
    public Damage damageAdd;
    public ArmourValue armourAdd;

    public bool isActive = false;
    public bool delete = false;

    public void Active(MovebleObject parent, MovebleObject move)
    {

        if (staticData.ArrayHasTag(targetTypes, "FRIENDLY"))
        {
            if (parent.team != move.team)
            {
                return;
            }
        }
        if (staticData.ArrayHasTag(targetTypes, "ENEMY"))
        {
            if (parent.team == move.team)
            {
                return;
            }
        }

        foreach (var i in removeConditions)
        {
            if (move.state.ContainsKey(i))
            {
                if (move.state[i])
                {
                    return;
                }
            }
        }
        if (useCounter > 0)
        {
            bool flag = false;
            foreach(var i in useCounterConditions)
            {
                if (parent.state[i])
                {
                   flag = true;
                }
            }
            if (flag)
            {
                useCounter--;
                if (useCounter == 0)
                {
                    delete = true;
                }
            }
        }

        move.extraDamageRightHand.Add(damageAdd);
        move.extraDamageLeftHand.Add(damageAdd);
        move.extraArmour.Add(armourAdd);
        move.speed += extraSpeed;
        //Debug.Log(extraHP);
        move.maxHits += extraHP;
        move.maxAP += extraAP;
        move.aim += extraAim;
        move.dodgeChance += extraDodge;
        move.critChance += extraCrit;
        move.extraParry += extraParry;
        move.extraBlock += extraBlock;
        move.reducedDamage += damageReduce;
        move.bonusDamage += extraBonusDamage;

        isActive = true;
    }

    public bool isPermanent()
    {
        return staticData.ArrayHasTag(type, "PERMANENT");
    }

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        damageAdd = null;
        armourAdd = null;
        if (damage != "-")
        {
            damageAdd = new Damage(staticData.GetDamageByKeyName(damage));
        }
        if(armour != "-")
        {
            armourAdd = new ArmourValue(staticData.GetArmourValueByKeyName(armour));
        }
    }

    public Passive(Passive p)
    {
        keyName = p.keyName;
        type = p.type;
        extraBlock = p.extraBlock;
        extraParry = p.extraParry;
        incStrength = p.incStrength;
        incAgility = p.incAgility;
        incStamina = p.incStamina;
        incWillpower = p.incWillpower;
        incInteligence = p.incInteligence;
        useCounter = p.useCounter;
        damage = p.damage;
        armour = p.armour;
        bashIfHit = p.bashIfHit;
        improveOneHand  = p.improveOneHand;
        improveTwoHand = p.improveTwoHand;
        improveOneParry = p.improveOneParry;
        improveTwoParry = p.improveTwoParry;
        extraSpeed = p.extraSpeed;
        extraDodge = p.extraDodge;
        extraBonusDamage = p.extraBonusDamage;
        extraHP = p.extraHP;
        extraAP = p.extraAP;
        extraAim = p.extraAim;
        healthRegeneration = p.healthRegeneration;
        damageReduce = p.damageReduce;
        time = p.time;
        reducePerOverwight = p.reducePerOverwight;
        extraApIfKill = p.extraApIfKill;
        extraCritForEachWound = p.extraCritForEachWound;
        extraCrit = p.extraCrit;
        extraDodgeWithoutOwrweight = p.extraDodgeWithoutOwrweight;
        extraTimeDefence = p.extraTimeDefence;
        mobileDefence = p.mobileDefence;
        image = p.image;
        removeConditions = p.removeConditions;
        Init();
}
}

﻿using UnityEngine;
using System.Collections;

public class PointXY
{
    public string abilityKeyName;
    public int x;
    public int y;
    public float xf;
    public float yf;
    public float h1;
    public float h2;
    public int realD;
    public float realH;
    public float realC;
    public bool resultArchery;
    public EstimateDamage est;

    public PointXY(int a, int b)
    {
        this.x = a;
        this.y = b;
        this.xf = 0;
        this.yf = 0;
        this.h1 = 0;
        this.h2 = 0;
        this.realD = 0;
        this.realH = 0;
        this.realC = 0;
        abilityKeyName = "";
    }

    public PointXY(PointXY p)
    {
        //Debug.Log(p);
        this.x = p.x;
        this.y = p.y;
        this.xf = p.xf;
        this.yf = p.yf;
        this.h1 = p.h1;
        this.h2 = p.h2;
        this.realD = p.realD;
        this.realH = p.realH;
        this.realC = p.realC;
        this.abilityKeyName = p.abilityKeyName;
    }

    public PointXY(PointXY s, PointXY f)
    {
        this.x = f.x - s.x;
        this.y = f.y - s.y;
        this.xf = 0;
        this.yf = 0;
        this.h1 = 0;
        this.h2 = 0;
        this.realD = 0;
        this.realH = 0;
        this.realC = 0;
    }

    public bool isEqual(PointXY p)
    {
        if (p != null)
        {
            if (this.x != p.x)
            {
                return false;
            }
            if (this.y != p.y)
            {
                return false;
            }
        }
        return true;
    }

    public void norm()
    {
        float len = this.x * this.x + this.y * this.y;
        len = Mathf.Sqrt(len);
        this.xf = this.x / len;
        this.yf = this.y / len;
    }

    public float len()
    {
        float len2 = this.x * this.x + this.y * this.y;
        len2 = Mathf.Sqrt(len2);
        return len2;
    }
}

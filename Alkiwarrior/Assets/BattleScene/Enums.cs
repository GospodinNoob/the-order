﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Activity {

	NoMove,
    Ally,
    Myself,
    Enemy,
    Move,
    Rotate
}

public enum Move
{

    Move,
    Sprint,
    Teleport
}

public enum Ability
{
    Empty,
    Attack,
    Item,
    ShieldBash,
    Cleave,
    DoubleStrike,
    CurvedTrajectoryShot,
    Defense,
    Wait
}

public enum WeaponType
{

}

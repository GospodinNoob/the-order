﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIUnitController : MonoBehaviour {

    Image noneHealth;
    Image fullHealth;
    Image minusHealth;
    Image extraEmptyAp;
    Image emptyAp;
    Image fullAp;
    Image extraFullAp;
    Image showSpendAp;

	// Use this for initialization
	void Awake () {
        fullHealth = this.transform.Find("FullHealth").GetComponent<Image>();
        minusHealth = this.transform.Find("MinusHealth").GetComponent<Image>();
        noneHealth = this.transform.Find("NoneHealth").GetComponent<Image>();
        extraEmptyAp = this.transform.Find("ExtraEmptyAp").GetComponent<Image>();
        emptyAp = this.transform.Find("EmptyAp").GetComponent<Image>();
        fullAp = this.transform.Find("FullAp").GetComponent<Image>();
        extraFullAp = this.transform.Find("ExtraFullAp").GetComponent<Image>();
        showSpendAp = this.transform.Find("ShowSpendAp").GetComponent<Image>();
    }
	
	public void UpdateByUnit(MovebleObject move)
    {
       // Debug.Log("up");
        fullHealth.fillAmount = 0.5f * move.curHits / move.maxHits;
        minusHealth.gameObject.SetActive(false);
        extraFullAp.gameObject.SetActive(false);
        extraEmptyAp.gameObject.SetActive(false);
        showSpendAp.gameObject.SetActive(false);
        emptyAp.fillAmount = move.maxAP * (15f / 360f) + 7.5f / 360f;
        fullAp.fillAmount = move.curAP * (15f / 360f) + 7.5f / 360f;
    }

    public void ShowSpendAp(MovebleObject move, int ap)
    {
        Debug.Log(ap);
        extraFullAp.gameObject.SetActive(false);
        extraEmptyAp.gameObject.SetActive(false);
        showSpendAp.gameObject.SetActive(true);
        showSpendAp.fillAmount = move.curAP * (15f / 360f) + 7.5f / 360f;
        emptyAp.fillAmount = move.maxAP * (15f / 360f) + 7.5f / 360f;
        fullAp.fillAmount = (move.curAP - ap) * (15f / 360f) + 7.5f / 360f;
    }

    public void ShowDamage(MovebleObject move, int damage)
    {
        if(move.go == this.gameObject)
        {
            return;
        }
        fullHealth.fillAmount = 0.5f * (move.curHits - damage) / move.maxHits;
        minusHealth.gameObject.SetActive(true);
        minusHealth.fillAmount = 0.5f * move.curHits / move.maxHits;
    }

    private void Update()
    {
        Color c = minusHealth.color;
        c.a = Mathf.Abs(Mathf.Sin(Time.time));
        minusHealth.color = c;
        Color a = showSpendAp.color;
        a.a = Mathf.Abs(Mathf.Sin(Time.time));
        showSpendAp.color = a;
    }
}

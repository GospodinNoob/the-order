﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]

public class Item {

    public string keyName;
    public string[] activeAbilities;
    public string[] passiveAbilities;
    public int pound;
    public string image;
    public List<Active> active = new List<Active>();
    public List<Passive> passive = new List<Passive>();

    StaticData staticData;

    public Item()
    {
        keyName = "";
        pound = 0;
        image = "";
        activeAbilities = new string[] { };
        passiveAbilities = new string[] { };
    }

    public Item(Item it)
    {
        keyName = it.keyName;
        activeAbilities = it.activeAbilities;
        passiveAbilities = it.passiveAbilities;
        pound = it.pound;
        image = it.image;
        Init();
    }    

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        foreach (var i in passiveAbilities)
        {
            if (i.Length > 2)
            {
                passive.Add(new Passive(staticData.GetPassiveByKeyName(i)));
                passive[passive.Count - 1].Init();
            }
        }
        foreach (var i in activeAbilities)
        {
            if (i.Length > 2)
            {
                active.Add(new Active(staticData.GetActiveByKeyName(i)));
                active[active.Count - 1].Init();
            }
        }
    }

}

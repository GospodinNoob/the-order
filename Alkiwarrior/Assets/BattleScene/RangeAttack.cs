﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeAttack {

	public RangeAttack()
    {
        map = null;
    }

    public ChunkController[][] map;


    public bool InRangeAttack(PointXY pAttack, PointXY pTarget, MovebleObject move, Ability activeAction, PointXY chosenPoint)
    {
     //   Debug.Log(pAttack + " " + pTarget);
        //Debug.Log(activeAction + "+");
        int rang = move.GetRangAttack();
       // Debug.Log(pTarget.x + " " + pTarget.y);
        //Debug.Log(map.Length + " ");
        if (map[pTarget.x][pTarget.y].obj.permanent)
        {
            return false;
        }
        //Debug.Log(activeAction);
        if ((activeAction == Ability.Attack) || (activeAction == Ability.DoubleStrike))
        {
            if (rang == 1)
            {
                if ((Mathf.Abs(pAttack.x - pTarget.x) <= 1) && (Mathf.Abs(pAttack.y - pTarget.y) <= 1) && (Mathf.Abs(pAttack.x - pTarget.x) + Mathf.Abs(pAttack.y - pTarget.y) == 1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            if (rang == 2)
            {
                if ((Mathf.Abs(pAttack.x - pTarget.x) <= 1) && (Mathf.Abs(pAttack.y - pTarget.y) <= 1))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            if ((rang == 3) || (rang == 4))
            {
                if ((Mathf.Abs(pAttack.x - pTarget.x) <= 1) && (Mathf.Abs(pAttack.y - pTarget.y) <= 1))
                {
                    return true;
                }
                //if (move.freeSpace)
                if(true)
                {
                    //Debug.Log(1);
                    if ((pAttack.x - pTarget.x == -2) && (pAttack.y - pTarget.y == 0))
                    {
                        return map[pAttack.x + 1][pAttack.y].isSmallObj();
                    }
                    if ((pAttack.x - pTarget.x == 2) && (pAttack.y - pTarget.y == 0))
                    {
                        return map[pAttack.x - 1][pAttack.y].isSmallObj();
                    }
                    if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 0))
                    {
                        return map[pAttack.x][pAttack.y + 1].isSmallObj();
                    }
                    if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 0))
                    {
                        return map[pAttack.x][pAttack.y - 1].isSmallObj();
                    }
                }
                else
                {
                    if ((pAttack.x - pTarget.x == -2) && (pAttack.y - pTarget.y == 0))
                    {
                        return map[pAttack.x + 1][pAttack.y].canMove(move);
                    }
                    if ((pAttack.x - pTarget.x == 2) && (pAttack.y - pTarget.y == 0))
                    {
                        return map[pAttack.x - 1][pAttack.y].canMove(move);
                    }
                    if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 0))
                    {
                        return map[pAttack.x][pAttack.y + 1].canMove(move);
                    }
                    if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 0))
                    {
                        return map[pAttack.x][pAttack.y - 1].canMove(move);
                    }
                }
            }
            if (rang == 4)
            {
                //if (move.freeSpace)
                if(true)
                {
                    if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 1))
                    {
                        return map[pAttack.x][pAttack.y - 1].isSmallObj() && map[pAttack.x - 1][pAttack.y - 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == -1))
                    {
                        return map[pAttack.x][pAttack.y - 1].isSmallObj() && map[pAttack.x + 1][pAttack.y - 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 1))
                    {
                        return map[pAttack.x][pAttack.y + 1].isSmallObj() && map[pAttack.x - 1][pAttack.y + 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == -1))
                    {
                        return map[pAttack.x][pAttack.y + 1].isSmallObj() && map[pAttack.x + 1][pAttack.y + 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == 2))
                    {
                        return map[pAttack.x - 1][pAttack.y].isSmallObj() && map[pAttack.x - 1][pAttack.y - 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == 2))
                    {
                        return map[pAttack.x - 1][pAttack.y].isSmallObj() && map[pAttack.x - 1][pAttack.y + 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == -2))
                    {
                        return map[pAttack.x + 1][pAttack.y].isSmallObj() && map[pAttack.x + 1][pAttack.y - 1].isSmallObj();
                    }

                    if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == -2))
                    {
                        return map[pAttack.x + 1][pAttack.y].isSmallObj() && map[pAttack.x + 1][pAttack.y + 1].isSmallObj();
                    }
                }
                else
                {
                    {
                        if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 1))
                        {
                            if (!InMap(pAttack.x - 1, pAttack.y - 1))
                            {
                                return true;
                            }
                            return map[pAttack.x][pAttack.y - 1].canMove(move) && map[pAttack.x - 1][pAttack.y - 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == -1))
                        {
                            if (!InMap(pAttack.x + 1, pAttack.y - 1))
                            {
                                return true;
                            }
                            return map[pAttack.x][pAttack.y - 1].canMove(move) && map[pAttack.x + 1][pAttack.y - 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 1))
                        {
                            if (!InMap(pAttack.x - 1, pAttack.y + 1))
                            {
                                return true;
                            }
                            return map[pAttack.x][pAttack.y + 1].canMove(move) && map[pAttack.x - 1][pAttack.y + 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == -1))
                        {
                            if (!InMap(pAttack.x + 1, pAttack.y + 1))
                            {
                                return true;
                            }
                            return map[pAttack.x][pAttack.y + 1].canMove(move) && map[pAttack.x + 1][pAttack.y + 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == 2))
                        {
                            if (!InMap(pAttack.x - 1, pAttack.y - 1))
                            {
                                return true;
                            }
                            return map[pAttack.x - 1][pAttack.y].canMove(move) && map[pAttack.x - 1][pAttack.y - 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == 2))
                        {
                            if (!InMap(pAttack.x - 1, pAttack.y + 1))
                            {
                                return true;
                            }
                            return map[pAttack.x - 1][pAttack.y].canMove(move) && map[pAttack.x - 1][pAttack.y + 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == -2))
                        {
                            if (!InMap(pAttack.x + 1, pAttack.y - 1))
                            {
                                return true;
                            }
                            return map[pAttack.x + 1][pAttack.y].canMove(move) && map[pAttack.x + 1][pAttack.y - 1].canMove(move);
                        }

                        if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == -2))
                        {
                            if(!InMap(pAttack.x + 1, pAttack.y + 1))
                            {
                                return true;
                            }
                            return map[pAttack.x + 1][pAttack.y].canMove(move) && map[pAttack.x + 1][pAttack.y + 1].canMove(move);
                        }
                    }
                }
            }
            if (rang > 4)
            {
                return (LenPoints(pAttack, pTarget) <= move.GetRangeAttack()) && (FreeAround(move, pAttack));
            }
        }
        else
        {
            //Debug.Log(activeAction);
            if ((activeAction == Ability.ShieldBash) || (activeAction == Ability.DoubleStrike))
            {
                return false;// LenPoints(pAttack, pTarget) <= move.GetAbilityRange(activeAction);
            }
            if (activeAction == Ability.CurvedTrajectoryShot)
            {
                return false;// FreeAround(move, pAttack) && (LenPoints(pAttack, pTarget) <= move.GetAbilityRange(activeAction));
            }
            if (pTarget.isEqual(pAttack))
            {
                return false;
            }
            //if ((map[pTarget.x][pTarget.y].movebleObject.player == move.player) || map[pTarget.x][pTarget.y].obj.permanent)
            if (map[pTarget.x][pTarget.y].obj.permanent)
            {
                return false;
            }
            if (map[pTarget.x][pTarget.y].movebleObject.isEmpty() && map[pTarget.x][pTarget.y].obj.isEmpty())
            {
                return false;
            }
            if (activeAction == Ability.Cleave)
            {
                //Debug.Log("+");
                if (LenPoints(pAttack, pTarget) > 0)// move.GetAbilityRange(activeAction))
                {
                    return false;
                }
                PointXY vec = new PointXY(pAttack, pTarget);
                //Debug.Log(vec.x + " " + vec.y);
                double a1 = Mathf.Atan2(vec.x, vec.y);
                if (a1 < 0)
                {
                    a1 += Mathf.PI * 2;
                }
                PointXY vec2 = new PointXY(pAttack, chosenPoint);
                double a2 = Mathf.Atan2(vec2.x, vec2.y);
                if (a2 < 0)
                {
                    a2 += Mathf.PI * 2;
                }
                double resultAngle = (a2 - a1);
                if (resultAngle < 0)
                {
                    resultAngle += Mathf.PI * 2;
                }
                //chunks[pTarget.x][pTarget.y].GetComponent<SpriteActive>().SetText(System.Math.Round(resultAngle, 2).ToString());
                //Debug.Log(Mathf.PI * 2 - Mathf.PI / 8f - 0.01f);
                if ((resultAngle < Mathf.PI / 8f * 3f + 0.11f) || (resultAngle > Mathf.PI * 2 - Mathf.PI / 4f - 0.1f))
                {
                    bool f = InRangeAttack(pAttack, pTarget, move, Ability.Attack, chosenPoint);
                    //Debug.Log(f);
                    return f;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    public float LenPoints(PointXY a, PointXY b)
    {
      //  Debug.Log(a + " " + b);
        return Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }

    PointXY GetLinesCross(float a, float b, float c, float a2, float b2, float c2)
    {
        PointXY ans = new PointXY(0, 0);
        ans.xf = -(c * b2 - c2 * b) / (a * b2 - a2 * b);
        ans.yf = -(a * c2 - a2 * c) / (a * b2 - a2 * b);
        return ans;
    }

    bool LineInSquare(int i, int j, float a, float b, float c)
    {
        PointXY left = new PointXY(GetLinesCross(1, 0, -(i - 0.5f), a, b, c));
        PointXY right = new PointXY(GetLinesCross(1, 0, -(i + 0.5f), a, b, c));
        PointXY top = new PointXY(GetLinesCross(0, 1, -(j + 0.5f), a, b, c));
        PointXY bot = new PointXY(GetLinesCross(0, 1, -(j - 0.5f), a, b, c));
        if ((Mathf.Abs(left.yf - j) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(right.yf - j) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(top.xf - i) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(bot.xf - i) <= 0.5f))
        {
            return true;
        }
        return false;
    }

    public float LineMinusAim(PointXY p1, PointXY p2)
    {
        float res = 0;
        float a = p1.y - p2.y;
        float b = p2.x - p1.x;
        float c = p1.x * p2.y - p1.y * p2.x;

        for (int i = 0; i < map.Length; i++)
        {
            for (int j = 0; j < map[0].Length; j++)
            {
                float d = Mathf.Abs(a * i + b * j + c) / Mathf.Sqrt(a * a + b * b);
                PointXY p = new PointXY(i, j);
                if (!p.isEqual(p1) && !p.isEqual(p2))
                {
                    if (LineInSquare(i, j, a, b, c))
                    {
                        if ((Mathf.Abs(i - p1.x) + Mathf.Abs(i - p2.x) == Mathf.Abs(p1.x - p2.x)) && (Mathf.Abs(j - p1.y) + Mathf.Abs(j - p2.y) == Mathf.Abs(p1.y - p2.y)))
                        {
                            float help = 0;
                            // chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, true);
                            if (!map[i][j].movebleObject.isEmpty())
                            {
                                help += 20;
                            }
                            if (!map[i][j].obj.isEmpty())
                            {
                                if (map[i][j].obj.smallBlock)
                                {
                                    help += 20;
                                }
                                else
                                {
                                    help += 60;
                                }
                            }
                            //Debug.Log(d);
                            if (d > 0.6f)
                            {
                                help /= 2f;
                            }
                            if (d > 0.9f)
                            {
                                //   chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                                help = 0;
                            }
                            res += help;
                        }
                    }
                    else
                    {
                        //  chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                    }
                }
                else
                {
                }
                //    if (d <= 0.5f)
                //  {
                //    chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, true);
                //}
            }
        }
        return res;
    }

    public bool FreeAround(MovebleObject m, PointXY a)
    {
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (!((i == 0) && (j == 0)))
                {
                    if (InMap(a.x + i, a.y + j))
                    {
                        if ((!map[a.x + i][a.y + j].movebleObject.isEmpty()) && (map[a.x + i][a.y + j].movebleObject.player != m.player))
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    bool InMap(int a, int b)
    {
        int mapWidth = map.Length;
        int mapHeight = map[0].Length;
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= mapWidth)
        {
            return false;
        }
        if (b >= mapHeight)
        {
            return false;
        }
        return true;
    }
}

﻿using UnityEngine;
using System.Collections;

public class StatusShow : MonoBehaviour {

    public GameObject onePercentHealth, nonePercentHealth, midPercentHealth, fullAPObj, noneApObj, waitApbj, waitNoneApObj;

    GameObject[] lenHealth, noneHealth, fullAP, emptyAp, waitAp, waitNoneAp;


    int n, k;
    float dx, dy;

    int maxHealth;
    int curHealth;
    int maxAP;
    int curAp;
    int waitApCur;

    int len;

    void ReShow()
    {
        //Debug.Log(curAp + " " + maxAP);
        for(int i = 0; i < 5; i++)
        {
            if (i < curAp)
            {
            //    Debug.Log(waitApCur);
                if (i < waitApCur)
                {
                    fullAP[i].active = true;
                    emptyAp[i].active = true;
                    waitAp[i].active = false;
                    waitNoneAp[i].active = false;
                }
                else
                {
                    fullAP[i].active = false;
                    emptyAp[i].active = false;
                    waitAp[i].active = true;
                    waitNoneAp[i].active = true;
                }
            }
            else
            {
                if(i < maxAP)
                {
                    //Debug.Log(i + " " + maxAP);
                  //  Debug.Log(waitApCur);
                    if (i < waitApCur)
                    {
                        fullAP[i].active = false;
                        emptyAp[i].active = true;
                        waitAp[i].active = false;
                        waitNoneAp[i].active = false;
                    }
                    else
                    {
                        fullAP[i].active = false;
                        emptyAp[i].active = false;
                        waitAp[i].active = false;
                        waitNoneAp[i].active = true;
                    }
                    
                }
                else
                {
                    //Debug.Log(i + " " + maxAP);
                    fullAP[i].active = false;
                    emptyAp[i].active = false;
                    waitAp[i].active = false;
                    waitNoneAp[i].active = false;
                }
            }
        }
        for(int i = 0; i < maxHP; i++)
        {
            if(maxHP - 1 - i < curHealth)
            {
                lenHealth[i].active = true;
            }
            else
            {
                if (maxHP - 1 - i < maxHealth)
                {
                    //Debug.Log(i);
                    lenHealth[i].active = false;
                }
                else {
                    lenHealth[i].active = false;
                    noneHealth[i].active = false;
                }
            }
        }
        if (curHealth <= 0)
        {
            this.gameObject.active = false;
           // Debug.Log(1);
        }
    }

    public void SetHealth(int a)
    {
        curHealth = a;
        ReShow();
    }

    public void SetAp(int a)
    {
        curAp = a;
        ReShow();
    }

    public void IniHealth(int a, int b)
    {
        maxHealth = b;
        curHealth = a;
        SetHealth(a);
    }

    public void IniAP(MovebleObject move)
    {
        curAp = move.curAP;
        maxAP = move.maxAP;
        waitApCur = move.maxAP - move.waitAP;
        SetAp(curAp);
    }

    public void MinusHpShow(bool tf, int dmg)
    {
        for (int i = 0; i < maxHP; i++)
        {
            lenHealth[i].GetComponent<SpriteRenderer>().color = defaultColorHp;
        }
        if (tf)
        {
            showMinusHp = tf;
            showDmg = dmg;
            startHp = Time.time;
        }
        else
        {
            showMinusHp = tf;
            for (int i = 0; i < maxHP; i++)
            {
                lenHealth[i].GetComponent<SpriteRenderer>().color = defaultColorHp;
            }
        }
    }

    public void MinusApShow(bool tf, int dmg, MovebleObject move)
    {
       // Debug.Log(tf + " " + dmg);
        for (int i = 0; i < 5; i++)
        {
            fullAP[i].GetComponent<SpriteRenderer>().color = defaultColorHp;
            waitAp[i].GetComponent<SpriteRenderer>().color = defaultColorWait;
        }
        if (dmg == 0)
        {
            tf = false;
        }
        if (tf)
        {
            showMinusAp = tf;
            showAp = dmg;
            maxAP = move.maxAP;
            waitApCur = move.maxAP - move.waitAP;
         //   Debug.Log(move.maxAP + " " + move.waitAP);
            startAp = Time.time;
        }
        else
        {
            showMinusAp = tf;
            waitApCur = move.maxAP - move.waitAP;
            for (int i = 0; i < 5; i++)
            {
                fullAP[i].GetComponent<SpriteRenderer>().color = defaultColorHp;
                waitAp[i].GetComponent<SpriteRenderer>().color = defaultColorWait;
            }
        }
       
    }
    int maxHP;

    void AlphaShowHp(int v)
    {
        for(int i = Mathf.Max(0, curHealth - showDmg); i < curHealth; i++)
        {
            lenHealth[maxHP - 1 - i].GetComponent<SpriteRenderer>().color = new Color(defaultColorHp.r, defaultColorHp.g, defaultColorHp.b, Mathf.Cos((startHp - Time.time) * 3));
        }
    }

    void AplhaShowAp(int v)
    {
        for (int i = Mathf.Max(0, curAp - showAp); i < curAp; i++)
        {
            fullAP[i].GetComponent<SpriteRenderer>().color = new Color(defaultColorAp.r, defaultColorAp.g, defaultColorAp.b, Mathf.Cos((startAp - Time.time) * 3));
            waitAp[i].GetComponent<SpriteRenderer>().color = new Color(defaultColorWait.r, defaultColorWait.g, defaultColorWait.b, Mathf.Cos((startAp - Time.time) * 3));
        }
    }

    float startHp;
    float startAp;
    bool showMinusAp;
    int showDmg;
    int showAp;
    bool showMinusHp;
    Color defaultColorHp;
    Color defaultColorAp;
    Color defaultColorWait;

	// Use this for initialization
	void Awake () {
        maxHP = 30;
        showMinusHp = false;
        defaultColorHp = onePercentHealth.GetComponent<SpriteRenderer>().color;
        defaultColorAp = fullAPObj.GetComponent<SpriteRenderer>().color;
        defaultColorWait = waitApbj.GetComponent<SpriteRenderer>().color;
        n = 6;
        k = 8;
        dx = Screen.width / n;
        dy = Screen.height / k;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }
        fullAP = new GameObject[5];
        emptyAp = new GameObject[5];
        lenHealth = new GameObject[maxHP];
        noneHealth = new GameObject[maxHP];
        waitAp = new GameObject[5];
        waitNoneAp = new GameObject[5];

        for(int i = 0; i < 5; i++)
        {
            fullAP[i] = (GameObject)GameObject.Instantiate(fullAPObj, this.transform.position, this.transform.rotation);
            fullAP[i].transform.rotation = Quaternion.Euler(0, 0, 10 + 180 + (i + 1) * 15);
            float scale = fullAP[i].transform.localScale.x * 1.4f;
            fullAP[i].transform.localScale = new Vector3(scale, scale, scale);
            fullAP[i].transform.parent = this.gameObject.transform;

            emptyAp[i] = (GameObject)GameObject.Instantiate(noneApObj, this.transform.position, this.transform.rotation);
            emptyAp[i].transform.rotation = Quaternion.Euler(0, 0, 10 + 180 + (i + 1) * 15);
            scale = emptyAp[i].transform.localScale.x * 1.4f;
            emptyAp[i].transform.localScale = new Vector3(scale, scale, scale);
            emptyAp[i].transform.parent = this.gameObject.transform;

            waitAp[i] = (GameObject)GameObject.Instantiate(waitApbj, this.transform.position, this.transform.rotation);
            waitAp[i].transform.rotation = Quaternion.Euler(0, 0, 10 + 180 + (i + 1) * 15);
            scale = waitAp[i].transform.localScale.x * 1.4f;
            waitAp[i].transform.localScale = new Vector3(scale, scale, scale);
            waitAp[i].transform.parent = this.gameObject.transform;

            waitNoneAp[i] = (GameObject)GameObject.Instantiate(waitNoneApObj, this.transform.position, this.transform.rotation);
            waitNoneAp[i].transform.rotation = Quaternion.Euler(0, 0, 10 + 180 + (i + 1) * 15);
            scale = waitNoneAp[i].transform.localScale.x * 1.4f;
            waitNoneAp[i].transform.localScale = new Vector3(scale, scale, scale);
            waitNoneAp[i].transform.parent = this.gameObject.transform;
        }

        for (int i = 0; i < maxHP; i++)
        {
            lenHealth[i] = (GameObject)GameObject.Instantiate(onePercentHealth, this.transform.position, this.transform.rotation);
            lenHealth[i].transform.rotation = Quaternion.Euler(0, 0, 120 +  i * 13);
            float scale = lenHealth[i].transform.localScale.x * 1.38f;
            lenHealth[i].transform.localScale = new Vector3(scale, scale, scale);
            lenHealth[i].transform.parent = this.gameObject.transform;

            noneHealth[i] = (GameObject)GameObject.Instantiate(nonePercentHealth, this.transform.position, this.transform.rotation);
            noneHealth[i].transform.rotation = Quaternion.Euler(0, 0, 120 + i * 13);
            scale = noneHealth[i].transform.localScale.x * 1.38f;
            noneHealth[i].transform.localScale = new Vector3(scale, scale, scale);
            noneHealth[i].transform.parent = this.gameObject.transform;
        }
	}
	
	// Update is called once per frame
	void Update () {
	    if (showMinusHp)
        {
            for(int i = 0; i < maxHP; i++)
            {
                AlphaShowHp(i);
            }
        }
        if (showMinusAp)
        {
            for(int i = 0; i < 5; i++)
            {
                AplhaShowAp(i);
            }
        }
	}
}

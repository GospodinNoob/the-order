﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Weapon{

    public string keyName;
    public int pound;
    public float attackRange;
    public int handStatus;
    public int baseBlock;
    public int baseParry;
    public string image;
    public string sprite;
    public string[] passiveAbilities;
    public string[] activeAbilities;
    public bool locked;
    public string[] types;
    public List<Active> active = new List<Active>();
    public List<Passive> passive = new List<Passive>();

    public Weapon(Weapon d)
    {
        this.keyName = d.keyName;
        this.pound = d.pound;
        this.attackRange = d.attackRange;
        this.handStatus = d.handStatus;
        this.baseBlock = d.baseBlock;
        this.baseParry = d.baseParry;
        this.passiveAbilities = d.passiveAbilities;
        this.activeAbilities = d.activeAbilities;
        image = d.image;
        sprite = d.sprite;
        locked = d.locked;
        types = d.types;
        Init();
    }

    StaticData staticData;

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        foreach(var i in passiveAbilities)
        {
            if (i.Length > 2)
            {
                passive.Add(new Passive(staticData.GetPassiveByKeyName(i)));
            }
        }
        foreach (var i in activeAbilities)
        {
            if (i.Length > 2)
            {
                active.Add(new Active(staticData.GetActiveByKeyName(i)));
            }
        }
    }

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class BattleSceneUI : MonoBehaviour
{
    public GameObject humanGO;
    public GameObject chunkPrefab;
    int n, k;
    int magicConstant = 53;

    int maxPlayerUnits;
    int maxEnemyUnits;
    int maxUnits;

    public GameObject cam;

    Vector3 defaultCameraPosition;
    public AudioSource au;

    bool gameResult;

    bool enemyTurn;


    //List<List<Chunk>> map;

    MapTemplate map;

    Chunk chosenChunk;
    PointXY chosenPoint;
    PointXY wayPoint;
    bool move = false;
    bool attack = false;

    Unit moveO;

    int mapHeight;
    int mapWidth;
    int shiftX;
    int shiftY;

    GameEvent ev;
    RangeAttack RA;

    int INF = 10000000;

    int curEnemyUnits;
    int language;

    float coef = (Screen.height / Screen.width);
    float dc;
    bool ini;
    bool play;
    int enemyUnitNow;
    bool reGame;
    bool block = false;
    int blockCounter;


    PointXY attackPoint;
    PointXY targetPoint;

    PointXY p;
    bool pressed;

    float cameraTimer;
    float cameraSpeed;
    float startCameraMove;
    bool cameraMove;
    bool returnWait;
    PointXY lastPoint;
    PointXY estimatedDamage;

    bool returnedToUnit;

    bool loadWindow;
    LoadHelpObject lHelp;
    bool nowActive;

    float dx;
    float dy;

    void Rotation(GameObject go, int vx, int vy)
    {
        // Debug.Log(vx.ToString() + " " + vy.ToString());
        float angle = Mathf.Atan2(vx, -vy);
        angle *= 57.2958f;
        go.GetComponent<HelperScript>().renderGO.gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        //Debug.Log(angle);
    }
    bool InMap(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= mapWidth)
        {
            return false;
        }
        if (b >= mapHeight)
        {
            return false;
        }
        return true;
    }

    public float GetScale()
    {
        return map.units[0].go.transform.localScale.x;
    }

    void GenEnemyUnit(int a, int b)
    {
        map.units.Add(new MovebleObject(staticData.GetRandomUnitWithMightByTag(0, 10000, "DEMON")));
        Transform tr = this.gameObject.transform;
        GameObject go = (GameObject)GameObject.Instantiate(humanGO, tr);
        map.units[map.units.Count - 1].go = go;
        map.units[map.units.Count - 1].team = "DEMON";
        map.units[map.units.Count - 1].point = new PointXY(a, b);
        map.units[map.units.Count - 1].rotation = new PointXY(0, -1);
        //units[map.units.Count - 1].unit.Init();
        map.units[map.units.Count - 1].go.GetComponent<UnitMove>().SetDx(dx);
        map.units[map.units.Count - 1].go.GetComponent<UnitMove>().SetMain(this.gameObject, cam, map.units[map.units.Count - 1]);
        ///map.units[map.units.Count - 1].go.GetComponent<HelperScript>().SetPlayerInd(false);
        map.units[map.units.Count - 1].go.name = map.units[map.units.Count - 1].keyName;
        Transform help = map.units[map.units.Count - 1].go.transform.parent;
        map.units[map.units.Count - 1].go.transform.parent = map.chunks[a][b].transform;
        map.units[map.units.Count - 1].go.transform.localPosition = new Vector3(0, 0, 0);
        map.units[map.units.Count - 1].go.transform.parent = help;
        ////Debug.Log('*');
        Rotation(map.units[map.units.Count - 1].go, map.units[map.units.Count - 1].rotation.x, map.units[map.units.Count - 1].rotation.y);
        //Debug.Log('*');
        float scale = map.units[map.units.Count - 1].go.transform.localScale.x * dx;
        map.units[map.units.Count - 1].go.transform.localScale = new Vector3(scale, scale, scale);
        map.map[a][b].movebleObject = map.units[map.units.Count - 1];
        map.map[a][b].movebleObject = map.units[map.units.Count - 1];
        map.units[map.units.Count - 1].units = map.units;
        //units[map.units.Count - 1].map = map.map;
        //map.units[map.units.Count - 1].IniAI();
        //Debug.Log(map.units.Count + " " + map.units.Count);
        map.units[map.units.Count - 1].go.GetComponent<ShowActive>().IniRender(map.units[map.units.Count - 1]);
        map.units[map.units.Count - 1].go.GetComponentInChildren<UIUnitController>().UpdateByUnit(map.units[map.units.Count - 1]);
        map.units[map.units.Count - 1].curAP = 0;
        //map.units[map.units.Count - 1].RecalcHp();
        //map.units[map.units.Count - 1].RecalcAP();
        //Debug.Log('*');
    }
    void GenerateEnemySquad(int a, int b, bool rotation)
    {
        int squadUnitCount = 0;
        if (curEnemyUnits <= 6)
        {
            squadUnitCount = curEnemyUnits;
        }
        else
        {
            squadUnitCount = Random.Range(4, 7);
        }
        curEnemyUnits -= squadUnitCount;
        if (rotation)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenEnemyUnit(a + i, b + j);
                        squadUnitCount--;
                        // curEnemyUnits--;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenEnemyUnit(a + i, b + j);
                        squadUnitCount--;
                    }
                }
            }
        }
    }
    bool EmptyChunkForGen(int x, int y)
    {
        if((map.map[x][y].movebleObject == null) && (map.map[x][y].obj == null))
        {
            return true;
        }
        if (InMap(x, y))
        {
            return (map.map[x][y].movebleObject == null) && (!map.map[x][y].obj.permanent);
        }
        else
        {
            return false;
        }
    }
    bool EmptySpaceForSquad(int a, int b, bool tf)
    {
        bool flag = true;
        if (tf)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    flag = flag && EmptyChunkForGen(a + i, b + j);
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    flag = flag && EmptyChunkForGen(a + i, b + j);
                }
            }
        }
        return flag;
    }
    void GenerateEnemies()
    {
        int a = Random.Range(2, mapWidth - 2);
        int b = Random.Range(2, mapHeight - 2);
        bool rotation = true;
        if (Random.Range(0, 2) == 0)
        {
            rotation = false;
        }
        int step = 0;
        curEnemyUnits = maxEnemyUnits;
        while ((curEnemyUnits > 0) && (step < 50))
        {
            step = 0;
            while (!EmptySpaceForSquad(a, b, rotation) && (step < 50))
            {
                a = Random.Range(2, mapWidth - 2);
                b = Random.Range(2, mapHeight - 2);
                rotation = true;
                if (Random.Range(0, 2) == 0)
                {
                    rotation = false;
                }
                step++;
            }
            //Debug.Log(step);
            if (step < 50)
            {
                if (rotation)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        for (int j = 0; j <= 2; j++)
                        {
                            if (map.map[a + i][b + j].obj != null)
                            {
                                map.map[a + i][b + j].obj.Clear();
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        for (int j = 0; j <= 1; j++)
                        {
                            if (map.map[a + i][b + j].obj != null)
                            {
                                map.map[a + i][b + j].obj.Clear();
                            }
                        }
                    }
                }
                //Debug.Log(rotation);
                GenerateEnemySquad(a, b, rotation);
            }
            //curEnemyUnits--;
        }
    }
    

    void Awake()
    {
        n = 8;
        k = 6;
        dx = Screen.width / n;
        dy = Screen.height / k;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }
    }

    StaticData staticData;

    void Start()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        IniTech();
        InitUI();
        map = new MapTemplate();
        map.CreateMap();
        StartIni();
        map.ClearMapInterface();
        map.CheckTextures();
        mapWidth = map.width;
        mapHeight = map.height;
    }

    Squad playerSquad;

    void IniTech()
    {
        RA = new RangeAttack();
        returnedToUnit = false;
        au.volume *= PlayerPrefs.GetFloat("Volume");
        returnWait = false;
        cameraMove = false;
        cameraTimer = -1;
        cameraSpeed = -1;
        language = PlayerPrefs.GetInt("Language");
        lHelp = new LoadHelpObject(language);
        p = new PointXY(0, 0);
        ev = new GameEvent("ChosenEvent", language);
        mapHeight = ev.height;
        mapWidth = ev.width;
        enemyTurn = false;
        defaultCameraPosition = new Vector3(cam.transform.position.x, cam.transform.position.y, cam.transform.position.z);
        maxPlayerUnits = 0;
        playerSquad = new Squad("BattleSquad");
        maxPlayerUnits = playerSquad.GetUnitsCou();
        maxEnemyUnits = ev.enemyCounter;
        maxUnits = maxPlayerUnits + maxEnemyUnits;
    }

    void Spawn(PointXY p)
    {
        //Debug.Log("P" + p.x + " " + p.y);
        int width = 0;
        if ((p.y == 0) || (p.y == mapHeight - 1))
        {
            width = 3;
        }
        else
        {
            width = 2;
        }
        int height = 5 - width;
        if (p.x != 0)
        {
            p.x -= 1;
        }
        if (p.y != 0)
        {
            p.y -= 1;
        }
        int a = p.x;
        int b = p.y;
        //Debug.Log(a + " " + b);
        for (int i = 0; i < playerSquad.units.Count; i++)
        {
            if (playerSquad.units[i] != null) {
                //playerSquad.units[i].Init();
                //units[i] = new Unit(new PointXY()));
                map.units.Add(playerSquad.units[i]);
                playerSquad.units[i].team = "PLAYER";
                Transform tr = this.gameObject.transform;
                GameObject go = (GameObject)GameObject.Instantiate(humanGO, tr);
                map.units[map.units.Count - 1].go = go;
                map.units[map.units.Count - 1].point = new PointXY(a + i % width, b + (i / width));
                map.units[map.units.Count - 1].rotation = new PointXY(1, 0);
                map.units[map.units.Count - 1].player = true;
                //units[i]playerSquad.units[i];
                //playerUnitsGO[i] = (GameObject)GameObject.Instantiate(humanGO, tr);
                //map.units[map.units.Count - 1].go.GetComponent<HelperScript>().SetPlayerInd(true);
                //Debug.Log(map.units[map.units.Count - 1].keyName);
                map.units[map.units.Count - 1].go.name = map.units[map.units.Count - 1].keyName;
                map.units[map.units.Count - 1].go.transform.position = new Vector3(tr.position.x - dx / 2 - 2 * dx + dx * (a + i % width), tr.position.y - dx / 2 - dx + dx * (b + (i / width)), tr.position.z);
                float scale = map.units[map.units.Count - 1].go.transform.localScale.x * dx;
                Rotation(map.units[map.units.Count - 1].go, map.units[map.units.Count - 1].rotation.x, map.units[map.units.Count - 1].rotation.y);
                map.units[map.units.Count - 1].go.transform.localScale = new Vector3(scale, scale, scale);
                //Debug.Log((a + i % width).ToString() + " " + (b + (i / width)).ToString());
                map.map[a + i % width][b + (i / width)].movebleObject = map.units[map.units.Count - 1];
                map.map[a + i % width][b + (i / width)].movebleObject = map.units[map.units.Count - 1];
                map.units[map.units.Count - 1].go.GetComponent<ShowActive>().IniRender(playerSquad.units[i]);
                map.units[map.units.Count - 1].go.GetComponentInChildren<UIUnitController>().UpdateByUnit(map.units[map.units.Count - 1]);
                //map.units[map.units.Count - 1].RecalcHp();
                //map.units[map.units.Count - 1].RecalcHp();
                map.units[map.units.Count - 1].go.GetComponent<UnitMove>().SetDx(dx);
                map.units[map.units.Count - 1].go.GetComponent<UnitMove>().SetMain(this.gameObject, cam, map.units[i]);
            }

            //playerUnits[i].unit.SetBurnedStatus(3);
            //Debug.Log(playerUnits[i].unit.freeSpace);
        }
        CheckInTheLine();
        RecalcTurnTeam("PLAYER");
        play = true;
    }
    void StartIni()
    {
        play = false;

        //Debug.Log(4);
        GenerateEnemies();

        chosenChunk = map.map[0][0];
        cam.GetComponent<Camera>().orthographicSize /= PlayerPrefs.GetFloat("CameraSize");
        //RA.map = map;
        //   Debug.Log('-');
    }
    bool EmptyChunksForGenPlayer(int a, int b, int x, int y)
    {
        bool ans = true;
        x += a;
        y += b;
        for (int i = a; i < x; i++)
        {
            for (int j = b; j < y; j++)
            {
                ans = ans && map.map[i][j].movebleObject.isEmpty() && (!map.map[i][j].obj.permanent);
            }
        }
        return ans;
    }
    /*void RoadGeneration()
    {
        return;
        int start = Random.Range(0, 4);
        int finish = Random.Range(0, 4);
        PointXY pS = new PointXY(0, 0);
        PointXY pF = new PointXY(0, 0);
        if (start == 0)
        {
            pS.x = 0;
            pS.y = Random.Range(0, mapHeight);
        }
        if (start == 1)
        {
            pS.x = mapWidth;
            pS.y = Random.Range(0, mapHeight);
        }
        if (start == 2)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = 0;
        }
        if (start == 3)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = mapHeight;
        }


        if (finish == 0)
        {
            pF.x = 0;
            pF.y = Random.Range(0, mapHeight);
        }
        if (finish == 1)
        {
            pF.x = mapWidth;
            pF.y = Random.Range(0, mapHeight);
        }
        if (finish == 2)
        {
            pF.x = Random.Range(0, mapWidth);
            pF.y = 0;
        }
        if (finish == 3)
        {
            pF.x = Random.Range(0, mapWidth);
            pF.y = mapHeight;
        }
        int step = 0;
        while (((LengthVec(pS.x, pS.y, pF.x, pF.y) < 6) || (start == finish)) && (step < 50))
        {
            step++;
            start = Random.Range(0, 4);
            finish = Random.Range(0, 4);
            pS = new PointXY(0, 0);
            pF = new PointXY(0, 0);
            if (start == 0)
            {
                pS.x = 0;
                pS.y = Random.Range(0, mapHeight - 1);
            }
            if (start == 1)
            {
                pS.x = mapWidth;
                pS.y = Random.Range(0, mapHeight - 1);
            }
            if (start == 2)
            {
                pS.x = Random.Range(0, mapWidth - 1);
                pS.y = 0;
            }
            if (start == 3)
            {
                pS.x = Random.Range(0, mapWidth - 1);
                pS.y = mapHeight;
            }


            if (finish == 0)
            {
                pF.x = 0;
                pF.y = Random.Range(0, mapHeight - 1);
            }
            if (finish == 1)
            {
                pF.x = mapWidth;
                pF.y = Random.Range(0, mapHeight - 1);
            }
            if (finish == 2)
            {
                pF.x = Random.Range(0, mapWidth - 1);
                pF.y = 0;
            }
            if (finish == 3)
            {
                pF.x = Random.Range(0, mapWidth - 1);
                pF.y = mapHeight;
            }
        }

        if (step < 50)
        {
            if (pS.x > pF.x)
            {
                PointXY ph = new PointXY(pF);
                pF = new PointXY(pS);
                pS = new PointXY(ph);
                //Debug.Log(2);
            }

            //Debug.Log(pS.x + " " + pS.y);
            // Debug.Log(pF.x + " " + pF.y);

            PointXY vec = new PointXY(pS, pF);
            vec.norm();
            vec.xf /= 10f;
            vec.yf /= 10f;
            int t = 0;
            //Debug.Log(1);
            step = 0;
            while ((pS.x + vec.xf * t <= pF.x) && (step < 1000))
            {
                step++;
                for (int i = 0; i < mapWidth; i++)
                {
                    for (int j = 0; j < mapHeight; j++)
                    {
                        if (LengthVec(pS.x + vec.xf * t, pS.y + vec.yf * t, i, j) <= 0.8)
                        {
                            //map[i][j].SetLandCover(3);
                            //map[i][j].GenerateObject(0);
                            chunks[i][j].GetComponent<SpriteActive>().setIdActive(map.map[i][j].obj.id, false);
                        }
                    }
                }
                t++;
            }
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if (true)//((map[i][j].landCoverId == 3))
                    {
                        bool tf = false;
                        if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i + 1, j) == 2)
                        {
                            if (!InMap(i - 1, j) && (GetRoadCounAround(i, j - 1) == 0))
                            {
                                if (GetRoadCounAround(i, j - 1) + GetRoadCounAround(i, j + 1) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i, j - 1))
                                    {
                                        chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(0, 4);
                                    }
                                    if (InMap(i, j + 1))
                                    {
                                        chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(3, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 3);
                                }
                                tf = true;
                            }
                            if (!InMap(i + 1, j) && (GetRoadCounAround(i, j + 1) == 0))
                            {
                                tf = true;
                                if (GetRoadCounAround(i, j - 1) + GetRoadCounAround(i, j + 1) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i, j - 1))
                                    {
                                        chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(0, 4);
                                    }
                                    if (InMap(i, j + 1))
                                    {
                                        chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(3, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 0);
                                }
                            }
                            if (!tf)
                            {
                                tf = true;
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                if (InMap(i, j - 1))
                                {
                                    chunks[i][j - 1].GetComponent<SpriteActive>().setGroundBorder(0, 4);
                                }
                                if (InMap(i, j + 1))
                                {
                                    chunks[i][j + 1].GetComponent<SpriteActive>().setGroundBorder(3, 4);
                                }
                            }

                        }
                        if ((GetRoadCounAround(i, j - 1) + GetRoadCounAround(i, j + 1) == 2) && (!tf))
                        {
                            if (!InMap(i, j - 1) && (GetRoadCounAround(i - 1, j) == 0))
                            {
                                if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i + 1, j) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i - 1, j))
                                    {
                                        chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(1, 4);
                                    }
                                    if (InMap(i + 1, j))
                                    {
                                        chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(2, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 3);
                                }
                                tf = true;
                            }
                            if (!InMap(i, j + 1) && (GetRoadCounAround(i + 1, j) == 0))
                            {
                                if (GetRoadCounAround(i + 1, j) + GetRoadCounAround(i - 1, j) == 0)
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                    if (InMap(i - 1, j))
                                    {
                                        chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(1, 4);
                                    }
                                    if (InMap(i + 1, j))
                                    {
                                        chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(2, 4);
                                    }
                                }
                                else
                                {
                                    chunks[i][j].GetComponent<SpriteActive>().setGround(3, 0);
                                }
                                tf = true;
                            }
                            if (!tf)
                            {
                                tf = true;
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3);
                                if (InMap(i - 1, j))
                                {
                                    chunks[i - 1][j].GetComponent<SpriteActive>().setGroundBorder(1, 4);
                                }
                                if (InMap(i + 1, j))
                                {
                                    chunks[i + 1][j].GetComponent<SpriteActive>().setGroundBorder(2, 4);
                                }
                            }

                        }
                        if (!tf)
                        {
                            if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i, j - 1) == 2)
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 0);

                            }
                            if (GetRoadCounAround(i - 1, j) + GetRoadCounAround(i, j + 1) == 2)
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 1);


                            }

                            if (GetRoadCounAround(i + 1, j) + GetRoadCounAround(i, j + 1) == 2)
                            {
                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 3);

                            }

                            if (GetRoadCounAround(i + 1, j) + GetRoadCounAround(i, j - 1) == 2)
                            {

                                chunks[i][j].GetComponent<SpriteActive>().setGround(3, 2);

                            }
                        }
                    }
                }
            }
        }
    }*/
    int GetRoadCounAround(int i, int j)
    {
        if (InMap(i, j))
        {
           // if (map[i][j].landCoverId == 3)
            {
                return 1;
            }
            return 0;
        }
        return 1;
    }
    float LengthVec(float xf, float yf, int x, int y)
    {
        return Mathf.Sqrt((x - xf) * (x - xf) + (y - yf) * (y - yf));
    }
    void EndGame(bool tf)
    {
        if (tf && !(PlayerPrefs.GetInt("NewGame") == 1))
        {
            PlayerPrefs.SetInt("CurWinBattle", PlayerPrefs.GetInt("CurWinBattle") + 1);
            PlayerPrefs.SetInt("Achivment1", 1);
            if (PlayerPrefs.GetInt("CurWinBattle") >= 5)
            {
                PlayerPrefs.GetInt("Achivment2", 1);
            }
            if (PlayerPrefs.GetInt("CurWinBattle") >= 25)
            {
                PlayerPrefs.GetInt("Achivment3", 1);
            }
        }
        int cur = 0;
        for (int i = 0; i < map.units.Count; i++)
        {
            if (map.units[i].isEmpty() && !map.units[i].player)
            {
                cur++;
            }
        }
        PlayerPrefs.SetInt("Achivment17", 1);
        PlayerPrefs.SetInt("CurKilledDemons", PlayerPrefs.GetInt("CurKilledDemons") + cur);
        if (PlayerPrefs.GetInt("CurKilledDemons") >= 25)
        {
            PlayerPrefs.SetInt("Achivment18", 1);
        }
        if (PlayerPrefs.GetInt("CurKilledDemons") >= 70)
        {
            PlayerPrefs.SetInt("Achivment19", 1);
        }
        PlayerPrefs.SetInt("NewGame", 0);
        if (tf)
        {
            PlayerPrefs.SetInt("Achivment0", 1);
            //Debug.Log("win");
            int a = PlayerPrefs.GetInt("DemonicInfluence");
            if ((ev.eventId != 3))
            {
                a -= maxEnemyUnits;
                a = Mathf.Max(0, a);

            }
            if (PlayerPrefs.GetInt("SpecialID") == 1)
            {
                PlayerPrefs.SetInt("SmallBand", PlayerPrefs.GetInt("SmallBand") - 1);
            }
            if (PlayerPrefs.GetInt("SpecialID") == 2)
            {
                PlayerPrefs.SetInt("MassBand", PlayerPrefs.GetInt("MassBand") - 1);
            }
            if (PlayerPrefs.GetInt("SpecialID") == 3)
            {
                PlayerPrefs.SetInt("Foothold", PlayerPrefs.GetInt("Foothold") - 1);
            }
            if (PlayerPrefs.GetInt("SpecialID") == 4)
            {
                PlayerPrefs.SetInt("Doomed", PlayerPrefs.GetInt("Doomed") - 1);
            }

            PlayerPrefs.SetInt("DemonicInfluence", a);
            PlayerPrefs.SetInt("Lose", 0);
            PlayerPrefs.SetInt("ExtraGold", maxEnemyUnits * 10);
            if (PlayerPrefs.GetInt("Gold") + PlayerPrefs.GetInt("ExtraGold") >= 1000)
            {
                PlayerPrefs.SetInt("Achivment13", 1);
            }
        }
        else
        {
            int a = PlayerPrefs.GetInt("DemonicInfluence");
            if ((ev.eventId != 3))
            {
                a += maxEnemyUnits;
                a = Mathf.Min(1000, a);
            }
            PlayerPrefs.SetInt("DemonicInfluence", a);
            PlayerPrefs.SetInt("Lose", 1);
            PlayerPrefs.SetInt("ExtraGold", 0);
            float coef = 1;
            switch (ev.eventId)
            {
                case 1:
                    coef = 1;
                    break;
                case 2:
                    coef = 2;
                    break;
                case 3:
                    coef = 0;
                    break;
            }
            if (UnityEngine.Random.Range(0f, 1000f) * coef < PlayerPrefs.GetInt("DemonicInfluence"))
            {
                PlayerPrefs.SetInt("SmallBand", PlayerPrefs.GetInt("SmallBand") + 1);
                GameEvent ev = new GameEvent(1, language, PlayerPrefs.GetInt("DemonicInfluence"), PlayerPrefs.GetInt("Time"));

                ev.Save("SmallBandEvent" + PlayerPrefs.GetInt("SmallBand").ToString());
            }
        }
        //Debug.Log(PlayerPrefs.GetInt("ExtraGold"));
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            if (map.units[i].id == 0)
            {
                //PlayerPrefs.SetInt("PlayerUnitOnMission" + map.units[i].unit.num.ToString(), 0);
            }
            else
            {
                map.units[i].SetExp(80);
                map.units[i].SetFatigue(PlayerPrefs.GetFloat("Time"));
                //units[i].unit.Save("PlayerUnitOnBase" + map.units[i].unit.num.ToString());
                map.units[i].Save("PlayerUnit" + i);
                //PlayerPrefs.SetInt("PlayerUnitOnMission" + map.units[i].unit.num.ToString(), 1);
            }
        }
        loadWindow = true;
        SceneManager.LoadSceneAsync("ResultsScene");
    }
    void RecalcTurnTeam(string team)
    {
        for (int i = 0; i < map.units.Count; i++)
        {
            if ((map.units[i].isAlive()) && map.units[i].team == team)
            {
                map.units[i].calculateNewTurn();
                if (map.units[i].curAP > 0)
                {
                    chosenPoint = new PointXY(map.units[i].point);
                    map.playerPoint = new PointXY(map.units[i].point);
                    chosenChunk = map.map[chosenPoint.x][chosenPoint.y];
                    map.playerChunk = map.map[map.playerPoint.x][map.playerPoint.y];
                }
            }
        }
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                //   Debug.Log(map[i][j].obj.go);
                //map.map[i][j].obj.calculateNewTurn();
            }
        }
        //playerPoint = map.units[0].point;
        //Debug.Log(chosenPoint.x + " " + chosenPoint.y);
        //Debug.Log(playerPoint);
        if (team == "PLAYER")
        {
            enemyTurn = false;
            map.playerPoint = new PointXY(chosenPoint);
            map.Dijkstra(map.playerPoint);
            map.playerChunk = map.map[map.playerPoint.x][map.playerPoint.y];
            UpdateUnit();
            //map.playerChunk.movebleObject.SetShowMinusAp(true, map.playerChunk.movebleObject.curAP);
            move = true;
            attack = true;
        }
        CameraMove(chosenPoint.x, chosenPoint.y);
    }

    void UpdateLogic()
    {
        int counter1 = 0;
        int counter2 = 0;
        if (!loadWindow && play)
        {
            if (!reGame)
            {
                if (counter1 == maxPlayerUnits)
                {
                    reGame = true;
                    gameResult = false;
                    // EndGame(false);
                }
                if (counter2 == maxEnemyUnits)
                {
                    reGame = true;
                    gameResult = true;
                    //  EndGame(true);
                }
            }
        }
    }
    MovebleObject GetUnit(PointXY p)
    {
        bool tf = true;
        MovebleObject un = null;
        for (int i = 0; i < map.units.Count; i++)
        {
            //Debug.Log(map.units[i].point.x + " " + map.units[i].point.y);
            if ((map.units[i].point.x == p.x) && (map.units[i].point.y == p.y))
            {
                un = map.units[i];
                tf = false;
                break;
            }
        }
        return un;
    }

    GameObject GetUnitGO(PointXY p)
    {
        GameObject un = null;
        bool tf = true;
        for (int i = 0; i < map.units.Count; i++)
        {
            if ((map.units[i].point.x == p.x) && (map.units[i].point.y == p.y))
            {
                un = map.units[i].go;
                tf = false;
                break;
            }
        }
        return un;
    }

    void CheckInTheLine()
    {
        for (int i = 0; i < maxUnits; i++)
        {
            int cou = 0;
            if (!map.units[i].isEmpty())
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (!((j == 0) && (k == 0)))
                        {
                            // Debug.Log(InMap(map.units[i].point.x + j, map.units[i].point.y + k));// && (!map[playerUnits[i].point.x + j][playerUnits[i].point.y + k].movebleObject.isEmpty()) && (!map[map.units[i].point.x + j][map.units[i].point.y + k].movebleObject.player))
                            // );
                            if (InMap(map.units[i].point.x + j, map.units[i].point.y + k) && (!map.map[map.units[i].point.x + j][map.units[i].point.y + k].movebleObject.isEmpty()) && (map.map[map.units[i].point.x + j][map.units[i].point.y + k].movebleObject.player == map.map[map.units[i].point.x][map.units[i].point.y].movebleObject.player))
                            {
                                cou++;
                            }
                        }
                    }
                }
                map.units[i].SetValueInTheLine(cou);
                //map.units[i].go.GetComponent<HelperScript>().SetInTheLineId(false);
            }
        }
    }

    public void AddBlock()
    {
        blockCounter++;
        block = true;
        //Debug.Log("AddBlock");
    }

    public void MinusBlock()
    {
        blockCounter--;
        block = blockCounter > 0;
    }

    public void SetBlock(bool tf, bool attack)
    {
        FullStop();
        //Debug.Log(blockCounter);
        if (!tf)
        {
            //Debug.Log(playerChunk.movebleObject.curAP);
            blockCounter--;
        }
        block = blockCounter != 0;
        //Debug.Log(blockCounter);
        if (!block)
        {
            map.Dijkstra(map.playerPoint);
            map.way = new List<PointXY>();
            map.CheckTextures();
        }
        // Debug.Log(blockCounter + " " + 2);
        if (!tf)
        {
            returnedToUnit = false;
        }
        if (!enemyTurn && map.playerChunk.movebleObject.curAP == 0)
        {
            CheckNextTurn();
        }
        if (enemyTurn)
        {
            if ((map.units[enemyUnitNow].curAP == 0))
            {
                enemyUnitNow++;
                //CheckNextTurn();
                if (enemyUnitNow != maxUnits)
                {
                    CameraMove(map.units[enemyUnitNow].point.x, map.units[enemyUnitNow].point.y);
                }
            }
        }
        else
        {
            if (chosenChunk.movebleObject.curAP == 0 && !enemyTurn)
            {
                CheckNextTurn();
            }
            else
            {

                map.Dijkstra(map.playerPoint);
                chosenPoint = map.playerPoint;
                chosenChunk = map.playerChunk;
                move = true;
                attack = true;
                //CheckTextures();
            }
            // Debug.Log(2);
            if (!enemyTurn)
            {
                CheckNextTurn();
            }
        }
    }
    public void Swap(PointXY p1, PointXY p2)
    {
        //Unit playerUn = GetUnit(p1);
        // Unit second = GetUnit(p2);
        // second.point = new PointXY(p1);
        //Debug.Log(playerUn.unit.id);
        //playerUn.point = new PointXY(p2);
        //Debug.Log(p1.x + " " + p1.y);
        chosenPoint = p2;
        chosenChunk = map.map[p2.x][p2.y];
        map.map[p1.x][p1.y].movebleObject.point = new PointXY(p2);
        MovebleObject un = map.map[p1.x][p1.y].movebleObject;
        map.map[p1.x][p1.y].movebleObject = map.map[p2.x][p2.y].movebleObject;
        map.map[p2.x][p2.y].movebleObject = un;
        map.playerChunk = map.map[p2.x][p2.y];
        map.playerPoint = p2;

        GetUnit(p2).rotation = new PointXY(p2.x - p1.x, p2.y - p1.y);
        CheckInTheLine();
        map.CheckTextures();
        //playerPoint = new PointXY(p2);
        //playerChunk = map.map[p2.x][p2.y];
        // Debug.Log(playerUn.unit.name);
        //Debug.Log(map.map[p2.x][p2.y].movebleObject.keyName);
        //Debug.Log(p1.x + " " + p1.y + " -> " + p2.x + " " + p2.y);
    }

    void GoTo(PointXY p1, PointXY p2, bool tf)
    {
        if (p1.isEqual(p2))
        {
            return;
        }
        map.map[p1.x][p1.y].movebleObject.go.GetComponentInChildren<UIUnitController>().UpdateByUnit(map.map[p1.x][p1.y].movebleObject);
        map.way = map.GetWay(p2);
        map.map[p1.x][p1.y].movebleObject.go.GetComponent<UnitMove>().SetWay(map.way);
    }
    void FullStop()
    {
        for (int i = 0; i < maxUnits; i++)
        {
            map.units[i].go.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }

    bool IsPlayerChunk(Chunk ch)
    {
        if (ch.movebleObject == null)
        {
            return false;
        }
        return ch.movebleObject.player;
    }

    
    void CheckNextTurn()
    {
        returnedToUnit = false;
        if (!enemyTurn && map.playerChunk.movebleObject.curAP > 0)
        {
            map.Dijkstra(map.playerPoint);
            CameraMove(map.playerPoint.x, map.playerPoint.y);
            chosenPoint = map.playerPoint;
            chosenChunk = map.playerChunk;
            //map.playerChunk.movebleObject.SetShowMinusAp(true, map.playerChunk.movebleObject.curAP);
            return;
        }
        MovebleObject un = map.units[0];
        bool tf = true;
        for (int i = 0; i < maxUnits; i++)
        {
            if ((map.units[i] != null) && (map.units[i].curAP > 0) && map.units[i].player)
            {
                tf = false;
                un = map.units[i];
                chosenPoint = new PointXY(map.units[i].point);
                map.playerPoint = new PointXY(map.units[i].point);
                chosenChunk = map.map[chosenPoint.x][chosenPoint.y];
                map.playerChunk = map.map[map.playerPoint.x][map.playerPoint.y];
            }
        }
        //Debug.Log(tf);
        if (tf)
        {
            // Debug.Log(tf);
            enemyTurn = true;
            for (int i = 0; i < maxUnits; i++)
            {
                if (map.units[i] != null && !map.units[i].player)
                {
                    map.units[i].calculateNewTurn();
                }
            }
            for (int i = 0; i < maxUnits; i++)
            {
                if (map.units[i] != null && !map.units[i].player)
                {
                    enemyUnitNow = i;
                    chosenPoint = new PointXY(map.units[i].point);
                    map.playerPoint = new PointXY(map.units[i].point);
                    chosenChunk = map.map[chosenPoint.x][chosenPoint.y];
                    map.playerChunk = map.map[map.playerPoint.x][map.playerPoint.y];
                    break;
                }
            }
        }
        else
        {
            attack = true;
            move = true;
            map.Dijkstra(map.playerPoint);
            UpdateUnit();
            map.CheckTextures();
        }
        CameraMove(chosenPoint.x, chosenPoint.y);
    }
    public int GetGround(PointXY p)
    {
        return 0;// map.map[p.x][p.y].landCoverId;
    }
    bool FreeAround(MovebleObject m, PointXY a)
    {
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if (!((i == 0) && (j == 0)))
                {
                    if (InMap(a.x + i, a.y + j))
                    {
                        if ((!map.map[a.x + i][a.y + j].movebleObject.isEmpty()) && (map.map[a.x + i][a.y + j].movebleObject.player != m.player))
                        {
                            //  Debug.Log(i + " " + j);
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }
    public float GetOneChunkLen()
    {
        float x1 = map.chunks[0][0].transform.position.x;
        float y1 = map.chunks[0][0].transform.position.y;
        float x2 = map.chunks[1][0].transform.position.x;
        float y2 = map.chunks[1][0].transform.position.y;
        return Mathf.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }
    void CameraMove(int nx, int ny)
    {
        //Debug.Log(block);
        //Debug.Log(blockCounter);
        if (!block)
        {
            float vx = map.chunks[nx][ny].transform.position.x - cam.transform.position.x;
            float vy = map.chunks[nx][ny].transform.position.y - cam.transform.position.y;

            float len = Mathf.Sqrt(vx * vx + vy * vy);

            if(len < GetOneChunkLen() * 0.2f)
            {
                return;
            }

            cameraMove = true;
            startCameraMove = Time.time;
            cameraSpeed = Screen.width / 6 * 3.8f;

            cam.GetComponent<Rigidbody2D>().velocity = new Vector2(vx / len, vy / len) * cameraSpeed;

            //Debug.Log(cam.GetComponent<Rigidbody2D>().velocity);
            cameraTimer = len / cameraSpeed;
        }
        //Debug.Log(cameraTimer);
    }
    PointXY GetLinesCross(float a, float b, float c, float a2, float b2, float c2)
    {
        PointXY ans = new PointXY(0, 0);
        ans.xf = -(c * b2 - c2 * b) / (a * b2 - a2 * b);
        ans.yf = -(a * c2 - a2 * c) / (a * b2 - a2 * b);
        return ans;
    }
    bool LineInSquare(int i, int j, float a, float b, float c)
    {
        PointXY left = new PointXY(GetLinesCross(1, 0, -(i - 0.5f), a, b, c));
        PointXY right = new PointXY(GetLinesCross(1, 0, -(i + 0.5f), a, b, c));
        PointXY top = new PointXY(GetLinesCross(0, 1, -(j + 0.5f), a, b, c));
        PointXY bot = new PointXY(GetLinesCross(0, 1, -(j - 0.5f), a, b, c));
        if ((Mathf.Abs(left.yf - j) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(right.yf - j) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(top.xf - i) <= 0.5f))
        {
            return true;
        }
        if ((Mathf.Abs(bot.xf - i) <= 0.5f))
        {
            return true;
        }
        return false;
    }
    float LineMinusAim(PointXY p1, PointXY p2)
    {
        float res = 0;
        float a = p1.y - p2.y;
        float b = p2.x - p1.x;
        float c = p1.x * p2.y - p1.y * p2.x;

        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                float d = Mathf.Abs(a * i + b * j + c) / Mathf.Sqrt(a * a + b * b);
                PointXY p = new PointXY(i, j);
                if (!p.isEqual(p1) && !p.isEqual(p2))
                {
                    if (LineInSquare(i, j, a, b, c))
                    {
                        if ((Mathf.Abs(i - p1.x) + Mathf.Abs(i - p2.x) == Mathf.Abs(p1.x - p2.x)) && (Mathf.Abs(j - p1.y) + Mathf.Abs(j - p2.y) == Mathf.Abs(p1.y - p2.y)))
                        {
                            float help = 0;
                            // chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, true);
                            if (!map.map[i][j].movebleObject.isEmpty())
                            {
                                help += 20;
                            }
                            if (!map.map[i][j].obj.isEmpty())
                            {
                                if (map.map[i][j].obj.smallBlock)
                                {
                                    help += 20;
                                }
                                else
                                {
                                    help += 60;
                                }
                            }
                            //Debug.Log(d);
                            if (d > 0.6f)
                            {
                                help /= 2f;
                            }
                            if (d > 0.9f)
                            {
                                //   chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                                help = 0;
                            }
                            res += help;
                        }
                    }
                    else
                    {
                        //  chunks[i][j].GetComponent<SpriteActive>().setIdActive(-10, false);
                    }
                }
            }
        }
        return res;
    }

    void MoveBoundCamera()
    {
        bool camBound = false;
        if (!cameraMove)
        {
            float vx = 0;
            float vy = 0;
            float len = GetOneChunkLen();
            if ((Input.mousePosition.x < Screen.height / 50) && (cam.transform.position.x > map.chunks[0][0].transform.position.x))
            {
                vx = -len;
                camBound = true;
            }
            if ((Input.mousePosition.y < Screen.height / 50) && (cam.transform.position.y > map.chunks[0][0].transform.position.y))
            {
                vy = -len;
                camBound = true;
            }
            if ((Input.mousePosition.x > Screen.width - Screen.width / 50) && (cam.transform.position.x < map.chunks[mapWidth - 1][mapHeight - 1].transform.position.x))
            {
                vx = len;
                camBound = true;
            }
            if ((Input.mousePosition.y > Screen.height - Screen.width / 50) && (cam.transform.position.y < map.chunks[mapWidth - 1][mapHeight - 1].transform.position.y))
            {
                vy = len;
                camBound = true;
            }
            vx *= 2;
            vy *= 2;
            cam.GetComponent<Rigidbody2D>().velocity = new Vector2(vx, vy);
        }

        if ((Time.time - startCameraMove >= cameraTimer) && (!camBound))
        {
            cam.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            cameraMove = false;
            if (returnWait)
            {
                returnWait = false;
                GoTo(lastPoint, map.playerPoint, true);
            }
        }
    }

    List<GameObject> squadButtons = new List<GameObject>();
    GameObject playerWindow;
    GameObject enemyWindow;
    GameObject castStatsWindow;
    GameObject spawnWindow;
    GameObject spawnButton;
    GameObject squadWindow;
    GameObject abilitiesWindow;
    GameObject playerPassiveWindow;
    GameObject enemyPassiveWindow;

    List<TargetShow> targets = new List<TargetShow>();

    List<TargetShow> newTargets = new List<TargetShow>();

    void UpdateChosen(MovebleObject move)
    {
        RedrawEnemyUnit();
    }

    void RedrawCurAbility(Active act, EstimateDamage est)
    {
        damage.text = "";
        crit.text = "";
        hit.text = "";
        ap.text = "";
        if((act == null) || (est == null))
        {
            return;
        }
        ap.text = "AP COST" + est.activationCost.ToString();
        if (act.damage == "-")
        {
            return;
        }
        //Debug.Log(act.keyName);
        damage.text = est.damage.ToString();
        crit.text = est.crit + "%";
        hit.text = est.hit + "%";
    }

    void NowClick(MovebleObject m, Active act, EstimateDamage est)
    {
        Accept.SetActive(true);
        TargetClick(m, act, est);
    }

    void PredictClick(MovebleObject m, Active act, EstimateDamage est)
    {
        Accept.SetActive(false);
        TargetClick(m, act, est);
    }

    void TargetClick(MovebleObject m, Active act, EstimateDamage est)
    {
        //Debug.Log(est.damage);
        if (chosenChunk.movebleObject != null)
        {
            chosenChunk.movebleObject.go.GetComponentInChildren<UIUnitController>().UpdateByUnit(chosenChunk.movebleObject);
        }
        if(act.damage != "-")
        {
            m.go.GetComponentInChildren<UIUnitController>().ShowDamage(m, (int)(0.8f * est.damage));
        }
        chosenChunk = map.map[m.point.x][m.point.y];
        chosenPoint = m.point;
        UpdateChosen(m);
        RedrawCurAbility(act, est);
        CameraMove(chosenPoint.x, chosenPoint.y);
    }

    void UpdatePredictTargets(Active act)
    {
        if (map.playerPoint.isEqual(chosenPoint))
        {
            for (int i = 0; i < newTargets.Count; i++)
            {
                newTargets[i].gameObject.SetActive(false);
            }
            return;
        }
        List<MovebleObject> target = map.RecalcTargets(map.playerChunk.movebleObject, wayPoint, act);
        //Debug.Log(target.Count);
        for (int i = 0; i < newTargets.Count; i++)
        {
            newTargets[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < target.Count; i++)
        {
            //Debug.Log(target[i].keyName);
            if (i >= newTargets.Count)
            {
                return;
            }
            newTargets[i].Init(target[i]);
            MovebleObject newA = target[i];
            newTargets[i].gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            PointXY aRot = new PointXY(target[i].point.x - wayPoint.x, target[i].point.y - wayPoint.y);
            EstimateDamage est = target[i].EstimateDamage(aRot, target[i].rotation, map.playerChunk.movebleObject, act);
            newTargets[i].gameObject.GetComponent<Button>().onClick.AddListener(delegate { PredictClick(newA, act, est); });
        }
        for (int i = target.Count; i < targets.Count; i++)
        {
            newTargets[i].gameObject.SetActive(false);
        }
    }

    Active chosenActive = null;

    void ActiveClick(Active act)
    {
        chosenActive = act;
        List<MovebleObject> target = map.RecalcTargets(map.playerChunk.movebleObject, map.playerPoint, act);
        for (int i = 0; i < targets.Count; i++)
        {
            targets[i].gameObject.SetActive(true);
        }
        for (int i = 0; i < target.Count; i++)
        {
            if (i >= targets.Count)
            {
                return;
            }
            targets[i].Init(target[i]);
            MovebleObject newA = target[i];
            targets[i].gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            PointXY aRot = new PointXY(target[i].point.x -  map.playerPoint.x, target[i].point.y - map.playerPoint.y);
            EstimateDamage est = target[i].EstimateDamage(aRot, target[i].rotation, map.playerChunk.movebleObject, act);
            targets[i].gameObject.GetComponent<Button>().onClick.AddListener(delegate { NowClick(newA, act, est); });
        }
        for (int i = target.Count; i < targets.Count; i++)
        {
            targets[i].gameObject.SetActive(false);
        }
        if (chosenPoint.isEqual(map.playerPoint))
        {
            for (int i = target.Count; i < targets.Count; i++)
            {
                newTargets[i].gameObject.SetActive(false);
            }
        }
        //else
        //{
            UpdatePredictTargets(act);
        //}
        if (chosenChunk.movebleObject != null) {
            PointXY aRot = new PointXY(chosenPoint.x - wayPoint.x, chosenPoint.y - wayPoint.y);
            EstimateDamage est = chosenChunk.movebleObject.EstimateDamage(aRot, chosenChunk.movebleObject.rotation, map.playerChunk.movebleObject, act);
            TargetClick(chosenChunk.movebleObject, act, est);
        }
        else
        {
            RedrawCurAbility(null, null);
        }
    }

    void UpdateUnit()
    {
        foreach(var i in actives)
        {
            i.gameObject.SetActive(true);
        }
        playerWindow.GetComponent<UnitBlock>().Init(map.playerChunk.movebleObject);
        for(int i = 0; i < map.playerChunk.movebleObject.active.Count; i++)
        {
            if (i >= actives.Count)
            {
                return;
            }
            actives[i].Init(map.playerChunk.movebleObject.active[i]);
            Active newA = map.playerChunk.movebleObject.active[i];
            //Debug.Log(newA.range);
            actives[i].gameObject.GetComponent<Button>().onClick.RemoveAllListeners();
            actives[i].gameObject.GetComponent<Button>().onClick.AddListener(delegate { ActiveClick(newA); });
        }
        for(int i = map.playerChunk.movebleObject.active.Count; i < actives.Count; i++)
        {
            actives[i].gameObject.SetActive(false);
        }
        ActiveClick(map.playerChunk.movebleObject.active[0]);
    }

    void ChoosePlayerUnit(MovebleObject move)
    {
        for(int i = 0; i < playerSquad.units.Count; i++)
        {
            if (playerSquad.units[i] == move)
            {
                SquadButtonClick(i);
                return;
            }
        }
    }

    void SquadButtonClick(int id)
    {
        if (playerSquad.units[id] != null)
        {
            map.playerPoint = playerSquad.units[id].point;
            map.playerChunk = map.map[map.playerPoint.x][map.playerPoint.y];
            chosenChunk = map.map[map.playerPoint.x][map.playerPoint.y];
            chosenPoint = map.playerPoint;
            UpdateUnit();
            map.Dijkstra(map.playerPoint);
            //Debug.Log(playerSquad.units[id].curAP);
            map.CheckTextures();
        }
    }

    void AcceptClick()
    {
        //Debug.Log("Accept");
        PointXY aRot = new PointXY(chosenChunk.movebleObject.point.x - map.playerPoint.x, chosenChunk.movebleObject.point.y - map.playerPoint.y);
        map.map[chosenPoint.x][chosenPoint.y].movebleObject.ActiveAbility(aRot, chosenChunk.movebleObject.rotation, map.playerChunk.movebleObject, chosenActive);
        map.playerChunk.movebleObject.go.GetComponentInChildren<UIUnitController>().UpdateByUnit(map.playerChunk.movebleObject);
    }

    List<ActiveShow> actives = new List<ActiveShow>();
    Active chosenAbility;

    Text damage;
    Text hit;
    Text crit;
    Text ap;

    GameObject Accept;

    void InitUI()
    {
        for (int i = 0; i < 6; i++)
        {
            squadButtons.Add(GameObject.Find("SquadUnitInBattle" + i.ToString()));
            squadButtons[i].GetComponent<UnitBlock>().Init(playerSquad.units[i]);
            int newI = i;
            squadButtons[i].GetComponent<Button>().onClick.AddListener(delegate { SquadButtonClick(newI); });
            playerSquad.units[i].unitButton = squadButtons[i].GetComponent<UnitBlock>();
        }

        for(int i = 0; i < 10; i++)
        {
            actives.Add(GameObject.Find("Active" + i.ToString()).GetComponent<ActiveShow>());
        }

        for (int i = 0; i < 10; i++)
        {
            targets.Add(GameObject.Find("Target" + i.ToString()).GetComponent<TargetShow>());
            newTargets.Add(GameObject.Find("NewTarget" + i.ToString()).GetComponent<TargetShow>());
        }
        playerWindow = GameObject.Find("PlayerWindow");
        playerWindow.SetActive(false);
        squadWindow = GameObject.Find("SquadWindow");
        squadWindow.SetActive(false);
        enemyWindow = GameObject.Find("EnemyWindow");
        enemyWindow.SetActive(false);
        Accept = GameObject.Find("AcceptButton");
        Accept.GetComponent<Button>().onClick.AddListener(AcceptClick);
        damage = GameObject.Find("Damage").GetComponent<Text>();
        hit = GameObject.Find("Hit").GetComponent<Text>();
        crit = GameObject.Find("Crit").GetComponent<Text>();
        ap = GameObject.Find("AP").GetComponent<Text>();
        castStatsWindow = GameObject.Find("CastStatWindow");
        castStatsWindow.SetActive(false);
        abilitiesWindow = GameObject.Find("AbilitiesWindow");
        abilitiesWindow.SetActive(false);
        spawnWindow = GameObject.Find("SpawnWindow");
        spawnButton = GameObject.Find("SpawnButton");
        spawnButton.SetActive(false);
        spawnButton.GetComponent<Button>().onClick.AddListener(SpawnClick);
        playerPassiveWindow = GameObject.Find("PlayerPassiveWindow");
        //playerPassiveWindow.SetActive(false);
        enemyPassiveWindow = GameObject.Find("EnemyPassiveWindow");
        //enemyPassiveWindow.SetActive(false);
    }

    void Update()
    {
        UpdateLogic();
        MoveBoundCamera();
        //SpawnClick();
        CheckClickSpawn();
        MoveCheckClick();
        SwapUnitClick();
        EnemyTurn();
    }

    void EnemyTurn()
    {
        if (!play && !reGame)
        {
            return;
        }
            //Debug.Log(enemyTurn + " " + block + " " + cameraMove);
        if (!enemyTurn)
        {
            return;
        }
        //Debug.Log(block);
        if (block)
        {
            return;
        }
        if (cameraMove)
        {
            return;
        }
        MovebleObject curUnit = map.GetNextNonLockedUnitInTeam("DEMON");
        if(curUnit == null)
        {
            RecalcTurnTeam("PLAYER");
            enemyTurn = false;
        }
        else
        {
            chosenPoint = new PointXY(curUnit.point);
           // Debug.Log(curUnit.curAP + " " + curUnit.keyName);
            if (!chosenPoint.isEqual(curUnit.point))
            {
                CameraMove(curUnit.point.x, curUnit.point.y);
                return;
            }
            map.Dijkstra(curUnit.point);
            //Debug.Log(curUnit.point);
            PointXY bestPoint = curUnit.ai.GetBestPoint(curUnit, map);
            //Debug.Log(bestPoint.abilityKeyName + " " + curUnit.curAP);
            if (!bestPoint.isEqual(curUnit.point))
            {
                blockCounter++;
                block = true;
                //Debug.Log(Mathf.CeilToInt(map.dist[bestPoint.x][bestPoint.y] / curUnit.GetSpeed()));
                curUnit.SpendAP(Mathf.CeilToInt(map.dist[bestPoint.x][bestPoint.y] / curUnit.GetSpeed()));
                GoTo(curUnit.point, bestPoint, false);
            }
            else
            {
                Active act = curUnit.GetLinkActiveByKeyName(bestPoint.abilityKeyName);
                PointXY targetPoint = curUnit.ai.GetTarget(act, curUnit, map, curUnit.point);
                PointXY aRot = new PointXY(targetPoint.x - curUnit.point.x, targetPoint.y - curUnit.point.y);
                curUnit.rotation = aRot;
                Rotation(curUnit.go, aRot.x, aRot.y);
                map.map[targetPoint.x][targetPoint.y].movebleObject.ActiveAbility(aRot, map.map[targetPoint.x][targetPoint.y].movebleObject.rotation, curUnit, act);
                //Debug.Log("ready");
            }
        }
    }

    PointXY mapWayChunk;

    void SwapUnitClick()
    {
        if (map.playerChunk == null)
        {
            return;
        }
        if (!play)
        {
            return;
        }
        if (Input.GetMouseButton(1) && !block)
        {
            if (Input.GetMouseButtonDown(1))
            {
                //Debug.Log('*');
                PointerEventData pointerData = new PointerEventData(EventSystem.current);

                pointerData.position = Input.mousePosition;

                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, results);

                if (results.Count > 0)
                {
                    foreach (var i in results)
                    {
                        if (i.gameObject.layer == LayerMask.NameToLayer("UI"))
                        {
                            return;
                        }
                    }
                }

                RaycastHit hit;
                Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                ray.origin = start;
                if (Physics.Raycast(ray, out hit))
                {
                    CoordinatsHelper cp = hit.transform.gameObject.GetComponent<CoordinatsHelper>();
                    if (cp == null)
                    {
                        return;
                    }
                    p = cp.point;
                    int it;
                    int jt;
                    it = p.x;
                    jt = p.y;
                    chosenPoint = new PointXY(it, jt);
                    chosenChunk = map.map[chosenPoint.x][chosenPoint.y];
                    //mapWayChunk = map.map[chosenPoint.x][chosenPoint.y];
                    if (chosenChunk.movebleObject != null)
                    {
                        if((chosenChunk.movebleObject.team == "PLAYER") && (chosenChunk.movebleObject.isAlive()))
                        {
                            ChoosePlayerUnit(chosenChunk.movebleObject);
                        }
                        else
                        {
                            RedrawEnemyUnit();
                        }
                    }
                }
            }
            //CheckTextures();
        }
    }

    void MoveCheckClick()
    {
        //Debug.Log(playerChunk + " " + play);
        if(map.playerChunk == null)
        {
            return;
        }
        if (!play)
        {
            return;
        }
        if (Input.GetMouseButton(0) && !block)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current);

                pointerData.position = Input.mousePosition;

                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, results);

                if (results.Count > 0)
                {
                    foreach (var i in results)
                    {
                        if (i.gameObject.layer == LayerMask.NameToLayer("UI"))
                        {
                            return;
                        }
                    }
                }

                RaycastHit hit;
                Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                ray.origin = start;
                if (Physics.Raycast(ray, out hit))
                {
                    CoordinatsHelper cp = hit.transform.gameObject.GetComponent<CoordinatsHelper>();
                    if (cp == null)
                    {
                        return;
                    }
                    p = cp.point;
                    int it;
                    int jt;
                    it = p.x;
                    jt = p.y;
                    chosenPoint = new PointXY(it, jt);
                    mapWayChunk = new PointXY(it, jt);
                    if (map.dist[it][jt] <= map.playerChunk.movebleObject.speed * map.playerChunk.movebleObject.curAP)
                    {
                        map.playerChunk.movebleObject.go.GetComponentInChildren<UIUnitController>().ShowSpendAp(map.playerChunk.movebleObject, Mathf.CeilToInt(map.dist[it][jt] / map.playerChunk.movebleObject.GetSpeed()));
                        chosenChunk = map.map[chosenPoint.x][chosenPoint.y];
                        wayPoint = new PointXY(chosenPoint);
                        UpdatePredictTargets(chosenActive);
                        map.way = map.GetWay(chosenPoint);
                        map.DrawWay();
                    }
                }
            }
            //CheckTextures();
        }
        if (Input.GetMouseButton(1) && !block)
        {
            if (Input.GetMouseButtonDown(1))
            {
                PointerEventData pointerData = new PointerEventData(EventSystem.current);

                pointerData.position = Input.mousePosition;

                List<RaycastResult> results = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointerData, results);

                if (results.Count > 0)
                {
                    foreach (var i in results)
                    {
                        if (i.gameObject.layer == LayerMask.NameToLayer("UI"))
                        {
                            return;
                        }
                    }
                }

                RaycastHit hit;
                Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                ray.origin = start;
                if (Physics.Raycast(ray, out hit))
                {
                    CoordinatsHelper cp = hit.transform.gameObject.GetComponent<CoordinatsHelper>();
                    if (cp == null)
                    {
                        return;
                    }
                    p = cp.point;
                    int it;
                    int jt;
                    it = p.x;
                    jt = p.y;
                    chosenPoint = new PointXY(it, jt);
                    wayPoint = new PointXY(chosenPoint);
                    if (map.dist[it][jt] < map.playerChunk.movebleObject.speed * map.playerChunk.movebleObject.curAP)
                    {
                        //Debug.Log("leftClick");
                        blockCounter++;
                        map.playerChunk.movebleObject.SpendAP(Mathf.CeilToInt(map.dist[it][jt] / map.playerChunk.movebleObject.GetSpeed()));
                        //chosenChunk.movebleObject.go.GetComponentInChildren<UIUnitController>().UpdateByUnit(chosenChunk.movebleObject);
                        map.way = map.GetWay(chosenPoint);
                        GoTo(map.playerPoint, chosenPoint, false);
                    }
                }
            }
            //CheckTextures();
        }
    }


    void RestartBattleClick()
    {
        SceneManager.LoadSceneAsync("BattleScene");
    }

    void EndGameClick()
    {
        EndGame(gameResult);
    }

    void RedrawPlayerUnit()
    {
        if (map.playerChunk != null)
        {
            if (map.playerChunk.movebleObject.player)
            {
                playerWindow.GetComponent<UnitBlock>().Init(map.playerChunk.movebleObject);
            }
            else
            {
                playerWindow.GetComponent<UnitBlock>().Init(null);
            }
        }
        else
        {
            playerWindow.GetComponent<UnitBlock>().Init(null);
        }
    }

    void RedrawEnemyUnit()
    {
        if (chosenChunk != null)
        {
            if (!chosenChunk.movebleObject.player)
            {
                enemyWindow.GetComponent<UnitBlock>().Init(chosenChunk.movebleObject);
            }
            else
            {
                enemyWindow.GetComponent<UnitBlock>().Init(null);
            }
        }
        else
        {
            enemyWindow.GetComponent<UnitBlock>().Init(null);
        }
    }

    void SpawnClick()
    {
        playerWindow.SetActive(true);
        squadWindow.SetActive(true);
        enemyWindow.SetActive(true);
        castStatsWindow.SetActive(true);
        abilitiesWindow.SetActive(true);
        spawnWindow.SetActive(false);
        wayPoint = new PointXY(chosenPoint);
        Spawn(chosenPoint);
        RedrawEnemyUnit();
        RedrawPlayerUnit();
        map.CheckTextures();
        RecalcTurnTeam("PLAYER");
        //RecalcTurnTeam("DEMON");
    }
  
    void CheckClickSpawn()
    {
        if (!play && !reGame)
        {
            if (Input.GetMouseButton(0) && !block)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    PointerEventData pointerData = new PointerEventData(EventSystem.current);

                    pointerData.position = Input.mousePosition;

                    List<RaycastResult> results = new List<RaycastResult>();
                    EventSystem.current.RaycastAll(pointerData, results);

                    if (results.Count > 0)
                    {
                        foreach(var i in results)
                        {
                            if(i.gameObject.layer == LayerMask.NameToLayer("UI"))
                            {
                                return;
                            }
                        }
                    }

                    RaycastHit hit;
                    pressed = true;
                    nowActive = true;
                    //Debug.Log("NotUi");
                    //if ((Input.mousePosition.y > (Screen.height - dy * 6) / 2) && (Input.mousePosition.y < Screen.height - (Screen.height - dy * 6) / 2))
                    {
                        for (int i = 0; i < mapWidth; i++)
                        {
                            for (int j = 0; j < mapHeight; j++)
                            {
                                map.dist[i][j] = INF;
                            }
                        }
                        //Debug.Log("+");
                        //Debug.Log(Input.mousePosition);
                        Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                        //Debug.Log(start);
                        Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                        start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                        ray.origin = start;
                        // Debug.Log(ray);
                        if (Physics.Raycast(ray, out hit))
                        {
                            CoordinatsHelper cp = hit.transform.gameObject.GetComponent<CoordinatsHelper>();
                            //Debug.Log(cp);
                            if(cp == null)
                            {
                                return;
                            }
                            p =cp.point;
                            int it;
                            int jt;
                            it = p.x;
                            jt = p.y;
                            chosenPoint = new PointXY(it, jt);

                           // Debug.Log(it + " " + jt);
                            if (((it == 0) && (jt == 0)) || ((it == mapWidth - 1) && (jt == 0))
                                    || ((it == 0) && (jt == mapHeight - 1)) || ((it == mapWidth - 1) && (jt == mapHeight - 1)))
                            {
                                return;
                            }
                            if ((it > 0) && (it < mapWidth - 1) && (jt > 0) && (jt < mapHeight - 1))
                            {
                                return;
                            }
                            for (int i = 0; i < mapWidth; i++)
                            {
                                for (int j = 0; j < mapHeight; j++)
                                {
                                    if (map.FreeSpaceForSpawn(new PointXY(it, jt)))
                                    {
                                      //  Debug.Log(LenPoints(new PointXY(i, j), new PointXY(it, jt)));
                                        if (LenPoints(new PointXY(i, j), new PointXY(it, jt)) <= 1.42f)
                                        {
                                            map.dist[i][j] = 1;
                                        }
                                    }
                                }
                            }
                            if (!play && !reGame)
                            {
                                if ((chosenPoint != null))
                                {
                                    if ((map.dist[chosenPoint.x][chosenPoint.y] < INF))
                                    {
                                        spawnButton.SetActive(true);
                                    }
                                }
                            }
                        }
                    }
                    map.RedrawSpawnArea();
                }
            }
        }
    }
/*
    void CheckClickGame()
    {
        int it = p.x;
        int jt = p.y;
        if (Input.GetMouseButton(0) && !block)
        {
            if (Input.GetMouseButtonDown(0) && !pressed)
            {
                RaycastHit hit;
                pressed = true;
                nowActive = true;
                if ((Input.mousePosition.y > (Screen.height - dy * 6) / 2) && (Input.mousePosition.y < Screen.height - (Screen.height - dy * 6) / 2))
                {
                    //Debug.Log(Input.mousePosition);
                    Vector3 start = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
                    //Debug.Log(start);
                    Ray ray = cam.GetComponent<Camera>().ScreenPointToRay(start);
                    start = new Vector3(ray.origin.x, ray.origin.y, -1000f);
                    ray.origin = start;
                    //Debug.Log(ray);
                    if (Physics.Raycast(ray, out hit))
                    {
                        p = hit.transform.gameObject.GetComponent<CoordinatsHelper>().point;
                        butttonPressed = p.x * mapWidth + p.y;
                        it = p.x;
                        jt = p.y;
                        //Debug.Log(returnedToUnit);
                        playerChunk.movebleObject.SetShowMinusAp(false);
                        if (activity == Activity.Rotate)
                        {
                            GetUnit(playerPoint).SetRotation(new PointXY(it - playerPoint.x, jt - playerPoint.y));
                            Rotation(GetUnitGO(playerPoint), it - playerPoint.x, jt - playerPoint.y);
                        }
                        if ((it == chosenPoint.x) && (jt == chosenPoint.y))
                        {
                            if ((!cameraMove))
                            {
                                TryAttack(playerPoint, playerChunk, it, jt);
                                TryMove(it, jt);
                                TryFriendlyAction(it, jt);
                            }
                        }
                        else
                        {
                            if (map[it][jt].obj.isEmpty() && map[it][jt].movebleObject.isEmpty() && (GetUnit(playerPoint).unit.curAP * GetUnit(playerPoint).unit.GetSpeed() >= dist[it][jt]))
                            {
                                int k = Mathf.CeilToInt(dist[it][jt] / GetUnit(playerPoint).unit.GetSpeed());
                                playerChunk.movebleObject.SetShowMinusAp(true, k);
                                //Debug.Log(k);
                            }
                        }
                        if (!block && !cameraMove)
                        {
                            //Debug.Log(activeAction + " " + playerChunk.movebleObject.it.rangable + " " + ((activeAction == -2) && (playerChunk.movebleObject.it.rangable)));

                            if (!(activity == Activity.Rotate) &&
                                nowActive && !map[it][jt].movebleObject.isEmpty() && map[it][jt].movebleObject.player && (map[it][jt].movebleObject.curAP > 0) && !((activeAction == Ability.Item) && (playerChunk.movebleObject.it.rangable)))
                            {
                                playerChunk = map[it][jt];
                                playerPoint = new PointXY(it, jt);
                                playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                                Dijkstra(playerPoint);
                                activeAction = Ability.Defense;
                                activity = Activity.Myself;
                            }
                            else
                            {
                                if ((!map[it][jt].movebleObject.isEmpty() && !map[it][jt].movebleObject.player) || (!map[it][jt].obj.isEmpty() && !map[it][jt].obj.permanent))
                                {
                                    activeAction = Ability.Attack;
                                }
                            }
                        }
                        if (!block && !cameraMove)
                        {
                            if (!(new PointXY(it, jt)).isEqual(playerPoint))
                            {
                                GetUnit(playerPoint).SetRotation(new PointXY(it - playerPoint.x, jt - playerPoint.y));
                                Rotation(GetUnitGO(playerPoint), it - playerPoint.x, jt - playerPoint.y);
                                if (playerChunk.movebleObject.damage.handStatus == 3)
                                {
                                    GetUnit(playerPoint).unit.SetExtraAim(LenPoints(playerPoint, new PointXY(it, jt)), LineMinusAim(playerPoint, new PointXY(it, jt)));
                                }
                            }
                            if (!((it == chosenPoint.x) && (jt == chosenPoint.y)))
                            {
                                CameraMove(it, jt);
                            }
                        }
                        move = true;
                        attack = true;
                        //Debug.Log(block + " " + cameraMove);
                        chosenChunk.movebleObject.SetShowMinusHP(false, 0);

                        showYourList = false;
                        showEnemyList = false;
                        if (nowActive)
                        {
                            chosenChunk = map[it][jt];
                            chosenPoint = new PointXY(it, jt);
                        }
                        if (map[it][jt].movebleObject.isEmpty() && map[it][jt].obj.isEmpty())
                        {
                            if (map[it][jt].obj.isEmpty() && (Mathf.CeilToInt(dist[it][jt] / GetUnit(playerPoint).unit.GetSpeed()) <= playerChunk.movebleObject.curAP))
                            {
                                way = GetWay(chosenPoint);
                                activity = Activity.Move;
                            }
                            else
                            {
                                activity = Activity.NoMove;
                            }
                        }

                        if (!map[it][jt].obj.isEmpty() || (!map[it][jt].movebleObject.isEmpty() && !map[it][jt].movebleObject.player))
                        {
                            activity = Activity.Enemy;
                        }
                        if (!map[it][jt].movebleObject.isEmpty() && map[it][jt].movebleObject.player)
                        {
                            if (!chosenPoint.isEqual(playerPoint))
                            {
                                activity = Activity.Ally;
                            }
                            else
                            {
                                activity = Activity.Myself;
                            }
                            if (!(activeAction == Ability.Item &&
                                playerChunk.movebleObject.it.active &&
                                (playerChunk.movebleObject.GetRangeItem() < LenPoints(chosenPoint, playerPoint))))
                            {
                                activity = Activity.Myself;
                            }
                        }
                        //if (activeAction == Ability.Attack)
                        {
                            if (!map[it][jt].movebleObject.isEmpty() && !map[it][jt].movebleObject.player)
                            {
                                playerChunk.movebleObject.EmulateAbility(Ability.Attack);
                                estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                if (playerChunk.movebleObject.curAP > 0)
                                {
                                    chosenChunk.movebleObject.SetShowMinusHP(true, (int)(0.8f * estimatedDamage.realD));
                                    playerChunk.movebleObject.SetShowMinusAp(true);
                                }
                            }
                            if (!map[it][jt].obj.isEmpty())
                            {
                                playerChunk.movebleObject.EmulateAbility(Ability.Attack);
                                estimatedDamage = map[it][jt].obj.EstimatedDamage(playerChunk.movebleObject);
                                if (playerChunk.movebleObject.curAP > 0)
                                {
                                    playerChunk.movebleObject.SetShowMinusAp(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void DrawBattle()
    {
        int it = p.x;
        int jt = p.y;
        //Debug.Log(returnedToUnit);
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                //Debug.Log(returnedToUnit);
                if (move && !block && !returnedToUnit)
                {
                    int k = Mathf.CeilToInt(dist[i][j] / GetUnit(playerPoint).unit.GetSpeed());
                    if (!enemyTurn && k <= GetUnit(playerPoint).unit.curAP)
                    {
                        if (k > 0)
                        {
                            chunks[i][j].GetComponent<SpriteActive>().SetText(k.ToString());
                        }
                    }
                    else
                    {
                        chunks[i][j].GetComponent<SpriteActive>().SetText("");
                    }
                }
                else
                {
                    chunks[i][j].GetComponent<SpriteActive>().SetText("");
                }
            }
        }
        if (!block)
        {
         //   DrawItems(activity == Activity.Myself || activity == Activity.Ally);
          //  DrawMyself(activity == Activity.Myself);
          //  DrawAbilities(activity == Activity.Enemy || activity == Activity.Ally);
          //  DrawMove(activity == Activity.Move);
        }
    }
    */

    float LenPoints(PointXY a, PointXY b)
    {
        return Mathf.Sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
    }
}

/*
    public void DrawItems(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        GUI.DrawTexture(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), fonTex);
        if ((playerChunk != null))
        {
            if (playerChunk.movebleObject.it.active || playerChunk.movebleObject.it.showen)
            {
                Texture2D tex = null;
                if (playerChunk.movebleObject.it.id == 1)
                {
                    tex = healhPotion;
                }
                if (playerChunk.movebleObject.it.id == 6)
                {
                    tex = itemsTexures[0];
                }
                if (playerChunk.movebleObject.it.id == 7)
                {
                    tex = itemsTexures[1];
                }
                if (playerChunk.movebleObject.it.id == 8)
                {
                    tex = itemsTexures[2];
                }
                if (playerChunk.movebleObject.it.id == 9)
                {
                    tex = itemsTexures[3];
                }
                if (playerChunk.movebleObject.it.id == 10)
                {
                    tex = itemsTexures[4];
                }
                if (playerChunk.movebleObject.it.id == 11)
                {
                    tex = itemsTexures[4];
                }
                if (playerChunk.movebleObject.it.id == 12)
                {
                    tex = itemsTexures[4];
                }
                if (playerChunk.movebleObject.it.id == 13)
                {
                    tex = itemsTexures[4];
                }
                //GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height - (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2), tex, ScaleMode.ScaleAndCrop);
                if (GUI.Button(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len, len), tex, itemStyle) && playerChunk.movebleObject.it.active)
                {
                    if (activeAction == Ability.Item && !confrimAction)
                    {
                        TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                        if (chosenPoint.isEqual(playerPoint))
                        {
                            activeAction = Ability.Defense;
                        }
                    }
                    else
                    {
                        activeAction = Ability.Item;
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.it.activationCost);
                    }
                }
                GUI.Box(new Rect(shiftAbilities + len * 2 + len / 2,
                    Screen.height - (Screen.height - dy * 6) / 2 + len / 2,
                    len / 2, len / 2), playerChunk.movebleObject.it.count.ToString());
                if (activeAction == Ability.Item)
                {
                    GUI.DrawTexture(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                }

            }
        }
        if ((playerChunk != null) && (!cameraMove) && !playerPoint.isEqual(chosenPoint))
        {
            if ((playerChunk.movebleObject.id != 0) && (playerChunk.movebleObject.player) && (playerChunk.movebleObject.curAP > 0))
            {
                if ((playerChunk.movebleObject.it.active) && (LenPoints(playerPoint, chosenPoint) <= playerChunk.movebleObject.GetRangeItem()) && (playerChunk.movebleObject.it.friendly == playerChunk.movebleObject.player))
                {
                    Texture2D tex = null;
                    if (playerChunk.movebleObject.it.id == 6)
                    {
                        tex = itemsTexures[0];
                    }
                    //GUI.DrawTexture(new Rect(Screen.width / 2, Screen.height - (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2, (Screen.height - dy * 6) / 2), tex, ScaleMode.ScaleAndCrop);
                    if (GUI.Button(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 6, (Screen.height - dy * 6) / 2), tex, itemStyle))
                    {
                        activeAction = Ability.Item;
                        //    Debug.Log(playerChunk.movebleObject.it.activationCost);
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.it.activationCost);
                    }
                    if (activeAction == Ability.Item)
                    {
                        GUI.DrawTexture(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 6, (Screen.height - dy * 6) / 2), chosen, ScaleMode.StretchToFill);
                    }
                }
                if (GUI.Button(new Rect(shiftAbilities + Screen.width / 6 * 5, Screen.height - (Screen.height - dy * 6) / 2, Screen.width / 6, (Screen.height - dy * 6) / 2), cancel, itemStyle))
                {
                    GetUnit(chosenPoint).unit.SetShowMinusHP(false, 0);
                    activeAction = Ability.Defense;
                    activity = Activity.Myself;
                    playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                    chosenChunk = playerChunk;
                    chosenPoint = playerPoint;
                    CameraMove(chosenPoint.x, chosenPoint.y);
                }
            }
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 2, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), overTex);
        }
    }

    public void DrawMyself(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        GUI.DrawTexture(new Rect(shiftAbilities, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), fonTex);
        if ((playerChunk != null))
        {
            if ((playerChunk.movebleObject.id != 0) && (playerChunk.movebleObject.player) && (playerChunk.movebleObject.curAP > 0))
            {
                if (GUI.Button(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, len, len), defense, itemStyle))
                {
                    if (activeAction == Ability.Defense && !confrimAction)
                    {
                        TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                        if (chosenPoint.isEqual(playerPoint))
                        {
                            activeAction = Ability.Defense;
                        }
                    }
                    else
                    {
                        activeAction = Ability.Defense;
                        activity = Activity.Myself;
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                    }
                }
                if (activity == Activity.Myself)
                {
                    if (activeAction == Ability.Defense)
                    {
                        GUI.DrawTexture(new Rect(shiftAbilities + 0, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                    }
                }
                if (GUI.Button(new Rect(shiftAbilities + len, Screen.height - (Screen.height - dy * 6) / 2, len, len), wait, itemStyle))
                {
                    if (activeAction == Ability.Wait && !confrimAction)
                    {
                        TryFriendlyAction(chosenPoint.x, chosenPoint.y);
                        if (chosenPoint.isEqual(playerPoint))
                        {
                            activeAction = Ability.Defense;
                        }
                    }
                    else
                    {
                        activeAction = Ability.Wait;
                        activity = Activity.Myself;
                        playerChunk.movebleObject.SetShowMinusAp(true, playerChunk.movebleObject.curAP);
                    }
                }
                if (activeAction == Ability.Wait)
                {
                    GUI.DrawTexture(new Rect(shiftAbilities + len, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                }
            }
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), overTex);
        }
    }

    public void DrawAbilities(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        int it = chosenPoint.x;
        int jt = chosenPoint.y;
        GUI.DrawTexture(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len * 4, len * 2), fonTex);
        if (!enemyTurn && (playerChunk != null) && (!playerChunk.movebleObject.isEmpty()) && (!block))
        {
            int cou = 0;
            //if (RA.InRangeAttack(playerPoint, chosenPoint, playerChunk.movebleObject, Ability.Attack, null))
            {
                if (GUI.Button(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len, len), attackTex, itemStyle))
                {
                    if (!enemyTurn && (playerChunk != null) && (!cameraMove) && !chosenChunk.movebleObject.player && (!chosenChunk.movebleObject.isEmpty() || !chosenChunk.obj.isEmpty()))
                    {
                        if (RA.InRangeAttack(playerPoint, chosenPoint, playerChunk.movebleObject, Ability.Attack, null))
                        {
                            //Debug.Log(activeAction);
                            if (activeAction == Ability.Attack && !confrimAction)
                            {
                                TryAttack(playerPoint, playerChunk, chosenPoint.x, chosenPoint.y);
                                // Debug.Log("TryAttack");
                            }
                            else
                            {
                                activeAction = Ability.Attack;

                                //Debug.Log(2);
                                playerChunk.movebleObject.EmulateAbility(Ability.Attack);

                                estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                playerChunk.movebleObject.SetShowMinusAp(true);
                            }
                        }
                    }
                }
                //Debug.Log(activeAction + " " + activity);
                if ((activeAction == Ability.Attack) && (activity == Activity.Enemy))
                {
                    GUI.DrawTexture(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                }
                cou++;
            }
            for (int i = 0; i < spellTex.Length; i++)
            {
                Ability ability = Ability.Attack;
                switch (i)
                {
                    case 0:
                        ability = Ability.ShieldBash;
                        break;
                    case 1:
                        ability = Ability.Cleave;
                        break;
                    case 2:
                        ability = Ability.DoubleStrike;
                        break;
                    case 3:
                        ability = Ability.CurvedTrajectoryShot;
                        break;
                }
                //   Debug.Log(playerChunk.movebleObject.shieldPerks[3] + " " +
                //      playerChunk.movebleObject.abilities[1] + " " +
                //     playerChunk.movebleObject.abilities[2] + " " +
                //    playerChunk.movebleObject.abilities[3]);
                if ((playerChunk.movebleObject.HaveAbility(ability)))// && (RA.InRangeAttack(playerPoint, chosenPoint, GetUnit(playerPoint).unit, activeAction, chosenPoint)))
                {
                    //Debug.Log(ability);
                    if (GUI.Button(new Rect(shiftAbilities + len * 4 + len * cou, Screen.height - (Screen.height - dy * 6) / 2, len, len), spellTex[i], itemStyle))
                    {

                        if ((activity == Activity.Enemy) &&
                            (playerChunk.movebleObject.abilities[i]) &&
                            (RA.InRangeAttack(playerPoint, chosenPoint, GetUnit(playerPoint).unit, activeAction, chosenPoint)))
                        {
                            if (activeAction == ability && !confrimAction)
                            {
                                TryAttack(playerPoint, playerChunk, chosenPoint.x, chosenPoint.y);
                            }
                            else
                            {
                                activeAction = ability;
                                //Debug.Log(i);
                                playerChunk.movebleObject.EmulateAbility(activeAction);
                                playerChunk.movebleObject.SetShowMinusAp(i);
                                if (i == 0)
                                {
                                    playerChunk.movebleObject.shieldBashAbility = true;
                                    //Debug.Log(1);
                                    estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                    playerChunk.movebleObject.shieldBashAbility = false;
                                }
                                if (i == 1)
                                {
                                    estimatedDamage = new PointXY(0, 0);
                                    PointXY help;
                                    for (int j = 0; j < mapWidth; j++)
                                    {
                                        for (int k = 0; k < mapHeight; k++)
                                        {
                                            if (RA.InRangeAttack(playerPoint, new PointXY(j, k), playerChunk.movebleObject, activeAction, chosenPoint))
                                            {
                                                if (!map[j][k].movebleObject.isEmpty() && !map[j][k].movebleObject.player)
                                                {
                                                    help = map[j][k].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                                    estimatedDamage.realD += help.realD;
                                                    estimatedDamage.x += (int)(0.8f * help.realD);
                                                    map[j][k].movebleObject.SetShowMinusHP(true, (int)(0.8f * help.realD));
                                                }
                                                if (!map[j][k].obj.isEmpty())
                                                {
                                                    help = map[j][k].obj.EstimatedDamage(playerChunk.movebleObject);
                                                    estimatedDamage.realD += help.realD;
                                                    estimatedDamage.x += (int)(0.8f * help.realD);
                                                }
                                            }
                                        }
                                    }
                                }
                                if (i == 2)
                                {
                                    // playerChunk.movebleObject.doubleHandedStrike = true;
                                    estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));
                                }
                                if (i == 3)
                                {
                                    estimatedDamage = map[it][jt].movebleObject.EstimatedDamage(playerChunk.movebleObject, GetUnit(chosenPoint).rotation, new PointXY(GetUnit(chosenPoint).point.x - GetUnit(playerPoint).point.x, GetUnit(chosenPoint).point.y - GetUnit(playerPoint).point.y), LenPoints(chosenPoint, playerPoint));

                                }
                            }
                            //Debug.Log(((float)playerChunk.movebleObject.curAbilityCulldown[i]) / (float)playerChunk.movebleObject.abilityCulldowns[i]);

                        }
                    }
                    GUI.DrawTexture(new Rect(len * 4 + len * cou, Screen.height - (Screen.height - dy * 6) / 2, len, (len) * ((float)playerChunk.movebleObject.curAbilityCulldown[i]) / (float)playerChunk.movebleObject.abilityCulldowns[i]), reload);
                    if (activeAction == ability)
                    {
                        GUI.DrawTexture(new Rect(shiftAbilities + len * 4 + len * cou, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen, ScaleMode.StretchToFill);
                    }
                    cou++;
                }
            }
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 4, Screen.height - (Screen.height - dy * 6) / 2, len * 4, len * 2), overTex);
        }
    }

    public void DrawMove(bool flag)
    {
        float len = (Screen.height - dy * 6) / 4;
        GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), fonTex);
        int it = chosenPoint.x;
        int jt = chosenPoint.y;
        if (GUI.Button(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len, len), moveTex, itemStyle))
        {
            if (activity == Activity.Move && !confrimAction)
            {
                TryMove(chosenPoint.x, chosenPoint.y);
            }
        }
        if (activity == Activity.Move)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len, len), chosen);
        }
        if (GUI.Button(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2 + len, len, len), rotateTex, itemStyle))
        {
            activity = Activity.Rotate;
        }
        if (activity == Activity.Rotate)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2 + len, len, len), chosen);
        }
        if (!flag)
        {
            GUI.DrawTexture(new Rect(shiftAbilities + len * 8, Screen.height - (Screen.height - dy * 6) / 2, len * 2, len * 2), overTex);
        }
    }
*/

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Armour{

    public string keyName;
    public bool locked;
    public int pound;
    public string[] passiveAbilities;
    public string[] activeAbilities;
    public string image;
    public string headSprite;
    public string bodySprite;
    public string rShoulderSprite;
    public string lShoulderSprite;
    public string lUpArmSprite;
    public string rUpArmSprite;
    public string lMidArmSprite;
    public string rMidArmSprite;
    public string lDownArmSprite;
    public string rDownArmSprite;
    public string lArmSprite;
    public string rArmSprite;
    public string rLegUpSprite;
    public string lLegUpSprite;
    public string rLegDownSprite;
    public string lLegDownSprite;

    public Armour(Armour a)
    {
        keyName = a.keyName;
        locked = a.locked;
        passiveAbilities = a.passiveAbilities;
        activeAbilities = a.activeAbilities;
        image = a.image;
        headSprite = a.headSprite;
        bodySprite = a.bodySprite;
        rShoulderSprite = a.rShoulderSprite;
        lShoulderSprite = a.lShoulderSprite;
        lUpArmSprite = a.lUpArmSprite;
        rUpArmSprite = a.rUpArmSprite;
        lMidArmSprite = a.lMidArmSprite;
        rMidArmSprite = a.rMidArmSprite;
        lDownArmSprite = a.lDownArmSprite;
        rDownArmSprite = a.rDownArmSprite;
        lArmSprite = a.lArmSprite;
        rArmSprite = a.rArmSprite;
        rLegDownSprite = a.rLegDownSprite;
        rLegUpSprite = a.rLegUpSprite;
        lLegDownSprite = a.lLegDownSprite;
        lLegUpSprite = a.lLegUpSprite;
        passiveAbilities = a.passiveAbilities;
        activeAbilities = a.activeAbilities;
        Init();
    }

    public List<Active> active = new List<Active>();
    public List<Passive> passive = new List<Passive>();

    StaticData staticData;

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        foreach (var i in passiveAbilities)
        {
            if (i.Length > 2)
            {
                //Debug.Log(staticData.GetPassiveByKeyName(i));
                passive.Add(new Passive(staticData.GetPassiveByKeyName(i)));
                passive[passive.Count - 1].Init();
            }
        }
        foreach (var i in activeAbilities)
        {
            if (i.Length > 2)
            {
                active.Add(new Active(staticData.GetActiveByKeyName(i)));
                active[active.Count - 1].Init();
            }
        }
    }
}

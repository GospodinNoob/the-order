﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Squad{

    public string keyName;
    public string[] types;
    public string unit1;
    public string unit2;
    public string unit3;
    public string unit4;
    public string unit5;
    public string unit6;
    public bool locked;
    public int might;

    StaticData staticData;

    public float GetSpeed()
    {
        float ans = 10000000000;
        foreach(var i in units)
        {
            ans = Mathf.Min(i.speed);
        }
        return ans;
    }

    public List<MovebleObject> units = new List<MovebleObject>();

    public Squad(string squadKey)
    {
        string s = squadKey + "_squad";

        for (int i = 0; i < 6; i++)
        {
            if (PlayerPrefs.GetInt(s + "_active") == 1)
            {
                units.Add(new MovebleObject(s + i.ToString()));
                units[units.Count - 1].Init();
            }
            else
            {
                units.Add(null);
            }
        }
    }

    public int GetUnitsCou()
    {
        int cou = 0;
        foreach (var i in units)
        {
            if (i != null)
            {
                cou++;
            }
        }
        return cou;
    }

    public void Init()
    {
        staticData = GameObject.Find("[StaticData]").GetComponent<StaticData>();
        units.Add(staticData.GetUnitByKeyName(unit1));
        units.Add(staticData.GetUnitByKeyName(unit2));
        units.Add(staticData.GetUnitByKeyName(unit3));
        units.Add(staticData.GetUnitByKeyName(unit4));
        units.Add(staticData.GetUnitByKeyName(unit5));
        units.Add(staticData.GetUnitByKeyName(unit6));
    }

    public void Save(string squadKey)
    {
        string s = squadKey + "_squad";

        for(int i = 0; i < units.Count; i++)
        {
            if(units[i] != null)
            {
                PlayerPrefs.SetInt(s + "_active", 1);
                units[i].Save(s + i.ToString());
            }
            else
            {
                PlayerPrefs.SetInt(s + "_active", 0);
            }
        }
    }
}

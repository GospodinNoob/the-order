﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleHandedAI : MonoBehaviour {

    public Unit[] units;
    public float[][] dist;
    public float[][] costMatrix;
    public ChunkController[][] map;
    public RangeAttack RA;

    public DoubleHandedAI()
    {
        RA = new RangeAttack();
    }

    public Unit GetTarget(MovebleObject un, PointXY p, Ability activeAction)
    {
        Unit target = null;
        int minusDmg = -1;
        float hit = -1;
        bool kill = false;
        // Debug.Log("Target");
        int maxunits = units.Length;
        for (int i = 0; i < maxunits; i++)
        {
            //Debug.Log(RA.InRangeAttack(p, units[i].point, un, activeAction, null) + " " + activeAction.ToString());
            if (!units[i].unit.isEmpty() && units[i].unit.player && RA.InRangeAttack(p, units[i].point, un, activeAction, null))
            {
                //Debug.Log(i);
                PointXY estDmg = null;// new PointXY(units[i].unit.EstimatedDamage(un, units[i].rotation, new PointXY(units[i].point.x - p.x, units[i].point.y - p.y), RA.LenPoints(p, units[i].point)));
                estDmg.realD = Mathf.Min(estDmg.realD, units[i].unit.curHits);
                Debug.Log("*" + estDmg.realD);
                if (kill)
                {
                    if (estDmg.realD == units[i].unit.curHits)
                    {
                        if (estDmg.realH > hit)
                        {
                            target = units[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;

                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    if (estDmg.realD == units[i].unit.curHits)
                    {
                        if (estDmg.realH * estDmg.realH * 3 > hit * minusDmg)//un.unit.archTypeValues[6] > hit * minusDmg)
                        {
                            target = units[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;
                            kill = true;
                        }
                    }
                    else
                    {
                        if (estDmg.realD * estDmg.realH > minusDmg * hit)
                        {
                            target = units[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;
                            kill = false;
                        }
                    }
                }
            }
        }
        //Debug.Log(minusDmg);
        if (minusDmg <= 0)
        {
            return null;
        }
        return target;
    }

    public PointXY GetBestPoint(MovebleObject move)
    {
        /*
        MovebleObject enemyUn = move;
        int mapWidth = dist.Length;
        int mapHeight = dist[0].Length;
        int INF = 10000000;
        int maxunits = units.Length;

        PointXY yourPoint = null;

        for (int i = 0; i < maxunits; i++)
        {
            if (units[i].unit == move)
            {
                yourPoint = new PointXY(units[i].point);
            }
        }
        //Debug.Log(enemyUn.curAP + " " + enemyUn.lastAttack);
        if ((enemyUn.curAP == 1) && (enemyUn.lastAttack))
        {
            PointXY bestPoint = null;
            float value = -INF;
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if (Mathf.CeilToInt(dist[i][j] / enemyUn.GetSpeed()) > 1)
                    {
                        costMatrix[i][j] = -INF;
                    }
                    else
                    {
                        for (int r = 0; r < maxunits; r++)
                        {
                            if (!units[r].unit.isEmpty())
                            {
                                costMatrix[i][j] += Mathf.Sqrt((units[r].point.x - i) * (units[r].point.x - i) + (units[r].point.y - j) * (units[r].point.y - j));
                            }
                        }
                    }
                    if (costMatrix[i][j] > value)
                    {
                        bestPoint = new PointXY(i, j);
                        value = costMatrix[i][j];
                    }
                }
            }
           // Debug.Log(value);
            bestPoint.resultArchery = false;
            return bestPoint;


        }

        PointXY p = new PointXY(-1, -1);
        PointXY p2 = new PointXY(-1, -1);
        p.ability = Ability.Attack;
        p2.ability = Ability.Attack;
        PointXY est, estHelp;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        int costMI = 10000;
        int needAp = enemyUn.curAP - 1;
        bool tfMI = false;// ((Mathf.Abs(units[r].point.x - j) <= 1) && (Mathf.Abs(units[r].point.y - k)) <= 1)
        tfMI = false;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        for (int j = 0; j < mapWidth; j++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
               // Debug.Log(j + " " + k);
              //  Debug.Log(enemyUn);
                //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                if (target != null)
                {
                    estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                    estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                    //  Debug.Log(map);
                    if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                    {
                        //Debug.Log(j + " " + k);
                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP)
                        {
                            if (estHelp.realD * ( estHelp.realH) > est.realD * ( est.realH))
                            {
                                //Debug.Log(j + " " + k);
                                tfMI = true;
                                p2 = new PointXY(j, k);
                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                est = new PointXY(estHelp);
                            }
                        }
                    }
                }
            }
        }
        if (tfMI)
        {
            if (enemyUn.UseAbility(Ability.DoubleStrike))
            {
                p2.ability = Ability.DoubleStrike;
            }
            return p2;
        }

        if (!tfMI)
        {
            if (enemyUn.curAP > 1)
            {
                est = new PointXY(-1, -1);
                int cost = 10000;
                bool tf = false;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                        Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                        estHelp = new PointXY(0, 0);
                        if (target != null)
                        {
                            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(target.point, new PointXY(j, k))));
                            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                            if (map[j][k].movebleObject.isEmpty() && map[j][k].obj.isEmpty())
                            {
                                if (p.x < 0)
                                {
                                    tf = true;
                                    tfMI = true;
                                    p = new PointXY(j, k);
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                                else
                                {
                                    if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                    {
                                        tf = true;
                                        tfMI = true;
                                        p = new PointXY(j, k);
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                        est = new PointXY(estHelp);
                                    }
                                    else
                                    {
                                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) == Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                        {
                                            if (estHelp.realD * ( estHelp.realH) > est.realD * ( est.realH))
                                            {
                                                tf = true;
                                                tfMI = true;
                                                p = new PointXY(j, k);
                                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                                est = new PointXY(estHelp);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                if (!tf)
                {
                    return null;
                }
                if (enemyUn.UseAbility(Ability.DoubleStrike))
                {
                    p2.ability = Ability.DoubleStrike;
                }
                return p;
            }
        }*/
        return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoHandedAI{

    public Unit[] units;
    public float[][] dist;
    public float[][] costMatrix;
    public ChunkController[][] map;
    public RangeAttack RA;

    public TwoHandedAI()
    {
        RA = new RangeAttack();
    }

    public Unit GetTarget(MovebleObject un, PointXY p, Ability activeAction)
    {
        //Debug.Log(activeAction);
        Unit target = null;
        int minusDmg = -1000000;
        float hit = 100;
        bool kill = false;
        // Debug.Log("Target");
        int maxunits = units.Length;
        for (int i = 0; i < maxunits; i++)
        {
          //  PointXY chosenPoint = null;
           // if (activeAction == 1)
            //{
             //   chosenPoint = new PointXY(units[i].point);
           // }
            if (!units[i].unit.isEmpty() && units[i].unit.player && RA.InRangeAttack(p, units[i].point, un, Ability.Attack, null))
            {
                // Debug.Log(i);
                PointXY estDmg = null;// new PointXY(units[i].unit.EstimatedDamage(un, units[i].rotation, new PointXY(units[i].point.x - p.x, units[i].point.y - p.y), RA.LenPoints(p, units[i].point)));
                estDmg.realD = Mathf.Min(estDmg.realD, units[i].unit.curHits);
                if (activeAction == Ability.Cleave)
                {
                    //Debug.Log(estDmg.realD);
                    estDmg.realD = 0;
                    estDmg.realH = 0;
                   // Debug.Log(activeAction);
                    for(int j = 0; j < maxunits; j++)
                    {
                        if (i == j)
                        {
                        //    Debug.Log(RA.InRangeAttack(p, units[j].point, un, activeAction, units[i].point));
                        }
                        if (!units[j].unit.isEmpty() && units[j].unit.player && RA.InRangeAttack(p, units[j].point, un, activeAction, units[i].point))
                        {
                            //Debug.Log(j);

                            PointXY estOne = null;// new PointXY(units[j].unit.EstimatedDamage(un, units[j].rotation, new PointXY(units[j].point.x - p.x, units[j].point.y - p.y), RA.LenPoints(p, units[j].point)));
                            //Debug.Log(estOne.realD);
                            estOne.realD = Mathf.Min(estOne.realD, units[j].unit.curHits);
                            estDmg.realD += estOne.realD;
                            estDmg.realH = (estDmg.realH * j + estOne.realH) / (j + 1);
                        }
                    }
                   // Debug.Log(estDmg.realD + " " + estDmg.realH + " " + minusDmg + " " + hit);
                    if (estDmg.realD * estDmg.realH > minusDmg * hit)
                    {
                        minusDmg = estDmg.realD;
                        hit = estDmg.realH;
                        target = units[i];
                       // Debug.Log(target);
                    }
                }
                else
                {
                    if (kill)
                    {
                        if (estDmg.realD == units[i].unit.curHits)
                        {
                            if (estDmg.realH > hit)
                            {
                                target = units[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;

                            }
                            else
                            {

                            }
                        }
                    }
                    else
                    {
                        if (estDmg.realD == units[i].unit.curHits)
                        {
                            if (estDmg.realH * estDmg.realH * 3 > hit * minusDmg)//un.unit.archTypeValues[6] > hit * minusDmg)
                            {
                                target = units[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = true;
                            }
                        }
                        else
                        {
                            if (estDmg.realD * estDmg.realH > minusDmg * hit)
                            {
                                target = units[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = false;
                            }
                        }
                    }
                }
            }
        }
        if (minusDmg <= 0)
        {
            return null;
        }
        return target;
    }

    bool Flag(MovebleObject enemyUn, int j, int k)
    {
        if (enemyUn.maxAP == 1)
        {
            return Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) <= enemyUn.curAP;
        }
        return Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < enemyUn.curAP;
    }

    public PointXY GetBestPoint(MovebleObject move)
    {
        MovebleObject enemyUn = move;
        int mapWidth = dist.Length;
        int mapHeight = dist[0].Length;
        int INF = 10000000;
        int maxunits = units.Length;

        PointXY yourPoint = null;

        for (int i = 0; i < maxunits; i++)
        {
            if (units[i].unit == move)
            {
                yourPoint = new PointXY(units[i].point);
            }
        }
        PointXY p = new PointXY(-1, -1);
        PointXY p2 = new PointXY(-1, -1);
        PointXY est, estHelp;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        int costMI = 10000;
        int needAp = enemyUn.curAP - 1;
        bool tfMI = false;// ((Mathf.Abs(units[r].point.x - j) <= 1) && (Mathf.Abs(units[r].point.y - k)) <= 1)
        tfMI = false;
        est = new PointXY(-1, -1);
        est.realD = -INF;
        est.realH = 1;
        /*
        for (int j = 0; j < mapWidth; j++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                if (target != null)
                {
                    estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(new PointXY(j, k), target.point)));
                    estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                    //  Debug.Log(map);
                    if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                    {
                        //Debug.Log(j + " " + k);
                        if (Flag(enemyUn, j, k))
                        {
                            if (estHelp.realD * (estHelp.realH) > est.realD * (est.realH))
                            {
                                //Debug.Log(j + " " + k);
                                tfMI = true;
                                p2 = new PointXY(j, k);
                                p2.ability = Ability.Attack;
                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                est = new PointXY(estHelp);
                            }
                        }
                    }
                    if (enemyUn.UseAbility(Ability.Cleave))
                    {
                        target = GetTarget(enemyUn, new PointXY(j, k), Ability.Cleave);
                        if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn)) && (map[j][k].obj.isEmpty()))
                        {
                            //Debug.Log(j + " " + k);
                            PointXY estDmg = new PointXY(0, 0);
                            for (int t = 0; t < maxunits; t++)
                            {
                                if (!units[t].unit.isEmpty() && RA.InRangeAttack(new PointXY(j, k), units[t].point, enemyUn, Ability.Cleave, target.point))
                                {
                                    if (units[t].unit.player)
                                    {
                                        PointXY estOne = new PointXY(units[t].unit.EstimatedDamage(enemyUn, units[t].rotation, new PointXY(units[t].point.x - j, units[t].point.y - k), RA.LenPoints(new PointXY(j, k), units[t].point)));
                                        estOne.realD = Mathf.Min(estOne.realD, units[t].unit.curHits);
                                        //Debug.Log(estOne.realD);
                                        estDmg.realD += estOne.realD;
                                        estDmg.realH = estDmg.realH * t + estOne.realH / (t + 1);
                                    }
                                    else
                                    {
                                        PointXY estOne = new PointXY(units[t].unit.EstimatedDamage(enemyUn, units[t].rotation, new PointXY(units[t].point.x - j, units[t].point.y - k), RA.LenPoints(new PointXY(j, k), units[t].point)));
                                        estOne.realD = Mathf.Min(estOne.realD, units[t].unit.curHits);
                                        //Debug.Log(estOne.realD);
                                        estDmg.realD -= estOne.realD;
                                       // estDmg.realH = estDmg.realH * t + estOne.realH / (t + 1);
                                    }
                                }
                            }
                            estHelp = new PointXY(estDmg);
                            //Debug.Log(estHelp.realD);
                            //if (Flag(enemyUn, j, k))
                            {
                                if (estHelp.realD * (estHelp.realH) > est.realD * (est.realH))
                                {
                                    //Debug.Log(j + " " + k);
                                    //Debug.Log("*");
                                    tfMI = true;
                                    p2 = new PointXY(j, k);
                                    p2.ability = Ability.Cleave;
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (tfMI)
        {
            return p2;
        }

        if (!tfMI)
        {
            if ((enemyUn.curAP > 1) || ((enemyUn.maxAP == 1) && (enemyUn.curAP == 1)))
            {
                est = new PointXY(-1, -1);
                int cost = 10000;
                bool tf = false;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        //enemyUn.EmulateAbility(Ability.Attack, Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()));
                        Unit target = GetTarget(enemyUn, new PointXY(j, k), Ability.Attack);
                        estHelp = new PointXY(0, 0);
                        if (target != null)
                        {
                            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn, target.rotation, new PointXY(target.point.x - j, target.point.y - k), RA.LenPoints(target.point, new PointXY(j, k))));
                            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                            if (map[j][k].movebleObject.isEmpty() && map[j][k].obj.isEmpty())
                            {
                                if (p.x < 0)
                                {
                                    tf = true;
                                    tfMI = true;
                                    p = new PointXY(j, k);
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                    est = new PointXY(estHelp);
                                }
                                else
                                {
                                    if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) < Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                    {
                                        tf = true;
                                        tfMI = true;
                                        p = new PointXY(j, k);
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                        est = new PointXY(estHelp);
                                    }
                                    else
                                    {
                                        if (Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed()) == Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.GetSpeed()))
                                        {
                                            if (estHelp.realD * (estHelp.realH) > est.realD * (est.realH))
                                            {
                                                tf = true;
                                                tfMI = true;
                                                p = new PointXY(j, k);
                                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.GetSpeed());
                                                est = new PointXY(estHelp);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                if (!tf)
                {
                    return null;
                }
                return p;
            }
        }*/
        return null;
    }
}

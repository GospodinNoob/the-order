﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEffects : MonoBehaviour {

    GameObject obj;
    public GameObject main;


    // Use this for initialization
    void Start()
    {
    }

    int segment = 0;
    float timer;

    public void SetMain(GameObject go)
    {
        main = go;
        obj = this.gameObject;
    }

    GameObject enemy;

    float cameraTimer;
    float cameraSpeed;
    float startCameraMove;
    bool cameraMove;

    float dx;

    public void SetDx(float a)
    {
        dx = a;
    }

    void Rotation(int vx, int vy)
    {
        float angle = Mathf.Atan2(vx, -vy);
        angle *= 57.2958f;
        this.gameObject.GetComponent<HelperScript>().renderGO.gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        //Debug.Log(angle);
    }

    bool damageFlag;
    float damageDelay;

    Unit attacker;
    PointXY pTarget;

    public void DamageTakenDelay(Unit m, PointXY p, float delay)
    {
        damageFlag = true;
        damageDelay = Time.time + delay;
        attacker = m;
        pTarget = new PointXY(p);
    }

    float len;

    int ChooseSoundStep(PointXY p)
    {
        return main.GetComponent<BattleSceneUI>().GetGround(p);
    }

    bool musicChange;
    Vector2 speed;
    bool wait;

    // Update is called once per frame
    void Update()
    {
        if (damageFlag)
        {
            if (Time.time > damageDelay)
            {
                damageFlag = false;
              //  main.GetComponent<BattleSceneUI>().DealDamage(attacker, pTarget.x, pTarget.y);
            }
        }
    }
}

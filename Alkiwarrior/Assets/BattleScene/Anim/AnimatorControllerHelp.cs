﻿using UnityEngine;
using System.Collections;
using DragonBones;

public class AnimatorControllerHelp : MonoBehaviour {

    public GameObject shields;
    public GameObject bows;
    public GameObject leftWeapon;
    public GameObject twoHandedSwords;
    public GameObject rightWeapon;
    public GameObject halberds;

    public GameObject mis;

    public GameObject headSprite;
    public GameObject bodySprite;
    public GameObject rShoulderSprite;
    public GameObject lShoulderSprite;
    public GameObject lUpArmSprite;
    public GameObject rUpArmSprite;
    public GameObject lMidArmSprite;
    public GameObject rMidArmSprite;
    public GameObject lDownArmSprite;
    public GameObject rDownArmSprite;
    public GameObject lArmSprite;
    public GameObject rArmSprite;
    public GameObject rLegUpSprite;
    public GameObject lLegUpSprite;
    public GameObject rLegDownSprite;
    public GameObject lLegDownSprite;


PointXY p2;

    public void SetSpeed(float speed)
    {
        if(speed < 0.01)
        {
            PlayAnim(unit.unitBase.animList[0]);
        }
        else
        {
            PlayAnim(unit.unitBase.animList[1]);
        }
    }

    float sp;

    int id, type;

    MovebleObject unit;
    GameObject animator;
    UnitBase baseAnim;
    bool start = false;

    private void Awake()
    {
        SpriteRenderer[] sp = this.transform.GetComponentsInChildren<SpriteRenderer>();
        foreach (var i in sp)
        {
            i.sortingLayerName = "Units";
            // Debug.Log(i.sortingLayerName);
        }
    }

    private bool ArrayHasTag(string[] array, string tag)
    {
        if (array == null)
        {
            return false;
        }
        if (array.Length == 0)
        {
            return false;
        }
        foreach (var i in array)
        {
            if (i.ToLower() == tag.ToLower())
            {
                // Debug.Log(tag);
                return true;
            }
        }
        return false;
    }

    public void Ini(MovebleObject move)
    {
        unit = move;
        RenderArmatueHelper[] buf = this.transform.GetComponentsInChildren<RenderArmatueHelper>();

        headSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.headSprite);
        bodySprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.bodySprite);
        rShoulderSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rShoulderSprite);
        lShoulderSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.lShoulderSprite);
        lUpArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.lUpArmSprite);
        rUpArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rUpArmSprite);
        lMidArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.lMidArmSprite);
        rMidArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rMidArmSprite);
        lDownArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.lDownArmSprite);
        rDownArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rDownArmSprite);
        lArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.lArmSprite);
        rArmSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rArmSprite);
        rLegUpSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rLegUpSprite);
        lLegUpSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.lLegUpSprite);
        rLegDownSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rLegUpSprite);
        lLegDownSprite.GetComponent<LayerHelper>().ActiveSprite(move.armour.rLegDownSprite);

        foreach (var i in buf)
        {
            //Debug.Log(i.gameObject.name);
            if(i.gameObject.name == move.unitBase.skeletName)
            {
                animator = i.gameObject;
            }
            else
            {
                i.gameObject.SetActive(false);
            }
        }
        animator.SetActive(true);
        start = true;
        if(ArrayHasTag(move.leftArm.types, "SHIELD")){
            shields.SetActive(true);
            shields.GetComponent<LayerHelper>().ActiveSprite(move.leftArm.sprite);
        }
        if (ArrayHasTag(move.leftArm.types, "DOUBLE"))
        {
            leftWeapon.SetActive(true);
            leftWeapon.GetComponent<LayerHelper>().ActiveSprite(move.leftArm.sprite);
        }
        if (ArrayHasTag(move.rightArm.types, "DOUBLE") || ArrayHasTag(move.rightArm.types, "ONE"))
        {
            rightWeapon.SetActive(true);
            rightWeapon.GetComponent<LayerHelper>().ActiveSprite(move.rightArm.sprite);
        }
        if (ArrayHasTag(move.rightArm.types, "HAL"))
        {
            halberds.SetActive(true);
            halberds.GetComponent<LayerHelper>().ActiveSprite(move.rightArm.sprite);
        }
        if (ArrayHasTag(move.rightArm.types, "TWOSW"))
        {
            twoHandedSwords.SetActive(true);
            twoHandedSwords.GetComponent<LayerHelper>().ActiveSprite(move.rightArm.sprite);
        }
        SpriteRenderer[] sp = this.transform.GetComponentsInChildren<SpriteRenderer>();
        foreach (var i in sp)
        {
            i.sortingLayerName = "Units";
            // Debug.Log(i.sortingLayerName);
        }
    }

    public void PlayAnim(string name)
    {
        lastAnim = name;
        animator.GetComponent<DragonBones.UnityArmatureComponent>().animation.Play(name);
    }

    public void PlayAnim(string name, int cou)
    {
        lastAnim = name;
        animator.GetComponent<DragonBones.UnityArmatureComponent>().animation.Play(name, cou);
    }

    string lastAnim;

	// Use this for initialization
	void Start () {
    }

    bool arrowDelay;
    float arrowTimer;
	
	// Update is called once per frame
	void Update () {
        if (!start)
        {
            return;
        }
        if (!animator.GetComponent<DragonBones.UnityArmatureComponent>().animation.isPlaying)
        {
            //Debug.Log(unit.unitBase.animList);
            lastAnim = unit.unitBase.animList[0];
            animator.GetComponent<DragonBones.UnityArmatureComponent>().animation.Play(unit.unitBase.animList[0]);
        }
        return;
        if (arrowDelay && (arrowTimer < Time.time))
        {
            arrowDelay = false;
            float speed = this.gameObject.transform.root.gameObject.GetComponent<BattleSceneUI>().GetOneChunkLen();
            float speed2 = 4;
            UnityEngine.Transform tr = this.gameObject.transform;//.parent.gameObject.transform;
            //tr.position = new Vector3(0, 0, 0);
            GameObject missle = (GameObject)GameObject.Instantiate(mis, tr);
            // Debug.Log(missle.name);
            // Debug.Log(missle.transform.position);
            missle.transform.localPosition = new Vector3(0, 0, 0);
            missle.transform.localRotation = new Quaternion(0, 0, 0, 1);
            double a2 = Mathf.Atan2(p2.x, p2.y);
            //  double a = Mathf.Atan2(tr.position.x, tr.position.y);
            // a = 0;
            missle.GetComponent<Rigidbody2D>().velocity = new Vector2(speed * speed2 * Mathf.Sin((float)(a2)), speed * speed2 * Mathf.Cos((float)(a2)));
            float scale = this.gameObject.transform.parent.transform.localScale.x;//.GetComponent<BattleSceneControl>().GetScale();
            scale *= 4 / 18.4f;
            missle.transform.localScale = new Vector3(scale, scale, scale);
            Destroy(missle, p2.len() / speed2);
        }
    }
}

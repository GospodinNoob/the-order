﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
/*
public class MachineBattles : MonoBehaviour
{

    public Texture2D defense, healhPotion;
    int n, k;
    float deltaX;
    int magicConstant = 53;

    int maxPlayerUnits;
    int maxEnemyUnits;

    Vector3 defaultCameraPosition;
    public AudioSource au;


    ArrayList way;

    bool enemyTurn;


    Chunk[][] map;

    Chunk chosenChunk;
    PointXY chosenPoint;
    bool move = false;
    bool attack = false;

    float dx;
    float dy;

    public GUIStyle coverButtonStyle;
    Unit[] playerUnits;
    Unit[] enemyUnits;

    float[][] dist;
    float[][] costMatrix;
    PointXY[][] parents;
    bool[][] flags;

    Unit moveO;

    int mapHeight;
    int mapWidth;
    int shiftX;
    int shiftY;

    GameEvent ev;

    bool InMap(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= mapWidth)
        {
            return false;
        }
        if (b >= mapHeight)
        {
            return false;
        }
        return true;
    }

    bool Valid(int a, int b)
    {
        if (a < 0)
        {
            return false;
        }
        if (b < 0)
        {
            return false;
        }
        if (a >= mapWidth)
        {
            return false;
        }
        if (b >= mapHeight)
        {
            return false;
        }
        if (map[a][b].obj.isEmpty() && map[a][b].movebleObject.isEmpty())
        {
            return true;
        }
        return false;
    }

    float Len(int a, int b, int c, int d, float v1, float v2)
    {
        return Mathf.Sqrt((a - c) * (a - c) + (b - d) * (b - d)) * (v1 + v2) / 2;
    }

    void DealDamage(Unit move, int x, int y)
    {
        //Debug.Log(move);
        move.SetRotation(new PointXY(x - move.point.x, y - move.point.y));
        if (!map[x][y].obj.isEmpty())
        {
            map[x][y].obj.DealDamage(move.unit);
        }
        if (!map[x][y].movebleObject.isEmpty())
        {
            GetUnit(new PointXY(x, y)).unit.DealDamage(move.unit, GetUnit(new PointXY(x, y)).rotation, move.rotation);
            // map[x][y].movebleObject.DealDamage(move, map[x][y].movebleObject.);
        }
    }

    int INF = 10000000;


    void Dijkstra(PointXY p1)
    {
        PointXY p = new PointXY(p1);
        int n = mapWidth;
        int k = mapHeight;

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < k; j++)
            {
                dist[i][j] = INF;
                flags[i][j] = false;
            }
        }

        dist[p.x][p.y] = 0;
        int step = 0;
        while ((dist[p.x][p.y] < INF - 1) && (step < 500))
        {
            step++;
            // Dep.yug.Log(dist[p.x][p.y]);
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    // Dep.yug.Log(Vp.xlid(p.x + i, p.y + j));
                    if (!((i != 0) && (j != 0)))
                    {
                        if (Valid(p.x + i, p.y + j) && (dist[p.x + i][p.y + j] > dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost)))
                        {

                            dist[p.x + i][p.y + j] = dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost);
                            //Debug.Log(p.x + " " + p.y);
                            parents[p.x + i][p.y + j] = new PointXY(p);
                        }
                    }
                    else
                    {
                        if (Valid(p.x + i, p.y + j) && (map[p.x + i][p.y].movebleObject.isEmpty()) && (map[p.x][p.y + j].movebleObject.isEmpty()) && (map[p.x + i][p.y].obj.isEmpty()) && (map[p.x][p.y + j].obj.isEmpty()) && (dist[p.x + i][p.y + j] > dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost)))
                        {

                            dist[p.x + i][p.y + j] = dist[p.x][p.y] + Len(p.x, p.y, p.x + i, p.y + j, map[p.x][p.y].moveCost, map[p.x + i][p.y + j].moveCost);
                            //Debug.Log(p.x + " " + p.y);
                            parents[p.x + i][p.y + j] = new PointXY(p);
                        }
                    }
                }
            }

            flags[p.x][p.y] = true;

            float min = INF;
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if ((dist[i][j] < min) && (!flags[i][j]))
                    {
                        p.x = i;
                        p.y = j;
                        min = dist[i][j];
                    }
                }
            }

        }


    }

    void GenEnemyUnit(int i, int a, int b)
    {
        enemyUnits[i] = new Unit(a, b, false, new PointXY(0, -1));
        Transform tr = this.gameObject.transform;
        float shift = Screen.height / 8;
        if (Screen.height / 8 <= Screen.width / 6)
        {
            shift = 0;
        }
        //Debug.Log('*');
        map[a][b].movebleObject = enemyUnits[i].unit;
        map[a][b].movebleObject.un = enemyUnits[i];
        enemyUnits[i].unit.curAP = 0;
        // enemyUnits[i].unit.RecalcHp();
        //enemyUnits[i].unit.RecalcAP();
        //Debug.Log('*');
    }

    void GenerateEnemySquad(int a, int b, bool rotation)
    {
        int squadUnitCount = 0;
        if (curEnemyUnits <= 6)
        {
            squadUnitCount = curEnemyUnits;
        }
        else
        {
            squadUnitCount = Random.Range(4, 7);
        }
        curEnemyUnits -= squadUnitCount;
        if (rotation)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenEnemyUnit(curEnemyUnits + squadUnitCount - 1, a + i, b + j);
                        squadUnitCount--;
                        // curEnemyUnits--;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenEnemyUnit(curEnemyUnits + squadUnitCount - 1, a + i, b + j);
                        squadUnitCount--;
                        //  curEnemyUnits--;
                    }
                }
            }
        }
    }

    void GenPlayerUnit(int i, int a, int b)
    {
        //Debug.Log(i);
        playerUnits[i] = new Unit(a, b, true, new PointXY(0, -1));
        float shift = Screen.height / 8;
        if (Screen.height / 8 <= Screen.width / 6)
        {
            shift = 0;
        }
        //Debug.Log('*');
        map[a][b].movebleObject = playerUnits[i].unit;
        map[a][b].movebleObject.un = playerUnits[i];
        playerUnits[i].unit.curAP = 0;
        //playerUnits[i].unit.RecalcHp();
        //playerUnits[i].unit.RecalcAP();
        //Debug.Log('*');
    }

    void GeneratePlayerSquad(int a, int b, bool rotation)
    {
        int squadUnitCount = 0;
        if (curPlayerUnits <= 6)
        {
            squadUnitCount = curPlayerUnits;
        }
        else
        {
            squadUnitCount = Random.Range(4, 7);
        }
        curPlayerUnits -= squadUnitCount;
        if (rotation)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenPlayerUnit(curPlayerUnits + squadUnitCount - 1, a + i, b + j);
                        squadUnitCount--;
                        // curEnemyUnits--;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    if ((squadUnitCount > 0))
                    {
                        GenPlayerUnit(curPlayerUnits + squadUnitCount - 1, a + i, b + j);
                        squadUnitCount--;
                        //  curEnemyUnits--;
                    }
                }
            }
        }
        /// Debug.Log("----------------");
    }


    int curEnemyUnits;
    int curPlayerUnits;

    bool EmptyChunkForGen(int x, int y)
    {
        if (InMap(x, y))
        {
            return map[x][y].movebleObject.isEmpty() && (!map[x][y].obj.permanent);
        }
        else
        {
            return false;
        }
    }

    bool EmptySpaceForSquad(int a, int b, bool tf)
    {
        bool flag = true;
        if (tf)
        {
            for (int i = 0; i <= 1; i++)
            {
                for (int j = 0; j <= 2; j++)
                {
                    flag = flag && EmptyChunkForGen(a + i, b + j);
                }
            }
        }
        else
        {
            for (int i = 0; i <= 2; i++)
            {
                for (int j = 0; j <= 1; j++)
                {
                    flag = flag && EmptyChunkForGen(a + i, b + j);
                }
            }
        }
        return flag;
    }

    void GenerateEnemies()
    {
        enemyUnits = new Unit[maxEnemyUnits];
        int a = Random.Range(0, mapWidth);
        int b = Random.Range(0, mapHeight);
        bool rotation = true;
        if (Random.Range(0, 2) == 0)
        {
            rotation = false;
        }
        int step = 0;
        curEnemyUnits = maxEnemyUnits;
        while ((curEnemyUnits > 0) && (step < 50))
        {
            step = 0;
            while (!EmptySpaceForSquad(a, b, rotation) && (step < 50))
            {
                a = Random.Range(0, mapWidth);
                b = Random.Range(0, mapHeight);
                rotation = true;
                if (Random.Range(0, 2) == 0)
                {
                    rotation = false;
                }
                step++;
            }
            //Debug.Log(step);
            if (step < 50)
            {
                if (rotation)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        for (int j = 0; j <= 2; j++)
                        {
                            map[a + i][b + j].obj.Clear();
                        }
                    }
                }
                else
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        for (int j = 0; j <= 1; j++)
                        {
                            map[a + i][b + j].obj.Clear();
                        }
                    }
                }
                //Debug.Log(rotation);
                GenerateEnemySquad(a, b, rotation);
            }
            //curEnemyUnits--;
        }
    }

    void GeneratePlayers()
    {
        playerUnits = new Unit[maxPlayerUnits];
        int a = Random.Range(0, mapWidth);
        int b = Random.Range(0, mapHeight);
        bool rotation = true;
        if (Random.Range(0, 2) == 0)
        {
            rotation = false;
        }
        int step = 0;
        curPlayerUnits = maxEnemyUnits;
        while ((curPlayerUnits > 0) && (step < 50))
        {
            step = 0;
            while (!EmptySpaceForSquad(a, b, rotation) && (step < 50))
            {
                a = Random.Range(0, mapWidth);
                b = Random.Range(0, mapHeight);
                rotation = true;
                if (Random.Range(0, 2) == 0)
                {
                    rotation = false;
                }
                step++;
            }
            //Debug.Log(step);
            if (step < 50)
            {
                if (rotation)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        for (int j = 0; j <= 2; j++)
                        {
                            map[a + i][b + j].obj.Clear();
                        }
                    }
                }
                else
                {
                    for (int i = 0; i <= 2; i++)
                    {
                        for (int j = 0; j <= 1; j++)
                        {
                            map[a + i][b + j].obj.Clear();
                        }
                    }
                }
                //Debug.Log(rotation);
                GeneratePlayerSquad(a, b, rotation);
            }
            //curEnemyUnits--;
        }
    }

    void SecondaryGroundGeneration(int a, int b, int id)
    {
        int curMax = a / b;
        Queue<PointXY> que;
        que = new Queue<PointXY>();
        PointXY p;
        while (a > 6)
        {
            int localMax = curMax;
            que.Clear();
            p = (new PointXY((int)Random.Range(0, mapWidth), (int)Random.Range(0, mapHeight)));
            //Debug.Log(p.x + " " + (p.y));
            while (!Valid(p.x, p.y))
            {
                p = (new PointXY((int)Random.Range(0, mapWidth), (int)Random.Range(0, mapHeight)));
                //   Debug.Log(p.x + " " + (p.y));
            }
            que.Enqueue(p);
            int step = 0;
            //Debug.Log(p.x + " " + (p.y));
            while ((a > 0) && (localMax > 0) && (step < 50) && (que.Count != 0))
            {
                step++;
                p = que.Dequeue();
                if (map[p.x][p.y].landCoverId != id)
                {
                    //   Debug.Log(p.x + " " + p.y);
                    //Debug.Log(name);
                    map[p.x][p.y].SetLandCover(id);
                    //Destroy(this.gameObject.transform.root.gameObject);
                    //Debug.Log(name);
                    float rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x + 1, p.y) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x + 1, p.y));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x - 1, p.y) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x - 1, p.y));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x, p.y - 1) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x, p.y - 1));
                    }
                    rand2 = Random.Range(0f, 1f);
                    if (Valid(p.x, p.y + 1) && (rand2 < Random.Range(0.5f, 0.7f)))
                    {
                        que.Enqueue(new PointXY(p.x, p.y + 1));
                    }
                    a--;
                    localMax--;
                }
            }
        }
    }

    string[] baseGround = { "Grass", "Mud" };

    float coef = (Screen.height / Screen.width);
    float dc;

    void Awake()
    {
        n = 6;
        k = 8;
        dx = Screen.width / n;
        dy = Screen.height / k;
        if (dx < dy)
        {
            dy = dx;
        }
        if (dy < dx)
        {
            dx = dy;
        }
        // Debug.Log(dx);
        deltaX = Screen.width - 6 * dx;
        deltaX /= 2;
        dc = -dx / 2 * 3; ;
        /* if ((int)(Screen.height / 8) != (int)dx)
         {
             dc += dx * (Screen.height / 8) / dx;
         }

        //Screen.SetResolution((int)(6 * dx), (int)(8 * dx), false);
    }

    // Use this for initialization

    int language;

    void WallGenerator()
    {
        int start = Random.Range(0, 4);
        int finish = Random.Range(0, 4);
        PointXY pS = new PointXY(0, 0);
        PointXY vec = new PointXY(0, 0);
        if (start == 0)
        {
            pS.x = 0;
            pS.y = Random.Range(0, mapHeight);
            vec.x = 1;
        }
        if (start == 1)
        {
            pS.x = mapWidth - 1;
            pS.y = Random.Range(0, mapHeight);
            vec.x = -1;
        }
        if (start == 2)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = 0;
            vec.y = 1;
        }
        if (start == 3)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = mapHeight - 1;
            vec.y = -1;
        }
        int t = 0;
        // Debug.Log((pS.x + vec.x * t).ToString() + " " + (pS.y + vec.y * t).ToString());
        //Debug.Log((vec.x).ToString() + " " + (vec.y).ToString());
        bool flag = false;
        while (InMap(pS.x + vec.x * t, pS.y + vec.y * t))
        {
            if (map[pS.x + vec.x * t][pS.y + vec.y * t].landCoverId != 3)
            {
                //  chunks[pS.x + vec.x * t][pS.y + vec.y * t].GetComponent<SpriteActive>().setIdActive(map[pS.x + vec.x * t][pS.y + vec.y * t].obj.id, false);
                //map[pS.x + vec.x * t][pS.y + vec.y * t].obj.Clear();
                map[pS.x + vec.x * t][pS.y + vec.y * t].obj = new Object(3);
                //chunks[pS.x + vec.x * t][pS.y + vec.y * t].GetComponent<SpriteActive>().setIdActive(map[pS.x + vec.x * t][pS.y + vec.y * t].obj.id, true);
                //Debug.Log((pS.x + vec.x * t).ToString() + " " + (pS.y + vec.y * t).ToString());
            }
            else
            {
                flag = true;
            }
            ++t;
        }
        int cou = 0;
        int len = t;
        int step = 0;
        t = 0;
        while (InMap(pS.x + vec.x * t, pS.y + vec.y * t))
        {
            //Debug.Log(t);
            if (map[pS.x + vec.x * t][pS.y + vec.y * t].obj.id == 3)
            {

                bool l = false;
                if (InMap(pS.x + vec.x * t - 1, pS.y + vec.y * t))
                {
                    l = map[pS.x + vec.x * t - 1][pS.y + vec.y * t].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapWidth - 1)) && (vec.x != 0))
                    {
                        l = true;
                    }
                }
                bool r = false;
                if (InMap(pS.x + vec.x * t + 1, pS.y + vec.y * t))
                {
                    r = map[pS.x + vec.x * t + 1][pS.y + vec.y * t].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapWidth - 1)) && (vec.x != 0))
                    {
                        r = true;
                    }
                }
                bool b = false;
                if (InMap(pS.x + vec.x * t, pS.y + vec.y * t - 1))
                {
                    b = map[pS.x + vec.x * t][pS.y + vec.y * t - 1].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapHeight - 1)) && (vec.y != 0))
                    {
                        b = true;
                    }
                }
                bool to = false;
                if (InMap(pS.x + vec.x * t, pS.y + vec.y * t + 1))
                {
                    to = map[pS.x + vec.x * t][pS.y + vec.y * t + 1].obj.id == 3;
                }
                else
                {
                    if (((t == 0) || (t == mapHeight - 1)) && (vec.y != 0))
                    {
                        to = true;
                    }
                }
                cou++;
            }
            ++t;
        }
        t = 0;
        while ((!flag))
        {
            t = 0;
            //step++;
            while (InMap(pS.x + vec.x * t, pS.y + vec.y * t))
            {
                if (Random.Range(0f, 1f) < (2f / len))
                {
                    map[pS.x + vec.x * t][pS.y + vec.y * t].obj = new Object(0);
                    cou++;
                    flag = true;
                }
                ++t;
            }
            //   Debug.Log(flag);
        }
    }

    void GenNextEvo()
    {
        Debug.Log(base1[0] + " " + base1[1] + " " + base1[2] + " " + base1[3] + " " + base1[4] + " " + base1[5] + " " + base1[6] + " " + base1[7]);
        maxBase1 = -INF;
        maxBase2 = -INF;
        maxBase3 = -INF;
        for(int i = 0; i < 5; i++)
        {
            generationList[i] = new float[8];
            for(int j = 0; j < 8; j++)
            {
                generationList[i][j] = base1[j] + Random.Range(-range, range);
                //Debug.Log(base1[j] + " " + generationList[i][j]);
            }
        }
        for (int i = 5; i < 10; i++)
        {
            generationList[i] = new float[8];
            for (int j = 0; j < 8; j++)
            {
                generationList[i][j] = base2[j] + Random.Range(-range, range);
            }
        }
        for (int i = 10; i < 15; i++)
        {
            generationList[i] = new float[8];
            for (int j = 0; j < 8; j++)
            {
                generationList[i][j] = base3[j] + Random.Range(-range, range);
            }
        }
        range /= 2f;
        curBase = 0;
    }

    float[] base1;
    float[] base2;
    float[] base3;

    int curBase;
    int generations;
    float range;

    void Start()
    {
        generations = 0;
        generationList = new float[15][];
        range = 0.4f;
        curBase = 0;

        Application.runInBackground = true;
        winEvr = 0;
        winMachine = 0;
        GenerateBattle();
        base1 = baseList;
        base2 = baseList;
        base3 = baseList;

       // GenNextEvo();

    }

    bool EmptyChunksForGenPlayer(int a, int b, int x, int y)
    {
        bool ans = true;
        x += a;
        y += b;
        for (int i = a; i < x; i++)
        {
            for (int j = b; j < y; j++)
            {
                ans = ans && map[i][j].movebleObject.isEmpty() && (!map[i][j].obj.permanent);
            }
        }
        return ans;
    }

    void RoadGeneration()
    {
        int start = Random.Range(0, 4);
        int finish = Random.Range(0, 4);
        PointXY pS = new PointXY(0, 0);
        PointXY pF = new PointXY(0, 0);
        if (start == 0)
        {
            pS.x = 0;
            pS.y = Random.Range(0, mapHeight);
        }
        if (start == 1)
        {
            pS.x = mapWidth;
            pS.y = Random.Range(0, mapHeight);
        }
        if (start == 2)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = 0;
        }
        if (start == 3)
        {
            pS.x = Random.Range(0, mapWidth);
            pS.y = mapHeight;
        }


        if (finish == 0)
        {
            pF.x = 0;
            pF.y = Random.Range(0, mapHeight);
        }
        if (finish == 1)
        {
            pF.x = mapWidth;
            pF.y = Random.Range(0, mapHeight);
        }
        if (finish == 2)
        {
            pF.x = Random.Range(0, mapWidth);
            pF.y = 0;
        }
        if (finish == 3)
        {
            pF.x = Random.Range(0, mapWidth);
            pF.y = mapHeight;
        }

        while ((LengthVec(pS.x, pS.y, pF.x, pF.y) < 8) || (start == finish))
        {
            start = Random.Range(0, 4);
            finish = Random.Range(0, 4);
            pS = new PointXY(0, 0);
            pF = new PointXY(0, 0);
            if (start == 0)
            {
                pS.x = 0;
                pS.y = Random.Range(0, mapHeight - 1);
            }
            if (start == 1)
            {
                pS.x = mapWidth;
                pS.y = Random.Range(0, mapHeight - 1);
            }
            if (start == 2)
            {
                pS.x = Random.Range(0, mapWidth - 1);
                pS.y = 0;
            }
            if (start == 3)
            {
                pS.x = Random.Range(0, mapWidth - 1);
                pS.y = mapHeight;
            }


            if (finish == 0)
            {
                pF.x = 0;
                pF.y = Random.Range(0, mapHeight - 1);
            }
            if (finish == 1)
            {
                pF.x = mapWidth;
                pF.y = Random.Range(0, mapHeight - 1);
            }
            if (finish == 2)
            {
                pF.x = Random.Range(0, mapWidth - 1);
                pF.y = 0;
            }
            if (finish == 3)
            {
                pF.x = Random.Range(0, mapWidth - 1);
                pF.y = mapHeight;
            }
        }

        if (pS.x > pF.x)
        {
            PointXY ph = new PointXY(pF);
            pF = new PointXY(pS);
            pS = new PointXY(ph);
            //Debug.Log(2);
        }

        //Debug.Log(pS.x + " " + pS.y);
        // Debug.Log(pF.x + " " + pF.y);

        PointXY vec = new PointXY(pS, pF);
        vec.norm();
        vec.xf /= 10f;
        vec.yf /= 10f;
        int t = 0;
        //Debug.Log(1);
        while (pS.x + vec.xf * t <= pF.x)
        {
            for (int i = 0; i < mapWidth; i++)
            {
                for (int j = 0; j < mapHeight; j++)
                {
                    if (LengthVec(pS.x + vec.xf * t, pS.y + vec.yf * t, i, j) <= 0.8)
                    {
                        map[i][j].SetLandCover(3);
                    }
                }
            }
            t++;
        }
    }

    float LengthVec(float xf, float yf, int x, int y)
    {
        return Mathf.Sqrt((x - xf) * (x - xf) + (y - yf) * (y - yf));
    }

    int enemyUnitNow;
    int turns;

    void GenerateBattle()
    {
        turns = 0;
        playerUnitNow = 0;
        enemyUnitNow = 0;
        ev = new GameEvent(400, 0, 0);
        ev.Save("ChosenEvent");

        returnWait = false;
        extraUnit = new Unit();
        extraUnit.unit.id = -100;
        p = new PointXY(0, 0);
        mapHeight = ev.height;
        mapWidth = ev.width;
        enemyTurn = false;
        maxEnemyUnits = ev.enemyCounter;
        maxPlayerUnits = ev.enemyCounter;
        //Debug.Log((Screen.height / 8) / dx);
        int groundId = ev.baseGroundId;
        dist = new float[mapWidth][];
        costMatrix = new float[mapWidth][];
        timer = new float[mapWidth][];
        parents = new PointXY[mapWidth][];
        flags = new bool[mapWidth][];
        map = new Chunk[mapWidth][];
        //Debug.Log(1);
        for (int i = 0; i < mapWidth; i++)
        {
            map[i] = new Chunk[mapHeight];
            timer[i] = new float[mapHeight];
            dist[i] = new float[mapHeight];
            costMatrix[i] = new float[mapHeight];
            parents[i] = new PointXY[mapHeight];
            flags[i] = new bool[mapHeight];
            for (int j = 0; j < mapHeight; j++)
            {

                map[i][j] = new Chunk();
                map[i][j].SetLandCover(groundId);
                float shift = Screen.height / 8;
                if (Screen.height / 8 <= Screen.width / 6)
                {
                    shift = 0;
                }
            }
        }

        int objectNumber = (int)Random.Range(0.1f * mapHeight * mapWidth, 0.3f * mapWidth * mapHeight);

        int secondaryTypePoint = (int)Random.Range(0.1f * mapHeight * mapWidth, (0.35f * mapHeight * mapWidth));

        int s = Random.Range(1, baseGround.Length + 1);

        int step = 0;
        // Debug.Log(s);
        while (((s == groundId)) && (step < 50))
        {
            step++;
            //   Debug.Log(s);
            s = Random.Range(1, baseGround.Length + 1);
        }
        int secondaryGenerationsPoint = Random.Range(1, secondaryTypePoint / 6);


        SecondaryGroundGeneration(secondaryTypePoint, secondaryGenerationsPoint, s);
        if (Random.Range(0f, 1f) < 0.4f)
        {
            RoadGeneration();
            if (Random.Range(0f, 1f) < 0.2f)
            {
                RoadGeneration();
            }
            if (Random.Range(0f, 1f) < 0.4f)
            {
                WallGenerator();
            }
        }
        for (int i = 0; i < objectNumber; i++)
        {
            int a = Random.Range(0, mapWidth);
            int b = Random.Range(0, mapHeight);
            if (map[a][b].obj.isEmpty())
            {
                map[a][b].obj.Generate(Random.Range(1, 3));
            }
        }
        //Debug.Log(2);
        GeneratePlayers();
        //Debug.Log(3);
        GenerateEnemies();
        //Debug.Log(4);
        CheckInTheLine();
        RecalcPlayerTurn();
        //Debug.Log(5);
        // Debug.Log('-');
    }

    void EndGame(bool tf)
    {
        float reward = 0;
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            if (playerUnits[i].unit.isEmpty())
            {
                reward += 3 * playerUnits[i].unit.maxHits;
            }
            else
            {
                reward += playerUnits[i].unit.maxHits;
            }
        }
        for (int i = 0; i < maxEnemyUnits; i++)
        {
            if (playerUnits[i].unit.isEmpty())
            {
                reward -= 3 * enemyUnits[i].unit.maxHits;
            }
            else
            {
                reward -= enemyUnits[i].unit.maxHits;
            }
        }

        RecalcReward(reward);

        if (tf)
        {
            winEvr++;
        }
        else
        {
            winMachine++;
        }
        enemyTurn = false;
        if (Random.Range(0, 2) % 2 == 0)
        {
            enemyTurn = true;
        }
        //curBase++;
        //Debug.Log(un.unit.archTypeValues[0] + " " + un.unit.archTypeValues[1] + " " + un.unit.archTypeValues[2] + " " + un.unit.archTypeValues[3] + " " + un.unit.archTypeValues[4] + " " + un.unit.archTypeValues[5] + " " + un.unit.archTypeValues[6] + " " + un.unit.archTypeValues[7]);
        if (curBase >= 15)
        {
           // GenNextEvo();
        }
        GenerateBattle();
    }

    void RecalcReward(float val)
    {
        if (val > maxBase1)
        {
            maxBase3 = maxBase2;
            maxBase2 = maxBase1;
            maxBase1 = val;
            base3 = base2;
            base2 = base1;
         //   base1 = un.unit.archTypeValues;
            return;
        }
        if (val > maxBase2)
        {
            maxBase3 = maxBase2;
            maxBase2 = val;
            base3 = base2;
         //   base2 = un.unit.archTypeValues;
            return;
        }
        if (val > maxBase3)
        {
            maxBase3 = val;
           // base3 = un.unit.archTypeValues;
            return;
        }
    }

    float maxBase1;
    float maxBase2;
    float maxBase3;

    int winEvr;
    int winMachine;

    void RecalcPlayerTurn()
    {
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            // Debug.Log(playerUnits[i].unit.id);
            if (!playerUnits[i].unit.isEmpty())
            {
                // Debug.Log(playerUnits[i].unit.id);
                playerUnits[i].unit.calculateNewTurn();
            }
        }
        //Debug.Log(1);
    }

    float GetEstimatedDamageFromPoint(Unit un, PointXY pos)
    {
        //Debug.Log(GetTargetCoefs(un, pos));
        if (GetTargetCoefs(un, pos) != null)
        {
            PointXY a = new PointXY(GetTargetCoefs(un, pos));
            a.realC *= 0.01f;
            a.realH = Mathf.Max(a.realH, 100);
            a.realH *= 0.01f;
            // Debug.Log(a.realD + " " + a.realH + " " + a.realC);
            float ans = a.realD * (1 + a.realC) * a.realH;
            // Debug.Log(ans);
            return ans;
        }
        else
        {
            return 0;
        }
    }

    float GetEstimateValueInTheLine(Unit un, PointXY pos)
    {
        float cou = 0;
        if (un.unit.isEmpty())
        {
            for (int j = -1; j <= 1; j++)
            {
                for (int k = -1; k <= 1; k++)
                {
                    if (Mathf.Abs(j + k) == 1)
                    {
                        if (InMap(pos.x + j, pos.y + k) && (!map[pos.x + j][pos.y + k].movebleObject.isEmpty()) && (map[pos.x + j][pos.y + k].movebleObject.player == un.unit.player) && (map[pos.x + j][pos.y + k].movebleObject != un.unit))
                        {
                            cou += 1;
                        }
                    }
                }
            }
        }
        if (un.unit.inTheLinePerk)
        {
            cou = 2.5f * cou;
        }
        else
        {
            cou = 0;
        }
        return cou;
    }

    float GetEstimateDefenseFromPoint(Unit un, PointXY pos)
    {
        return un.unit.GetDefense(GetEstimateValueInTheLine(un, pos) * 0.01f) * un.unit.curHits;
    }

    float GetEstimateDamageTakenInNextTurn(Unit un, PointXY pos)
    {
        float ans = 0;
        ArrayList ar = new ArrayList();
        PointXY resultVec;
        if (!pos.isEqual(un.point))
        {
            ar = GetWay(pos);
            resultVec = new PointXY(((PointXY)ar[0]).x - ((PointXY)ar[1]).x, ((PointXY)ar[0]).y - ((PointXY)ar[1]).y);
        }
        else
        {
            resultVec = new PointXY(un.rotation);
        }
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            if (!playerUnits[i].unit.isEmpty())
            {
                PointXY at = new PointXY(pos.x - playerUnits[i].point.x, pos.y - playerUnits[i].point.y);
                PointXY estH = un.unit.EstimatedDamage(playerUnits[i].unit, resultVec, at);
                ans += estH.realD * (1 + estH.realC * 0.01f) * estH.realH * 0.01f;
            }
        }
        return ans;
    }

    float GetValueFromPoint(Unit un, PointXY pos)
    {
        float val = un.unit.archTypeValues[0] * GetEstimatedDamageFromPoint(un, pos) + un.unit.archTypeValues[1] * GetEstimateDefenseFromPoint(un, pos) + un.unit.archTypeValues[2] * GetEstimateDamageTakenInNextTurn(un, pos) - un.unit.archTypeValues[3] * Mathf.CeilToInt(dist[pos.x][pos.y] / GetUnit(playerPoint).unit.speed);
        if (Mathf.CeilToInt(dist[pos.x][pos.y] / GetUnit(playerPoint).unit.speed) == 1)
        {
            val += un.unit.archTypeValues[4];
        }
        //Debug.Log(val);
        return val;
    }

    PointXY GetBestPoint(Unit un)
    {
        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                costMatrix[i][j] = -INF;
                if (dist[i][j] != INF)
                {
                    costMatrix[i][j] = GetValueFromPoint(un, new PointXY(i, j));
                }
            }
        }

        for (int i = 0; i < maxPlayerUnits; i++)
        {
            if (!playerUnits[i].unit.isEmpty())
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (!((j == 0) && (k == 0)))
                        {
                            if (InMap(playerUnits[i].point.x + j, playerUnits[i].point.x + k))
                            {
                                //Debug.Log((playerUnits[i].point.x + j) + " " + (playerUnits[i].point.x + k));
                                //Debug.Log(dist[playerUnits[i].point.x + j][playerUnits[i].point.x + k]);
                                if (dist[playerUnits[i].point.x + j][playerUnits[i].point.x + k] != INF)
                                {
                                    ArrayList way = GetWay(new PointXY(playerUnits[i].point.x + j, playerUnits[i].point.x + k));
                                    PointXY es = new PointXY(playerUnits[i].unit.EstimatedDamage(un.unit, playerUnits[i].rotation, new PointXY(-j, -k)));
                                    float val = es.realD * (1 + es.realC * 0.01f) * es.realH * 0.01f;
                                    val /= way.Count;
                                    //Debug.Log(way.Count);
                                    for (int it = 0; it < way.Count - 1; it++)
                                    {
                                        //  Debug.Log(it);
                                        // Debug.Log(way[i]);
                                        // Debug.Log(((PointXY)way[it]).x + " " + ((PointXY)way[it]).y);
                                        costMatrix[((PointXY)way[it]).x][((PointXY)way[it]).y] += val * (way.Count - it) * un.unit.archTypeValues[7];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if (Mathf.CeilToInt(dist[i][j] / un.unit.speed) > (un.unit.curAP - 1))
                {
                    costMatrix[i][j] = -INF;
                }
            }
        }

        float max = -INF;
        PointXY maxP = new PointXY(0, 0);

        for (int i = 0; i < mapWidth; i++)
        {
            for (int j = 0; j < mapHeight; j++)
            {
                if (costMatrix[i][j] > max)
                {
                    maxP = new PointXY(i, j);
                    max = costMatrix[i][j];
                }
            }
        }
        if (max == -INF)
        {
            return null;
        }
        //Debug.Log(max);
        return maxP;
    }

    // Update is called once per frame

    void GoNearestPoint(Unit enemyUn, Unit targetUnit)
    {

        PointXY p = new PointXY(-1, -1);
        PointXY est, estHelp;
        est = new PointXY(-1, -1);
        int costMI = 10000;
        int needAp = enemyUn.unit.curAP - 1;
        bool tfMI = false;// ((Mathf.Abs(playerUnits[r].point.x - j) <= 1) && (Mathf.Abs(playerUnits[r].point.y - k)) <= 1)
        for (int j = 0; j < mapWidth; j++)
        {
            for (int k = 0; k < mapHeight; k++)
            {
                Unit target = GetTarget(enemyUn, new PointXY(j, k));
                if (target != null)
                {
                    estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn.unit, target.rotation, new PointXY(target.point.x - j, target.point.y - k)));
                    estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                    if ((map[j][k].movebleObject.isEmpty() || (map[j][k].movebleObject == enemyUn.unit)) && (map[j][k].obj.isEmpty()))
                    {
                        if (estHelp.realD > est.realD)
                        {
                            if (Mathf.CeilToInt(dist[j][k] / enemyUn.unit.speed) < enemyUn.unit.curAP)
                            {
                                tfMI = true;
                                p = new PointXY(j, k);
                                targetUnit = target;
                                costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.unit.speed);
                                est = new PointXY(estHelp);
                            }
                        }
                    }
                }
            }
        }

        if (tfMI)
        {
            //Debug.Log(p.x + " " + p.y);
            if (enemyUn.point.isEqual(p))
            {
                //PointXY a = new PointXY(GetTargetCoefs(enemyUn, p));
                //Debug.Log(a.realD + " " + a.realH + " " + a.realC);
                attacker = enemyUn;
                enemyUn.unit.doAction(3);
                targetPoint = new PointXY(targetUnit.point);
                block = true;
                targetUnit.unit.DealDamage(enemyUn.unit, targetUnit.rotation, new PointXY(targetUnit.point.x - enemyUn.point.x, targetUnit.point.y - enemyUn.point.y));
                //this.gameObject.GetComponent<UnitMove>().SetAttack(enemyUn, GetUnitGO(enemyUn.point), -enemyUn.point.x + targetUnit.point.x, -enemyUn.point.y + targetUnit.point.y, chunks[targetPoint.x][targetPoint.y]);
            }
            else
            {
                enemyUn.unit.doAction(2, costMI);
                block = true;
                GoTo(enemyUn.point, p);
            }
        }
        else
        {
            if (enemyUn.unit.curAP > 1)
            {
                est = new PointXY(-1, -1);
                int cost = 10000;
                bool tf = false;
                for (int j = 0; j < mapWidth; j++)
                {
                    for (int k = 0; k < mapHeight; k++)
                    {
                        Unit target = GetTarget(enemyUn, new PointXY(j, k));

                        // Debug.Log(target);
                        if (target != null)
                        {
                            estHelp = new PointXY(target.unit.EstimatedDamage(enemyUn.unit, target.rotation, new PointXY(target.point.x - j, target.point.y - k)));
                            estHelp.realD = Mathf.Min(estHelp.realD, target.unit.curHits);
                            if (map[j][k].movebleObject.isEmpty() && map[j][k].obj.isEmpty())
                            {
                                if (p.x < 0)
                                {
                                    tf = true;
                                    tfMI = true;
                                    p = new PointXY(j, k);
                                    targetUnit = target;
                                    costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.unit.speed);
                                    est = new PointXY(estHelp);
                                }
                                else
                                {
                                    if (dist[j][k] <= dist[p.x][p.y])
                                    {
                                        tf = true;
                                        tfMI = true;
                                        p = new PointXY(j, k);
                                        targetUnit = target;
                                        costMI = Mathf.CeilToInt(dist[j][k] / enemyUn.unit.speed);
                                        est = new PointXY(estHelp);
                                    }
                                }

                            }
                        }
                    }
                }
                // Debug.Log(p.x + " " + p.y);
                if (!tf || (p.x < 0) || (dist[p.x][p.y] == INF))
                {
                    enemyUn.unit.doAction(1);
                }
                else
                {
                    ArrayList longWay = GetWay(p);
                    int i = 0;
                    while (Mathf.CeilToInt(dist[((PointXY)longWay[i]).x][((PointXY)longWay[i]).y] / enemyUn.unit.speed) >= enemyUn.unit.curAP)
                    {
                        i++;
                    }
                    p = (PointXY)longWay[i];
                    cost = Mathf.CeilToInt(dist[p.x][p.y] / enemyUn.unit.speed);
                    enemyUn.unit.doAction(2, cost);
                    block = true;
                    GoTo(enemyUn.point, p);
                }
            }
            else
            {
                enemyUn.unit.doAction(1);
            }
        }
    }

    Unit GetTarget(Unit un, PointXY p)
    {
        Unit target = null;
        int minusDmg = -1;
        float hit = -1;
        bool kill = false;

        if (!un.unit.player)
        {
            for (int i = 0; i < maxPlayerUnits; i++)
            {
                if (!playerUnits[i].unit.isEmpty() && InRangeAttack(p, playerUnits[i].point, un.unit.damage.attackRange, un.unit))
                {
                    PointXY estDmg = new PointXY(playerUnits[i].unit.EstimatedDamage(un.unit, playerUnits[i].rotation, new PointXY(playerUnits[i].point.x - p.x, playerUnits[i].point.y - p.y)));
                    estDmg.realD = Mathf.Min(estDmg.realD, playerUnits[i].unit.curHits);
                    if (kill)
                    {
                        if (estDmg.realD == playerUnits[i].unit.curHits)
                        {
                            if (estDmg.realH > hit)
                            {
                                target = playerUnits[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;

                            }
                            else
                            {

                            }
                        }
                    }
                    else
                    {
                        if (estDmg.realD == playerUnits[i].unit.curHits)
                        {
                            if (estDmg.realH * estDmg.realH * un.unit.archTypeValues[6] > hit * minusDmg)
                            {
                                target = playerUnits[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = true;
                            }
                        }
                        else
                        {
                            if (estDmg.realD * estDmg.realH > minusDmg * hit)
                            {
                                target = playerUnits[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = false;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < maxEnemyUnits; i++)
            {
                if (!enemyUnits[i].unit.isEmpty() && InRangeAttack(p, enemyUnits[i].point, un.unit.damage.attackRange, un.unit))
                {
                    PointXY estDmg = new PointXY(enemyUnits[i].unit.EstimatedDamage(un.unit, enemyUnits[i].rotation, new PointXY(enemyUnits[i].point.x - p.x, enemyUnits[i].point.y - p.y)));
                    estDmg.realD = Mathf.Min(estDmg.realD, enemyUnits[i].unit.curHits);
                    if (kill)
                    {
                        if (estDmg.realD == enemyUnits[i].unit.curHits)
                        {
                            if (estDmg.realH > hit)
                            {
                                target = enemyUnits[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;

                            }
                            else
                            {

                            }
                        }
                    }
                    else
                    {
                        if (estDmg.realD == enemyUnits[i].unit.curHits)
                        {
                            if (estDmg.realH * estDmg.realH * un.unit.archTypeValues[6] > hit * minusDmg)
                            {
                                target = enemyUnits[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = true;
                            }
                        }
                        else
                        {
                            if (estDmg.realD * estDmg.realH > minusDmg * hit)
                            {
                                target = enemyUnits[i];
                                minusDmg = estDmg.realD;
                                hit = estDmg.realH;
                                kill = false;
                            }
                        }
                    }
                }
            }
        }
        if (minusDmg <= 0)
        {
            return null;
        }
        return target;
    }

    PointXY GetTargetCoefs(Unit un, PointXY p)
    {
        Unit target = null;
        int minusDmg = -1;
        float crit = -1;
        float hit = -1;
        bool kill = false;

        for (int i = 0; i < maxPlayerUnits; i++)
        {
            // Debug.Log(playerUnits[i].point.x + " " + playerUnits[i].point.y);
            if (!playerUnits[i].unit.isEmpty() && InRangeAttack(p, playerUnits[i].point, un.unit.damage.attackRange, un.unit))
            {
                PointXY estDmg = new PointXY(playerUnits[i].unit.EstimatedDamage(un.unit, playerUnits[i].rotation, new PointXY(playerUnits[i].point.x - p.x, playerUnits[i].point.y - p.y)));
                estDmg.realD = Mathf.Min(estDmg.realD, playerUnits[i].unit.curHits);
                if (kill)
                {
                    if (estDmg.realD == playerUnits[i].unit.curHits)
                    {
                        if (estDmg.realH > hit)
                        {
                            crit = estDmg.realC;
                            target = playerUnits[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;

                        }
                        else
                        {

                        }
                    }
                }
                else
                {
                    if (estDmg.realD == playerUnits[i].unit.curHits)
                    {
                        if (estDmg.realH * estDmg.realH * killCoef > hit * minusDmg)
                        {
                            target = playerUnits[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;
                            kill = true;
                            crit = estDmg.realC;
                        }
                    }
                    else
                    {
                        if (estDmg.realD * estDmg.realH > minusDmg * hit)
                        {
                            target = playerUnits[i];
                            minusDmg = estDmg.realD;
                            hit = estDmg.realH;
                            kill = false;
                            crit = estDmg.realC;
                        }
                    }
                }
            }
        }
        PointXY ans = new PointXY(0, 0);
        ans.realD = minusDmg;
        ans.realH = hit;
        ans.realC = crit;
        if (minusDmg <= 0)
        {
            return null;
        }
        return ans;
    }

    int killCoef = 3;

    void EnemyAI(Unit enemyUn)
    {
        Dijkstra(enemyUn.point);
        // Debug.Log(enemyUn);
        bool tf = false;
        Unit targetUnit = new Unit();
        if (enemyUn.unit.it.active && (enemyUn.unit.it.id == 2) && ((enemyUn.unit.maxHits - enemyUn.unit.curHits) * enemyUn.unit.archTypeValues[5] >= 3) && (enemyUn.unit.it.id != 0) && (enemyUn.unit.curAP >= enemyUn.unit.it.activationCost))
        {
            enemyUn.unit.ActivateItem();
        }
        else
        {
            GoNearestPoint(enemyUn, targetUnit);
        }

    }

    void MachineAI(Unit enemyUn)
    {
        Dijkstra(enemyUn.point);
        bool tf = false;
        Unit targetUnit = new Unit();
        if (enemyUn.unit.it.active && (enemyUn.unit.it.id == 2) && (enemyUn.unit.maxHits - enemyUn.unit.curHits >= 3) && (enemyUn.unit.it.id != 0) && (enemyUn.unit.curAP >= enemyUn.unit.it.activationCost))
        {
            enemyUn.unit.ActivateItem();
        }
        else
        {
            targetUnit = GetTarget(enemyUn, enemyUn.point);
            //Debug.Log(targetUnit);
            if (targetUnit == null)
            {
                if (enemyUn.unit.curAP == 1)
                {
                    enemyUn.unit.doAction(1);
                }
            }
            if (GetBestPoint(enemyUn) != null)
            {
                PointXY max = new PointXY(GetBestPoint(enemyUn));
                if (max.isEqual(enemyUn.point))
                {
                    //Debug.Log(1);
                    if (enemyUn.unit.curAP > 0)
                    {
                        targetUnit = GetTarget(enemyUn, enemyUn.point);
                        if (targetUnit != null)
                        {
                            enemyUn.unit.doAction(3);
                            block = true;
                            attacker = enemyUn;
                            targetPoint = new PointXY(targetUnit.point);
                            targetUnit.unit.DealDamage(enemyUn.unit, targetUnit.rotation, new PointXY(targetUnit.point.x - enemyUn.point.x, targetUnit.point.y - enemyUn.point.y));
                        }
                        else
                        {
                            enemyUn.unit.doAction(1);
                        }
                    }
                }
                else
                {
                    //Debug.Log(Mathf.CeilToInt(dist[max.x][max.y] / enemyUn.unit.speed));
                    enemyUn.unit.doAction(2, Mathf.CeilToInt(dist[max.x][max.y] / enemyUn.unit.speed));
                    GoTo(enemyUn.point, max);
                }
            }
            else
            {
                enemyUn.unit.doAction(1);
            }
        }
        // Debug.Log(enemyUn.unit.curAP);
    }

    float[][] generationList;
    float[] curList;
    float[] baseList = { 3.4f, 0.7f, 0.34f, 0.89f, 1.6f, 4.43f, 5.46f, 4.9f };

    void Update()
    {
        //Debug.Log(enemyTurn);
        if (turns > 300)
        {
            GenerateBattle();
        }
        turns++;
        int counter1 = 0;
        int counter2 = 0;
        
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            if (playerUnits[i].unit.isEmpty())
            {
                counter1++;
            }
        }
        // Debug.Log(maxEnemyUnits);
        for (int i = 0; i < maxEnemyUnits; i++)
        {
            //   Debug.Log("i " + i.ToString());
            if (enemyUnits[i].unit.isEmpty())
            {
                counter2++;
            }
        }
        if (counter1 == maxPlayerUnits)
        {
            Debug.Log(counter1 + " " + counter2 + " " + ev.enemyCounter);
            EndGame(false);
        }
        if (counter2 == maxEnemyUnits)
        {
            Debug.Log(counter1 + " " + counter2 + " " + ev.enemyCounter);
            EndGame(true);
        }
        //Debug.Log(counter1 + " " + counter2);
        if (((counter1 != maxPlayerUnits) && (counter2 != maxEnemyUnits)))
        {
            //Debug.Log(counter1 + " " + counter2);
            //Debug.Log(enemyTurn);
            if (enemyUnitNow == maxEnemyUnits)
            {
                enemyTurn = false;
                enemyUnitNow = 0;
                RecalcPlayerTurn();
            }
            if (maxPlayerUnits == playerUnitNow)
            {
                enemyTurn = true;
                playerUnitNow = 0;
                RecalcEnemyTurn();
            }
            if (enemyTurn)
            {
                int i = enemyUnitNow;
                //Debug.Log(i);
                if (!enemyUnits[i].unit.isEmpty())
                {
                    //EnemyAI(enemyUnits[i], enemyUnitsGO[i]);
                    MachineAI(enemyUnits[i]);
                }
                if (enemyUnits[enemyUnitNow].unit.isEmpty())
                {
                    enemyUnitNow++;
                }
                if (enemyUnitNow == maxEnemyUnits)
                {
                    // enemyTurn = false;
                    RecalcPlayerTurn();
                }
                else
                {
                    if (enemyUnits[enemyUnitNow].unit.curAP == 0)
                    {
                        enemyUnitNow++;
                    }
                    if (enemyUnitNow == maxEnemyUnits)
                    {
                        // enemyTurn = false;
                        RecalcPlayerTurn();
                    }
                }
            }
            else
            {
                int i = playerUnitNow;
                if (!playerUnits[i].unit.isEmpty())
                {
                    EnemyAI(playerUnits[i]);
                    //MachineAI(playerUnits[i]);
                }
                if (playerUnits[playerUnitNow].unit.isEmpty())
                {
                    playerUnitNow++;
                }
                if (playerUnitNow == maxPlayerUnits)
                {
                    enemyTurn = true;
                    //RecalcPlayerTurn();
                    RecalcEnemyTurn();
                }
                else
                {
                    if (playerUnits[playerUnitNow].unit.curAP == 0)
                    {
                        playerUnitNow++;
                    }
                    if (playerUnitNow == maxPlayerUnits)
                    {
                        enemyTurn = true;
                        RecalcEnemyTurn();
                        //RecalcPlayerTurn();
                    }
                }
            }

        }

    }

    int playerUnitNow;

    Unit extraUnit;

    Unit GetUnit(PointXY p)
    {
        bool tf = true;
        Unit un = extraUnit;
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            //Debug.Log(playerUnits[i].unit);
            if ((playerUnits[i].point.isEqual(p) && (!playerUnits[i].unit.isEmpty())))
            {
                un = playerUnits[i];
                tf = false;
                break;
            }
        }
        if (tf)
        {
            for (int i = 0; i < maxEnemyUnits; i++)
            {
                if ((enemyUnits[i].point.x == p.x) && (enemyUnits[i].point.y == p.y) && (enemyUnits[i].unit.id != 0))
                {
                    un = enemyUnits[i];
                    break;
                }
            }
        }
        return un;
    }

    ArrayList GetWay(PointXY p)
    {
        ArrayList newWay = new ArrayList();
        newWay.Add(p);
        int step = 0;
        //Debug.Log(dist[p.x][p.y] + " " + (dist[p.x][p.y] > 0));
        while (dist[p.x][p.y] > 0)
        {
            p = parents[p.x][p.y];
            newWay.Add(p);
            //Debug.Log(dist[p.x][p.y] + " " + (dist[p.x][p.y] > 0));
        }
        //if (step > 50)
        //{
        //  Debug.Log(dist[p.x][p.y]);
        //}
        return newWay;
    }

    bool block = false;

    void CheckInTheLine()
    {
        for (int i = 0; i < maxPlayerUnits; i++)
        {
            int cou = 0;
            //Debug.Log(i + " " + playerUnits[i]);
            if (!playerUnits[i].unit.isEmpty())
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (Mathf.Abs(j + k) == 1)
                        {
                            if (InMap(playerUnits[i].point.x + j, playerUnits[i].point.y + k) && (!map[playerUnits[i].point.x + j][playerUnits[i].point.y + k].movebleObject.isEmpty()) && (map[playerUnits[i].point.x + j][playerUnits[i].point.y + k].movebleObject.player))
                            {
                                cou++;
                            }
                        }
                    }
                }
                // Debug.Log(cou);
                playerUnits[i].unit.SetValueInTheLine(cou);
            }
        }
        for (int i = 0; i < maxEnemyUnits; i++)
        {
            int cou = 0;
            if (!enemyUnits[i].unit.isEmpty())
            {
                for (int j = -1; j <= 1; j++)
                {
                    for (int k = -1; k <= 1; k++)
                    {
                        if (!((j == 0) && (k == 0)))
                        {
                            // Debug.Log(InMap(enemyUnits[i].point.x + j, enemyUnits[i].point.y + k));// && (!map[playerUnits[i].point.x + j][playerUnits[i].point.y + k].movebleObject.isEmpty()) && (!map[enemyUnits[i].point.x + j][enemyUnits[i].point.y + k].movebleObject.player))
                            // );
                            if (InMap(enemyUnits[i].point.x + j, enemyUnits[i].point.y + k) && (!map[enemyUnits[i].point.x + j][enemyUnits[i].point.y + k].movebleObject.isEmpty()) && (!map[enemyUnits[i].point.x + j][enemyUnits[i].point.y + k].movebleObject.player))
                            {
                                cou++;
                            }
                        }
                    }
                }
                enemyUnits[i].unit.SetValueInTheLine(cou);
            }
        }
    }

    Unit attacker;

    void GoTo(PointXY p1, PointXY p2)
    {
        if (p1.isEqual(p2))
        {
            return;
        }
        //Unit playerUn = GetUnit(p1);
        way = GetWay(p2);
       // Debug.Log(way.Count);
        for(int i = way.Count - 1; i > 0; i--)
        {
            Swap((PointXY)way[i], (PointXY)way[i - 1]);
        }
    }

    public void Swap(PointXY p1, PointXY p2)
    {
        Unit playerUn = GetUnit(p1);
        Unit second = GetUnit(p2);
        second.point = new PointXY(p1);
        playerUn.point = new PointXY(p2);
        map[p1.x][p1.y].movebleObject = map[p2.x][p2.y].movebleObject;
        map[p2.x][p2.y].movebleObject = playerUn.unit;
        playerUn.SetRotation(new PointXY(p2.x - p1.x, p2.y - p1.y));
        CheckInTheLine();
    }

    void RecalcEnemyTurn()
    {
        for (int i = 0; i < maxEnemyUnits; i++)
        {
            // Debug.Log(playerUnits[i].unit.id);
            if (!enemyUnits[i].unit.isEmpty())
            {
                // Debug.Log(playerUnits[i].unit.id);
                enemyUnits[i].unit.calculateNewTurn();
            }
        }
    }

    float[][] timer;

    int butttonPressed = -1;

    public Texture2D backGroundTexture;

    public GUISkin skin;
    public Texture2D list;

    public int GetGround(PointXY p)
    {
        return map[p.x][p.y].landCoverId;
    }

    bool InRangeAttack(PointXY pAttack, PointXY pTarget, int rang, MovebleObject move)
    {
        if (map[pTarget.x][pTarget.y].obj.permanent)
        {
            return false;
        }
        if (rang == 1)
        {
            if ((Mathf.Abs(pAttack.x - pTarget.x) <= 1) && (Mathf.Abs(pAttack.y - pTarget.y) <= 1) && (Mathf.Abs(pAttack.x - pTarget.x) + Mathf.Abs(pAttack.y - pTarget.y) == 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if (rang == 2)
        {
            if ((Mathf.Abs(pAttack.x - pTarget.x) <= 1) && (Mathf.Abs(pAttack.y - pTarget.y) <= 1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        if ((rang == 3) || (rang == 4))
        {
            if ((Mathf.Abs(pAttack.x - pTarget.x) <= 1) && (Mathf.Abs(pAttack.y - pTarget.y) <= 1))
            {
                return true;
            }
            if (move.freeSpace)
            {
                //Debug.Log(1);
                if ((pAttack.x - pTarget.x == -2) && (pAttack.y - pTarget.y == 0))
                {
                    return map[pAttack.x + 1][pAttack.y].isSmallObj();
                }
                if ((pAttack.x - pTarget.x == 2) && (pAttack.y - pTarget.y == 0))
                {
                    return map[pAttack.x - 1][pAttack.y].isSmallObj();
                }
                if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 0))
                {
                    return map[pAttack.x][pAttack.y + 1].isSmallObj();
                }
                if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 0))
                {
                    return map[pAttack.x][pAttack.y - 1].isSmallObj();
                }
            }
            else
            {
                if ((pAttack.x - pTarget.x == -2) && (pAttack.y - pTarget.y == 0))
                {
                    return map[pAttack.x + 1][pAttack.y].isEmpty(move);
                }
                if ((pAttack.x - pTarget.x == 2) && (pAttack.y - pTarget.y == 0))
                {
                    return map[pAttack.x - 1][pAttack.y].isEmpty(move);
                }
                if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 0))
                {
                    return map[pAttack.x][pAttack.y + 1].isEmpty(move);
                }
                if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 0))
                {
                    return map[pAttack.x][pAttack.y - 1].isEmpty(move);
                }
            }
        }
        if (rang == 4)
        {
            if (move.freeSpace)
            {
                if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 1))
                {
                    return map[pAttack.x][pAttack.y - 1].isSmallObj() && map[pAttack.x - 1][pAttack.y - 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == -1))
                {
                    return map[pAttack.x][pAttack.y - 1].isSmallObj() && map[pAttack.x + 1][pAttack.y - 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 1))
                {
                    return map[pAttack.x][pAttack.y + 1].isSmallObj() && map[pAttack.x - 1][pAttack.y + 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == -1))
                {
                    return map[pAttack.x][pAttack.y + 1].isSmallObj() && map[pAttack.x + 1][pAttack.y + 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == 2))
                {
                    return map[pAttack.x - 1][pAttack.y].isSmallObj() && map[pAttack.x - 1][pAttack.y - 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == 2))
                {
                    return map[pAttack.x - 1][pAttack.y].isSmallObj() && map[pAttack.x - 1][pAttack.y + 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == -2))
                {
                    return map[pAttack.x + 1][pAttack.y].isSmallObj() && map[pAttack.x + 1][pAttack.y - 1].isSmallObj();
                }

                if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == -2))
                {
                    return map[pAttack.x + 1][pAttack.y].isSmallObj() && map[pAttack.x + 1][pAttack.y + 1].isSmallObj();
                }
            }
            else
            {
                {
                    if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == 1))
                    {
                        return map[pAttack.x][pAttack.y - 1].isEmpty(move) && map[pAttack.x - 1][pAttack.y - 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == 2) && (pAttack.x - pTarget.x == -1))
                    {
                        return map[pAttack.x][pAttack.y - 1].isEmpty(move) && map[pAttack.x + 1][pAttack.y - 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == 1))
                    {
                        return map[pAttack.x][pAttack.y + 1].isEmpty(move) && map[pAttack.x - 1][pAttack.y + 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == -2) && (pAttack.x - pTarget.x == -1))
                    {
                        return map[pAttack.x][pAttack.y + 1].isEmpty(move) && map[pAttack.x + 1][pAttack.y + 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == 2))
                    {
                        return map[pAttack.x - 1][pAttack.y].isEmpty(move) && map[pAttack.x - 1][pAttack.y - 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == 2))
                    {
                        return map[pAttack.x - 1][pAttack.y].isEmpty(move) && map[pAttack.x - 1][pAttack.y + 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == 1) && (pAttack.x - pTarget.x == -2))
                    {
                        return map[pAttack.x + 1][pAttack.y].isEmpty(move) && map[pAttack.x + 1][pAttack.y - 1].isEmpty(move);
                    }

                    if ((pAttack.y - pTarget.y == -1) && (pAttack.x - pTarget.x == -2))
                    {
                        return map[pAttack.x + 1][pAttack.y].isEmpty(move) && map[pAttack.x + 1][pAttack.y + 1].isEmpty(move);
                    }
                }
            }
        }
        return false;
    }

    Chunk playerChunk;
    PointXY playerPoint;
    PointXY attackPoint;

    public Texture2D longTapInd;

    PointXY targetPoint;

    PointXY p;
    bool pressed;

    float cameraTimer;
    float cameraSpeed;
    float startCameraMove;
    bool cameraMove;

    bool returnWait;

    PointXY lastPoint;

    PointXY estimatedDamage;


    void OnGUI()
    {
        if (winEvr + winMachine > 0)
        {
            GUI.Label(new Rect(0, 0, Screen.width / 2, Screen.height), winEvr.ToString() + " " + (winEvr *100f / (winEvr + winMachine)).ToString()+"%");
            GUI.Label(new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height), winMachine.ToString() + " " + (winMachine *100f / (winEvr + winMachine)).ToString() + "%");
        }
    }

}
*/
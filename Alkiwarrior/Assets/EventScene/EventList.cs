﻿using UnityEngine;
using System.Collections;
using System;

using UnityEngine.SceneManagement;

public class EventList : MonoBehaviour {
    /*


    int a;
    int b;
    float dx;
    int curCheckUnit;
    float time;
    bool toNext;
    public AudioSource au;
    float gentimer;

    public Texture2D chosen;

    string[] baseGround = { "Grass", "Mud" };

    ArrayList units;
    ArrayList eventList;
    bool[] onMission;
    int eventId;

    bool skillMenu;
    bool equipMenu;
    float verticalScrollBarValue;
    bool rightArm;
    int leftArm;
    bool armourShow;

    int height;
    int width;

    public GUIStyle scrollUp;
    public GUIStyle scrollDown;

    int demonicInfluence;
    float add;

    GameEvent ev;
    int eventCounter;

    void GenerateEvent()
    {
        CheckEndGame(1);
        CheckEndGame(a);
        GameEvent ev2 = new GameEvent(demonicInfluence, time, language);
        noEvents = false;
        eventList.Add(ev2);
        if (eventList.Count == 1)
        {
            ev = ev2;
        }
        Sort();
    }

    bool lose;

    int language;

    string[] endGameText = { "Demons took your world, resistance is futile", "Ваш мир захвачен, сопротивление бесполезно" };
    string[] almostEndGameText = { "Your world is doomed", "Ваш мир обречён"};
    string[] footHoldText = { "Demons have created a foothold in your world", "Демоны закрепились в вашем мире" };
    string[] massBandText = { "The massed band of demons invaded your world", "Большие группы демонов разгуливают по вашему миру" };
    string[] winText = { "You killed the Demon Lord and save the world, this time...! ", "Вы убили предводителя демонов и спасли мир, в этот раз..." };

    void CheckEndGame(int a)
    {
        // Debug.Log(demonicInfluence);
        if (PlayerPrefs.GetInt("Win") == 1)
        {
            PlayerPrefs.SetInt("Achivment20", 1);
            PlayerPrefs.SetInt("NewGame", 1);
            badNews = winText[language];
            toNext = false;
            lose = true;
        }
        else
        {
            if (demonicInfluence + a >= 1000)
            {
                badNews = endGameText[language];
                lose = true;
                PlayerPrefs.SetInt("NewGame", 1);
                toNext = false;
            }
            else
            {
                //Debug.Log(1);
                lose = false;
                if ((demonicInfluence < 900) && (demonicInfluence + a >= 900))
                {
                    badNews = almostEndGameText[language];
                    PlayerPrefs.SetInt("Doomed", PlayerPrefs.GetInt("Doomed") + 1);
                    GameEvent ev = new GameEvent(4, language, demonicInfluence, time);
                    ev.Save("DoomedEvent" + doomedCounter.ToString());
                    doomedCounter++;

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;

                    lose = true;
                    toNext = false;
                }
                if ((demonicInfluence < 700) && (demonicInfluence + a >= 700))
                {
                    badNews = footHoldText[language];
                    PlayerPrefs.SetInt("Foothold", PlayerPrefs.GetInt("Foothold") + 1);
                    GameEvent ev = new GameEvent(3, language, demonicInfluence, time);
                    ev.Save("FootholdEvent" + footholdCounter.ToString());
                    footholdCounter++;

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;

                    lose = true;
                    toNext = false;
                }
                if ((demonicInfluence < 500) && (demonicInfluence + a >= 500))
                {
                    badNews = massBandText[language];
                    PlayerPrefs.SetInt("MassBand", PlayerPrefs.GetInt("MassBand") + 1);
                    GameEvent ev = new GameEvent(2, language, demonicInfluence, time);

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;

                    ev.Save("MassBandEvent" + massBandCounter.ToString());
                    massBandCounter++;
                    lose = true;
                    toNext = false;
                }
            }
            demonicInfluence += a;
        }
    }

    bool generate;
    float generatorTime;

    bool noEvents;
    int gold;
    // Use this for initialization

    string[] goldText = { "Gold", "Золото" };

    int smallBandCounter;
    int massBandCounter;
    int footholdCounter;
    int doomedCounter;

    int unitsInBarraks;

    void LoadUnits()
    {
        int cou = 0;
        int c = unitsInBarraks;
        for (int i = 0; i < c; i++)
        {
            if (PlayerPrefs.GetInt("NewGameBattle") == 0)
            {
                //units[i] = new MovebleObject(1, true);
                //Debug.Log(PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()));
                if (PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()) == 1)
                {
                    PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
                    if (PlayerPrefs.GetInt("PlayerUnitOnMission" + cou.ToString()) == 1)
                    {
                        MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString(), language);
                        if (b.id != 0)
                        {
                            units.Add(b);
                          //   Debug.Log(b.name);
                        }
                    }
                    else
                    {
                        unitsInBarraks--;
                     //   Debug.Log(1);
                    }
                    PlayerPrefs.SetInt("PlayerUnitOnMission" + cou.ToString(), 1);
                    cou++;
                }
                else
                {
                //    MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString(), language);
                    if (b.id != 0)
                    {
                       // Debug.Log(b);
                        units.Add(b);
                        // Debug.Log(b.id);
                    }
                }
            }
            else
            {
                //PlayerPrefs.SetInt("DemonicInfluence", 400);
               // units.Add(new MovebleObject(1, language));
            }
        }
        //Debug.Log(unitsInBarraks);
        PlayerPrefs.SetInt("NewGameBattle", 0);
        fatiguedCou = 0;
        for (int i = 0; i < unitsInBarraks; i++)
        {
            //Debug.Log(1);
            ((MovebleObject)units[i]).RecalcFatigue(time);
            if (((MovebleObject)units[i]).fatigueFlag)
            {
                fatiguedCou++;
            }
        }
    }

    int fatiguedCou;

    void Start()
    {
        //  Debug.Log(eventList);
        au.volume *= PlayerPrefs.GetFloat("Volume");
        unitsInBarraks =  PlayerPrefs.GetInt("UnitsInBarraks");
        //Debug.Log(unitsInBarraks);
        shift = 0;
        units = new ArrayList();
        language = PlayerPrefs.GetInt("Language");
        lHelp = new LoadHelpObject(language);
        loadWindow = false;
       // Debug.Log(eventList);
        LoadUnits();
       // Debug.Log(eventList);

        toNext = false;
        int fontSize = 18;

        skin.button.fontSize = fontSize * Screen.height / 485;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        skin.label.fontSize = fontSize * Screen.height / 485;

        increasingDemonicInfluence = 0;

        increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 + 
            PlayerPrefs.GetInt("MassBand") * 5 +
            PlayerPrefs.GetInt("Foothold") * 10 +
            PlayerPrefs.GetInt("Doomed") * 15;

        gold = PlayerPrefs.GetInt("Gold");
        toNext = false;
        noEvents = true;
        //Debug.Log(eventList);
        gentimer = PlayerPrefs.GetFloat("GenTimer");
        eventList = new ArrayList();
        //Debug.Log(eventList);
        eventCounter = PlayerPrefs.GetInt("EventCounter");
        time = PlayerPrefs.GetFloat("Time");
        a = 0;
        int exeptionID = PlayerPrefs.GetInt("EventExeption");
        int cou = 0;
        //Debug.Log(exeptionID);
        for (int i = 0; i < eventCounter; i++)
        {
            if (i != exeptionID)
            {
                eventList.Add(new GameEvent("Event" + i.ToString(), language));
                ((GameEvent)eventList[i]).num = cou;
                cou++;
            }
            //  Debug.Log(((GameEvent)eventList[i]).baseGroundId);
        }
        PlayerPrefs.SetInt("EventExeption", -1);
        eventCounter = cou;
        demonicInfluence = PlayerPrefs.GetInt("DemonicInfluence");
        add = demonicInfluence;
        CheckEndGame(0);
        if (PlayerPrefs.GetInt("NewGame") == 1)
        {
            GenerateEvent();
        }
        else
        {
            if (eventList.Count != 0)
            {
                ev = (GameEvent)eventList[0];
                noEvents = false;
            }
            else
            {
                noEvents = true;
            }
        }
        dx = Screen.width / 6;
        PlayerPrefs.SetInt("NewGame", 0);
        goldValue = PlayerPrefs.GetFloat("CurExtraGold");
        lastGold = PlayerPrefs.GetFloat("LastGold");
    }

    MovebleObject recrut;

    void Save()
    {
        PlayerPrefs.SetFloat("LastGold", lastGold);
        PlayerPrefs.SetFloat("Time", time);
        PlayerPrefs.SetFloat("GenTimer", gentimer);
        PlayerPrefs.SetInt("EventCounter", eventList.Count);
        //Debug.Log(eventList.Count);
        for (int i = 0; i < eventList.Count; i++)
        {
            ((GameEvent)eventList[i]).Save("Event" + i.ToString());
            //Debug.Log(i);
        }
        PlayerPrefs.SetInt("DemonicInfluence", demonicInfluence);
        //Debug.Log(PlayerPrefs.GetInt("EventExeption"));

        PlayerPrefs.SetInt("UnitsInBarraks", unitsInBarraks);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
        }
    }

    int increasingDemonicInfluence;
    float goldValue;
    float lastGold;

    // Update is called once per frame
    void Update()
    {
        if (toNext)
        {
           // Debug.Log(goldValue);
            if (time - lastGold > goldValue)
            {
             //   Debug.Log(time);
                lastGold = time;
                gold++;
                PlayerPrefs.SetInt("Gold", gold);
            }
            time += Time.deltaTime;
            fatiguedCou = 0;
            for (int i = 0; i < unitsInBarraks; i++)
            {
                ((MovebleObject)units[i]).RecalcFatigue(time);
                if (((MovebleObject)units[i]).fatigueFlag)
                {
                    fatiguedCou++;
                }
            }
            int k = Mathf.FloorToInt(add);
            add += Time.deltaTime * increasingDemonicInfluence;
            k = Mathf.FloorToInt(add) - k;
            CheckEndGame(k);
            if (time >= gentimer)
            {
                toNext = false;
                GenerateEvent();
                gentimer = UnityEngine.Random.Range(2f, 5f) + time;
            }
        }

        for(int i = 0; i < eventList.Count; i++)
        {
            if (((GameEvent)eventList[i]).endTime <= time)
            {
                //Debug.Log(time);
                if (UnityEngine.Random.Range(0f, 1000f) < demonicInfluence)
                {
                    PlayerPrefs.SetInt("SmallBand", PlayerPrefs.GetInt("SmallBand") + 1);
                    GameEvent ev = new GameEvent(1, language, demonicInfluence, time);

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;

                    ev.Save("SmallBandEvent" + smallBandCounter.ToString());
                    smallBandCounter++;
                }
                CheckEndGame(((GameEvent)eventList[i]).enemyCounter);
                eventList.Remove(eventList[i]);
                i--;
            }
        }
        if (Input.GetKeyUp(KeyCode.Menu))
        {
            showMenu = !showMenu;
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("MainMenuScene");
        }
    }

    public Texture2D okTexture;

    string baseGroundId;

    public Texture2D backGroundTexture;

    public GUISkin skin;

    public Texture2D dark;

    string[] emptyEventListText = {"Empty event list", "Не выбрано событие"};
    string[] enemyUnitsText = { "Enemies:", "Враги:" };
    string[] mapSizeText = {"Map:", "Размер:" };
    string[] baseGroundCoverText = {"Ground:", "Ландшафт:" };

    void StatsWindow3(int WindowID)
    {
       // Debug.Log(ev.baseGroundId - 1);
        GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 3 * 2), enemyUnitsText[language] + " " + ev.enemyCounter.ToString() + '\n' +mapSizeText[language] +  " " + ev.width.ToString() + "*" + ev.height.ToString() + '\n'
            + baseGroundCoverText[language] + " " + ev.baseGround[ev.baseGroundId - 1]);
        if (showMenu)
        {
            // GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }
    void StatsWindow4(int WindowID)
    {
        GUI.Box(new Rect(0, 0, Screen.width / 2, Screen.height / 3 * 2), "Empty event list");
        if (showMenu)
        {
            // GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
        }
    }

    string[] progressText = {"Progress", "Прогресс" };
    string[] eventsText = {"Events", "События" };
    string[] barraksText = {"Barraks", "Казармы" };

    void MenuWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, 80, Screen.width / 2, Screen.height / 8), progressText[language]))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("ProgressScene");
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8, Screen.width / 2, Screen.height / 8), eventsText[language]))
        {
            Save();
            showMenu = false;
        }
        if (GUI.Button(new Rect(0, 80 + Screen.height / 8 * 2, Screen.width / 2, Screen.height / 8), barraksText[language]))
        {
            Save();
            PlayerPrefs.SetInt("ToBattle", 0);
            loadWindow = true;
            SceneManager.LoadSceneAsync("BarraksScene");
            showMenu = false;
        }
      //  Debug.Log(Input.mousePosition);
       // if (new Rect(0, 80 + Screen.height / 8 * 2, Screen.width / 2, Screen.height / 8).Contains(Input.mousePosition))
        //{
         //   GUI.DrawTexture(new Rect(0, 80 + Screen.height / 8 * 2, Screen.width / 2, Screen.height / 8), chosen);
        //}
    }

    string badNews;

    void LoseWindow(int WindowID)
    {
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height / 3 * 2), badNews);
        if (GUI.Button(new Rect(20, Screen.height / 2, Screen.width - 40, Screen.height / 8), "ОК"))
        {
            Save();
            lose = false;
            if ((demonicInfluence >= 1000) || (PlayerPrefs.GetInt("Win") == 1))
            {
                PlayerPrefs.SetInt("NewGame", 1);
                loadWindow = true;
                SceneManager.LoadSceneAsync("MainMenuScene");
            }
        }
    }

    void DemonicWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30), "", freeButtonStyle))
        {
            Save();
            loadWindow = true;
            SceneManager.LoadSceneAsync("ProgressScene");
        }
        if (hoverStart)
        {
            GUI.DrawTexture(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30), chosen);
        }
    //    Debug.Log(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30).Contains(Input.mousePosition));
    //    if(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30).Contains(Input.mousePosition))
     //   {
      //      GUI.DrawTexture(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30), chosen);
       // }
        GUI.Box(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30), demonicInfluenceText[language] + " " + demonicInfluence.ToString());
    }

    void ResoursesWindow(int WindowID)
    {
        GUI.Box(new Rect(0, Screen.height / 30, Screen.width / 2, Screen.height / 4 - Screen.height / 30), goldText[language] + " " + gold.ToString());
    }

    void BarraksWindow(int WindowID)
    {
        if (GUI.Button(new Rect(0, Screen.height / 30, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6 - Screen.height / 30), "", freeButtonStyle))
        {
            Save();
            PlayerPrefs.SetInt("ChosenEquipUnit", 0);
            PlayerPrefs.SetInt("ToBattle", 0);
            loadWindow = true;
            SceneManager.LoadSceneAsync("BarraksScene");
        }
        if (hoverBarraks)
        {
            GUI.DrawTexture(new Rect(0, Screen.height / 30, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6 - Screen.height / 30), chosen);
        }
        GUI.Box(new Rect(0, Screen.height / 30, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6 - Screen.height / 30), unitInBarraks[language] + " " + unitsInBarraks + '\n' + fatiguedText[language] + fatiguedCou);
    }

    void MissionDescriptionWindow(int WindowID)
    {
        if (ev != null)
        {
          //  Debug.Log(ev.desc);
            GUI.Box(new Rect(0, Screen.height / 30, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6 - Screen.height / 30), ev.desc);
        }
    }

    string[] descMissionText = { "Mission description", "Описание миссии" };


    string[] fatiguedText = { "Fatigued units: ", "Солдат на отдыхе: " };
    string[] unitInBarraks = { "Units in barraks:", "Солдат в казармах:" };

    int shift;

    int playerUnitsCounter;

    void EventsWindow(int WindowID)
    {
        GUI.Box(new Rect(0, Screen.height / 30, Screen.width / 4, Screen.height / 8 - Screen.height / 30), timeText[language] + " " + Math.Round(time, 2).ToString());
        if (!noEvents)
        {
            if (GUI.Button(new Rect(0, Screen.height / 8 * 3 + Screen.height / 9 * 2 + Screen.height / 30, Screen.width / 2, Screen.height / 9), fightText[language]))
            {
                PlayerPrefs.SetInt("EventExeption", ev.num);
                ev.Save("ChosenEvent");
                PlayerPrefs.SetInt("SpecialID", 0);
                Save();
                PlayerPrefs.SetInt("ToBattle", 0);
                PlayerPrefs.SetInt("BackToBattle", 1);
                loadWindow = true;
                SceneManager.LoadSceneAsync("PlaningScene");
            }
        }
        string st = stopSearchText[language];
        if (!toNext)
        {
            st = startSearchText[language];
        }
        if (GUI.Button(new Rect(Screen.width / 4, Screen.height / 30, Screen.width / 4, Screen.height / 8 - Screen.height / 30), st))
        {
            toNext = !toNext;
        }
        if (GUI.Button(new Rect(Screen.width / 4 + Screen.width / 8 - Screen.height / 18, Screen.height / 8 * 3 + Screen.height / 9 + Screen.height / 30, Screen.height / 9, Screen.height / 9), "", scrollDown))
        {
            shift++;
            shift = Mathf.Min(shift, eventList.Count);
        }
        if (GUI.Button(new Rect(0 + Screen.width / 8 - Screen.height / 18, Screen.height / 8 * 3 + Screen.height / 9 + Screen.height / 30, Screen.height / 9, Screen.height / 9), "", scrollUp))
        {
            shift--;
            shift = Mathf.Max(shift, 0);
        }
        //Debug.Log(ev.num);
        for (int i = shift; i < Mathf.Min(eventList.Count, shift + 3); i++)
        {
            if (GUI.Button(new Rect(0, Screen.height / 8 * (i - shift + 1), Screen.width / 2, Screen.height / 8), ""))
            {
                ev = (GameEvent)eventList[i];
                noEvents = false;
            }
            GUI.Box(new Rect(0, Screen.height / 8 * (i - shift + 1), Screen.width / 2, Screen.height / 8), enemyUnitsText[language] + " " + ((GameEvent)eventList[i]).enemyCounter.ToString() + "       " + mapSizeText[language] + " " + ((GameEvent)eventList[i]).width.ToString() + "*" + ((GameEvent)eventList[i]).height.ToString() + '\n' + ((GameEvent)eventList[i]).endTime.ToString() + "     " + baseGroundCoverText[language] + " " + ((GameEvent)eventList[i]).baseGround[((GameEvent)eventList[i]).baseGroundId - 1]);
            if (i == ev.num)
            {
                GUI.DrawTexture(new Rect(0, Screen.height / 8 * (i - shift + 1), Screen.width / 2, Screen.height / 8), chosen, ScaleMode.StretchToFill);
            }
            //GUI.Box(new Rect(Screen.width / 4, Screen.height / 8 * (i - shift + 1), Screen.width / 4, Screen.height / 8), ((GameEvent)eventList[i]).endTime.ToString() + '\n' + baseGroundCoverText[language] + " " + ev.baseGround[ev.baseGroundId - 1]);
        }
    }

    bool squadStatus;

    bool showMenu;
    
    void Sort()
    {
        for(int i = 0; i < eventList.Count; i++)
        {
            for(int j = 0; j < eventList.Count - 1; j++)
            {
                if (((GameEvent)eventList[j]).endTime > ((GameEvent)eventList[j + 1]).endTime)
                {
                    GameEvent buf = ((GameEvent)eventList[j]);
                    eventList[j] = eventList[j + 1];
                    eventList[j + 1] = buf;
                }
            }
        }
        for(int i = 0; i < eventList.Count; i++)
        {
            ((GameEvent)eventList[i]).num = i;
        }
    }


    string[] newMessageText = {"New message", "Новое событие" };
    string[] missionBriffingText = {"Mission briefing", "Описание миссии" };
    string[] fightText = {"Fight", "В бой!" };
    string[] eventsDescText = { "Events", "События" };

    bool loadWindow;
    LoadHelpObject lHelp;

    string[] loadText = { "Loading", "Загрузка" };

    void LoadWindow(int WindowID)
    {
        int fontSize = 25;
        skin.box.fontSize = fontSize * Screen.height / 485;
        skin.window.fontSize = fontSize * Screen.height / 485;
        GUI.Box(new Rect(0, 0, Screen.width, Screen.height), lHelp.text);
    }

    void OnGUI()
    {
        GUI.skin = skin;
        //Debug.Log(time);
        if (loadWindow)
        {
            GUI.Window(1, new Rect(0, 0, Screen.width, Screen.height), LoadWindow, loadText[language]);
        }
        else
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), backGroundTexture, ScaleMode.ScaleAndCrop);
            if (lose)
            {
                GUI.Window(6, new Rect(0, 0, Screen.width, Screen.height), LoseWindow, newMessageText[language]);
            }
            else
            {
                hoverStart = false;
                hoverBarraks = false;
                Vector2 newPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
                if (new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height / 4).Contains(newPos))
                {
                    hoverStart = true;
                }
                if (new Rect(0, Screen.height / 4, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6).Contains(newPos))
                {
                    hoverBarraks = true;
                }
                GUI.Window(4, new Rect(Screen.width / 2, Screen.height / 4, Screen.width / 2, Screen.height / 4 * 3), EventsWindow, eventsDescText[language]);
                GUI.Window(5, new Rect(0, 0, Screen.width / 2, Screen.height / 4), ResoursesWindow, resoursesText[language]);
                GUI.Window(6, new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height / 4), DemonicWindow, demonicInfluenceText[language]);
                GUI.Window(7, new Rect(0, Screen.height / 4, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6), BarraksWindow, barraksText[language]);
                GUI.Window(8, new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 4, Screen.height / 4 * 3 - Screen.height / 6), MissionDescriptionWindow, descMissionText[language]);

                if (GUI.Button(new Rect(0, Screen.height / 6 * 5, Screen.width / 2, Screen.height / 6), menuText[language]))
                {
                    showMenu = !showMenu;
                    Save();
                    loadWindow = true;
                    SceneManager.LoadSceneAsync("MainMenuScene");
                }
                // if (showMenu)
                {
                    //GUI.Window(7, new Rect(0, 0, Screen.width / 2, Screen.height), MenuWindow, menuText[language]);
                    //     GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), dark, ScaleMode.ScaleAndCrop);
                }
            }
        }
    }

    bool hoverStart;
    bool hoverBarraks;

    string[] stopSearchText = { "Stop searching", "Прекратить поиск" };
    string[] startSearchText = { "Start earching", "Начать поиск" };
    string[] timeText = { "Time", "Время" };
    string[] menuText = { "Menu", "Меню" };
    string[] demonicInfluenceText = { "Demonic influence", "Дем. влияние" };
    string[] resoursesText = { "Resourses", "Ресурсы" };

    public GUIStyle freeButtonStyle;*/
}

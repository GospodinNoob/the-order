﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.IO;
using UnityEngine.UI;

public class BaseWindowUI : MonoBehaviour {

    public GameObject missionBtnPrefab;



    int a;
    float time;
    bool toNext;
    public AudioSource au;
    float gentimer;

    List<Button> missions = new List<Button>();

    ArrayList units;
    ArrayList eventList;
    bool[] onMission;
    int eventId;



    int demonicInfluence;
    float add;

    GameEvent ev;
    int eventCounter;

    Button Settings;
    Button BackToGame;
    Button ToMainMenu;
    Button CloseGame;
    GameObject MenuWindow;
    GameObject SettingsWindow;

    void SettingsClick()
    {
        SettingsWindow.SetActive(true);
    }

    void BackToGameClick()
    {
        MenuWindow.SetActive(false);
        SetLang();
    }

    void CloseGameWindowClick()
    {
        Save();
        Application.Quit();
    }

    void ToMainMenuClick()
    {
        Save();
        SceneManager.LoadSceneAsync("MainMenuScene");
    }

    void GenerateEvent()
    {
        CheckEndGame(1);
        CheckEndGame(a);
        GameEvent ev2 = new GameEvent(demonicInfluence, time, language);
        noEvents = false;
        eventList.Add(ev2);
        if (eventList.Count == 1)
        {
            ev = ev2;
        }
        Sort();
        RedrawMissions();
    }

    void GenerateEvent(int id)
    {
        GameEvent ev2 = new GameEvent(id, language, demonicInfluence, time);
        noEvents = false;
        eventList.Add(ev2);
        if (eventList.Count == 1)
        {
            ev = ev2;
        }
        Sort();
        RedrawMissions();
    }

    bool lose;

    int language;

    string[] endGameText = { "Demons took your world, resistance is futile", "Ваш мир захвачен, сопротивление бесполезно" };
    string[] almostEndGameText = { "Your world is doomed", "Ваш мир обречён" };
    string[] footHoldText = { "Demons have created a foothold in your world", "Демоны закрепились в вашем мире" };
    string[] massBandText = { "The massed band of demons invaded your world", "Большие группы демонов разгуливают по вашему миру" };
    string[] winText = { "You killed the Demon Lord and save the world, this time...! ", "Вы убили предводителя демонов и спасли мир, в этот раз..." };

    void CheckEndGame(int a)
    {
        // Debug.Log(demonicInfluence);
        if(a > 0)
        {
            UpdateInfl();
        }
        if (PlayerPrefs.GetInt("Win") == 1)
        {
            //PlayerPrefs.SetInt("Achivment20", 1);
            PlayerPrefs.SetInt("NewGame", 1);
            badNews = winText[language];
            StopTimeClick();
            RedrawBadNews();
        }
        else
        {
            if (demonicInfluence + a >= 1000)
            {
                badNews = endGameText[language];
                PlayerPrefs.SetInt("NewGame", 1);
                StopTimeClick();
                RedrawBadNews();
            }
            else
            {
                //Debug.Log(1);
                lose = false;
                if ((demonicInfluence < 900) && (demonicInfluence + a >= 900))
                {
                    badNews = almostEndGameText[language];
                    PlayerPrefs.SetInt("Doomed", PlayerPrefs.GetInt("Doomed") + 1);
                    GameEvent ev = new GameEvent(4, language, demonicInfluence, time);
                    ev.Save("DoomedEvent" + doomedCounter.ToString());
                    doomedCounter++;

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;
                    GenerateEvent(1);
                    UpdateInfl();
                    StopTimeClick();
                    RedrawBadNews();
                }
                if ((demonicInfluence < 700) && (demonicInfluence + a >= 700))
                {
                    badNews = footHoldText[language];
                    PlayerPrefs.SetInt("Foothold", PlayerPrefs.GetInt("Foothold") + 1);
                    GameEvent ev = new GameEvent(3, language, demonicInfluence, time);
                    ev.Save("FootholdEvent" + footholdCounter.ToString());
                    footholdCounter++;

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;
                    GenerateEvent(3);
                    UpdateInfl();
                    StopTimeClick();
                    RedrawBadNews();
                }
                if ((demonicInfluence < 500) && (demonicInfluence + a >= 500))
                {
                    badNews = massBandText[language];
                    PlayerPrefs.SetInt("MassBand", PlayerPrefs.GetInt("MassBand") + 1);
                    GameEvent ev = new GameEvent(2, language, demonicInfluence, time);

                    increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                        PlayerPrefs.GetInt("MassBand") * 5 +
                        PlayerPrefs.GetInt("Foothold") * 10 +
                        PlayerPrefs.GetInt("Doomed") * 15;
                    UpdateInfl();
                    ev.Save("MassBandEvent" + massBandCounter.ToString());
                    massBandCounter++;
                    GenerateEvent(2);
                    StopTimeClick();
                    RedrawBadNews();
                }
            }
            demonicInfluence += a;
        }
    }

    void RedrawBadNews()
    {
        EventWindow.SetActive(true);
        EventMainText.text = badNews;
    }

    bool generate;
    float generatorTime;

    bool noEvents;
    int gold;
    // Use this for initialization

    int smallBandCounter;
    int massBandCounter;
    int footholdCounter;
    int doomedCounter;

    int unitsInBarraks;

    void LoadUnits()
    {
        int cou = 0;
        int c = unitsInBarraks;
        for(int i = 0; i < c; i++)
        {
            units.Add(new MovebleObject("PlayerUnitOnBase" + i.ToString()));
        }
        /*for (int i = 0; i < c; i++)
        {
            if (PlayerPrefs.GetInt("NewGameBattle") == 0)
            {
                //units[i] = new MovebleObject(1, true);
                Debug.Log(PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()));
                if (PlayerPrefs.GetInt("PlayerUnitBool" + i.ToString()) == 1)
                {
                    PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
                    if (PlayerPrefs.GetInt("PlayerUnitOnMission" + cou.ToString()) == 1)
                    {
                        MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString());
                        if (b.isAlive())
                        {
                            units.Add(b);
                               Debug.Log(b.name);
                        }
                    }
                    else
                    {
                        unitsInBarraks--;
                        //   Debug.Log(1);
                    }
                    PlayerPrefs.SetInt("PlayerUnitOnMission" + cou.ToString(), 1);
                    cou++;
                }
                else
                {
                    MovebleObject b = new MovebleObject("PlayerUnitOnBase" + i.ToString());
                    if (b.id != 0)
                    {
                        // Debug.Log(b);
                        units.Add(b);
                        // Debug.Log(b.id);
                    }
                }
            }
            else
            {
                //PlayerPrefs.SetInt("DemonicInfluence", 400);
                //units.Add(new MovebleObject(1, language));
            }
        }*/
        Debug.Log(unitsInBarraks);
        PlayerPrefs.SetInt("NewGameBattle", 0);
        fatiguedCou = 0;
        for (int i = 0; i < unitsInBarraks; i++)
        {
            //Debug.Log(1);
            ((MovebleObject)units[i]).RecalcFatigue(time);
            if (((MovebleObject)units[i]).fatigueFlag)
            {
                fatiguedCou++;
            }
        }
    }

    int fatiguedCou;

    GameObject EventWindow;
    Text EventLablel;
    Text EventMainText;
    Button EventOkButton;

    void EventOkClick()
    {
        EventWindow.SetActive(false);
        Save();
        if ((demonicInfluence >= 1000) || (PlayerPrefs.GetInt("Win") == 1))
        {
            PlayerPrefs.SetInt("NewGame", 1);
            InitLoadWindow();
            SceneManager.LoadSceneAsync("MainMenuScene");
        }
    }

    void Start()
    {
        //  Debug.Log(eventList);


        au.volume *= PlayerPrefs.GetFloat("Volume");
        unitsInBarraks = PlayerPrefs.GetInt("UnitsInBarraks");
        units = new ArrayList();
        LoadUnits();
        toNext = false;
        increasingDemonicInfluence = 0;
        language = PlayerPrefs.GetInt("Language");
        increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
            PlayerPrefs.GetInt("MassBand") * 5 +
            PlayerPrefs.GetInt("Foothold") * 10 +
            PlayerPrefs.GetInt("Doomed") * 15;
        gold = PlayerPrefs.GetInt("Gold");
        gentimer = PlayerPrefs.GetFloat("GenTimer");
        eventList = new ArrayList();
        eventCounter = PlayerPrefs.GetInt("EventCounter");
        time = PlayerPrefs.GetFloat("Time");
        a = 0;
        int exeptionID = PlayerPrefs.GetInt("EventExeption");
        int cou = 0;
        for (int i = 0; i < eventCounter; i++)
        {
            if (i != exeptionID)
            {
                eventList.Add(new GameEvent("Event" + i.ToString(), language));
                ((GameEvent)eventList[i]).num = cou;
                cou++;
            }
            //  Debug.Log(((GameEvent)eventList[i]).baseGroundId);
        }
        PlayerPrefs.SetInt("EventExeption", -1);
        eventCounter = cou;
        demonicInfluence = PlayerPrefs.GetInt("DemonicInfluence");
        add = demonicInfluence;
        goldValue = PlayerPrefs.GetFloat("CurExtraGold");
        lastGold = PlayerPrefs.GetFloat("LastGold");

        stopTimeBtn = GameObject.Find("StopSearching").GetComponent<Button>();
        stopTimeBtn.onClick.AddListener(StopTimeClick);
        startTimeBtn = GameObject.Find("StartSearch").GetComponent<Button>();
        startTimeBtn.onClick.AddListener(StartTimeClick);
        toBarraks = GameObject.Find("BarraksBtn").GetComponent<Button>();
        toBarraks.onClick.AddListener(ToBarraksClick);
        toSpecial = GameObject.Find("SpecialMissions").GetComponent<Button>();
        toSpecial.onClick.AddListener(ToSpecialMissionsClick);
        loadWindow = GameObject.Find("LoadingWindow");
        loadWindow.SetActive(false);
        goldText = GameObject.Find("ResContent").GetComponent<TextLocalaze>();
        timeText = GameObject.Find("Time").GetComponent<TextLocalaze>();
        inflText = GameObject.Find("InfContent").GetComponent<TextLocalaze>();
        //Debug.Log(inflText + "*");
        toConstruction = GameObject.Find("Fight").GetComponent<Button>();
        toConstruction.onClick.AddListener(ToSquadClick);
        EventMainText = GameObject.Find("MainEventText").GetComponent<Text>();
        EventLablel = GameObject.Find("LabelEvent").GetComponent<Text>();
        EventWindow = GameObject.Find("EventWindow");
        EventOkButton = GameObject.Find("EventOkButton").GetComponent<Button>();
        EventOkButton.onClick.AddListener(EventOkClick);
        EventWindow.SetActive(false);

        Settings = GameObject.Find("Settings").GetComponent<Button>();
        Settings.onClick.AddListener(SettingsClick);
        BackToGame = GameObject.Find("BackToGame").GetComponent<Button>();
        BackToGame.onClick.AddListener(BackToGameClick);
        ToMainMenu = GameObject.Find("ToMenu").GetComponent<Button>();
        ToMainMenu.onClick.AddListener(ToMainMenuClick);
        CloseGame = GameObject.Find("CloseGame").GetComponent<Button>();
        CloseGame.onClick.AddListener(CloseGameWindowClick);
        MenuWindow = GameObject.Find("MenuWindow");
        MenuWindow.SetActive(false);
        SettingsWindow = GameObject.Find("SettingsWindow");
        SettingsWindow.SetActive(false);

        SBCou = GameObject.Find("SBCou").GetComponent<Text>();
        MBCou = GameObject.Find("MBCou").GetComponent<Text>();
        FHCou = GameObject.Find("FHCou").GetComponent<Text>();
        ARCou = GameObject.Find("ARCou").GetComponent<Text>();

        MissionDescription = GameObject.Find("DescrContent").GetComponent<Text>();
        MissionStats = GameObject.Find("StatsContent").GetComponent<Text>();
       // Debug.Log(MissionDescription);

        missionsScroll = GameObject.Find("MissionsContent");
        CheckEndGame(0);
        if (PlayerPrefs.GetInt("NewGame") == 1)
        {
            GenerateEvent();
        }
        else
        {
            if (eventList.Count != 0)
            {
                ev = (GameEvent)eventList[0];
                noEvents = false;
            }
            else
            {
                noEvents = true;
            }
        }
        //Debug.Log(eventList.Count);
        PlayerPrefs.SetInt("NewGame", 0);

        UpdateGold();
        UpdateTime();
        UpdateInfl();
        Sort();
        RedrawMissions();
    }

    GameObject missionsScroll;

    TextLocalaze goldText;
    TextLocalaze timeText;
    TextLocalaze inflText;

    Text SBCou;
    Text MBCou;
    Text FHCou;
    Text ARCou;


    GameObject loadWindow;
    Button stopTimeBtn;
    Button startTimeBtn;
    Button toBarraks;
    Button toSpecial;
    Button toConstruction;

    void Save()
    {
        PlayerPrefs.SetFloat("LastGold", lastGold);
        PlayerPrefs.SetFloat("Time", time);
        PlayerPrefs.SetFloat("GenTimer", gentimer);
        PlayerPrefs.SetInt("EventCounter", eventList.Count);
        //Debug.Log(eventList.Count);
        for (int i = 0; i < eventList.Count; i++)
        {
            ((GameEvent)eventList[i]).Save("Event" + i.ToString());
            //Debug.Log(i);
        }
        PlayerPrefs.SetInt("DemonicInfluence", demonicInfluence);
        //Debug.Log(PlayerPrefs.GetInt("EventExeption"));

        PlayerPrefs.SetInt("UnitsInBarraks", unitsInBarraks);
        for (int i = 0; i < unitsInBarraks; i++)
        {
            PlayerPrefs.SetInt("PlayerUnitBool" + i.ToString(), 0);
            ((MovebleObject)units[i]).Save("PlayerUnitOnBase" + i.ToString());
        }
    }

    int increasingDemonicInfluence;
    float goldValue;
    float lastGold;

    void SetLang()
    {
        language = PlayerPrefs.GetInt("Language");
        foreach (var i in eventList)
        {
            ((GameEvent)i).lang = PlayerPrefs.GetInt("Language");
            ((GameEvent)i).Generate();
        }
        Sort();
        RedrawMissions();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (!SettingsWindow.active)
            {
                MenuWindow.SetActive(!MenuWindow.active);
            }
            SetLang();
        }
        if (toNext)
        {
            // Debug.Log(goldValue);
            if (time - lastGold > goldValue)
            {
                //   Debug.Log(time);
                lastGold = time;
                gold++;
                PlayerPrefs.SetInt("Gold", gold);
                UpdateGold();
            }
            if((int)(10 * time) < (int)(10 * (time + Time.deltaTime)))
            {
                UpdateTime();
            }
            time += Time.deltaTime;
            fatiguedCou = 0;
            for (int i = 0; i < unitsInBarraks; i++)
            {
                ((MovebleObject)units[i]).RecalcFatigue(time);
                if (((MovebleObject)units[i]).fatigueFlag)
                {
                    fatiguedCou++;
                }
            }
            int k = Mathf.FloorToInt(add);
            add += Time.deltaTime * increasingDemonicInfluence;
            k = Mathf.FloorToInt(add) - k;
            CheckEndGame(k);
            if (time >= gentimer)
            {
                StopTimeClick();
                GenerateEvent();
                gentimer = UnityEngine.Random.Range(2f, 5f) + time;
            }
            for (int i = 0; i < eventList.Count; i++)
            {
                if ((((GameEvent)eventList[i]).endTime <= time) && ((((GameEvent)eventList[i]).eventId > 0)))
                {
                   // Debug.Log(((GameEvent)eventList[i]).eventId);
                    if (UnityEngine.Random.Range(0f, 1000f) < demonicInfluence)
                    {
                        PlayerPrefs.SetInt("SmallBand", PlayerPrefs.GetInt("SmallBand") + 1);
                        GameEvent ev = new GameEvent(1, language, demonicInfluence, time);

                        increasingDemonicInfluence = PlayerPrefs.GetInt("SmallBand") * 2 +
                            PlayerPrefs.GetInt("MassBand") * 5 +
                            PlayerPrefs.GetInt("Foothold") * 10 +
                            PlayerPrefs.GetInt("Doomed") * 15;

                        ev.Save("SmallBandEvent" + smallBandCounter.ToString());
                        GenerateEvent(1);
                        smallBandCounter++;
                    }
                    CheckEndGame(((GameEvent)eventList[i]).enemyCounter);
                    eventList.RemoveAt(i);
                    i--;
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {

        }
    }

    void UpdateGold()
    {
        sendList.Clear();
        sendList.Add(gold.ToString());
        float val = (1f / goldValue);
        string s = val.ToString();
        if(val > 0)
        {
            s = "+" + s;
        }
        sendList.Add(s);
        goldText.SetValues(sendList);
    }
    void UpdateTime()
    {
        sendList.Clear();
        string s = (((int)(10 * time) % 300) / 10f).ToString();
        if((int)(10 * time) % 10 == 0)
        {
            s += ".0";
        }
        sendList.Add(s);
        sendList.Add(((int)time / 30).ToString());
        timeText.SetValues(sendList);
        PlayerPrefs.SetFloat("Time", time);
    }
    void UpdateInfl()
    {
        sendList.Clear();
        sendList.Add(demonicInfluence.ToString());
        int val = increasingDemonicInfluence;
        string s = val.ToString();
        if (val > 0)
        {
            s = "+" + s;
        }
        sendList.Add(s);
        inflText.SetValues(sendList);
        SBCou.text = PlayerPrefs.GetInt("SmallBand").ToString();
        MBCou.text = PlayerPrefs.GetInt("MassBand").ToString();
        FHCou.text = PlayerPrefs.GetInt("Foothold").ToString();
        ARCou.text = PlayerPrefs.GetInt("Doomed").ToString();
    }

    List<string> sendList = new List<string>();

    Text MissionDescription;
    Text MissionStats;

    public Sprite[] spritesSpecial;

    void RedrawMissions()
    {
       // Debug.Log("Redraw " + eventList.Count);
        foreach(var i in missions)
        {
            Destroy(i.gameObject);
        }
        missions.Clear();
        int cur = 0;
        foreach(var i in eventList)
        {
            GameObject go = GameObject.Instantiate(missionBtnPrefab, this.transform.position, this.transform.rotation);
            go.transform.parent = missionsScroll.transform;
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, 0);
            go.GetComponentsInChildren<Text>()[0].text = ((GameEvent)i).GetShortDescr();
            go.GetComponentsInChildren<Text>()[1].text = ((GameEvent)i).GetTime();
           // Debug.Log(((GameEvent)i).GetShortDescr() + " " + ((GameEvent)i).GetTime());
            int id = cur;
            go.GetComponent<Button>().onClick.AddListener(delegate { OnMissionClick(id); });
            cur++;
           // Debug.Log(((GameEvent)i).GetShortDescr() + " " + go.GetComponentInChildren<Text>().text);
            missions.Add(go.GetComponent<Button>());
            //((GameEvent)i).baseGround;
            if(((GameEvent)i).eventId < 0)
            {
                go.GetComponentsInChildren<Image>()[1].sprite = spritesSpecial[-((GameEvent)i).eventId - 1];
                go.GetComponentsInChildren<Text>()[1].text = "";
            }
            else
            {
                go.GetComponentsInChildren<Image>()[1].enabled = false;
            }
        }
        //if (MissionDescription != null)
        {
            if (ev == null)
            {
                MissionDescription.text = "";
                MissionStats.text = "";
            }
            else
            {
                MissionDescription.text = ev.GetShortDescr();
                MissionStats.text = ev.GetStatsDesr();
            }
        }
        UpdateGold();
        UpdateTime();
        UpdateInfl();
    }

    string badNews;

    int playerUnitsCounter;

    void Sort()
    {
        for (int i = 0; i < eventList.Count; i++)
        {
            for (int j = 0; j < eventList.Count - 1; j++)
            {
                if (((((GameEvent)eventList[j +1]).eventId > 0) && ((((GameEvent)eventList[j]).endTime > ((GameEvent)eventList[j + 1]).endTime))) 
                    || ((((GameEvent)eventList[j]).eventId < 0) && (((GameEvent)eventList[j]).eventId < ((GameEvent)eventList[j + 1]).eventId)))
                {
                    Debug.Log(((GameEvent)eventList[j]).eventId + " " + ((GameEvent)eventList[j + 1]).eventId);
                    GameEvent buf = ((GameEvent)eventList[j]);
                    eventList[j] = eventList[j + 1];
                    eventList[j + 1] = buf;
                }
            }
        }
        for (int i = 0; i < eventList.Count; i++)
        {
            ((GameEvent)eventList[i]).num = i;
        }
       // RedrawMissions();
    }

    void OnMissionClick(int i)
    {
        Debug.Log(i);
        ev = (GameEvent)eventList[i];
        RedrawMissions();
    }

    void StartTimeClick()
    {
        toNext = true;
        startTimeBtn.gameObject.SetActive(false);
    }

    void StopTimeClick()
    {
        toNext = false;
        startTimeBtn.gameObject.SetActive(true);
    }

    void ToBarraksClick()
    {
        Save();
        PlayerPrefs.SetInt("ChosenEquipUnit", 0);
        PlayerPrefs.SetInt("ToBattle", 0);
        InitLoadWindow();
        SceneManager.LoadSceneAsync("BarraksScene");
    }

    void InitLoadWindow()
    {
        loadWindow.SetActive(true);
        LoadHelpObject lHelp = new LoadHelpObject(PlayerPrefs.GetInt("Language"));

        GameObject.Find("MainLoadText").GetComponent<Text>().text = lHelp.text;
    }

    void ToSpecialMissionsClick()
    {
        Save();
        InitLoadWindow();
        SceneManager.LoadSceneAsync("ProgressScene");
    }

    void ToSquadClick()
    {
        PlayerPrefs.SetInt("EventExeption", ev.num);
        ev.Save("ChosenEvent");
        if (ev.eventId > 0)
        {
            PlayerPrefs.SetInt("SpecialID", 0);
        }
        else
        {
            PlayerPrefs.SetInt("SpecialID", -ev.eventId);
        }
        Save();
        PlayerPrefs.SetInt("ToBattle", 0);
        PlayerPrefs.SetInt("BackToBattle", 1);
        InitLoadWindow();
        SceneManager.LoadSceneAsync("PlaningScene");
    }

    void ExitClick()
    {
        Save();
        InitLoadWindow();
        SceneManager.LoadSceneAsync("MainMenuScene");
    }
}

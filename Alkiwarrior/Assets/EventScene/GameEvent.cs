﻿using UnityEngine;
using System.Collections;

public class GameEvent{

    string[] rusCover = { "Трава", "Грязь" };
    string[] engCover = { "Grass", "Mud" };
    string[] groundCover = { "Ground cover", "Ландшафт" };
    string[] enemyCounterText = { "Enemies", "Противники" };
    string[] mapSize = { "Map size", "Размер карты" };
    string[] shortDay = { "D", "Д" };
    string[] shortMonth = { "M", "М" };

    public string GetStatsDesr()
    {
        string s = "";
        s += groundCover[lang] + " " + this.baseGround[this.baseGroundId - 1] + '\n';
        s += enemyCounterText[lang] + " " + this.enemyCounter + '\n';
        s += mapSize[lang] + " " + this.width + "*" + this.height;
        return s;
    }


    string[] rusDesc = {
        "Группа демонов, вышедшая на охоту",
        "Маленькая рейдовая группа демонов",
        "Группа разбойников"
    };

    string[] engDesc =
    {
        "Hunting group of demons",
        "Small raid group of demons",
        "Gang of robbers"
    };

    public string desc;

    public int num;

    public string[] baseGround;

    public int lang;

    public void Generate()
    {
        if (eventId > 0)
        {
          //  Debug.Log(this.eventId - 1);
            if (lang == 0)
            {
                desc = engDesc[this.eventId - 1];
                baseGround = engCover;
            }
            if (lang == 1)
            {
                desc = rusDesc[this.eventId - 1];
                baseGround = rusCover;
            }
        }
        else
        {
            if (lang == 0)
            {
                baseGround = engCover;
            }
            if (lang == 1)
            {
                baseGround = rusCover;
            }
            if (eventId == 1)
            {
                this.desc = smallBandDesc[lang];
            }
            if (eventId == 2)
            {
                this.desc = massBandDesc[lang];
            }
            if (eventId == 3)
            {
                this.desc = footholdDesc[lang];
            }
            if (eventId == 4)
            {
                this.desc = doomedDesc[lang];
            }
        }
       // Debug.Log(desc);
    }

    public string GetTime()
    {
        string s = (((int)(10 * endTime) % 300) / 10f).ToString();
        if ((int)(10 * endTime) % 10 == 0)
        {
            s += ".0";
        }
        string ans = s + " " + shortDay[lang] + " " + ((int)endTime / 30).ToString() + " " + shortMonth[lang];
        return ans;
    }

    public int enemyCounter;
    public int eventId;
    public int height;
    public int width;
    public int baseGroundId;
    public float endTime;
    public int might;

    string[] smallBandDesc = { "Small band of demons", "Маленькая группа демонов" };
    string[] massBandDesc = { "Big band f demons", "Большая группа демонов" };
    string[] footholdDesc = { "Strong fortified demon's foothold", "Хорошо защищённый плацдарм демонов" };
    string[] doomedDesc = { "Army of demons", "Армия демонов, самоубийственная миссия"};

    public GameEvent(int a, int la, int demonic, float time)
    {
        lang = la;
        this.eventId = 0;
        Generate();
        //Debug.Log(lang);
        this.might = (int)((demonic + time) - 400);
        if (a == 0)
        {
            this.enemyCounter = 0;
            this.eventId = 0;
            this.height = 0;
            this.width = 0;
            this.baseGroundId = 0;
            this.endTime = 0;
        }
        if (a == 1)
        {
            this.enemyCounter = Random.Range(0, 5) + 2 + (int)Mathf.Pow(2.74f, 1 + 1.4f * (demonic + time) / 1000f);
            this.eventId = -1;
            this.height = Random.Range(10, 12);
            this.width = Random.Range(10, 12);
         //   Debug.Log(baseGround);
            this.baseGroundId = Random.Range(1, baseGround.Length + 1);
            this.endTime = -1;
            this.desc = smallBandDesc[lang];
        }
        if (a == 2)
        {
            this.enemyCounter = Random.Range(0, 5) + 6 + (int)Mathf.Pow(2.74f, 1 + 1.4f * (demonic + time) / 1000f);
            this.eventId = -2;
            this.height = Random.Range(10, 12);
            this.width = Random.Range(10, 12);
            this.baseGroundId = (int)Random.Range(1, baseGround.Length + 1);
            this.endTime = -1;
            this.desc = massBandDesc[lang];
        }
        if (a == 3)
        {
            this.enemyCounter = Random.Range(0, 5) + 10 + (int)Mathf.Pow(2.74f, 1 + 1.4f * (demonic + time) / 1000f);
            this.eventId = -3;
            this.height = Random.Range(15, 20);
            this.width = Random.Range(15, 20);
            this.baseGroundId = (int)Random.Range(1, baseGround.Length + 1);
            this.endTime = -1;
            this.desc = footholdDesc[lang];
        }
        if (a == 4)
        {
            this.enemyCounter = Random.Range(0, 5) + 20 + (int)Mathf.Pow(2.74f, 1 + 1.4f * (demonic + time) / 1000f);
            this.eventId = -4;
            this.height = Random.Range(17, 20);
            this.width = Random.Range(17, 20);
            this.baseGroundId = (int)Random.Range(1, baseGround.Length + 1);
            this.endTime = -1;
            this.desc = doomedDesc[lang];
        }
        Generate();
    }

    public string GetShortDescr()
    {
        return desc;
    }

    public GameEvent(int demonic, float time, int la)
    {
        lang = la;

        this.eventId = Random.Range(1, 3);
        Generate();
        this.height = Random.Range(6, 10) + this.enemyCounter / 5;
        this.width = Random.Range(6, 10) + this.enemyCounter / 5;
        this.baseGroundId = (int)Random.Range(1, baseGround.Length + 1);
        switch (this.eventId)
        {
            case 1:
                this.might = (int)((demonic + time) - 400);
                this.enemyCounter = Random.Range(0, 3) + Mathf.Min((int)Mathf.Pow(2.74f, 1 + 1.4f * (demonic + time) / 1000f), 30);
                this.endTime = (float)System.Math.Round(Random.Range(3f, 10f) + time, 2);
                break;
            case 2:
                this.might = (int)((demonic + time) - 400);
                this.enemyCounter = Random.Range(0, 3) + Mathf.Min((int)Mathf.Pow(2.74f, 1 + 1.4f * (time) / 1000f), 10);
                this.endTime = (float)System.Math.Round(Random.Range(1f, 4f) + time, 2);
                break;
            case 3:
                this.might = (int)((demonic + time) - 400);
                this.enemyCounter = Random.Range(0, 3) + Mathf.Min((int)Mathf.Pow(2.74f, 1 + 1.4f * (time) / 1000f), 10);
                this.endTime = (float)System.Math.Round(Random.Range(1f, 3f) + time, 2);
                break;
        }
    }

    public GameEvent(string s, int la)
    {
        lang = la;
        this.enemyCounter = PlayerPrefs.GetInt(s + "_enemyCounter");
        this.eventId = PlayerPrefs.GetInt(s + "_eventID");
      //  Debug.Log(this.eventId);
        this.height = PlayerPrefs.GetInt(s + "_height");
        this.width = PlayerPrefs.GetInt(s + "_width");
        this.baseGroundId = PlayerPrefs.GetInt(s + "_baseGroundId");
        this.endTime = PlayerPrefs.GetFloat(s + "_endTime");
        this.might = PlayerPrefs.GetInt(s + "_might");
        Generate();
        // Debug.Log(s + "_baseGroundId");
    }

    public void Save(string s)
    {
        PlayerPrefs.SetInt(s + "_enemyCounter", this.enemyCounter);
        PlayerPrefs.SetInt(s + "_eventID", this.eventId);
        PlayerPrefs.SetInt(s + "_height", this.height);
        PlayerPrefs.SetInt(s + "_width", this.width);
      //  Debug.Log(this.eventId);
      //  Debug.Log(s + "_baseGroundId");
        PlayerPrefs.SetInt(s + "_baseGroundId", this.baseGroundId);
        PlayerPrefs.SetFloat(s + "_endTime", this.endTime);
        PlayerPrefs.SetInt(s + "_might", this.might);
    }
}
